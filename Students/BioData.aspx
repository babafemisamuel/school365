﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BioData.aspx.cs" Inherits="School365.Students.BioData1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>FEDERAL COLLEGE OF EDUCATION OSIELE. ABEOKUTA</title>
    <style type="text/css">
        .auto-style1 {
            width: 413px;
        }

        .auto-style2 {
            width: 153px;
        }

        .auto-style3 {
            width: 165px;
        }

        .auto-style4 {
            width: 326px;
        }

        .auto-style5 {
            font-size: small;
        }
        .auto-style6 {
            width: 86px;
            height: 67px;
        }
        .auto-style7 {
            width: 266px;
        }
    </style>
</head>
<body style="font-family: 'Segoe UI';font-weight:100; font-size: small;">
    <form id="form1" runat="server">
        <p align="left"><img alt="Osiele" class="auto-style6" src="../Admin/Results/Image2.PNG" /> </p>
         <table align="center">
            <tr>
           <td align="center" class="auto-style1"><strong>FEDERAL COLLEGE OF EDUCATION, ABEOKUTA</strong></td>
            </tr>
            <tr>
                <td align="center" class="auto-style1"><strong>BIO DATA</strong></td>
            </tr>
        </table>
        <p align="center"><asp:Image ID="UserImage" Width="121px" height="121px" runat="server"/></p>
        <br />
        <br />
      
        <table style="border: 1px solid #000; width: 100%; margin-top: 5px;" border="0" cellspacing="0" class="auto-style5">
            <tr>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style2" colspan="">Surname</td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style4" colspan=""><%Response.Write(user.UserInfo(StudentID).Surname); %></td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style3" colspan="">Firstname</td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style1" colspan=""><%Response.Write(user.UserInfo(StudentID).Firstname); %></td>
            </tr>
            <tr>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style2" colspan="">Middlename</td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style4" colspan=""><%Response.Write(user.UserInfo(StudentID).Middlename); %></td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style3" colspan="">Marital Status</td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style1" colspan=""><%Response.Write(user.UserInfo(StudentID).MaritalStatus); %></td>
            </tr>
            <tr>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style2" colspan="">Date of Birth</td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style4" colspan=""><%Response.Write(user.UserInfo(StudentID).DateOfBirth); %></td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style3" colspan="">Place of Birth</td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style1" colspan=""><%Response.Write(user.UserInfo(StudentID).PlaceOfBirth); %></td>
            </tr>
            <tr>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style2" colspan="">Nationality</td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style4" colspan=""><%Response.Write(user.UserInfo(StudentID).Nationality); %></td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style3" colspan="">State</td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style1" colspan=""><%Response.Write(user.UserInfo(StudentID).SOR); %></td>
            </tr>
            <tr>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style2" colspan="">LGA</td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style4" colspan=""><%Response.Write(user.UserInfo(StudentID).LGA); %></td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style3" colspan="">Religion</td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style1" colspan=""><%Response.Write(user.UserInfo(StudentID).Religion); %></td>
            </tr>

        </table>
        <hr />
        <table style="border: 1px solid #000; width: 100%; margin-top: 5px;" border="0" cellspacing="0" class="auto-style5">
            <tr>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style2" colspan="">Sponsor&#39;s Details</td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style4" colspan=""><%Response.Write(user.UserInfo(StudentID).SponsorsName); %></td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style3" colspan="">Address of Sponsor</td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style1" colspan=""><%Response.Write(user.UserInfo(StudentID).SponsorsAddress); %></td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style7" colspan="">Phone Number of Sponsor</td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style1" colspan=""><%Response.Write(user.UserInfo(StudentID).SponsorsPhoneNumber); %></td>
            </tr>

        </table>
        <hr />
        <div class="col-md-12">
            <h3>Jamb Details</h3>

            <%var getInfo = jambDetail.Select(a => new { a.RegNo, a.Center, a.Year }).FirstOrDefault(); %>
            <table style="border: 1px solid #000; width: 100%; margin-top: 5px;" border="0" cellspacing="0" class="auto-style5">

                <thead>
                    <tr>
                        <th>Registration Number</th>
                        <th>Center Number</th>
                        <th>Year</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style1" colspan=""><%Response.Write(getInfo == null ? "" : getInfo.RegNo); %></td>
                        <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style1" colspan=""><%Response.Write(getInfo == null ? "" : getInfo.Center); %></td>
                        <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style1" colspan=""><%Response.Write(getInfo == null ? "" : getInfo.Year); %></td>
                    </tr>

                </tbody>
            </table>
            <table style="border: 1px solid #000; width: 100%; margin-top: 5px;" border="0" cellspacing="0" class="auto-style5">

                <thead>
                    <tr>
                        <th>Subject</th>
                        <th>Score</th>
                    </tr>
                </thead>
                <tbody>
                    <%
                        double sum = 0;
                        double defNum=0;
                        foreach (var item in jambDetail)
                        {
                            sum = sum + (double.TryParse(item.Score, out defNum)==true?defNum:0);
                    %>
                    <tr>
                        <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style1" colspan=""><%Response.Write(item.Subject); %></td>
                        <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style1" colspan=""><%Response.Write(item.Score); %></td>
                    </tr>
                    <%}
                    %>
                    <tr>
<td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style1" colspan="">TOTAL SCORE</td>
                        <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style1" colspan=""><%Response.Write(sum); %></td>
                    </tr>
                </tbody>
            </table>

        </div>
        <hr /><br />
        <div class="col-md-12">
            <h3>O'Level Details</h3>

            <%
                foreach (var item in OlevelCenterNo)
                {
                    var getOlevel = olevel.Where(a => a.CenterNumber == item).ToList();
                    var getDetail = getOlevel.Select(a => new { a.ExamType, a.CenterNumber, a.ExamYear, a.RegNo }).FirstOrDefault();
            %>
            <table style="border: 1px solid #000; width: 100%; margin-top: 5px;" border="0" cellspacing="0" class="auto-style5">
                <thead>
                    <tr>
                        <th>Exam Type</th>

                        <th>Exam Year</th>
                        <th>Center Number</th>
                        <th>Reg Number</th>
                    </tr>

                </thead>
                <tbody>
                    <tr>
                        <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style1" colspan=""><%Response.Write(getDetail == null ? "" : getDetail.ExamType); %></td>
                        <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style1" colspan=""><%Response.Write(getDetail == null ? "" : getDetail.ExamYear); %></td>
                        <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style1" colspan=""><%Response.Write(getDetail == null ? "" : getDetail.CenterNumber); %></td>
                        <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style1" colspan=""><%Response.Write(getDetail == null ? "" : getDetail.RegNo); %></td>
                    </tr>
                </tbody>
            </table>
            <table style="border: 1px solid #000; width: 100%; margin-top: 5px;" border="0" cellspacing="0" class="auto-style5">
                <thead>
                    <tr>
                        <th>Subject Name</th>
                        <th>Grade</th>
                    </tr>
                </thead>
                <tbody>
                    <%
                        foreach (var setOlevel in getOlevel)
                        {
                    %>
                    <tr>
                        <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style1" colspan=""><%Response.Write(setOlevel.SubjectName); %></td>
                        <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style1" colspan=""><%Response.Write(setOlevel.Grade); %></td>
                    </tr>
                    <% }
                    %>
                </tbody>
            </table>
            <hr />
            <%  }
            %>
        </div>
    </form>
</body>
</html>
