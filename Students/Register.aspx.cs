﻿using School365.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EntityFramework.Extensions;


namespace School365.Students
{
    public partial class Register : System.Web.UI.Page
    {

        StudentModel db = new StudentModel();
        // public List<Result> db.Results = new List<Result>();
        public string value = "";
        static int getStudentID = 0;
        static int SubCombID = 0;
        static double Level = 0;
        double gettotalTNU = 0;
        double gettotalTNUII = 0;
        double getCarryOverTnu = 0;
        double getCarryOverTnuII = 0;
        static string matricNo = "";
        int getSessionID = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            Response.CacheControl = "private";
            Response.ExpiresAbsolute = DateTime.Now.AddDays(-1d);
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            value = Request.QueryString["grab"].ToString();
            if (!IsPostBack)
            {
                // db.Results = (from d in db.Results select d).ToList();
                string Major = "";
                string Minor = "";
                string subComb = "";
                var getValues = (from d in db.Students
                                 where d.InvoiceNumber == value
                                 select new { d.Major, d.Minor, d.StudentID, d.Level, d.Registered, d.MatricNo }).First();

                if (getValues.Registered.ToUpper() == "TRUE")
                {
                    allSubject.Visible = false;
                    allSubjectII.Visible = false;
                    submit.Visible = false;
                    CheckAll.Visible = false;
                    carryOver.Visible = false;
                }
                Major = getValues.Major;
                Minor = getValues.Minor;
                Level = getValues.Level;
                getStudentID = getValues.StudentID;
                matricNo = getValues.MatricNo;
                subComb = Major + "/" + Minor;

                var getSubCombID = (from d in db.SubjectCombinations where d.SubjectCombinName == subComb select d.SubjectCombinID).First();
                //Trying to access it below for button
                SubCombID = getSubCombID;
                if (getSubCombID == null)
                {
                    Response.Write("Subject Combination not found....please contact Admin");
                }
                else
                {
                    //for carryovers
                    var getCarryOvers = from r in db.Results
                                        where r.StudentID == getStudentID
                                        join d in db.CarryOvers on r.ResultID equals d.ResultID
                                        join f in db.Departments on r.DepartmentID equals f.DepartmantID
                                        join a in db.Subjects on r.SubjectID equals a.SubjectID
                                        join s in db.SubjectCombinations on r.SubjectCombinID equals s.SubjectCombinID
                                        select new { s.SubjectCombinID, s.SubjectCombinName, f.DepartmantID, a.SubjectID, a.SubjectName, a.SubjectValue, a.SubjectCode, a.SubjectUnit, a.Semester };
                    carryOver.DataSource = getCarryOvers.OrderBy(a => a.DepartmantID).Distinct().ToList();
                    carryOver.DataBind();

                    var getAllSubject = from a in db.AllCombineds
                                        where a.SubjectCombineID == getSubCombID
                                        join j in db.SubjectCombinations on a.SubjectCombineID equals j.SubjectCombinID
                                        join d in db.Departments on a.DepartmentID equals d.DepartmantID
                                        join h in db.Subjects on a.SubjectID equals h.SubjectID
                                        select new { j.SubjectCombinID, j.SubjectCombinName, d.DepartmantID, h.SubjectCode, h.SubjectID, h.SubjectName, h.SubjectValue, h.SubjectUnit, h.SubjectLevel, h.Semester };

                    var getSubjectbyLevel = from d in getAllSubject where d.SubjectLevel == getValues.Level.ToString() && d.Semester == "1" select d;

                   // Response.Write(getSubjectbyLevel.Count());

                    allSubject.DataSource = getSubjectbyLevel.OrderBy(a => a.DepartmantID).ToList();
                    allSubject.DataBind();

                    var getSubjectbyLevelII = from d in getAllSubject where d.SubjectLevel == getValues.Level.ToString() && d.Semester == "2" select d;
                    allSubjectII.DataSource = getSubjectbyLevelII.OrderBy(a => a.DepartmantID).ToList();
                    allSubjectII.DataBind();
                }
            }
        }
        protected void submit_Click(object sender, EventArgs e)
        {
             getSessionID = (from d in db.Sessions where d.CurrentSession == true select d.SessionID).FirstOrDefault();
            foreach (GridViewRow rows in carryOver.Rows)
            {
                int SubjectID = int.Parse(rows.Cells[3].Text);
                var getSemester = (from d in db.Subjects where d.SubjectID == SubjectID select new { d.Semester, d.SubjectCode }).FirstOrDefault();
                if (getSemester.Semester == "1")
                {
                    getCarryOverTnu += double.Parse(rows.Cells[7].Text);
                }
                else
                {
                    getCarryOverTnuII += double.Parse(rows.Cells[7].Text);
                }
            }
            foreach (GridViewRow rows in allSubject.Rows)
            {
                if ((rows.Cells[0].FindControl("Crs") as CheckBox).Checked)
                {
                    gettotalTNU += double.Parse(rows.Cells[7].Text);
                }
            }

            foreach (GridViewRow rows in allSubjectII.Rows)
            {
                if ((rows.Cells[0].FindControl("Crs") as CheckBox).Checked)
                {
                    gettotalTNUII += double.Parse(rows.Cells[7].Text);
                }
            }


            if ((gettotalTNU + getCarryOverTnu) <= 100)
            {
                foreach (GridViewRow rows in allSubject.Rows)
                {
                    if ((rows.Cells[0].FindControl("Crs") as CheckBox).Checked)
                    {
                        var InsertResult = new Result
                        {
                            MatricNo = matricNo,
                            StudentID = getStudentID,
                            SubjectCombinID = int.Parse(rows.Cells[5].Text),
                            DepartmentID = int.Parse(rows.Cells[1].Text),
                            SubjectID = int.Parse(rows.Cells[3].Text),
                            Level = Level,
                            TNU = double.Parse(rows.Cells[7].Text),
                            SessionID = getSessionID,
                            Semester=1,
                           
                        };

                        db.Results.Add(InsertResult);
                        db.SaveChanges();
                        allSubject.Visible = false;
                    }
                }
            }
            else
            {
                Response.Write("EXCEEDED YOUR UNIT FOR FIRST SEMESTER");
            }


            //for second semester
            if ((gettotalTNUII + getCarryOverTnuII) <= 100)
            {
                foreach (GridViewRow rows in allSubjectII.Rows)
                {
                    if ((rows.Cells[0].FindControl("Crs") as CheckBox).Checked)
                    {
                        var InsertResult = new Result
                        {
                            StudentID = getStudentID,
                            MatricNo = matricNo,
                            SubjectCombinID = int.Parse(rows.Cells[5].Text),
                            DepartmentID = int.Parse(rows.Cells[1].Text),
                            SubjectID = int.Parse(rows.Cells[3].Text),
                            Level = Level,
                            TNU = double.Parse(rows.Cells[7].Text),
                            SessionID = getSessionID,
                            Semester=2,
                        };
                        db.Results.Add(InsertResult);
                        db.SaveChanges();
                        allSubject.Visible = false;
                    }
                }

                foreach (GridViewRow rows in carryOver.Rows)
                {
                    var InsertCarryOver = new Result
                    {
                        StudentID = getStudentID,
                        MatricNo = matricNo,
                        SubjectCombinID = int.Parse(rows.Cells[5].Text),
                        DepartmentID = int.Parse(rows.Cells[1].Text),
                        SubjectID = int.Parse(rows.Cells[3].Text),
                        Level = Level,
                        TNU = double.Parse(rows.Cells[7].Text),
                        SessionID = getSessionID,
                        Semester=int.Parse(rows.Cells[9].Text),
                    };
                    db.Results.Add(InsertCarryOver);
                    db.SaveChanges();
                    carryOver.Visible = false;
                }
                var UpdateStudent = (from d in db.Students where d.StudentID == getStudentID select d).FirstOrDefault();
                UpdateStudent.Registered = "True";              
                CalcLastValues();
                string parameter = "grab=" + value;
                Response.Redirect("CourseForm.aspx?" + parameter);
            }
            else
            {
                Response.Write("EXCEEDED YOUR UNIT FOR FIRST Second Semester");
            }


        }
        private void CalcLastValues()
        {
            double getLevel = (from d in db.Students where d.StudentID == getStudentID select d.Level).FirstOrDefault();
            double getLastLevel = getLevel - 100;
            if (getLastLevel != 0)
            {
                var getOldSession = (from d in db.Results where d.StudentID == getStudentID && d.Level == getLastLevel select d.SessionID).FirstOrDefault();
                var getPrevDept = (from d in db.Results
                                   where d.StudentID == getStudentID && d.SessionID == getOldSession
                                   select d.DepartmentID).Distinct().ToList();
                foreach (var setPrevDept in getPrevDept)
                {
                    var previousTnup = (from d in db.Results where d.StudentID == getStudentID && d.SessionID == getOldSession && d.DepartmentID == setPrevDept && (d.EXAM+d.CA) >= 50 select d.TNUP).Distinct().FirstOrDefault();
                    db.Results.Where(x => x.StudentID == getStudentID && x.DepartmentID == setPrevDept && x.SessionID == getSessionID).Update(x => new Result() { TNUP = previousTnup });
                }
                //var previousTcp = (from d in db.Results where d.StudentID == getStudentID && d.SessionID == getSession select d.TCP).LastOrDefault();
            }
        }

        protected void CheckAll_Click(object sender, EventArgs e)
        {
            foreach (GridViewRow item in allSubject.Rows)
            {
                if ((item.Cells[0].FindControl("Crs") as CheckBox).Checked == false)
                {
                    (item.Cells[0].FindControl("Crs") as CheckBox).Checked = true;
                }
            }

            foreach (GridViewRow item in allSubjectII.Rows)
            {
                if ((item.Cells[0].FindControl("Crs") as CheckBox).Checked == false)
                {
                    (item.Cells[0].FindControl("Crs") as CheckBox).Checked = true;
                }
            }
        }
    }
}