﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Students/CollegeStudent.Master" AutoEventWireup="true" CodeBehind="StudentResult.aspx.cs" Inherits="School365.Students.StudentResult" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">Student Result</h4>
                </div>
            </div>
            <!-- /.row -->
            <!-- .row -->
            <div class="white-box">
     <div>
            <asp:DropDownList ID="Session" runat="server" DataSourceID="LinqDataSource1" DataTextField="SessionYear" DataValueField="SessionID">
            </asp:DropDownList>
            &nbsp;&nbsp;&nbsp;
        <asp:DropDownList ID="Semesters" runat="server">
            <asp:ListItem>1</asp:ListItem>
            <asp:Listitem>2</asp:Listitem>
        </asp:DropDownList>
            &nbsp;<asp:DropDownList ID="Levels" runat="server">
            </asp:DropDownList>
            &nbsp;
         <asp:TextBox ID="MatricNo" runat="server" placeholder="Matric No" ReadOnly="true"></asp:TextBox>
        <asp:Button ID="GetGrades" runat="server" Text="Get Data" OnClick="GetGrades_Click" />
            <asp:LinqDataSource ID="LinqDataSource1" runat="server" ContextTypeName="School365.Model.StudentModel" EntityTypeName="" OrderBy="SessionYear" Select="new (SessionID, SessionYear)" TableName="Sessions">
            </asp:LinqDataSource>

        </div>

        <br />
        <p align="left">
            <img alt="" class="auto-style1" src="../Results/Image2.PNG" /></p>
        <table align="center">
            <tr>
                <td align="center"><strong>FEDERAL COLLEGE OF EDUCATION, ABEOKUTA</strong></td>
            </tr>
            <tr>
                <td align="center"><strong>RESULT SLIP</strong></td>
            </tr>
            <tr>
                <td align="center"><strong><%Response.Write(Semester == 1 ? "1ST SEMESTER" : "2ND SEMESTER"); %> <%Response.Write("  " + Session.SelectedItem.Text); %></strong></td>
            </tr>
        </table>
        <br />
        
        <div>
           <%-- <%var getAllSessionID = (from d in db.Results where d.MatricNo == MatricNo.Text select d.SessionID).Distinct().ToList();%>--%>

            <%

                string getSessionYear = (from d in db.Sessions where d.SessionID == SessionID select d.SessionYear).FirstOrDefault();
                var getResult = (from d in db.Results
                                 where d.MatricNo == MatricNo.Text && d.SessionID == SessionID && d.Semester == 1
                                 join s in db.Subjects on d.SubjectID equals s.SubjectID
                                 select new { d.StudentID, s.SubjectCode, s.SubjectName, s.SubjectUnit, s.SubjectValue, d.CA, d.EXAM, d.Level }).OrderBy(a => a.SubjectCode).ToList();
                var getResultII = (from d in db.Results
                                   where d.MatricNo == MatricNo.Text && d.SessionID == SessionID && d.Semester == 2
                                   join s in db.Subjects on d.SubjectID equals s.SubjectID
                                   select new { d.StudentID, s.SubjectCode, s.SubjectName, s.SubjectUnit, s.SubjectValue, d.CA, d.EXAM, d.Level }).OrderBy(a => a.SubjectCode).ToList();
                var Level = getResult.Select(a => a.Level).FirstOrDefault();
                    %>
            <table align="center" style="width: 100%; font-family: 'Times New Roman'; font-size: 14px">
                <tr>
                    <td><strong>MATRIC NO :<%Response.Write(MatricNo.Text); %> </td>
                     
                </tr>
                <tr>
                    <td>STUDENT NAME :<%Response.Write(StudentFullName); %> </td>
                     <td></td>
                </tr>
                
                <tr>
                    <td>SUB/COMB : <%Response.Write(SubComb); %></td>
                    <td></td>
                </tr>
                <tr>
                  
                    <td> Level : <%Response.Write(Level); %> </td>
                    </strong>
                    <td></td>
                   
                </tr>
            </table>
        <div>
            <table style="width: 100%; margin-top: 5px; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; border-left: 1px solid #000; font-size: 12px;" border="0" cellspacing="0">
                <tr>
                    <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5">S/N</td>
                    <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5">COURSE CODE</td>
                    <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5">SCT</td>
                    <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5">COURSE TITLE</td>

                    <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5">UNITS</td>
                                       <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5">STATUS</td>
                    <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5">GRADE</td>
                    <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5">SCORE</td>
                    <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5">ABS</td>
                </tr>

                <%
                    int sn = 1;
                    double numOfUnit = 0;
                    double numOfUnitFailed = 0;
                    /*
                     var getSubjects = from d in db.AllCombineds
                              where d.SubjectCombineID.ToString() == SubComb.SelectedItem.Value
                              join f in db.Subjects on d.SubjectID equals f.SubjectID
                              select new { f.SubjectCode, f.SubjectID };
                    
                    */


                    var StudentResult = (from d in db.Results
                                         where d.MatricNo == MatricNo.Text && d.SessionID == SessionID && d.Semester == Semester && d.Level == AllLevel
                                         join f in db.Subjects on d.SubjectID equals f.SubjectID
                                         join s in db.Departments on d.DepartmentID equals s.DepartmantID
                                         join g in db.SubjectCombinations on d.SubjectCombinID equals g.SubjectCombinID
                                         select new { d.StudentID, f.SubjectName, f.SubjectCode, f.SubjectUnit, f.SubjectValue, d.EXAM, d.CA, s.DepartmantID, s.DepartmentCode, s.DepartmentName, g.SubjectCombinID, g.SubjectCombinName }).OrderBy(a => a.SubjectCode).ToList();
                    foreach (var SessionIDs in StudentResult)
                    {
                        numOfUnit += SessionIDs.SubjectValue;
                        numOfUnitFailed += (SessionIDs.CA + SessionIDs.EXAM) <= 39 ? SessionIDs.SubjectValue : 0;
                %>
                <tr>
                    <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(sn++); %></td>
                    <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(SessionIDs.SubjectCode); %></td>
                    <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write("A"); %></td>
                    <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(SessionIDs.SubjectName.ToUpper()); %></td>
                    <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(SessionIDs.SubjectValue); %></td>
                    <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(SessionIDs.SubjectUnit); %></td>

                    <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(util.GradeName(SessionIDs.EXAM + SessionIDs.CA)); %></td>
                    <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(SessionIDs.EXAM + SessionIDs.CA); %></td>
                    <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(""); %></td>
                </tr>
                <%}
                %>
                <tr>
                    <td colspan="4" align="center" style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000;" class="auto-style5"><strong>Number of Credit Unit Registered : <%Response.Write(numOfUnit);%></strong></td>
                    <td colspan="4" style="border-bottom: 1px solid #000; border-top: 1px solid #000;" class="auto-style5"><strong>Total Number of Credit Unit Failed: <%Response.Write(numOfUnitFailed);%></strong></td>
                </tr>

            </table>
            <hr />
            <%
                var getFailed = (from d in StudentResult where (d.CA + d.EXAM) <= 39 select d).ToList();

                if (getFailed.Count() != 0)
                {
            %>
            <p align="center">Carried course(s) for the next semester</p>
            <table style="width: 100%; margin-top: 5px; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; border-left: 1px solid #000; font-size: 12px;" border="0" cellspacing="0">
                <tr>
                    <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5">S/N</td>
                    <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5">COURSE CODE</td>
                    <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5">SCT</td>
                    <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5">COURSE TITLE</td>
                                        <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5">STATUS</td>
                    <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5">UNIT</td>
                    <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5">GRADE</td>
                    <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5">SCORE</td>
                    <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5">ABS</td>
                </tr>
                <%

                    int _sn = 1;
                    double _numOfUnit = 0;
                    double _numOfUnitFailed = 0;

                    foreach (var _SessionID in getFailed)
                    {
                %>
                <tr>
                    <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(_sn++); %></td>
                    <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(_SessionID.SubjectCode); %></td>
                    <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write("A"); %></td>
                    <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(_SessionID.SubjectName.ToUpper()); %></td>
                                        <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(_SessionID.SubjectValue); %></td>
                    <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(_SessionID.SubjectUnit); %></td>
                    <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(util.GradeName(_SessionID.EXAM + _SessionID.CA)); %></td>
                    <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(_SessionID.EXAM + _SessionID.CA); %></td>
                    <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(""); %></td>
                </tr>
                <% }
                %>
                <tr>
                    <td colspan="4" align="center" style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000;" class="auto-style5"><strong>Number of Courses Registered : <%Response.Write(_sn - 1);%></strong></td>
                    <td colspan="4" style="border-bottom: 1px solid #000; border-top: 1px solid #000;" class="auto-style5"><strong></strong></td>
                </tr>
            </table>
            <% }
            %>
        </div>
        <hr /><br /><hr />
        <table style="width: 100%; margin-top: 5px; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; border-left: 1px solid #000; font-size: 12px;" border="0" cellspacing="0">
            <tr>
                <td style="border-top: 1px solid #000; border-left: 1px solid #000; border-right:1px solid #000;" class="auto-style5">SUBJECT COMBINATION </td>
                <td style="border-top: 1px solid #000; border-right: 1px solid #000; border-left:1px solid #000;"" class="auto-style5">CODE</td>
                <td colspan="4" align="center" style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5">PREVIOUS</td>
                <td colspan="4" align="center" style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5">CURRENT</td>   
                <td colspan="4" align="center" style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5">CUMMULATIVE</td>             
            </tr>
            <tr>
                <td style="border-bottom: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"></td>
                <td style="border-bottom: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"></td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5">TCP</td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5">TNU</td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5">TNUP</td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5">GPA</td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5">TCP</td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5">TNU</td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5">TNUP</td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5">GPA</td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5">CTCP</td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5">CTNU</td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5">CTNUP</td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5">CGPA</td>
            </tr>
            <%
                var getAllDept = (from d in StudentResult select new { d.StudentID, d.DepartmantID, d.DepartmentCode, d.DepartmentName, d.SubjectCombinID, d.SubjectCombinName }).Distinct().OrderBy(a => a.DepartmentCode).ToList();
                double prevtcp = 0; double prevtnu = 0; double prevtnup = 0; double prevcgpa = 0;
                double currtcp = 0; double currtnu = 0; double currtnup = 0; double currcgpa = 0;
                double cummtcp = 0; double cummtnu = 0; double cummtnup = 0; double cummcgpa = 0;
                foreach (var setAllDept in getAllDept)
                {%>
            <tr>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(setAllDept.DepartmentName); %></td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(setAllDept.DepartmentCode); %></td>


                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(util.PrevdeptTcp(Semester, AllLevel, setAllDept.StudentID, SessionID, setAllDept.DepartmantID)); %></td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(util.PrevdeptTnu(Semester, AllLevel, setAllDept.StudentID, SessionID, setAllDept.DepartmantID)); %></td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(util.PrevdeptTnup(Semester, AllLevel, setAllDept.StudentID, SessionID, setAllDept.DepartmantID)); %></td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(util.PrevdeptCgpa(Semester, AllLevel, setAllDept.StudentID, SessionID, setAllDept.DepartmantID)); %></td> 
                              
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(util.CurrdeptTcp(Semester, AllLevel, setAllDept.StudentID, SessionID, setAllDept.DepartmantID)); %></td>
                  <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(util.CurrdeptTnu(Semester, AllLevel, setAllDept.StudentID, SessionID, setAllDept.DepartmantID)); %></td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(util.CurrdeptTnup(Semester, AllLevel, setAllDept.StudentID, SessionID, setAllDept.DepartmantID)); %></td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(util.DeptGpa(Semester, AllLevel, setAllDept.StudentID, SessionID, setAllDept.DepartmantID)); %></td>
                
                
                
                 <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(util.cummTcp(Semester, AllLevel, setAllDept.StudentID, SessionID, setAllDept.DepartmantID)); %></td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(util.cummTnu(Semester, AllLevel, setAllDept.StudentID, SessionID, setAllDept.DepartmantID)); %></td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(util.cummTnup(Semester, AllLevel, setAllDept.StudentID, SessionID, setAllDept.DepartmantID)); %></td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(util.cummCgpa(Semester, AllLevel, setAllDept.StudentID, SessionID, setAllDept.DepartmantID)); %></td> 
            </tr>
            
            <%  
                    prevtcp += util.PrevdeptTcp(Semester, AllLevel, setAllDept.StudentID, SessionID, setAllDept.DepartmantID);
                    prevtnu += util.PrevdeptTnu(Semester, AllLevel, setAllDept.StudentID, SessionID, setAllDept.DepartmantID);
                    prevtnup += util.PrevdeptTnup(Semester, AllLevel, setAllDept.StudentID, SessionID, setAllDept.DepartmantID);

                    currtcp += util.CurrdeptTcp(Semester, AllLevel, setAllDept.StudentID, SessionID, setAllDept.DepartmantID);
                    currtnu += util.CurrdeptTnu(Semester, AllLevel, setAllDept.StudentID, SessionID, setAllDept.DepartmantID);
                    currtnup += util.CurrdeptTnup(Semester, AllLevel, setAllDept.StudentID, SessionID, setAllDept.DepartmantID);

                    cummtcp += util.cummTcp(Semester, AllLevel, setAllDept.StudentID, SessionID, setAllDept.DepartmantID);
                    cummtnu += util.cummTnu(Semester, AllLevel, setAllDept.StudentID, SessionID, setAllDept.DepartmantID);
                    cummtnup += util.cummTnup(Semester, AllLevel, setAllDept.StudentID, SessionID, setAllDept.DepartmantID);

                }
                prevcgpa = prevtcp / prevtnu;
                currcgpa = currtcp / currtnu;
                cummcgpa = cummtcp / cummtnu;

                %>


            <tr>
                <td colspan="2" style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5">OVERALL PERFOMANCE</td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(prevtcp); %></td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(prevtnu); %></td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(prevtnup); %></td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(string.Format("{0:0.00}", prevcgpa)); %></td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(currtcp); %></td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(currtnu); %></td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(currtnup); %></td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(string.Format("{0:0.00}", currcgpa)); %></td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(cummtcp); %></td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(cummtnu); %></td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(cummtnup); %></td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(string.Format("{0:0.00}", cummcgpa)); %></td>
                
            </tr>
           
        </table>
        </div>
                </div>
            </div>
         </div>
</asp:Content>
