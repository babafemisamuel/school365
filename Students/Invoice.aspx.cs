﻿using School365.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace School365.Student
{
    public partial class Invoice : System.Web.UI.Page
    {
        StudentModel db=new StudentModel();
        string InvoiceNumber = "";
        string StudentKey = "";
        
        protected void Page_Load(object sender, EventArgs e)
        {
          
            StudentKey = System.Guid.NewGuid().ToString();
            InvoiceNumber = Guid.NewGuid().ToString().Substring(0, new Guid().ToString().IndexOf("-"));
            if (!IsPostBack)
            {
                try
                {
                    //
                    var allSubjectCombination = db.SubjectCombinations.OrderBy(a => a.SubjectCombinName).ToList();

                    subjectComb.DataSource = allSubjectCombination;
                    subjectComb.DataTextField = "SubjectCombinName";
                    subjectComb.DataValueField = "SubjectCombinName";
                    DataBind();


                    //major.DataSource = (from d in db.Departments select new { d.DepartmentName, d.DepartmentCode }).Distinct().ToList();
                    //major.DataTextField = "DepartmentName";
                    //major.DataValueField = "DepartmentCode";
                    //DataBind();

                    //minor.DataSource = (from g in db.Departments select new { g.DepartmentName, g.DepartmentCode }).Distinct().ToList();
                    //minor.DataTextField = "DepartmentName";
                    //minor.DataValueField = "DepartmentCode";
                    //DataBind();

                    //school.DataSource = (from c in db.Facultys select new { c.FacultyID, c.FacultyCode }).Distinct().ToList();
                    //school.DataTextField = "FacultyCode";
                    //school.DataValueField = "FacultyID";
                    //DataBind();
                }
                catch (Exception ex)
                {
                    Response.Write(ex.Message);
                }
               
               
            }
          
        }
        protected void submit_Click(object sender, EventArgs e)
        {            
            try
            {
                string[] subCombVal = subjectComb.SelectedItem.Text.Split('/');
                string major = subCombVal[0];
                string minor = subCombVal[1];
                var school= db.Departments.Where(a => a.DepartmentCode == major).Select(a => a.Facultys.FacultyCode).FirstOrDefault();
                int CurricullumID = db.Curricullums.Where(a => a.IsCurrent == true).FirstOrDefault().CurricullumID;
                var InsertResult = new Model.Student
                {
                   // DateEntered=DateTime.Now,
                    StudentKey= StudentKey,
                    Firstname = firstname.Text.ToUpper(),
                    Surname = surname.Text.ToUpper(),
                    Middlename = othername.Text.ToUpper(),
                    Nationality = country.SelectedItem.Text.ToUpper(),
                    SOR = sor.SelectedItem.Text.ToUpper(),
                    LGA = lga.Text.ToUpper(),
                    Email = email.Text,
                    PhoneNumber = phoneNumber.Text,
                    Gender = sex.Text.ToUpper(),
                    FacultyName = school,
                    Major = major,
                    Minor = minor,
                    Registered="False",
                    Level=100,
                    InvoiceNumber= InvoiceNumber,
                    JambRegNo=JambRegNo.Text,
                    CurriculumID=CurricullumID
                   
                };
                db.Students.Add(InsertResult);
                db.SaveChanges();
                //string parameters = "surname=" + surname.Text + "&firstname=" + firstname.Text + "&othernames=" +othername.Text + "&SOR=" +sor.SelectedItem.Text + "&LGA=" + lga.Text + "&Nationality=" +country.SelectedItem.Text + "&EmailAdress=" + email.Text + "&PhoneNumber=" + phoneNumber.Text + "&InvoiceNo=" + InvoiceNumber + "&DateEntered=" + DateTime.Now.Year + "&Major=" + major.SelectedItem.Value + "&Minor=" + minor.SelectedItem.Value + "&Gender=" + sex.Text;
                string parameters = "InvoiceNumber=" + InvoiceNumber;
                Response.Redirect("GeneratedInvoice.aspx?"+ parameters);
                firstname.Text="";
                surname.Text="";
                othername.Text="";                
                lga.Text="";
                email.Text = "";
                JambRegNo.Text = "";
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }
           
        }
    }
}