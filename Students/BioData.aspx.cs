﻿using School365.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace School365.Students
{
    public partial class BioData1 : System.Web.UI.Page
    {
        StudentModel db = new StudentModel();
        public List<int> OlevelID = new List<int>();
        public List<int> UtmeID = new List<int>();
        public List<string> OlevelCenterNo = new List<string>();
        public List<OlevelDetail> olevel = new List<OlevelDetail>();
        public List<JambDetail> jambDetail = new List<JambDetail>();
        public int StudentID = 0;
        // StdGen user = new StdGen();
        public StudentInfo user = new StudentInfo();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (User.Identity.IsAuthenticated == false)
                Response.Redirect("UserLogin.aspx");
            StudentID = int.Parse(User.Identity.Name);
            user.UserInfo(StudentID);
            string StudnetAddress = user.UserInfo(StudentID).StudentAddress;

            if (user.UserInfo(StudentID).StudentAddress == "" || user.UserInfo(StudentID).StudentAddress == null)
                Response.Redirect("UserEdit.aspx");
            if (user.UserInfo(StudentID).StudentImage != null)
            {
                UserImage.ImageUrl = "data:image/jpeg;base64," + Convert.ToBase64String(user.UserInfo(StudentID).StudentImage);
            }

            AllData();
        }
        private void AllData()
        {
            int StudentIDs = user.UserInfo(StudentID).StudentID;
            olevel = db.OlevelDetails.Where(a => a.StudentID == StudentIDs).Select(a => a).OrderBy(a => a.SubjectName).ToList();
            jambDetail = db.JambDetails.Where(a => a.StudentID == StudentIDs).Select(a => a).OrderBy(a => a.Subject).ToList();
            OlevelID = olevel.Select(a => a.OlevelDetailsID).ToList();
            UtmeID = jambDetail.Select(a => a.JambDetailID).ToList();
            OlevelCenterNo = olevel.Select(a => a.CenterNumber).Distinct().ToList();

        }
    }
}