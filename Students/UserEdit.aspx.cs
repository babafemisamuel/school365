﻿using School365.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace School365.Students
{
    public partial class UserEdit : System.Web.UI.Page
    {
        StudentModel db = new StudentModel();
        public int value = 0;
        string FileName = "";
        string StudentImage = "";
        School365.Students.StdGen user = new School365.Students.StdGen();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (User.Identity.IsAuthenticated == false)
                Response.Redirect("UserLogin.aspx");
            value = int.Parse(User.Identity.Name);
            if (!IsPostBack)
            {
               
                var getStudentDetail = (from d in db.Students where d.StudentID == value select d).FirstOrDefault();
                FirstName.Text = getStudentDetail.Firstname;
                SurnName.Text = getStudentDetail.Surname;
                MiddleName.Text = getStudentDetail.Middlename;
                sor.Items.Add(new ListItem (getStudentDetail.SOR, getStudentDetail.SOR));
                sor.Items.FindByText(getStudentDetail.SOR).Selected = true;
                if(getStudentDetail.Nationality!=null)
                //country.Items.FindByText(getStudentDetail.Nationality).Selected = true;



               
                Sex.Items.FindByText(getStudentDetail.Gender).Selected = true;
                Email.Text = getStudentDetail.Email;
                PhoneNumber.Text = getStudentDetail.PhoneNumber;
                Lga.Text = getStudentDetail.LGA;

                MotherName.Text = getStudentDetail.MotherName;
                FormerFirstName.Text = getStudentDetail.FormerFirstame;
                FormerMiddleName.Text = getStudentDetail.FormerMiddlename;
                FormerSurname.Text = getStudentDetail.FormerSurname;
                Saltn.Text = getStudentDetail.Salution;


                if(getStudentDetail.Religion != null)
                    Religion.Items.FindByText(getStudentDetail.Religion).Selected = true;

                if (getStudentDetail.Religion != null)
                    MaritalStatus.Items.FindByText(getStudentDetail.MaritalStatus).Selected = true;

                    DateOfBirth.Text = getStudentDetail.DateOfBirth;
                PlaceOfBirth.Text = getStudentDetail.PlaceOfBirth;
                HomeAddress.Text = getStudentDetail.HomeAddress;
                StudentAddress.Text = getStudentDetail.StudentAddress;
            }
        }

        protected void submit_Click(object sender, EventArgs e)
        {
            var UpdateStudent = (from d in db.Students
                                 where d.StudentID == value
                                 select d).FirstOrDefault();
            if (getImage.HasFile != null && getImage.PostedFile.ContentLength > 0)
            {
                FileName = getImage.FileName;
                byte[] ImageValue = getImage.FileBytes;
                UpdateStudent.ImageName = FileName;
                UpdateStudent.StudentImage = ImageValue;
                db.SaveChanges();
            }
            else
            {
                Response.Write("No Image is Selected");
            }
            UpdateStudent.Firstname = FirstName.Text;
            UpdateStudent.Surname = SurnName.Text;
            UpdateStudent.Middlename = MiddleName.Text;
            UpdateStudent.SOR = sor.SelectedItem.Text;
            UpdateStudent.Nationality = country.SelectedItem.Text;
            UpdateStudent.Gender = Sex.SelectedItem.Text;
            UpdateStudent.Email = Email.Text;
            UpdateStudent.PhoneNumber = PhoneNumber.Text;
            UpdateStudent.LGA = Lga.Text;


            UpdateStudent.MotherName = MotherName.Text;
            UpdateStudent.FormerFirstame = FormerFirstName.Text;
            UpdateStudent.FormerMiddlename = FormerMiddleName.Text;
            UpdateStudent.FormerSurname = FormerSurname.Text;
            UpdateStudent.Salution = Saltn.Text;
            UpdateStudent.Religion = Religion.SelectedItem.Text;
            UpdateStudent.MaritalStatus = MaritalStatus.SelectedItem.Text;
            UpdateStudent.DateOfBirth = DateOfBirth.Text;
            UpdateStudent.PlaceOfBirth = PlaceOfBirth.Text;
            UpdateStudent.StudentAddress = StudentAddress.Text;
            UpdateStudent.HomeAddress = HomeAddress.Text;
            UpdateStudent.SponsorsAddress = SponsorsAddress.Text;
            UpdateStudent.SponsorsName = SponsorsName.Text;
            UpdateStudent.SponsorsPhoneNumber = SponsorsPhoneNumber.Text;

            db.SaveChanges();
            Response.Redirect("User.aspx");
        }
    }
}