﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UserLogin.aspx.cs" Inherits="School365.Students.UserLogin" %>
<%@ OutputCache NoStore="true" Duration="1" VaryByParam="*" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Student Login</title>
    <link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
    <!-- animation CSS -->
    <link href="css/animate.css" rel="stylesheet" />
    <!-- Custom CSS -->
    <link href="css/style.css" rel="stylesheet" />
    <!-- color CSS -->
    <link href="css/colors/blue.css" id="theme" rel="stylesheet" />
</head>
<body>
  
    <div class="preloader">
        <div class="cssload-speeding-wheel"></div>
    </div>
    <section id="wrapper" class="login-register">
        <div class="login-box">
            <div class="white-box">
                <form runat="server" class="form-horizontal form-material" id="loginform">
                    <h3 class="box-title m-b-20">Sign In</h3>
                    <asp:Label ID="message" runat="server" CssClass="text-danger box-title" style="font-size: x-small; font-weight: 700; color: #FF0066; font-style: italic;"></asp:Label>
                    <div class="form-group ">

                        <div class="col-xs-12">
                            <asp:TextBox ID="InvoiceNo" runat="server" CssClass="form-control" placeholder="Invoice No/Jamb No/Matric No" required=""></asp:TextBox>
          
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                             <asp:TextBox ID="Passsword" TextMode="Password" CssClass="form-control" runat="server" placeholder="Password" required=""></asp:TextBox>
          
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <div class="checkbox checkbox-primary pull-left p-t-0">
                               
                            </div>
                            <a href="javascript:void(0)" id="to-recover" class="text-dark pull-right"><i class="fa fa-lock m-r-5"></i>Forgot pwd?</a>
                        </div>
                    </div>
                    <div class="form-group text-center m-t-20">
                        <div class="col-xs-12">
                            <%--<asp:Button ID="Log" runat="server" Text="Log in" OnClick="Login_Click"/>--%>
                            <asp:Button ID="Login" runat="server" class="btn btn-info  btn-block" Text="Log in" OnClick="Login_Click" />                          
                        </div>
                    </div>
                  
                    <div class="form-group m-b-0">
                        <div class="col-sm-12 text-center">
                            <p>Generate Invoice? <a href="Invoice.aspx" class="text-primary m-l-5"><b>Create here</b></a></p>
                        </div>
                    </div>
                </form>
               
            </div>
        </div>
    </section>

    <script src="plugins/bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- Menu Plugin JavaScript -->
    <script src="plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>

    <!--slimscroll JavaScript -->
    <script src="js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="js/waves.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="js/custom.min.js"></script>
    <!--Style Switcher -->
    <script src="plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
</body>
</html>
