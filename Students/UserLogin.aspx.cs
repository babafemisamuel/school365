﻿using School365.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace School365.Students
{
    public partial class UserLogin : System.Web.UI.Page
    {
        StudentModel db = new StudentModel();
        private Model.Student StudentAuth;
        private Model.Pin getStudentPin;
        School365.Students.StdGen user = new School365.Students.StdGen();
        StudentInfo std = new StudentInfo();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Login_Click(object sender, EventArgs e)
        {
            int num = user.Auth(InvoiceNo.Text, Passsword.Text);

            if (HttpContext.Current.Session["StudentID"] != null)
            {
                var info = HttpContext.Current.Session["StudentID"].ToString();
                std.UserInfo(int.Parse(info));
            }

            CheckAuth(num);

        }
        private void CheckAuth(int num)
        {
            if (num == 0)
            {
                message.Text = "Username or password incorrect";
            }
            else if (num == 1)
            {
                message.Text = "Pin has exhausted please get another pin";

            }
            else if (num == 2)
            {
                FormsAuthentication.SetAuthCookie(HttpContext.Current.Session["StudentID"].ToString(), false);
                FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(
                    1,
                     HttpContext.Current.Session["StudentID"].ToString(),
                    DateTime.Now,
                    DateTime.Now.AddMinutes(10),
                    false,
                    "student"
                    );
                HttpCookie cookie = new HttpCookie(
                    FormsAuthentication.FormsCookieName,
                    FormsAuthentication.Encrypt(ticket));
                Response.Cookies.Add(cookie);
                Response.Redirect("User.aspx", false);
            }
            else if (num == 4)
            {
                message.Text = "Pin has already been allocated to someone else";
            }
            else if (num == 5)
            {
                FormsAuthentication.SetAuthCookie(HttpContext.Current.Session["StudentID"].ToString(), false);
                FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(
                    1,
                     HttpContext.Current.Session["StudentID"].ToString(),
                    DateTime.Now,
                    DateTime.Now.AddMinutes(10),
                    false,
                    "student"
                    );
                HttpCookie cookie = new HttpCookie(
                    FormsAuthentication.FormsCookieName,
                    FormsAuthentication.Encrypt(ticket));
                Response.Cookies.Add(cookie);
                Response.Redirect("User.aspx", false);
            }

            else
            {
                FormsAuthentication.SetAuthCookie(HttpContext.Current.Session["StudentID"].ToString(), false);
                FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(
                    1,
                     HttpContext.Current.Session["StudentID"].ToString(),
                    DateTime.Now,
                    DateTime.Now.AddMinutes(10),
                    false,
                    "student"
                    );
                HttpCookie cookie = new HttpCookie(
                    FormsAuthentication.FormsCookieName,
                    FormsAuthentication.Encrypt(ticket));
                Response.Cookies.Add(cookie);
                Response.Redirect("User.aspx", false);
            }
        }
    }
}