﻿using School365.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace School365.Students
{
    public class StudentInfo
    {
        StudentModel db = new StudentModel();
        public Model.Student UserInfo(int id)
        {
            var stdInfo = db.Students.Where(a => a.StudentID == id).FirstOrDefault();
            return stdInfo;
        }
    }
}