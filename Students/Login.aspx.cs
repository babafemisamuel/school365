﻿using School365.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace School365.Student
{
    public partial class Login : System.Web.UI.Page
    {
        StudentModel db = new StudentModel();
        private Model.Student StudentAuth;
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected void submit_Click(object sender, EventArgs e)
        {
           
            Model.Pin getStudentPin;
            int StudentID = db.Students.Where(a => a.InvoiceNumber == invoiceNum.Text || a.MatricNo == pin.Text)
                                             .Select(a => a.StudentID).FirstOrDefault();
            if (StudentID == 0)
                StudentID = db.JambDetails.Where(a => a.RegNo == invoiceNum.Text).Select(a => a.StudentID).FirstOrDefault();
            if (StudentID == 0)
                errorDisplay.Text = "Username or Password incorrect";

            //check if pin is in database
            getStudentPin = db.Pins.Where(a => a.PinKey == pin.Text).FirstOrDefault();
            //null redirect to login page
            if (getStudentPin == null)
                errorDisplay.Text = "Username or Password incorrect";

            if (getStudentPin.StudentID == null)
            {
                getStudentPin.StudentID = StudentID;
                getStudentPin.Counter = 1;
                db.SaveChanges();               
               // HttpContext.Current.Session["StudentID"] = StudentID.ToString();

                //string parameter = "grab=" + invoiceNum.Text;
                Response.Redirect("User.aspx");
            }
            else if (getStudentPin.StudentID != StudentID)
            {
                errorDisplay.Text = "Username or Password incorrect";
            }

            else if (getStudentPin.Counter > 5)
            {
                errorDisplay.Text = "Pin Finished";
                //string parameter = "grab=" + invoiceNum.Text;
              //  Response.Redirect("User.aspx");
            }

            else
            {
                getStudentPin.Counter = getStudentPin.Counter + 1;
                db.SaveChanges();               
                Response.Redirect("User.aspx");
            }
            
            
            //if (StudentAuth==null)
            //{
            //    errorDisplay.Text = "Username or Password is incorrect";
            //}
            //else
            //{
            //    string parameter ="grab="+StudentAuth.InvoiceNumber;
            //    Response.Redirect("Profile.aspx?"+parameter);
            //}           
        }
    }
}