﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Students/CollegeStudent.Master" AutoEventWireup="true" CodeBehind="SubCourseRegistration.aspx.cs" Inherits="School365.Students.SubCourseRegistration" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">Student Profile</h4>
                    <i class="text-danger"><b>ALL DATA MUST BE FILLED BEFORE PROCEEDING </b></i>
                </div>

            </div>
            <!-- /.row -->
            <!-- .row -->
               <div class="white-box">
            <div class="row">
                <div class="col-md-12 col-xs-12">
                 
                        <div class="col-md-6">
                              <asp:DropDownList ID="Department" runat="server" CssClass="form-control">
                            <asp:ListItem Value="beds">BED (S)</asp:ListItem>
                            <asp:ListItem Value="beda">BED (E)</asp:ListItem>
                        </asp:DropDownList>
                        </div>
                          <div class="col-md-3">
                              <asp:Button runat="server" ID="Submit" CssClass="btn btn-success" Text="Submit" OnClick="Submit_Click" />
                        </div>
                      
                    </div>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
