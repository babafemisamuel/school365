﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace School365.Students
{
    public partial class CollegeStudent : System.Web.UI.MasterPage
    {
        public int StudentID = 0;
        // StdGen user = new StdGen();
       public StudentInfo user = new StudentInfo();
        protected void Page_Load(object sender, EventArgs e)
        {
            if(HttpContext.Current.Session["StudentID"]==null)
                Response.Redirect("UserLogin.aspx");
            StudentID = int.Parse(HttpContext.Current.Session["StudentID"].ToString());
            user.UserInfo(StudentID);
            string StudnetAddress = user.UserInfo(StudentID).StudentAddress;
            //StdGen user = new StdGen();
            if (user.UserInfo(StudentID).Firstname == "SAMPLE")
                Response.Redirect("UserLogin.aspx");

            if (user.UserInfo(StudentID).StudentImage != null)
            {
                studentImage.ImageUrl = "data:image/jpeg;base64," + Convert.ToBase64String(user.UserInfo(StudentID).StudentImage);
                UserImage.ImageUrl = "data:image/jpeg;base64," + Convert.ToBase64String(user.UserInfo(StudentID).StudentImage);
            }
           
        }
    }
}