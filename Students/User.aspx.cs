﻿using School365.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace School365.Students
{
    public partial class User : System.Web.UI.Page
    {
        public int StudentID = 0;
        // StdGen user = new StdGen();
        public  StudentInfo user = new StudentInfo();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (User.Identity.IsAuthenticated == false)
                Response.Redirect("UserLogin.aspx");
            StudentID = int.Parse(User.Identity.Name);
            user.UserInfo(StudentID);
            if (user.UserInfo(StudentID).StudentAddress == "" || user.UserInfo(StudentID).StudentAddress == null)
                Response.Redirect("UserEdit.aspx");

            if (user.UserInfo(StudentID).StudentImage != null)
            {
                studentImage.ImageUrl = "data:image/jpeg;base64," + Convert.ToBase64String(user.UserInfo(StudentID).StudentImage);
            }
            //StudentModel db = new StudentModel();
            //if (!IsPostBack)
            //{
            //    StudentID = int.Parse(Request.Cookies["StudentID"].Value);
            //}
            //var allStudents = db.Students.Where(a => a.StudentID == StudentID).FirstOrDefault();


        }
    }
}