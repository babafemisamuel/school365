﻿using School365.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace School365.Students
{
    public partial class CourseForm : System.Web.UI.Page
    {
        public StudentModel db = new StudentModel();
        public int value = 0;
        public List<int> StudentID = new List<int>();
        public int sn = 0;
        public int snI = 0;
        public double totalUnit = 0;
        public double totalUnitII = 0;
        public string SessionYear = "";
        public int SessionID = 0;
        int StudentIDs = 0;
        StudentInfo user = new StudentInfo();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (User.Identity.IsAuthenticated == false)
                Response.Redirect("UserLogin.aspx");
            StudentIDs = int.Parse(User.Identity.Name);
            user.UserInfo(StudentIDs);
            string StudnetAddress = user.UserInfo(StudentIDs).StudentAddress;

            if (user.UserInfo(StudentIDs).StudentAddress == "" || user.UserInfo(StudentIDs).StudentAddress == null)
                Response.Redirect("UserEdit.aspx");

            value = user.UserInfo(StudentIDs).StudentID;
            if (!IsPostBack)
            {
                SessionYear = (from d in db.Sessions where d.CurrentSession == true select d.SessionYear).FirstOrDefault();
                SessionID = (from d in db.Sessions where d.CurrentSession == true select d.SessionID).FirstOrDefault();
                StudentID = (from d in db.Students where d.StudentID == value join f in db.Results on d.StudentID equals f.StudentID select d.StudentID).ToList();
                var studentDetail = (from d in db.Students where d.StudentID == value select d).FirstOrDefault();


                names.Text = studentDetail == null ? " " : studentDetail.Surname.ToUpper() + " " + studentDetail.Firstname.ToUpper() + " " + studentDetail.Middlename.ToUpper();
                email.Text = studentDetail == null ? " " : studentDetail.Email;
                country.Text = studentDetail == null ? " " : studentDetail.Nationality;
                gender.Text = studentDetail == null ? " " : studentDetail.Gender;
                state.Text = studentDetail == null ? " " : studentDetail.SOR;
                phoneNo.Text = studentDetail == null ? " " : studentDetail.PhoneNumber;
                lga.Text = studentDetail == null ? " " : studentDetail.LGA;
                program.Text = studentDetail == null ? " " : studentDetail.Major + "/" + studentDetail.Minor;
                level.Text = studentDetail == null ? " " : studentDetail.Level.ToString();
                matricno.Text = studentDetail == null ? " " : studentDetail.MatricNo;
                if (studentDetail.ImageName == null)
                {

                }
                else
                {
                    studentImage.ImageUrl = "data:image/jpeg;base64," + Convert.ToBase64String(studentDetail.StudentImage);
                }
            }

        }
    }
}