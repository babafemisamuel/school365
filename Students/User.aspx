﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Students/CollegeStudent.Master" AutoEventWireup="true" CodeBehind="User.aspx.cs" Inherits="School365.Students.User" %>
<%@ OutputCache NoStore="true" Duration="1" VaryByParam="*" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">Student Profile</h4>
                </div>

            </div>
            <!-- /.row -->
            <!-- .row -->
            <div class="row">
                <div class="col-md-4 col-xs-12">
                    <div class="white-box">
                 <asp:Image ID="studentImage" Width="100%" runat="server" CssClass="img-circle" ImageUrl="~/Image/studentGuy.png" />
                       <%-- <div class="user-bg">
                            <%
                                if (user.UserInfo(StudentID).StudentImage == null)
                                {%>
                          
                            <%}
                            %>
                            <%
                                if (user.UserInfo(StudentID).StudentImage != null)
                                {%>
                            <asp:Image ID="Image1" Width="100%" runat="server" CssClass="img-circle" ImageUrl="data:image/jpeg;base64,<%Convert.ToBase64String(user.UserInfo(StudentID).StudentImage); %>" />
                            <%}
                            %>
                        </div>--%>
                        
                        <div class="user-btm-box">
                            <!-- .row -->
                            <div class="row text-center m-t-10">
                                <div class="col-md-6 b-r">
                                    <strong>Name</strong>
                                    <p><%Response.Write(user.UserInfo(StudentID).Surname + " " + user.UserInfo(StudentID).Firstname + " " + user.UserInfo(StudentID).Middlename); %></p>
                                </div>
                                <div class="col-md-6">
                                    <strong>Subject Combination</strong>
                                    <p><%Response.Write(user.UserInfo(StudentID).Major + "/" + user.UserInfo(StudentID).Minor); %></p>
                                </div>
                            </div>
                            <!-- /.row -->
                            <hr>
                            <!-- .row -->
                            <div class="row text-center m-t-12">
                                <div class="col-md-8 b-r">
                                    <strong>Email ID</strong>
                                    <p><%Response.Write(user.UserInfo(StudentID).Email);%> </p>
                                </div>
                                <div class="col-md-4">
                                    <strong>Phone</strong>
                                    <p><%Response.Write(user.UserInfo(StudentID).PhoneNumber);%></p>
                                </div>
                            </div>
                            <!-- /.row -->
                            <hr>
                            <!-- .row -->
                            <div class="row text-center m-t-10">
                                <div class="col-md-12">
                                    <strong>Address</strong>
                                    <p>
                                        <p><%Response.Write(user.UserInfo(StudentID).StudentAddress);%></p>
                                        <br />
                                        <p><%Response.Write(user.UserInfo(StudentID).HomeAddress);%></p>
                                        <br />
                                        <p><%Response.Write(user.UserInfo(StudentID).SponsorsAddress);%></p>
                                    </p>
                                </div>
                            </div>
                            <hr>
                            <!-- /.row -->

                        </div>
                    </div>
                </div>
                <div class="col-md-8 col-xs-12">
                    <div class="white-box">
                        <div class="row">
                            <div class="col-md-3 col-xs-6 b-r">
                                <strong>Full Name</strong>
                                <br>
                                <p class="text-muted"><%Response.Write(user.UserInfo(StudentID).Surname + " " + user.UserInfo(StudentID).Firstname + " " + user.UserInfo(StudentID).Middlename); %></p>
                            </div>
                            <div class="col-md-3 col-xs-6 b-r">
                                <strong>Mobile</strong>
                                <br>
                                <p class="text-muted"><%Response.Write(user.UserInfo(StudentID).PhoneNumber);%></p>

                            </div>
                            <div class="col-md-4 col-xs-6 b-r">
                                <strong>Email</strong>
                                <br>
                                <p class="text-muted"><%Response.Write(user.UserInfo(StudentID).Email);%></p>
                            </div>
                            <div class="col-md-2 col-xs-6">
                                <strong>Year</strong>
                                <br>
                                <p class="text-muted"><%Response.Write(user.UserInfo(StudentID).Level);%></p>
                            </div>
                        </div>
                        <hr>

                        <h4 class="m-t-30">Skill Set</h4>
                        <hr>
                         <%string val = (user.UserInfo(StudentID).Level/400 * 100).ToString()+"%"; %>
                        <h5>EDU<span class="pull-right"><%Response.Write(val);%></span></h5>
                       
                        <div class="progress">
                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width:<%Response.Write(val);%>;"><span class="sr-only"><%Response.Write(val);%> Complete</span> </div>
                        </div>
                        <h5>GSE <span class="pull-right"><%Response.Write(val);%></span></h5>
                        <div class="progress">
                            <div class="progress-bar progress-bar-custom" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100" style="width: <%Response.Write(val);%>;"><span class="sr-only"><%Response.Write(val);%> Complete</span> </div>
                        </div>
                        <h5><%Response.Write(user.UserInfo(StudentID).Major);%><span class="pull-right"><%Response.Write(val);%></span></h5>
                        <div class="progress">
                            <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:<%Response.Write(val);%>;"><span class="sr-only"><%Response.Write(val);%> Complete</span> </div>
                        </div>
                        <h5><%Response.Write(user.UserInfo(StudentID).Minor);%> <span class="pull-right"><%Response.Write(val);%></span></h5>
                        <div class="progress">
                            <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:<%Response.Write(val);%>;"><span class="sr-only"><%Response.Write(val);%> Complete</span> </div>
                        </div>
                       
                    </div>
                </div>
            </div>
            <!-- /.row -->
            <!-- .right-sidebar -->
            <div class="right-sidebar">
                <div class="slimscrollright">
                    <div class="rpanel-title">Service Panel <span><i class="ti-close right-side-toggle"></i></span></div>
                    <div class="r-panel-body">
                        <ul>
                            <li><b>Layout Options</b></li>
                            <li>
                                <div class="checkbox checkbox-info">
                                    <input id="checkbox1" type="checkbox" class="fxhdr">
                                    <label for="checkbox1">Fix Header </label>
                                </div>
                            </li>
                            <li>
                                <div class="checkbox checkbox-warning">
                                    <input id="checkbox2" type="checkbox" checked="" class="fxsdr">
                                    <label for="checkbox2">Fix Sidebar </label>
                                </div>
                            </li>
                            <li>
                                <div class="checkbox checkbox-success">
                                    <input id="checkbox4" type="checkbox" class="open-close">
                                    <label for="checkbox4">Toggle Sidebar </label>
                                </div>
                            </li>
                        </ul>
                        <ul id="themecolors" class="m-t-20">
                            <li><b>With Light sidebar</b></li>
                            <li><a href="javascript:void(0)" theme="default" class="default-theme">1</a></li>
                            <li><a href="javascript:void(0)" theme="green" class="green-theme">2</a></li>
                            <li><a href="javascript:void(0)" theme="gray" class="yellow-theme">3</a></li>
                            <li><a href="javascript:void(0)" theme="blue" class="blue-theme working">4</a></li>
                            <li><a href="javascript:void(0)" theme="purple" class="purple-theme">5</a></li>
                            <li><a href="javascript:void(0)" theme="megna" class="megna-theme">6</a></li>
                            <li><b>With Dark sidebar</b></li>
                            <br />
                            <li><a href="javascript:void(0)" theme="default-dark" class="default-dark-theme">7</a></li>
                            <li><a href="javascript:void(0)" theme="green-dark" class="green-dark-theme">8</a></li>
                            <li><a href="javascript:void(0)" theme="gray-dark" class="yellow-dark-theme">9</a></li>
                            <li><a href="javascript:void(0)" theme="blue-dark" class="blue-dark-theme">10</a></li>
                            <li><a href="javascript:void(0)" theme="purple-dark" class="purple-dark-theme">11</a></li>
                            <li><a href="javascript:void(0)" theme="megna-dark" class="megna-dark-theme">12</a></li>
                        </ul>
                        <ul class="m-t-20 chatonline">
                            <li><b>Chat option</b></li>
                            <li>
                                <a href="javascript:void(0)">
                                    <img src="../plugins/images/users/varun.jpg" alt="user-img" class="img-circle">
                                    <span>Varun Dhavan <small class="text-success">online</small></span></a>
                            </li>
                            <li>
                                <a href="javascript:void(0)">
                                    <img src="../plugins/images/users/genu.jpg" alt="user-img" class="img-circle">
                                    <span>Genelia Deshmukh <small class="text-warning">Away</small></span></a>
                            </li>
                            <li>
                                <a href="javascript:void(0)">
                                    <img src="../plugins/images/users/ritesh.jpg" alt="user-img" class="img-circle">
                                    <span>Ritesh Deshmukh <small class="text-danger">Busy</small></span></a>
                            </li>
                            <li>
                                <a href="javascript:void(0)">
                                    <img src="../plugins/images/users/arijit.jpg" alt="user-img" class="img-circle">
                                    <span>Arijit Sinh <small class="text-muted">Offline</small></span></a>
                            </li>
                            <li>
                                <a href="javascript:void(0)">
                                    <img src="../plugins/images/users/govinda.jpg" alt="user-img" class="img-circle">
                                    <span>Govinda Star <small class="text-success">online</small></span></a>
                            </li>
                            <li>
                                <a href="javascript:void(0)">
                                    <img src="../plugins/images/users/hritik.jpg" alt="user-img" class="img-circle">
                                    <span>John Abraham<small class="text-success">online</small></span></a>
                            </li>
                            <li>
                                <a href="javascript:void(0)">
                                    <img src="../plugins/images/users/john.jpg" alt="user-img" class="img-circle">
                                    <span>Hritik Roshan<small class="text-success">online</small></span></a>
                            </li>
                            <li>
                                <a href="javascript:void(0)">
                                    <img src="../plugins/images/users/pawandeep.jpg" alt="user-img" class="img-circle">
                                    <span>Pwandeep rajan <small class="text-success">online</small></span></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- /.right-sidebar -->
        </div>
        <!-- /.container-fluid -->
        <footer class="footer text-center">2017 &copy; Federal College of Education Osiele </footer>
    </div>
</asp:Content>
