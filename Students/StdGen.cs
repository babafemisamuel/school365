﻿using School365.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;

namespace School365.Students
{

    public  class StdGen
    {
       
        public int Auth (string userString,string pin)
        {
            StudentModel db = new StudentModel();
            Model.Pin getStudentPin;           
            int StudentID = db.Students.Where(a => a.InvoiceNumber == userString || a.MatricNo == userString)
                                             .Select(a => a.StudentID).FirstOrDefault() ;
            if (StudentID == 0)
                StudentID = db.JambDetails.Where(a => a.RegNo == userString ).Select(a => a.StudentID).FirstOrDefault();
            if (StudentID == 0)
            {
                StudentID = db.Students.Where(a => a.JambRegNo == userString).FirstOrDefault()==null?0: db.Students.Where(a => a.JambRegNo == userString).FirstOrDefault().StudentID;
            }
                
            if (StudentID == 0)
                return 0;
            //check if pin is in database
            getStudentPin = db.Pins.Where(a => a.PinKey == pin).FirstOrDefault();
            //null redirect to login page
            if (getStudentPin == null)
                return 0;
            if (getStudentPin.StudentID == null)
            {
                getStudentPin.StudentID = StudentID;
                getStudentPin.Counter = 1;
                db.SaveChanges();
               //HttpContext.Current.Session


                HttpContext.Current.Session["StudentID"] = StudentID.ToString();
               
                return 2;
            }
            else if (getStudentPin.StudentID != StudentID){
                return 4;
            }
               
            else if (getStudentPin.Counter > 5){
                return 1;
            }
          
            else
            {
                getStudentPin.Counter = getStudentPin.Counter + 1;
                db.SaveChanges();
                HttpContext.Current.Session["StudentID"] = StudentID.ToString();
                return 5;
            }
            //return 2;
        }
        
        public  int AdminAuth( string UserName,string Password)
        {
            StudentModel db = new StudentModel();
            int AdminID = db.Admin.Where(a => a.Username == UserName && a.Password == Password).Select(a => a.AdminID).FirstOrDefault();
            if (AdminID == 0)
            {
                return 0;
            }
            else
            {
                //Session usr= new Session("")
                HttpCookie cookie = new HttpCookie("AdminCookie");
                cookie.Value = AdminID.ToString();
                HttpContext.Current.Response.Cookies.Add(cookie);
                return AdminID;
            }
        }
        public  Model.Admin AdminInfo()
        {

            StudentModel db = new StudentModel();
            HttpCookie cookie = HttpContext.Current.Request.Cookies.Get("AdminCookie");
            if (cookie == null)
            {
                return db.Admin.Where(a => a.Username == "SAMPLE").FirstOrDefault();
            }
            else
            {
                int data = cookie.Value == null ? 0 : int.Parse(cookie.Value);
                Model.Admin AdminInfo = db.Admin.Where(a => a.AdminID == data).FirstOrDefault();
                return AdminInfo;
            }            
        }
    }
}