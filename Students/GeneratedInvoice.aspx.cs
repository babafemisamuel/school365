﻿using School365.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace School365.Students
{
    public partial class GeneratedInvoice : System.Web.UI.Page
    {
        StudentModel db = new StudentModel();
        public string FullName = "";
        public string Email = "";
        public string PhoneNumber = "";
        public string InvoiceNumber = "";
        public DateTime DatePrinted;
        public string SubjectCombination = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.CacheControl = "private";
            Response.ExpiresAbsolute = DateTime.Now.AddDays(-1d);
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            InvoiceNumber = Request.QueryString["InvoiceNumber"];
            var students = db.Students.Where(a => a.InvoiceNumber == InvoiceNumber).Select(a => a).FirstOrDefault();
            if (students == null)
                Response.Redirect("Invoice.aspx");
            FullName = students.Surname + " " + students.Firstname + " " + students.Middlename;
            Email = students.Email;
            PhoneNumber = students.PhoneNumber;
            DatePrinted = DateTime.Now;
            
            SubjectCombination = students.Major + "/" + students.Minor;


            // public string FirstName= students.

            //string parameters = "surname=" + surname.Text + "&firstname=" + firstname.Text + "&othernames=" + othername.Text + "&SOR=" + sor.SelectedItem.Text + "&LGA=" + lga.Text + "&Nationality=" + country.SelectedItem.Text + "&EmailAdress=" + email.Text + "&PhoneNumber=" + phoneNumber.Text + "&InvoiceNo=" + InvoiceNumber + "&DateEntered=" + DateTime.Now.Year + "&Major=" + major.SelectedItem.Value + "&Minor=" + minor.SelectedItem.Value + "&Gender=" + sex.Text;
        }
    }
}