﻿using School365.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace School365.Students
{
    public partial class BioData : System.Web.UI.Page
    {
        StudentModel db = new StudentModel();
        public List<int> OlevelID = new List<int>();
        public List<int> UtmeID = new List<int>();
        public List<string> OlevelCenterNo = new List<string>();
        public List<OlevelDetail> olevel = new List<OlevelDetail>();
        public List<JambDetail> jambDetail = new List<JambDetail>();
        int StudentID = 0;
        // StdGen user = new StdGen();
        StudentInfo user = new StudentInfo();


        protected void Page_Load(object sender, EventArgs e)
        {
            if (User.Identity.IsAuthenticated == false)
                Response.Redirect("UserLogin.aspx");
            StudentID = int.Parse(User.Identity.Name);
            user.UserInfo(StudentID);

            string StudnetAddress = user.UserInfo(StudentID).StudentAddress;

            if (user.UserInfo(StudentID).StudentAddress == "" || user.UserInfo(StudentID).StudentAddress == null)
                Response.Redirect("UserEdit.aspx");
            if (!IsPostBack)
            {          
                getSubjects();
            }
            AllData();

        }

       
        private void OneSitting()
        {
            //getInfo
            var InsertWaec1 = new OlevelDetail
            {
                StudentID = user.UserInfo(StudentID).StudentID,
                CenterNumber = CntrNo.Text,
                ExamType = allExmTypr.SelectedItem.Text,
                RegNo = ORegNo.Text,
                ExamYear = ExamYr.Text,
                SubjectName = oLeel1.Text,
                Grade = Grade1.Text
            };
            db.OlevelDetails.Add(InsertWaec1);
            db.SaveChanges();

            var InsertWaec2 = new OlevelDetail
            {
                StudentID = user.UserInfo(StudentID).StudentID,
                CenterNumber = CntrNo.Text,
                ExamType = allExmTypr.SelectedItem.Text,
                RegNo = ORegNo.Text,
                ExamYear = ExamYr.Text,
                SubjectName = oLeel2.Text,
                Grade = Grade2.Text
            };
            db.OlevelDetails.Add(InsertWaec2);
            db.SaveChanges();

            var InsertWaec3 = new OlevelDetail
            {
                StudentID = user.UserInfo(StudentID).StudentID,
                CenterNumber = CntrNo.Text,
                ExamType = allExmTypr.SelectedItem.Text,
                RegNo = ORegNo.Text,
                ExamYear = ExamYr.Text,
                SubjectName = oLeel3.Text,
                Grade = Grade3.Text
            };
            db.OlevelDetails.Add(InsertWaec3);
            db.SaveChanges();

            var InsertWaec4 = new OlevelDetail
            {
                StudentID = user.UserInfo(StudentID).StudentID,
                CenterNumber = CntrNo.Text,
                ExamType = allExmTypr.SelectedItem.Text,
                RegNo = ORegNo.Text,
                ExamYear = ExamYr.Text,
                SubjectName = oLeel4.Text,
                Grade = Grade4.Text
            };
            db.OlevelDetails.Add(InsertWaec4);
            db.SaveChanges();

            var InsertWaec5 = new OlevelDetail
            {
                StudentID = user.UserInfo(StudentID).StudentID,
                CenterNumber = CntrNo.Text,
                ExamType = allExmTypr.SelectedItem.Text,
                RegNo = ORegNo.Text,
                ExamYear = ExamYr.Text,
                SubjectName = oLeel5.Text,
                Grade = Grade5.Text
            };
            db.OlevelDetails.Add(InsertWaec5);
            db.SaveChanges();

            var InsertWaec6 = new OlevelDetail
            {
                StudentID = user.UserInfo(StudentID).StudentID,
                CenterNumber = CntrNo.Text,
                ExamType = allExmTypr.SelectedItem.Text,
                RegNo = ORegNo.Text,
                ExamYear = ExamYr.Text,
                SubjectName = oLeel6.Text,
                Grade = Grade6.Text
            };
            db.OlevelDetails.Add(InsertWaec6);
            db.SaveChanges();

            var InsertWaec7 = new OlevelDetail
            {
                StudentID = user.UserInfo(StudentID).StudentID,
                CenterNumber = CntrNo.Text,
                ExamType = allExmTypr.SelectedItem.Text,
                RegNo = ORegNo.Text,
                ExamYear = ExamYr.Text,
                SubjectName = oLeel7.Text,
                Grade = Grade7.Text
            };
            db.OlevelDetails.Add(InsertWaec7);
            db.SaveChanges();

            var InsertWaec8 = new OlevelDetail
            {
                StudentID = user.UserInfo(StudentID).StudentID,
                CenterNumber = CntrNo.Text,
                ExamType = allExmTypr.SelectedItem.Text,
                RegNo = ORegNo.Text,
                ExamYear = ExamYr.Text,
                SubjectName = oLeel8.Text,
                Grade = Grade8.Text
            };
            db.OlevelDetails.Add(InsertWaec8);
            db.SaveChanges();

            var InsertWaec9 = new OlevelDetail
            {
                StudentID = user.UserInfo(StudentID).StudentID,
                CenterNumber = CntrNo.Text,
                ExamType = allExmTypr.SelectedItem.Text,
                RegNo = ORegNo.Text,
                ExamYear = ExamYr.Text,
                SubjectName = oLeel9.Text,
                Grade = Grade9.Text
            };
            db.OlevelDetails.Add(InsertWaec9);
            db.SaveChanges();
        }
       
        private void getSubjects()
        {
            
            string filePath = Server.MapPath("~/SecSubj.xml");
            using (DataSet ds = new DataSet())
            {
                ds.ReadXml(filePath);
                oLeel1.DataSource = ds;
                oLeel1.DataTextField = "ListItem_Text";
                oLeel1.DataBind();

                oLeel2.DataSource = ds;
                oLeel2.DataTextField = "ListItem_Text";
                oLeel2.DataBind();

                oLeel3.DataSource = ds;
                oLeel3.DataTextField = "ListItem_Text";
                oLeel3.DataBind();

                oLeel4.DataSource = ds;
                oLeel4.DataTextField = "ListItem_Text";
                oLeel4.DataBind();

                oLeel5.DataSource = ds;
                oLeel5.DataTextField = "ListItem_Text";
                oLeel5.DataBind();

                oLeel6.DataSource = ds;
                oLeel6.DataTextField = "ListItem_Text";
                oLeel6.DataBind();

                oLeel7.DataSource = ds;
                oLeel7.DataTextField = "ListItem_Text";
                oLeel7.DataBind();

                oLeel8.DataSource = ds;
                oLeel8.DataTextField = "ListItem_Text";
                oLeel8.DataBind();

                oLeel9.DataSource = ds;
                oLeel9.DataTextField = "ListItem_Text";
                oLeel9.DataBind();


             
            }
        }

        protected void sub_Click(object sender, EventArgs e)
        {
            try
            {
                InsertJamb();

            }

            catch (Exception ex)
            {
                Error.Text = "Error" + ex.Message;
            }
        }
        private void InsertJamb()
        {
           
            var Insertjamb1 = new Model.JambDetail
            {
                StudentID = user.UserInfo(StudentID).StudentID,
                Center = Center.Text,
                RegNo = RegNo.Text,
                Year = Year.SelectedItem.Text,
                Subject = Subj1.Text,
                Score = Scr1.Text
            };
            db.JambDetails.Add(Insertjamb1);
            db.SaveChanges();

            var Insertjamb2 = new Model.JambDetail
            {
                StudentID = user.UserInfo(StudentID).StudentID,
                Center = Center.Text,
                RegNo = RegNo.Text,
                Year = Year.SelectedItem.Text,
                Subject = Subj2.Text,
                Score = Scr2.Text
            };
            db.JambDetails.Add(Insertjamb2);
            db.SaveChanges();

            var Insertjamb3 = new Model.JambDetail
            {
                StudentID = user.UserInfo(StudentID).StudentID,
                Center = Center.Text,
                RegNo = RegNo.Text,
                Year = Year.SelectedItem.Text,
                Subject = Subj3.Text,
                Score = Scr3.Text
            };
            db.JambDetails.Add(Insertjamb3);
            db.SaveChanges();

            var Insertjamb4 = new Model.JambDetail
            {
                StudentID = user.UserInfo(StudentID).StudentID,
                Center = Center.Text,
                RegNo = RegNo.Text,
                Year = Year.SelectedItem.Text,
                Subject = Subj4.Text,
                Score = Scr4.Text
            };
            db.JambDetails.Add(Insertjamb4);
            db.SaveChanges();
        }

        protected void Submit_Click(object sender, EventArgs e)
        {
            try
            {
                OneSitting();
               
            }
            catch (Exception ex)
            {
                Error.Text = "Error" +ex.Message;
               
            }
           
        }

        private void AllData()
        {
            StudentID = user.UserInfo(StudentID).StudentID;
            olevel = db.OlevelDetails.Where(a => a.StudentID == StudentID).Select(a=>a).OrderBy(a=>a.SubjectName).ToList();
            jambDetail = db.JambDetails.Where(a => a.StudentID == StudentID).Select(a => a).OrderBy(a=>a.Subject).ToList();
            OlevelID = olevel.Select(a => a.OlevelDetailsID).ToList();
            UtmeID = jambDetail.Select(a => a.JambDetailID).ToList();
            OlevelCenterNo = olevel.Select(a => a.CenterNumber).Distinct().ToList();

        }
    }
}