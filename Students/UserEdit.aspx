﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Students/CollegeStudent.Master" AutoEventWireup="true" CodeBehind="UserEdit.aspx.cs" Inherits="School365.Students.UserEdit" %>

<%@ OutputCache NoStore="true" Duration="1" VaryByParam="*" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">Student Profile</h4>
                    <i class="text-danger"><b>ALL DATA MUST BE FILLED BEFORE PROCEEDING </b></i>
                </div>

            </div>
            <!-- /.row -->
            <!-- .row -->
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <div class="white-box">
                        <div class="">
                            <div class="row">
                                <div class="col-lg-4"></div>
                                <div class="col-lg-4"></div>
                                <div class="col-lg-4" style="float: right">
                                    <label>Upload Image</label>
                                    <asp:FileUpload ID="getImage" runat="server" placeholder="Image Upload" CssClass="form-control" />
                                </div>
                            </div>
                            <br />
                            <div class="row">

                                <div class="col-sm-4 col-sm-offset-0">

                                    <div class="">
                                        <asp:TextBox ID="SurnName" runat="server" Text="" CssClass="form-control" placeholder="Surname"></asp:TextBox>
                                    </div>
                                    <hr />
                                    <div class=" ">
                                        SELECT YOUR COUNTRY
                                        <asp:DropDownList ID="country" runat="server" CssClass="form-control">

                                            <asp:ListItem>NIGERIA</asp:ListItem>
                                            <asp:ListItem>OTHERS</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>

                                    <hr />
                                    <div class="">
                                        SELECT YOUR GENDER
                                        <asp:DropDownList ID="Sex" runat="server" CssClass="form-control">

                                            <asp:ListItem>MALE</asp:ListItem>
                                            <asp:ListItem>FEMALE</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>

                                </div>
                                <div class="col-sm-4 col-sm-offset-0">
                                    <div>
                                        <asp:TextBox ID="FirstName" runat="server" Text="" CssClass="form-control" placeholder="Firstname"></asp:TextBox>
                                    </div>


                                    <hr />
                                    <div class="">
                                        SELECT YOUR STATE OF ORIGIN
                                        <asp:DropDownList ID="sor" runat="server" CssClass="form-control">

                                            <asp:ListItem>Abia</asp:ListItem>
                                            <asp:ListItem>Adamawa</asp:ListItem>
                                            <asp:ListItem>Akwa-Ibom</asp:ListItem>
                                            <asp:ListItem>Anambra</asp:ListItem>
                                            <asp:ListItem>Abuja</asp:ListItem>
                                            <asp:ListItem>Bauchi</asp:ListItem>
                                            <asp:ListItem>Bayelsa</asp:ListItem>
                                            <asp:ListItem>Benue</asp:ListItem>
                                            <asp:ListItem>Borno</asp:ListItem>
                                            <asp:ListItem>Cross River</asp:ListItem>
                                            <asp:ListItem>Delta</asp:ListItem>
                                            <asp:ListItem>Ebonyi</asp:ListItem>
                                            <asp:ListItem>Edo</asp:ListItem>
                                            <asp:ListItem>Ekiti</asp:ListItem>
                                            <asp:ListItem>Enugu</asp:ListItem>
                                            <asp:ListItem>Gombe</asp:ListItem>
                                            <asp:ListItem>Imo</asp:ListItem>
                                            <asp:ListItem>Jigawa</asp:ListItem>
                                            <asp:ListItem>Kaduna</asp:ListItem>
                                            <asp:ListItem>Kano</asp:ListItem>
                                            <asp:ListItem>Katsina</asp:ListItem>
                                            <asp:ListItem>Kebbi</asp:ListItem>
                                            <asp:ListItem>Kogi</asp:ListItem>
                                            <asp:ListItem>Kwara</asp:ListItem>
                                            <asp:ListItem>Lagos</asp:ListItem>
                                            <asp:ListItem>Nassarawa</asp:ListItem>
                                            <asp:ListItem>Niger</asp:ListItem>
                                            <asp:ListItem>Ogun</asp:ListItem>
                                            <asp:ListItem>Ondo</asp:ListItem>
                                            <asp:ListItem>Osun</asp:ListItem>
                                            <asp:ListItem>Oyo</asp:ListItem>
                                            <asp:ListItem>Plateau</asp:ListItem>
                                            <asp:ListItem>Rivers</asp:ListItem>
                                            <asp:ListItem>Sokoto</asp:ListItem>
                                            <asp:ListItem>Taraba</asp:ListItem>
                                            <asp:ListItem>Yobe</asp:ListItem>
                                            <asp:ListItem>Zamfara</asp:ListItem>
                                            <asp:ListItem>Others</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <hr />
                                    <div class="">
                                        <asp:TextBox ID="PhoneNumber" runat="server" Text="" CssClass="form-control" placeholder="Phone Number"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-sm-4 col-sm-offset-0">
                                    <div class="">
                                        <asp:TextBox ID="MiddleName" runat="server" Text="" CssClass="form-control" placeholder="Middlename"></asp:TextBox>
                                    </div>
                                    <hr />
                                    <div>
                                        <asp:TextBox ID="Lga" runat="server" Text="" placeholder="Local Govt Area" CssClass="form-control"></asp:TextBox>
                                    </div>
                                    <hr />
                                    <div class="">
                                        <asp:TextBox ID="Email" runat="server" Text="" CssClass="form-control" placeholder="Email"></asp:TextBox>
                                    </div>
                                    <hr />


                                </div>

                            </div>
                        </div>

                        <br />

                        <hr />
                        <h3>MORE DETAILS</h3>


                        <div class="col-md-4">

                            <asp:TextBox ID="MotherName" CssClass="form-control" runat="server" placeholder="Mother's Name"></asp:TextBox><br />
                            <asp:TextBox ID="FormerFirstName" CssClass="form-control" runat="server" placeholder="Former Mother's First Name"></asp:TextBox><br />
                            <asp:TextBox ID="FormerMiddleName" CssClass="form-control" runat="server" placeholder="Former Mother's Middle Name"></asp:TextBox><br />
                            <asp:TextBox ID="HomeAddress" CssClass="form-control" runat="server" placeholder="Student Home Address"></asp:TextBox><br />
                            <asp:TextBox ID="StudentAddress" CssClass="form-control" runat="server" placeholder="Student School Address"></asp:TextBox><br />
                        </div>
                        <div class="col-md-4">
                            <asp:TextBox ID="FormerSurname" CssClass="form-control" runat="server" placeholder="Former Mother's Surname Name"></asp:TextBox><br />
                            <asp:TextBox ID="Saltn" runat="server" CssClass="form-control" placeholder="Student Salutaion (Mr,Mrs,Dr, etc..)"></asp:TextBox><br />
                            <asp:DropDownList ID="MaritalStatus" CssClass="form-control" runat="server">
                                <asp:ListItem>Single</asp:ListItem>
                                <asp:ListItem>Married</asp:ListItem>
                                <asp:ListItem>Divorced</asp:ListItem>
                                <asp:ListItem>Complicated</asp:ListItem>
                            </asp:DropDownList><br />
                            <asp:TextBox ID="SponsorsName" CssClass="form-control" runat="server" placeholder="Sponsors Name"></asp:TextBox><br />
                            <asp:TextBox ID="SponsorsAddress" CssClass="form-control" runat="server" placeholder="Sponsors Address"></asp:TextBox><br />
                        </div>
                        <div class="col-md-4">
                           SELECT RELIGION
                            <asp:DropDownList ID="Religion" CssClass="form-control" runat="server">
                                
                                <asp:ListItem>Christianity</asp:ListItem>
                                <asp:ListItem>Islam</asp:ListItem>
                                <asp:ListItem>Other</asp:ListItem>
                            </asp:DropDownList><br />
                            <asp:TextBox ID="DateOfBirth" CssClass="form-control" runat="server" placeholder="Date of Birth (Day/Month/Year)"></asp:TextBox><br />
                            <asp:TextBox ID="PlaceOfBirth" CssClass="form-control" runat="server" placeholder="Place Of Birth"></asp:TextBox><br />
                            <asp:TextBox ID="SponsorsPhoneNumber" CssClass="form-control" runat="server" placeholder="SponsorsPhoneNumber"></asp:TextBox><br />
                        </div>

                        <br />
                        <div class="row">
                            <div class="col-lg-6">
                                <asp:Button ID="submit" runat="server" Text="Save" CssClass="btn btn-success" OnClick="submit_Click" />
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
