﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Students/CollegeStudent.Master" AutoEventWireup="true" CodeBehind="UserResults.aspx.cs" Inherits="School365.Students.BioData" %>

<%@ OutputCache NoStore="true" Duration="1" VaryByParam="*" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">Student Results</h4>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <div class="white-box">
                        <strong><em>
                            <asp:Label ID="Error" runat="server" Text="" CssClass="auto-style2"></asp:Label>
                        </em></strong>

                        <%
                            if (jambDetail.Count() == 0)
                            {
                        %>
                        <button type="button" class="btn btn-success btn" data-toggle="modal" data-target="#jambDetail" data-whatever="@mdo">Add UTME Details</button>
                        <%}
                        %>
                        <%
                            if (OlevelCenterNo.Count <= 1)
                            {
                        %>
                        <button type="button" class="btn btn-info btn" data-toggle="modal" data-target="#oleveldetails" data-whatever="@mdo">Add O' Level Details</button>
                        <%}
                        %>
                        <div class="modal fade bs-example-modal-lg" id="oleveldetails" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                        <h4 class="modal-title" id="useroleve">Olevel Details</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <asp:TextBox ID="ExamCntr" runat="server" CssClass="form-control" placeholder="EXAM CENTER"></asp:TextBox>
                                                </div>
                                                <div class="col-md-4">
                                                    <asp:TextBox ID="CntrNo" runat="server" CssClass="form-control" placeholder="CENTER NUMBER"></asp:TextBox>
                                                </div>
                                                <div class="col-md-4">
                                                    <asp:TextBox ID="ORegNo" runat="server" CssClass="form-control" placeholder="REGISTERATION NUMBER"></asp:TextBox>
                                                </div>

                                            </div>
                                            <div class="row">
                                                <br />
                                                <br />
                                                <div class="col-md-6">
                                                    <asp:DropDownList ID="allExmTypr" CssClass="form-control" runat="server">
                                                        <asp:ListItem>Select Exam Type</asp:ListItem>
                                                        <asp:ListItem>NECO</asp:ListItem>
                                                        <asp:ListItem>NABTEB</asp:ListItem>
                                                        <asp:ListItem>WAEC</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="col-md-6">
                                                    <asp:DropDownList ID="ExamYr" runat="server" CssClass="form-control">
                                                        <asp:ListItem>2008</asp:ListItem>
                                                        <asp:ListItem>2009</asp:ListItem>
                                                        <asp:ListItem>2010</asp:ListItem>
                                                        <asp:ListItem>2012</asp:ListItem>
                                                        <asp:ListItem>2013</asp:ListItem>
                                                        <asp:ListItem>2014</asp:ListItem>
                                                        <asp:ListItem>2015</asp:ListItem>

                                                        <asp:ListItem>2016</asp:ListItem>
                                                        <asp:ListItem>2017</asp:ListItem>
                                                        <asp:ListItem>2018</asp:ListItem>
                                                        <asp:ListItem>2019</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <br />
                                                <br />
                                                <div class="col-md-8">
                                                    <asp:DropDownList ID="oLeel1" CssClass="form-control" runat="server">
                                                    </asp:DropDownList>
                                                    <br />
                                                    <br />
                                                    <asp:DropDownList ID="oLeel2" CssClass="form-control" runat="server">
                                                    </asp:DropDownList>
                                                    <br />
                                                    <br />
                                                    <asp:DropDownList ID="oLeel3" CssClass="form-control" runat="server">
                                                    </asp:DropDownList>
                                                    <br />
                                                    <br />
                                                    <asp:DropDownList ID="oLeel4" CssClass="form-control" runat="server">
                                                    </asp:DropDownList>
                                                    <br />
                                                    <br />
                                                    <asp:DropDownList ID="oLeel5" CssClass="form-control" runat="server">
                                                    </asp:DropDownList>
                                                    <br />
                                                    <br />
                                                    <asp:DropDownList ID="oLeel6" CssClass="form-control" runat="server">
                                                    </asp:DropDownList>
                                                    <br />
                                                    <br />
                                                    <asp:DropDownList ID="oLeel7" CssClass="form-control" runat="server">
                                                    </asp:DropDownList>
                                                    <br />
                                                    <br />
                                                    <asp:DropDownList ID="oLeel8" CssClass="form-control" runat="server">
                                                    </asp:DropDownList>
                                                    <br />
                                                    <br />
                                                    <asp:DropDownList ID="oLeel9" CssClass="form-control" runat="server">
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="col-md-4">
                                                    <asp:DropDownList ID="Grade1" CssClass="form-control" runat="server">
                                                        <asp:ListItem>SELECT GRADE</asp:ListItem>
                                                        <asp:ListItem>Awaiting Result</asp:ListItem>
                                                        <asp:ListItem>A1</asp:ListItem>
                                                        <asp:ListItem>B2</asp:ListItem>
                                                        <asp:ListItem>B3</asp:ListItem>
                                                        <asp:ListItem>C4</asp:ListItem>
                                                        <asp:ListItem>C5</asp:ListItem>
                                                        <asp:ListItem>C6</asp:ListItem>
                                                        <asp:ListItem>D7</asp:ListItem>
                                                        <asp:ListItem>E8</asp:ListItem>
                                                        <asp:ListItem>F9</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <br />
                                                    <br />
                                                    <asp:DropDownList ID="Grade2" CssClass="form-control" runat="server">
                                                        <asp:ListItem>SELECT GRADE</asp:ListItem>
                                                        <asp:ListItem>Awaiting Result</asp:ListItem>
                                                        <asp:ListItem>A1</asp:ListItem>
                                                        <asp:ListItem>B2</asp:ListItem>
                                                        <asp:ListItem>B3</asp:ListItem>
                                                        <asp:ListItem>C4</asp:ListItem>
                                                        <asp:ListItem>C5</asp:ListItem>
                                                        <asp:ListItem>C6</asp:ListItem>
                                                        <asp:ListItem>D7</asp:ListItem>
                                                        <asp:ListItem>E8</asp:ListItem>
                                                        <asp:ListItem>F9</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <br />
                                                    <br />
                                                    <asp:DropDownList ID="Grade3" CssClass="form-control" runat="server">
                                                        <asp:ListItem>SELECT GRADE</asp:ListItem>
                                                        <asp:ListItem>Awaiting Result</asp:ListItem>
                                                        <asp:ListItem>A1</asp:ListItem>
                                                        <asp:ListItem>B2</asp:ListItem>
                                                        <asp:ListItem>B3</asp:ListItem>
                                                        <asp:ListItem>C4</asp:ListItem>
                                                        <asp:ListItem>C5</asp:ListItem>
                                                        <asp:ListItem>C6</asp:ListItem>
                                                        <asp:ListItem>D7</asp:ListItem>
                                                        <asp:ListItem>E8</asp:ListItem>
                                                        <asp:ListItem>F9</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <br />
                                                    <br />
                                                    <asp:DropDownList ID="Grade4" CssClass="form-control" runat="server">
                                                        <asp:ListItem>SELECT GRADE</asp:ListItem>
                                                        <asp:ListItem>Awaiting Result</asp:ListItem>
                                                        <asp:ListItem>A1</asp:ListItem>
                                                        <asp:ListItem>B2</asp:ListItem>
                                                        <asp:ListItem>B3</asp:ListItem>
                                                        <asp:ListItem>C4</asp:ListItem>
                                                        <asp:ListItem>C5</asp:ListItem>
                                                        <asp:ListItem>C6</asp:ListItem>
                                                        <asp:ListItem>D7</asp:ListItem>
                                                        <asp:ListItem>E8</asp:ListItem>
                                                        <asp:ListItem>F9</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <br />
                                                    <br />
                                                    <asp:DropDownList ID="Grade5" CssClass="form-control" runat="server">
                                                        <asp:ListItem>SELECT GRADE</asp:ListItem>
                                                        <asp:ListItem>Awaiting Result</asp:ListItem>
                                                        <asp:ListItem>A1</asp:ListItem>
                                                        <asp:ListItem>B2</asp:ListItem>
                                                        <asp:ListItem>B3</asp:ListItem>
                                                        <asp:ListItem>C4</asp:ListItem>
                                                        <asp:ListItem>C5</asp:ListItem>
                                                        <asp:ListItem>C6</asp:ListItem>
                                                        <asp:ListItem>D7</asp:ListItem>
                                                        <asp:ListItem>E8</asp:ListItem>
                                                        <asp:ListItem>F9</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <br />
                                                    <br />
                                                    <asp:DropDownList ID="Grade6" CssClass="form-control" runat="server">
                                                        <asp:ListItem>SELECT GRADE</asp:ListItem>
                                                        <asp:ListItem>Awaiting Result</asp:ListItem>
                                                        <asp:ListItem>A1</asp:ListItem>
                                                        <asp:ListItem>B2</asp:ListItem>
                                                        <asp:ListItem>B3</asp:ListItem>
                                                        <asp:ListItem>C4</asp:ListItem>
                                                        <asp:ListItem>C5</asp:ListItem>
                                                        <asp:ListItem>C6</asp:ListItem>
                                                        <asp:ListItem>D7</asp:ListItem>
                                                        <asp:ListItem>E8</asp:ListItem>
                                                        <asp:ListItem>F9</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <br />
                                                    <br />
                                                    <asp:DropDownList ID="Grade7" CssClass="form-control" runat="server">
                                                        <asp:ListItem>SELECT GRADE</asp:ListItem>
                                                        <asp:ListItem>Awaiting Result</asp:ListItem>
                                                        <asp:ListItem>A1</asp:ListItem>
                                                        <asp:ListItem>B2</asp:ListItem>
                                                        <asp:ListItem>B3</asp:ListItem>
                                                        <asp:ListItem>C4</asp:ListItem>
                                                        <asp:ListItem>C5</asp:ListItem>
                                                        <asp:ListItem>C6</asp:ListItem>
                                                        <asp:ListItem>D7</asp:ListItem>
                                                        <asp:ListItem>E8</asp:ListItem>
                                                        <asp:ListItem>F9</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <br />
                                                    <br />
                                                    <asp:DropDownList ID="Grade8" CssClass="form-control" runat="server">
                                                        <asp:ListItem>SELECT GRADE</asp:ListItem>
                                                        <asp:ListItem>Awaiting Result</asp:ListItem>
                                                        <asp:ListItem>A1</asp:ListItem>
                                                        <asp:ListItem>B2</asp:ListItem>
                                                        <asp:ListItem>B3</asp:ListItem>
                                                        <asp:ListItem>C4</asp:ListItem>
                                                        <asp:ListItem>C5</asp:ListItem>
                                                        <asp:ListItem>C6</asp:ListItem>
                                                        <asp:ListItem>D7</asp:ListItem>
                                                        <asp:ListItem>E8</asp:ListItem>
                                                        <asp:ListItem>F9</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <br />
                                                    <br />
                                                    <asp:DropDownList ID="Grade9" CssClass="form-control" runat="server">
                                                        <asp:ListItem>SELECT GRADE</asp:ListItem>
                                                        <asp:ListItem>Awaiting Result</asp:ListItem>
                                                        <asp:ListItem>A1</asp:ListItem>
                                                        <asp:ListItem>B2</asp:ListItem>
                                                        <asp:ListItem>B3</asp:ListItem>
                                                        <asp:ListItem>C4</asp:ListItem>
                                                        <asp:ListItem>C5</asp:ListItem>
                                                        <asp:ListItem>C6</asp:ListItem>
                                                        <asp:ListItem>D7</asp:ListItem>
                                                        <asp:ListItem>E8</asp:ListItem>
                                                        <asp:ListItem>F9</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="modal-footer">

                                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                            <asp:Button ID="Submit" runat="server" Text="REGISTER" CssClass="btn btn-info" OnClick="Submit_Click" />
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>


                <div>
                </div>
                <br />
                <br />

                <div class="col-md-4">

                    <!-- sample modal content -->
                    <div class="modal fade bs-example-modal-lg" id="jambDetail" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h4 class="modal-title" id="myLargeModalLabel">Jamb Details</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <asp:TextBox ID="RegNo" runat="server" CssClass="form-control" placeholder="JAMB REGISTRATION NUMBER"></asp:TextBox>
                                        </div>
                                        <div class="col-md-4">
                                            <asp:TextBox ID="Center" runat="server" CssClass="form-control" placeholder="CENTER NUMBER"></asp:TextBox>
                                        </div>
                                        <div class="col-md-4">
                                            <asp:DropDownList ID="Year" runat="server" CssClass="form-control">
                                                <asp:ListItem>SELECT YEAR</asp:ListItem>
                                                <asp:ListItem>2000</asp:ListItem>
                                                <asp:ListItem>2001</asp:ListItem>
                                                <asp:ListItem>2002</asp:ListItem>
                                                <asp:ListItem>2003</asp:ListItem>
                                                <asp:ListItem>2004</asp:ListItem>
                                                <asp:ListItem>2005</asp:ListItem>
                                                <asp:ListItem>2006</asp:ListItem>
                                                <asp:ListItem>2007</asp:ListItem>
                                                <asp:ListItem>2008</asp:ListItem>
                                                <asp:ListItem>2009</asp:ListItem>
                                                <asp:ListItem>2010</asp:ListItem>
                                                <asp:ListItem>2011</asp:ListItem>
                                                <asp:ListItem>2012</asp:ListItem>
                                                <asp:ListItem>2013</asp:ListItem>
                                                <asp:ListItem>2014</asp:ListItem>
                                                <asp:ListItem>2015</asp:ListItem>
                                                <asp:ListItem>2016</asp:ListItem>
                                                <asp:ListItem>2017</asp:ListItem>
                                                <asp:ListItem>2018</asp:ListItem>
                                                <asp:ListItem>2019</asp:ListItem>

                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <br />
                                        <br />
                                        <div class="col-md-6">
                                            <asp:DropDownList ID="Subj1" CssClass="form-control" runat="server">
                                                <asp:ListItem>Mathematics</asp:ListItem>
                                                <asp:ListItem>English Language</asp:ListItem>
                                                <asp:ListItem>Agricultural Science</asp:ListItem>
                                                <asp:ListItem>Applied Electricity</asp:ListItem>
                                                <asp:ListItem>Auto Mechanics</asp:ListItem>
                                                <asp:ListItem>Biology</asp:ListItem>
                                                <asp:ListItem>Building Construction</asp:ListItem>
                                                <asp:ListItem>Chemistry</asp:ListItem>
                                                <asp:ListItem>Christian Religious Knowledge</asp:ListItem>
                                                <asp:ListItem>Clothing and Textile</asp:ListItem>
                                                <asp:ListItem>Commerce</asp:ListItem>
                                                <asp:ListItem>Economics</asp:ListItem>
                                                <asp:ListItem>Electronics</asp:ListItem>
                                                <asp:ListItem>Financial Accounting</asp:ListItem>
                                                <asp:ListItem>Foods and Nutrition</asp:ListItem>
                                                <asp:ListItem>French</asp:ListItem>
                                                <asp:ListItem>Further Mathematics</asp:ListItem>
                                                <asp:ListItem>Geography</asp:ListItem>
                                                <asp:ListItem>Government</asp:ListItem>
                                                <asp:ListItem>Health Science</asp:ListItem>
                                                <asp:ListItem>History</asp:ListItem>
                                                <asp:ListItem>Home Management</asp:ListItem>
                                                <asp:ListItem>Islamic Studies</asp:ListItem>
                                                <asp:ListItem>Literature in English</asp:ListItem>
                                                <asp:ListItem>Metalwork</asp:ListItem>
                                                <asp:ListItem>Music</asp:ListItem>
                                                <asp:ListItem>Physical Education</asp:ListItem>
                                                <asp:ListItem>Physics</asp:ListItem>
                                                <asp:ListItem>Shorthand</asp:ListItem>
                                                <asp:ListItem>Technical Drawing</asp:ListItem>
                                                <asp:ListItem>Typewriting</asp:ListItem>
                                                <asp:ListItem>Visual Arts</asp:ListItem>
                                                <asp:ListItem>Woodwork</asp:ListItem>
                                                <asp:ListItem>Yoruba</asp:ListItem>

                                            </asp:DropDownList>
                                            <br />
                                            <br />
                                            <asp:DropDownList ID="Subj2" CssClass="form-control" runat="server">
                                                <asp:ListItem>Mathematics</asp:ListItem>
                                                <asp:ListItem>English Language</asp:ListItem>
                                                <asp:ListItem>Agricultural Science</asp:ListItem>
                                                <asp:ListItem>Applied Electricity</asp:ListItem>
                                                <asp:ListItem>Auto Mechanics</asp:ListItem>
                                                <asp:ListItem>Biology</asp:ListItem>
                                                <asp:ListItem>Building Construction</asp:ListItem>
                                                <asp:ListItem>Chemistry</asp:ListItem>
                                                <asp:ListItem>Christian Religious Knowledge</asp:ListItem>
                                                <asp:ListItem>Clothing and Textile</asp:ListItem>
                                                <asp:ListItem>Commerce</asp:ListItem>
                                                <asp:ListItem>Economics</asp:ListItem>
                                                <asp:ListItem>Electronics</asp:ListItem>
                                                <asp:ListItem>Financial Accounting</asp:ListItem>
                                                <asp:ListItem>Foods and Nutrition</asp:ListItem>
                                                <asp:ListItem>French</asp:ListItem>
                                                <asp:ListItem>Further Mathematics</asp:ListItem>
                                                <asp:ListItem>Geography</asp:ListItem>
                                                <asp:ListItem>Government</asp:ListItem>
                                                <asp:ListItem>Health Science</asp:ListItem>
                                                <asp:ListItem>History</asp:ListItem>
                                                <asp:ListItem>Home Management</asp:ListItem>
                                                <asp:ListItem>Islamic Studies</asp:ListItem>
                                                <asp:ListItem>Literature in English</asp:ListItem>
                                                <asp:ListItem>Metalwork</asp:ListItem>
                                                <asp:ListItem>Music</asp:ListItem>
                                                <asp:ListItem>Physical Education</asp:ListItem>
                                                <asp:ListItem>Physics</asp:ListItem>
                                                <asp:ListItem>Shorthand</asp:ListItem>
                                                <asp:ListItem>Technical Drawing</asp:ListItem>
                                                <asp:ListItem>Typewriting</asp:ListItem>
                                                <asp:ListItem>Visual Arts</asp:ListItem>
                                                <asp:ListItem>Woodwork</asp:ListItem>
                                                <asp:ListItem>Yoruba</asp:ListItem>
                                            </asp:DropDownList>
                                            <br />
                                            <br />
                                            <asp:DropDownList ID="Subj3" CssClass="form-control" runat="server">
                                                <asp:ListItem>Mathematics</asp:ListItem>
                                                <asp:ListItem>English Language</asp:ListItem>
                                                <asp:ListItem>Agricultural Science</asp:ListItem>
                                                <asp:ListItem>Applied Electricity</asp:ListItem>
                                                <asp:ListItem>Auto Mechanics</asp:ListItem>
                                                <asp:ListItem>Biology</asp:ListItem>
                                                <asp:ListItem>Building Construction</asp:ListItem>
                                                <asp:ListItem>Chemistry</asp:ListItem>
                                                <asp:ListItem>Christian Religious Knowledge</asp:ListItem>
                                                <asp:ListItem>Clothing and Textile</asp:ListItem>
                                                <asp:ListItem>Commerce</asp:ListItem>
                                                <asp:ListItem>Economics</asp:ListItem>
                                                <asp:ListItem>Electronics</asp:ListItem>
                                                <asp:ListItem>Financial Accounting</asp:ListItem>
                                                <asp:ListItem>Foods and Nutrition</asp:ListItem>
                                                <asp:ListItem>French</asp:ListItem>
                                                <asp:ListItem>Further Mathematics</asp:ListItem>
                                                <asp:ListItem>Geography</asp:ListItem>
                                                <asp:ListItem>Government</asp:ListItem>
                                                <asp:ListItem>Health Science</asp:ListItem>
                                                <asp:ListItem>History</asp:ListItem>
                                                <asp:ListItem>Home Management</asp:ListItem>
                                                <asp:ListItem>Islamic Studies</asp:ListItem>
                                                <asp:ListItem>Literature in English</asp:ListItem>
                                                <asp:ListItem>Metalwork</asp:ListItem>
                                                <asp:ListItem>Music</asp:ListItem>
                                                <asp:ListItem>Physical Education</asp:ListItem>
                                                <asp:ListItem>Physics</asp:ListItem>
                                                <asp:ListItem>Shorthand</asp:ListItem>
                                                <asp:ListItem>Technical Drawing</asp:ListItem>
                                                <asp:ListItem>Typewriting</asp:ListItem>
                                                <asp:ListItem>Visual Arts</asp:ListItem>
                                                <asp:ListItem>Woodwork</asp:ListItem>
                                                <asp:ListItem>Yoruba</asp:ListItem>
                                            </asp:DropDownList>
                                            <br />
                                            <br />
                                            <asp:DropDownList ID="Subj4" CssClass="form-control" runat="server">
                                                <asp:ListItem>Mathematics</asp:ListItem>
                                                <asp:ListItem>English Language</asp:ListItem>
                                                <asp:ListItem>Agricultural Science</asp:ListItem>
                                                <asp:ListItem>Applied Electricity</asp:ListItem>
                                                <asp:ListItem>Auto Mechanics</asp:ListItem>
                                                <asp:ListItem>Biology</asp:ListItem>
                                                <asp:ListItem>Building Construction</asp:ListItem>
                                                <asp:ListItem>Chemistry</asp:ListItem>
                                                <asp:ListItem>Christian Religious Knowledge</asp:ListItem>
                                                <asp:ListItem>Clothing and Textile</asp:ListItem>
                                                <asp:ListItem>Commerce</asp:ListItem>
                                                <asp:ListItem>Economics</asp:ListItem>
                                                <asp:ListItem>Electronics</asp:ListItem>
                                                <asp:ListItem>Financial Accounting</asp:ListItem>
                                                <asp:ListItem>Foods and Nutrition</asp:ListItem>
                                                <asp:ListItem>French</asp:ListItem>
                                                <asp:ListItem>Further Mathematics</asp:ListItem>
                                                <asp:ListItem>Geography</asp:ListItem>
                                                <asp:ListItem>Government</asp:ListItem>
                                                <asp:ListItem>Health Science</asp:ListItem>
                                                <asp:ListItem>History</asp:ListItem>
                                                <asp:ListItem>Home Management</asp:ListItem>
                                                <asp:ListItem>Islamic Studies</asp:ListItem>
                                                <asp:ListItem>Literature in English</asp:ListItem>
                                                <asp:ListItem>Metalwork</asp:ListItem>
                                                <asp:ListItem>Music</asp:ListItem>
                                                <asp:ListItem>Physical Education</asp:ListItem>
                                                <asp:ListItem>Physics</asp:ListItem>
                                                <asp:ListItem>Shorthand</asp:ListItem>
                                                <asp:ListItem>Technical Drawing</asp:ListItem>
                                                <asp:ListItem>Typewriting</asp:ListItem>
                                                <asp:ListItem>Visual Arts</asp:ListItem>
                                                <asp:ListItem>Woodwork</asp:ListItem>
                                                <asp:ListItem>Yoruba</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-md-6">
                                            <asp:TextBox ID="Scr1" runat="server" CssClass="form-control" placeholder="Score"></asp:TextBox>
                                            <br />
                                            <br />
                                            <asp:TextBox ID="Scr2" runat="server" CssClass="form-control" placeholder="Score"></asp:TextBox>
                                            <br />
                                            <br />
                                            <asp:TextBox ID="Scr3" runat="server" CssClass="form-control" placeholder="Score"></asp:TextBox>
                                            <br />
                                            <br />
                                            <asp:TextBox ID="Scr4" runat="server" CssClass="form-control" placeholder="Score"></asp:TextBox>
                                        </div>
                                        <br />
                                        <br />

                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
                                    <asp:Button ID="sub" runat="server" Text="REGISTER" CssClass="btn btn-info" OnClick="sub_Click" />
                                </div>
                            </div>

                        </div>

                    </div>

                </div>

            </div>

            <div class="row">

                <div class="col-md-12 col-xs-12">

                    <div class="white-box">
                        <div class="panel panel-default">
                            <div class="panel-heading">Results </div>
                            <div class="panel-body">
                                <ul class="nav nav-pills m-b-30 ">
                                    <li class="active"><a href="#navpills-1" data-toggle="tab" aria-expanded="false">UTME DETAILS</a> </li>
                                    <li class=""><a href="#navpills-2" data-toggle="tab" aria-expanded="false">O'Level Details</a> </li>

                                </ul>
                                <div class="tab-content br-n pn">
                                    <div id="navpills-1" class="tab-pane active">
                                        <div class="row">

                                            <div class="col-md-12">
                                                <h2>Jamb Details</h2>

                                                <%var getInfo = jambDetail.Select(a => new { a.RegNo, a.Center, a.Year }).FirstOrDefault(); %>
                                                <table class="table table-bordered table-bordered">

                                                    <thead>
                                                        <tr>
                                                            <th>Registration Number</th>
                                                            <th>Center Number</th>
                                                            <th>Year</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td><%Response.Write(getInfo == null ? "" : getInfo.RegNo); %></td>
                                                            <td><%Response.Write(getInfo == null ? "" : getInfo.Center); %></td>
                                                            <td><%Response.Write(getInfo == null ? "" : getInfo.Year); %></td>
                                                        </tr>

                                                    </tbody>
                                                </table>
                                                <table class="table table-bordered table-bordered">

                                                    <thead>
                                                        <tr>
                                                            <th>Subject</th>
                                                            <th>Score</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <%
                                                            foreach (var item in jambDetail)
                                                            {
                                                        %>
                                                        <tr>
                                                            <td><%Response.Write(item.Subject); %></td>
                                                            <td><%Response.Write(item.Score); %></td>
                                                        </tr>
                                                        <%}
                                                        %>
                                                    </tbody>
                                                </table>

                                            </div>
                                        </div>
                                    </div>
                                    <div id="navpills-2" class="tab-pane">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <h2>O'Level Details</h2>

                                                <%
                                                    foreach (var item in OlevelCenterNo)
                                                    {
                                                        var getOlevel = olevel.Where(a => a.CenterNumber == item).ToList();
                                                        var getDetail = getOlevel.Select(a => new { a.ExamType, a.CenterNumber, a.ExamYear, a.RegNo }).FirstOrDefault();
                                                %>
                                                <table class=" table table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <th>Exam Type</th>

                                                            <th>Exam Year</th>
                                                            <th>Center Number</th>
                                                            <th>Reg Number</th>
                                                        </tr>

                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td><%Response.Write(getDetail == null ? "" : getDetail.ExamType); %></td>
                                                            <td><%Response.Write(getDetail == null ? "" : getDetail.ExamYear); %></td>
                                                            <td><%Response.Write(getDetail == null ? "" : getDetail.CenterNumber); %></td>
                                                            <td><%Response.Write(getDetail == null ? "" : getDetail.RegNo); %></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <table class="table table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <th>Subject Name</th>
                                                            <th>Grade</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <%
                                                            foreach (var setOlevel in getOlevel)
                                                            {
                                                        %>
                                                        <tr>
                                                            <td><%Response.Write(setOlevel.SubjectName); %></td>
                                                            <td><%Response.Write(setOlevel.Grade); %></td>
                                                        </tr>
                                                        <% }
                                                        %>
                                                    </tbody>
                                                </table>
                                                <hr />
                                                <%  }
                                                %>
                                            </div>

                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

