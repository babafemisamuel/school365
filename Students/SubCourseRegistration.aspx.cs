﻿using School365.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace School365.Students
{
    public partial class SubCourseRegistration : System.Web.UI.Page
    {
        int value = 0;
        StudentModel db = new StudentModel();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Submit_Click(object sender, EventArgs e)
        {
            value = int.Parse(User.Identity.Name);
            var student = db.Students.Where(a => a.StudentID == value).FirstOrDefault();
            if (student != null)
            {
                student.SubCourse = Department.SelectedItem.Value;
                db.SaveChanges();
                Response.Redirect("UserRegister.aspx");
            }
        }
    }
}