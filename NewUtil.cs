﻿using School365.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace School365
{
    public class NewUtil
    {       
        public static double DeptTcp(int SessionID, int Level, int Semester, int DepartmentID, int StudentID)
        {
            Util util = new Util();
            StudentModel db = new StudentModel();
            int tcp = 0;
            var getTcp = (from d in db.Results
                          where d.StudentID == StudentID
                            && d.SessionID == SessionID
                            && d.Semester == Semester
                            && d.DepartmentID == DepartmentID
                            && d.Level == Level
                          join s in db.Subjects
                          on d.SubjectID equals s.SubjectID
                          select new { s.SubjectValue, d.SubjectID, d.CA, d.EXAM }).ToList();

            foreach (var item in getTcp)
            {
                tcp += (util.GradeValue(item.CA + item.EXAM));
            }
            return tcp;
        }
        public static double DeptTnup(int SessionID, int Level, int Semester, int DepartmentID, int StudentID)
        {
            StudentModel db = new StudentModel();
            var getTcp = (from d in db.Results
                          where d.StudentID == StudentID
                            && d.SessionID == SessionID
                            && d.Semester == Semester
                            && d.DepartmentID == DepartmentID
                            && d.Level == Level
                            && d.CA + d.EXAM > 39
                          join s in db.Subjects
                          on d.SubjectID equals s.SubjectID
                          select s.SubjectValue).ToList();

            return getTcp.Count == 0 ? 0 : getTcp.Sum();

        }
        public static double DeptTnu(int SessionID, int Level, int Semester, int DepartmentID, int StudentID)
        {
            StudentModel db = new StudentModel();
            var getTnu = (from d in db.Results
                          where d.StudentID == StudentID
                            && d.SessionID == SessionID
                            && d.Semester == Semester
                            && d.DepartmentID == DepartmentID
                            && d.Level == Level
                          join s in db.Subjects
                          on d.SubjectID equals s.SubjectID
                          select s.SubjectValue).ToList();
            return getTnu.Count == 0 ? 0 : getTnu.Sum();
        }
       
        public static string DeptGpa(int SessionID, int Level, int Semester, int DepartmentID, int StudentID)
        {
            return string.Format("{0:0.00}", DeptTcp(SessionID, Level, Semester, DepartmentID, StudentID) / DeptTnu(SessionID, Level, Semester, DepartmentID, StudentID));
        }
        public static double PrevDeptTcp(int SessionID, int Level, int Semester, int DepartmentID, int StudentID)
        {
            Util util = new Util();
            StudentModel db = new StudentModel();
            int tcp = 0;

            if (Level >= 100 && Semester > 1)
            {
                Semester--;
                if (Semester == 0)
                {
                    Level = -100;
                    var getTcp = (from d in db.Results
                                  where d.StudentID == StudentID
                                    && d.SessionID == SessionID
                                    && d.Semester == 2
                                    && d.DepartmentID == DepartmentID
                                    && d.Level == Level
                                  join s in db.Subjects
                                  on d.SubjectID equals s.SubjectID
                                  select new { s.SubjectValue, d.SubjectID, d.CA, d.EXAM }).ToList();

                    foreach (var item in getTcp)
                    {
                        tcp += (util.GradeValue(item.CA + item.EXAM));
                    }
                    return tcp;
                }
                else
                {
                    var getTcp = (from d in db.Results
                                  where d.StudentID == StudentID
                                    && d.SessionID == SessionID
                                    && d.Semester == 1
                                    && d.DepartmentID == DepartmentID
                                    && d.Level == Level
                                  join s in db.Subjects
                                  on d.SubjectID equals s.SubjectID
                                  select new { s.SubjectValue, d.SubjectID, d.CA, d.EXAM }).ToList();

                    foreach (var item in getTcp)
                    {
                        tcp += (util.GradeValue(item.CA + item.EXAM));
                    }
                    return tcp;
                }


            }
            return 0;
        }

        public static double PrevDepTnu(int SessionID, int Level, int Semester, int DepartmentID, int StudentID)
        {
            StudentModel db = new StudentModel();
            if (Level >= 100 && Semester > 1)
            {
                Semester--;
                if (Semester == 0)
                {
                    Level = -100;
                    var getTnu = (from d in db.Results
                                  where d.StudentID == StudentID
                                    && d.SessionID == SessionID
                                    && d.Semester == 2
                                    && d.DepartmentID == DepartmentID
                                    && d.Level == Level
                                  join s in db.Subjects
                                  on d.SubjectID equals s.SubjectID
                                  select s.SubjectValue).ToList();
                    return getTnu.Count == 0 ? 0 : getTnu.Sum();
                }
                else
                {
                    var getTnu = (from d in db.Results
                                  where d.StudentID == StudentID
                                    && d.SessionID == SessionID
                                    && d.Semester == 1
                                    && d.DepartmentID == DepartmentID
                                    && d.Level == Level
                                  join s in db.Subjects
                                  on d.SubjectID equals s.SubjectID
                                  select s.SubjectValue).ToList();
                    return getTnu.Count == 0 ? 0 : getTnu.Sum();
                }
            }
            return 0;

        }

        public static double PrevDepTnup(int SessionID, int Level, int Semester, int DepartmentID, int StudentID)
        {
            StudentModel db = new StudentModel();

            if (Level >= 100 && Semester > 1)
            {
                Semester--;
                if (Semester == 0)
                {
                    Level = -100;
                    var getTcp = (from d in db.Results
                                  where d.StudentID == StudentID
                                    && d.SessionID == SessionID
                                    && d.Semester == 2
                                    && d.DepartmentID == DepartmentID
                                    && d.Level == Level
                                    && d.CA + d.EXAM > 39
                                  join s in db.Subjects
                                  on d.SubjectID equals s.SubjectID
                                  select s.SubjectValue).ToList();

                    return getTcp.Count == 0 ? 0 : getTcp.Sum();
                }
                else
                {
                    var getTcp = (from d in db.Results
                                  where d.StudentID == StudentID
                                    && d.SessionID == SessionID
                                    && d.Semester == 1
                                    && d.DepartmentID == DepartmentID
                                    && d.Level == Level
                                    && d.CA + d.EXAM > 39
                                  join s in db.Subjects
                                  on d.SubjectID equals s.SubjectID
                                  select s.SubjectValue).ToList();

                    return getTcp.Count == 0 ? 0 : getTcp.Sum();
                }
            }
            return 0;
        }

        public static double CurrDeptTcp (int SessionID, int Level, int Semester, int DepartmentID, int StudentID)
        {
            double currTcp= DeptTcp(SessionID,Level, Semester, DepartmentID,StudentID) + PrevDeptTcp(SessionID, Level, Semester, DepartmentID, StudentID);
            return currTcp;
        }
        public static double CurrTnup(int SessionID, int Level, int Semester, int DepartmentID, int StudentID)
        {
            double currTnup = DeptTnup(SessionID, Level, Semester, DepartmentID, StudentID) + PrevDepTnup(SessionID, Level, Semester, DepartmentID, StudentID);
            return currTnup;
        }
        public static string PrevCgpa(int SessionID, int Level, int Semester, int DepartmentID, int StudentID)
        {
            return string.Format("{0:0.00}", PrevDeptTcp(SessionID, Level, Semester, DepartmentID, StudentID) / PrevDepTnu(SessionID, Level, Semester, DepartmentID, StudentID));
        }
        public static double AllStdBySubComb(int Session, int SubComb,double Semester)
        {
            StudentModel db = new StudentModel();
            return db.Results.Where(d => d.SessionID == Session && d.Semester == Semester && d.SubjectCombinID == SubComb).Distinct().Count();
        }
        public static double AllStdByGender(int Session, int SubComb, double Semester,string Gender)
        {
            StudentModel db = new StudentModel();
            List<int> allStudentID = (from d in db.Students where d.Gender.ToUpper() == Gender.ToUpper() select d.StudentID).ToList();

            List<int> allResultStdID = (from d in db.Results
                                        where d.SessionID == Session && d.SubjectCombinID == SubComb && d.Semester == Semester
                                        select d.StudentID).Distinct().ToList();

            var newValue = allStudentID.Intersect(allResultStdID);
            return newValue.Count();
            //return db.Results.Where(d => d.SessionID == Session && d.Semester == Semester && d.SubjectCombinID == SubComb).Distinct().Count();
        }
        public static double AllStudentDeptLevel(int Session, int SubComb, double Semester, double Level)
        {
            StudentModel db = new StudentModel();
            return db.Results.Where(a => a.SessionID == Session && a.SubjectCombinID == SubComb && a.Semester == Semester && a.Level == Level).Select(c => c.StudentID).Distinct().Count();
        }
        public static int MinGrade(int Level,int Semester,int SubjComb)
        {
            StudentModel db = new StudentModel();
            int val = db.MinMaxs.Where(a => a.Level == Level && a.Semester == Semester && a.SubjectCombinID == SubjComb).Select(a=>a.Minimum).FirstOrDefault();
            return val;
        }
        public static int MaxGrade(int Level, int Semester, int SubjComb)
        {
            StudentModel db = new StudentModel();
            int val = db.MinMaxs.Where(a => a.Level == Level && a.Semester == Semester && a.SubjectCombinID == SubjComb).Select(a => a.Maximum).FirstOrDefault();
            return val;
        }

        //for result slip
        public static double DeptCurrTcp(int SessionID, int Level, int DepartmentID, int StudentID)
        {
            Util util = new Util();
            StudentModel db = new StudentModel();
            int tcp = 0;
            var getTcp = (from d in db.Results
                          where d.StudentID == StudentID
                            && d.SessionID == SessionID                          
                            && d.DepartmentID == DepartmentID
                            && d.Level == Level
                          join s in db.Subjects
                          on d.SubjectID equals s.SubjectID
                          select new { s.SubjectValue, d.SubjectID, d.CA, d.EXAM }).ToList();

            foreach (var item in getTcp)
            {
                tcp += util.GradeValue(item.CA + item.EXAM) * (int)item.SubjectValue;
            }
            return tcp;
        }
        public static double DeptCurrTnup(int SessionID, int Level, int DepartmentID, int StudentID)
        {
            StudentModel db = new StudentModel();
            var getTcp = (from d in db.Results
                          where d.StudentID == StudentID
                            && d.SessionID == SessionID
                           
                            && d.DepartmentID == DepartmentID
                            && d.Level == Level
                            && d.CA + d.EXAM > 39
                          join s in db.Subjects
                          on d.SubjectID equals s.SubjectID
                          select s.SubjectValue).ToList();

            return getTcp.Count == 0 ? 0 : getTcp.Sum();

        }
        public static double DeptCurrTnu(int SessionID, int Level, int DepartmentID, int StudentID)
        {
            StudentModel db = new StudentModel();
            var getTnu = (from d in db.Results
                          where d.StudentID == StudentID
                            && d.SessionID == SessionID
                            
                            && d.DepartmentID == DepartmentID
                            && d.Level == Level
                          join s in db.Subjects
                          on d.SubjectID equals s.SubjectID
                          select s.SubjectValue).ToList();
            return getTnu.Count == 0 ? 0 : getTnu.Sum();
        }

        public static string DeptCurrGpa(int SessionID, int Level, int DepartmentID, int StudentID)
        {
            return string.Format("{0:0.00}", DeptCurrTcp(SessionID, Level, DepartmentID, StudentID) / DeptCurrTnu(SessionID, Level, DepartmentID, StudentID));
        }
        //public static double AllStdBySubComb(int Session, int SubComb, int Semester)
        //{
        //    StudentModel db = new StudentModel();
        //    return db.Results.Where(d => d.SessionID == Session && d.Semester == Semester && d.SubjectCombinID == SubComb).Count();
        //}

        public static double PrevTcp(int DepartmentID,int StudentID,int SessionID)
        {
            double tcp = 0;
            StudentModel db = new StudentModel();
            Util util = new Util();
            var results = db.Results.AsQueryable();
            var getResult=results.Where(a => a.StudentID == StudentID && a.SessionID < SessionID && a.DepartmentID == DepartmentID).Select(a => new { a.CA, a.EXAM, a.Subjects.SubjectValue }).ToList();
            getResult.ForEach(a =>
            {
                tcp+=util.GradeValue(a.CA + a.EXAM) * a.SubjectValue;
            });
            return tcp;
        }
        public static double PrevTnu(int DepartmentID, int StudentID, int SessionID)
        {
            double tnu = 0;
            StudentModel db = new StudentModel();
            Util util = new Util();
            var results = db.Results.AsQueryable();
            tnu = results.Where(a => a.StudentID == StudentID && a.SessionID < SessionID && a.DepartmentID == DepartmentID).Select(a => a.Subjects.SubjectValue).Count() == 0 ? 0 : results.Where(a => a.StudentID == StudentID && a.SessionID < SessionID && a.DepartmentID == DepartmentID).Select(a => a.Subjects.SubjectValue).Sum();
            return tnu;
        }
        public static double PrevTnup(int DepartmentID, int StudentID, int SessionID)
        {
            double tnup = 0;
            StudentModel db = new StudentModel();
            Util util = new Util();
            var results = db.Results.AsQueryable();
            tnup = results.Where(a => a.StudentID == StudentID && a.SessionID < SessionID && a.DepartmentID == DepartmentID && a.CA+a.EXAM>39).Select(a => a.Subjects.SubjectValue).Count() == 0 ? 0 : results.Where(a => a.StudentID == StudentID && a.SessionID < SessionID && a.DepartmentID == DepartmentID).Select(a => a.Subjects.SubjectValue).Sum();
            return tnup;
        }

        public static double CurrTcp(int DepartmentID, int StudentID, int SessionID)
        {
            double tcp = 0;
            StudentModel db = new StudentModel();
            Util util = new Util();
            var results = db.Results.AsQueryable();
            var getResult = results.Where(a => a.StudentID == StudentID && a.SessionID ==SessionID && a.DepartmentID == DepartmentID).Select(a => new { a.CA, a.EXAM, a.Subjects.SubjectValue }).ToList();
            getResult.ForEach(a =>
            {
                tcp += util.GradeValue(a.CA + a.EXAM) * a.SubjectValue;
            });
            return tcp;
        }
        public static double CurrTnu(int DepartmentID, int StudentID, int SessionID)
        {
            double tnu = 0;
            StudentModel db = new StudentModel();
            Util util = new Util();
            var results = db.Results.AsQueryable();
            tnu = results.Where(a => a.StudentID == StudentID && a.SessionID ==SessionID && a.DepartmentID == DepartmentID).Select(a => a.Subjects.SubjectValue).Count() == 0 ? 0 : results.Where(a => a.StudentID == StudentID && a.SessionID ==SessionID && a.DepartmentID == DepartmentID).Select(a => a.Subjects.SubjectValue).Sum();
            return tnu;
        }
        public static double CurrTnup(int DepartmentID, int StudentID, int SessionID)
        {
            double tnup = 0;
            StudentModel db = new StudentModel();
            Util util = new Util();
            var results = db.Results.AsQueryable();
            tnup = results.Where(a => a.StudentID == StudentID && a.SessionID==SessionID && a.DepartmentID == DepartmentID && a.CA + a.EXAM > 39).Select(a => a.Subjects.SubjectValue).Count() == 0 ? 0 : results.Where(a => a.StudentID == StudentID && a.SessionID ==SessionID && a.DepartmentID == DepartmentID).Select(a => a.Subjects.SubjectValue).Sum();
            return tnup;
        }
    }
}