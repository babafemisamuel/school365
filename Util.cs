﻿using EntityFramework.Extensions;
using School365.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace School365
{

    public class Util
    {
        StudentModel db = new StudentModel();
        public int GradeValue(double total)
        {


            Grade grade = (db.Grades
                .Where(g => total >= g.Minimum && total <= g.Maximum)
                .Count() > 0) ? db.Grades.First(g => total >= g.Minimum && total <= g.Maximum) : null;
            return (grade != null) ? grade.Value : 0;
        }
        public string GradeName(double total)
        {
            var results = db.Results.AsQueryable();
            Grade grade = (db.Grades
                .Where(g => total >= g.Minimum && total <= g.Maximum)
                .Count() > 0) ? db.Grades.First(g => total >= g.Minimum && total <= g.Maximum) : null;
            return (grade != null) ? grade.Name : null;
        }

        public double PrevDeptTnu(int Level, int StudentID, int Semester, int DepartmentID, int SessionID)
        {
            var results = db.Results.AsQueryable();
            double tcp = 0;
            double tcpII = 0;
            double tcpIII = 0;
            double getTcp = 0;
            double getTcpII = 0;
            double igetTcp = 0;
            //var getDptID = (from d in results where d.StudentID == StudentID select d.DepartmentID).Distinct().ToList();
            //calculate the total of the last level

            if (Semester == 1 && Level == 100)
            {
                return 0;
            }

            else if (Semester == 1 && Level > 100)
            {

                var getSubjs = (from d in results where d.DepartmentID == DepartmentID && d.StudentID == StudentID && d.Level < Level && d.Semester == 1 join f in db.Subjects on d.SubjectID equals f.SubjectID select new { d.CA, d.EXAM, d.SubjectID, f.SubjectValue }).ToList();
                foreach (var setSubjs in getSubjs)
                {
                    double value = GradeValue(setSubjs.CA + setSubjs.EXAM) * setSubjs.SubjectValue;
                    tcp = tcp + value;
                }
                var getSubjsII = (from d in results where d.DepartmentID == DepartmentID && d.StudentID == StudentID && d.Level < Level && d.Semester == 2 join f in db.Subjects on d.SubjectID equals f.SubjectID select new { d.CA, d.EXAM, d.SubjectID, f.SubjectValue }).ToList();
                foreach (var setSubjsII in getSubjsII)
                {
                    double valueII = GradeValue(setSubjsII.CA + setSubjsII.EXAM) * setSubjsII.SubjectValue;
                    tcpII = tcpII + valueII;
                }

                return tcp + tcpII;
                //foreach (var setDeptID in getDptID)
                //{
                //    double getValue = (from d in results where d.DepartmentID == setDeptID && d.StudentID == StudentID && d.Level < Level && d.Semester == 1 select d.TCP).Distinct().Sum();
                //    getTcp = getTcp + getValue;
                //}
                //foreach (var setDeptID in getDptID)
                //{
                //    double getValue = (from d in results where d.DepartmentID == setDeptID && d.StudentID == StudentID && d.Level < Level && d.Semester == 2 select d.TCP).Distinct().Sum();
                //    getTcpII = getTcpII + getValue;
                //}
                //return (getTcp + getTcpII);
            }
            else
            {
                if (Level == 100 && Semester == 2)
                {

                    var getSubjs = (from d in results where d.DepartmentID == DepartmentID && d.StudentID == StudentID && d.Level == Level && d.Semester == 1 join f in db.Subjects on d.SubjectID equals f.SubjectID select new { d.CA, d.EXAM, d.SubjectID, f.SubjectValue }).ToList();
                    foreach (var setSubjs in getSubjs)
                    {
                        double value = GradeValue(setSubjs.CA + setSubjs.EXAM) * setSubjs.SubjectValue;
                        tcp = tcp + value;
                    }
                    /* double getValue = (from d in results where d.DepartmentID == setDeptID && d.StudentID == StudentID && d.Level == Level && d.Semester == 1 select d.TCP).Distinct().Sum();
                     getTcp = getTcp + getValue;*/

                    //return getTcp;
                    return tcp;
                }
                else
                {

                    var getSubjs = (from d in results where d.DepartmentID == DepartmentID && d.StudentID == StudentID && d.Level < Level && d.Semester == 1 join f in db.Subjects on d.SubjectID equals f.SubjectID select new { d.CA, d.EXAM, d.SubjectID, f.SubjectValue }).ToList();
                    foreach (var setSubjs in getSubjs)
                    {
                        double value = GradeValue(setSubjs.CA + setSubjs.EXAM) * setSubjs.SubjectValue;
                        tcp = tcp + value;
                    }
                    var getSubjsII = (from d in results where d.DepartmentID == DepartmentID && d.StudentID == StudentID && d.Level < Level && d.Semester == 2 join f in db.Subjects on d.SubjectID equals f.SubjectID select new { d.CA, d.EXAM, d.SubjectID, f.SubjectValue }).ToList();
                    foreach (var setSubjsII in getSubjsII)
                    {
                        double valueII = GradeValue(setSubjsII.CA + setSubjsII.EXAM) * setSubjsII.SubjectValue;
                        tcpII = tcpII + valueII;
                    }
                    var getSubjsIII = (from d in results where d.DepartmentID == DepartmentID && d.StudentID == StudentID && d.Level == Level && d.Semester == Semester join f in db.Subjects on d.SubjectID equals f.SubjectID select new { d.CA, d.EXAM, d.SubjectID, f.SubjectValue }).ToList();
                    foreach (var setSubjsIII in getSubjsIII)
                    {
                        double valueIII = GradeValue(setSubjsIII.CA + setSubjsIII.EXAM) * setSubjsIII.SubjectValue;
                        tcpIII = tcpIII + valueIII;
                    }
                }

                return tcp + tcpII + tcpIII;
            }
        }
        public double PrevDeptTcp(int Level, int StudentID, int DepartmentID, int Semester)
        {

            var results = db.Results.AsQueryable();
            double tcp = 0;
            double tcpII = 0;
            double tcpIII = 0;
            double getTcp = 0;
            double getTcpII = 0;
            double igetTcp = 0;
            //var getDptID = (from d in results where d.StudentID == StudentID select d.DepartmentID).Distinct().ToList();
            //calculate the total of the last level

            if (Semester == 1 && Level == 100)
            {
                return 0;
            }

            else if (Semester == 1 && Level > 100)
            {

                var getSubjs = (from d in results where d.DepartmentID == DepartmentID && d.StudentID == StudentID && d.Level < Level && d.Semester == 1 join f in db.Subjects on d.SubjectID equals f.SubjectID select new { d.CA, d.EXAM, d.SubjectID, f.SubjectValue }).ToList();
                foreach (var setSubjs in getSubjs)
                {
                    double value = GradeValue(setSubjs.CA + setSubjs.EXAM) * setSubjs.SubjectValue;
                    tcp = tcp + value;
                }
                var getSubjsII = (from d in results where d.DepartmentID == DepartmentID && d.StudentID == StudentID && d.Level < Level && d.Semester == 2 join f in db.Subjects on d.SubjectID equals f.SubjectID select new { d.CA, d.EXAM, d.SubjectID, f.SubjectValue }).ToList();
                foreach (var setSubjsII in getSubjsII)
                {
                    double valueII = GradeValue(setSubjsII.CA + setSubjsII.EXAM) * setSubjsII.SubjectValue;
                    tcpII = tcpII + valueII;
                }

                return tcp + tcpII;
                //foreach (var setDeptID in getDptID)
                //{
                //    double getValue = (from d in results where d.DepartmentID == setDeptID && d.StudentID == StudentID && d.Level < Level && d.Semester == 1 select d.TCP).Distinct().Sum();
                //    getTcp = getTcp + getValue;
                //}
                //foreach (var setDeptID in getDptID)
                //{
                //    double getValue = (from d in results where d.DepartmentID == setDeptID && d.StudentID == StudentID && d.Level < Level && d.Semester == 2 select d.TCP).Distinct().Sum();
                //    getTcpII = getTcpII + getValue;
                //}
                //return (getTcp + getTcpII);
            }
            else
            {
                if (Level == 100 && Semester == 2)
                {

                    var getSubjs = (from d in results where d.DepartmentID == DepartmentID && d.StudentID == StudentID && d.Level == Level && d.Semester == 1 join f in db.Subjects on d.SubjectID equals f.SubjectID select new { d.CA, d.EXAM, d.SubjectID, f.SubjectValue }).ToList();
                    foreach (var setSubjs in getSubjs)
                    {
                        double value = GradeValue(setSubjs.CA + setSubjs.EXAM) * setSubjs.SubjectValue;
                        tcp = tcp + value;
                    }
                    /* double getValue = (from d in results where d.DepartmentID == setDeptID && d.StudentID == StudentID && d.Level == Level && d.Semester == 1 select d.TCP).Distinct().Sum();
                     getTcp = getTcp + getValue;*/

                    //return getTcp;
                    return tcp;
                }
                else
                {

                    var getSubjs = (from d in results where d.DepartmentID == DepartmentID && d.StudentID == StudentID && d.Level < Level && d.Semester == 1 join f in db.Subjects on d.SubjectID equals f.SubjectID select new { d.CA, d.EXAM, d.SubjectID, f.SubjectValue }).ToList();
                    foreach (var setSubjs in getSubjs)
                    {
                        double value = GradeValue(setSubjs.CA + setSubjs.EXAM) * setSubjs.SubjectValue;
                        tcp = tcp + value;
                    }
                    var getSubjsII = (from d in results where d.DepartmentID == DepartmentID && d.StudentID == StudentID && d.Level < Level && d.Semester == 2 join f in db.Subjects on d.SubjectID equals f.SubjectID select new { d.CA, d.EXAM, d.SubjectID, f.SubjectValue }).ToList();
                    foreach (var setSubjsII in getSubjsII)
                    {
                        double valueII = GradeValue(setSubjsII.CA + setSubjsII.EXAM) * setSubjsII.SubjectValue;
                        tcpII = tcpII + valueII;
                    }
                    var getSubjsIII = (from d in results where d.DepartmentID == DepartmentID && d.StudentID == StudentID && d.Level == Level && d.Semester == Semester join f in db.Subjects on d.SubjectID equals f.SubjectID select new { d.CA, d.EXAM, d.SubjectID, f.SubjectValue }).ToList();
                    foreach (var setSubjsIII in getSubjsIII)
                    {
                        double valueIII = GradeValue(setSubjsIII.CA + setSubjsIII.EXAM) * setSubjsIII.SubjectValue;
                        tcpIII = tcpIII + valueIII;
                    }


                    return tcp + tcpII + tcpIII;
                }
            }
        }
        public double PrevDeptTnup(int Level, int StudentID, int DepartmentID, int Semester)
        {
            var results = db.Results.AsQueryable();
            double tnup = 0;
            double tnupII = 0;
            double tnupIII = 0;
            //var getDptID = (from d in results where d.StudentID == StudentID select d.DepartmentID).Distinct().ToList();
            if (Semester == 1 && Level == 100)
            {
                return 0.00;
            }
            if (Level == 100 && Semester == 2)
            {

                {
                    //get current year semester
                    var getSubjs = (from d in results where d.DepartmentID == DepartmentID && d.StudentID == StudentID && d.Level == Level && d.Semester == 1 join f in db.Subjects on d.SubjectID equals f.SubjectID select new { d.CA, d.EXAM, d.SubjectID, f.SubjectValue }).ToList();
                    foreach (var setSubjs in getSubjs)
                    {
                        double value = setSubjs.SubjectValue;
                        if ((setSubjs.CA + setSubjs.EXAM) >= 40)
                        {
                            tnup = tnup + value;
                        }
                        else
                        {
                            tnup = tnup + 0;
                        }
                    }

                }
                return tnup;
            }

            //calculate the total of the last level
            if (Semester == 1 && Level > 100)
            {

                var getSubjs = (from d in results where d.DepartmentID == DepartmentID && d.StudentID == StudentID && d.Level < Level && d.Semester == 1 join f in db.Subjects on d.SubjectID equals f.SubjectID select new { d.CA, d.EXAM, d.SubjectID, f.SubjectValue }).ToList();
                foreach (var setSubjs in getSubjs)
                {
                    double value = setSubjs.SubjectValue;
                    if ((setSubjs.CA + setSubjs.EXAM) >= 40)
                    {
                        tnup = tnup + value;
                    }
                    else
                    {
                        tnup = tnup + 0;
                    }
                }
                var getSubjsII = (from d in results where d.DepartmentID == DepartmentID && d.StudentID == StudentID && d.Level < Level && d.Semester == 2 join f in db.Subjects on d.SubjectID equals f.SubjectID select new { d.CA, d.EXAM, d.SubjectID, f.SubjectValue }).ToList();
                foreach (var setSubjsII in getSubjsII)
                {
                    double value = setSubjsII.SubjectValue;
                    if ((setSubjsII.CA + setSubjsII.EXAM) >= 40)
                    {
                        tnupII = tnupII + value;
                    }
                    else
                    {
                        tnupII = tnupII + 0;
                    }
                }

                return tnup + tnupII;
            }

            if (Semester > 1 && Level > 100)
            {
                //get previous total years
                var getSubjs = (from d in results where d.DepartmentID == DepartmentID && d.StudentID == StudentID && d.Level < Level && d.Semester == 1 join f in db.Subjects on d.SubjectID equals f.SubjectID select new { d.CA, d.EXAM, d.SubjectID, f.SubjectValue }).ToList();
                foreach (var setSubjs in getSubjs)
                {
                    double value = setSubjs.SubjectValue;
                    if ((setSubjs.CA + setSubjs.EXAM) >= 40)
                    {
                        tnup = tnup + value;
                    }
                    else
                    {
                        tnup = tnup + 0;
                    }


                    var getSubjsII = (from d in results where d.DepartmentID == DepartmentID && d.StudentID == StudentID && d.Level < Level && d.Semester == 2 join f in db.Subjects on d.SubjectID equals f.SubjectID select new { d.CA, d.EXAM, d.SubjectID, f.SubjectValue }).ToList();
                    foreach (var setSubjsII in getSubjsII)
                    {
                        double _value = setSubjsII.SubjectValue;
                        if ((setSubjsII.CA + setSubjsII.EXAM) >= 40)
                        {
                            tnupII = tnupII + _value;
                        }
                        else
                        {
                            tnupII = tnupII + 0;
                        }
                    }
                    var getSubjsIII = (from d in results where d.DepartmentID == DepartmentID && d.StudentID == StudentID && d.Level == Level && d.Semester == Semester join f in db.Subjects on d.SubjectID equals f.SubjectID select new { d.CA, d.EXAM, d.SubjectID, f.SubjectValue }).ToList();
                    foreach (var setSubjsIII in getSubjsIII)
                    {
                        double __value = setSubjsIII.SubjectValue;
                        if ((setSubjsIII.CA + setSubjsIII.EXAM) >= 40)
                        {
                            tnupIII = tnupIII + __value;
                        }
                        else
                        {
                            tnupIII = tnupIII + 0;
                        }
                    }
                }
                return tnup + tnupII + tnupIII;
            }
            else
            {
                return 0;
            }
        }
        public double CummDeptTnu(int Level, int StudentID, int Semester, int DepartmentID, int SessionID)
        {
            var results = db.Results.AsQueryable();

            double total = 0;
            if (Level == 100 && Semester == 1)
            {
                var tnu = results.Where(a => a.Semester == Semester && a.DepartmentID == DepartmentID && a.Level == Level && a.StudentID == StudentID && a.SessionID == SessionID).Select(a => a.Subjects.SubjectValue).ToList();
                total = tnu.Count == 0 ? 0 : tnu.Sum();
                return total;
            }
            if (Semester == 2 && Level == 100)
            {
                var tnu = results.Where(a => a.Semester <= Semester && a.DepartmentID == DepartmentID && a.Level == Level && a.StudentID == StudentID && a.SessionID == SessionID).Select(a => a.Subjects.SubjectValue).ToList();
                total = tnu.Count == 0 ? 0 : tnu.Sum();
                return total;
            }
            if (Level > 100 && Semester == 1)
            {
                var _tnu = results.Where(a => a.Semester == Semester && a.DepartmentID == DepartmentID && a.Level == Level && a.StudentID == StudentID && a.SessionID == SessionID).Select(a => a.Subjects.SubjectValue).ToList();
                var tnu = results.Where(a => a.DepartmentID == DepartmentID && a.Level < Level && a.StudentID == StudentID).Select(a => a.Subjects.SubjectValue).ToList();
                double _init = _tnu.Count == 0 ? 0 : _tnu.Sum();
                double init = tnu.Count == 0 ? 0 : tnu.Sum();
                total = init + _init;
                return total;
            }
            if (Level > 100 && Semester > 1)
            {
                var tnu = results.Where(a => a.DepartmentID == DepartmentID && a.Level <= Level && a.StudentID == StudentID).Select(a => a.Subjects.SubjectValue).ToList();
                total = tnu.Count == 0 ? 0 : tnu.Sum();
                return total;
            }
            return total;
            
        }

        //updated with session
        public double _CummDeptTnup(int Level, int StudentID, int Semester, int DepartmentID, int SessionID)
        {
            var results = db.Results.AsQueryable();

            double total = 0;
            if (Level == 100 && Semester == 1)
            {
                var tnup = results.Where(a => a.Semester == Semester && a.DepartmentID == DepartmentID && a.Level == Level && a.StudentID == StudentID && a.SessionID == SessionID && (a.EXAM + a.CA) > 39).Select(a => a.Subjects.SubjectValue).ToList();
                total = tnup.Count == 0 ? 0 : tnup.Sum();
                return total;
            }
            if (Semester == 2 && Level == 100)
            {
                var tnup = results.Where(a => a.Semester <= Semester && a.DepartmentID == DepartmentID && a.Level == Level && a.StudentID == StudentID && a.SessionID == SessionID && (a.EXAM + a.CA) > 39).Select(a => a.Subjects.SubjectValue).ToList();
                total = tnup.Count == 0 ? 0 : tnup.Sum();
                return total;
            }
            if (Level > 100 && Semester == 1)
            {
                var _tnup = results.Where(a => a.Semester == Semester && a.DepartmentID == DepartmentID && a.Level == Level && a.StudentID == StudentID && a.SessionID == SessionID && (a.EXAM + a.CA) > 39).Select(a => a.Subjects.SubjectValue).ToList();
                var tnup = results.Where(a => a.DepartmentID == DepartmentID && a.Level < Level && a.StudentID == StudentID && (a.EXAM + a.CA) > 39).Select(a => a.Subjects.SubjectValue).ToList();
                double _init = tnup.Count == 0 ? 0 : tnup.Sum();
                double init = _tnup.Count == 0 ? 0 : _tnup.Sum();
                total = init + _init;
                return total;
            }
            if (Level > 100 && Semester > 1)
            {
                var tnup = results.Where(a => a.DepartmentID == DepartmentID && a.Level <= Level && a.StudentID == StudentID && (a.EXAM + a.CA) > 39).Select(a => a.Subjects.SubjectValue).ToList();
                total = tnup.Count == 0 ? 0 : tnup.Sum();
                return total;
            }

            return total;
            //var getTnup = db.Results
            //                      .Where(a => a.Semester == Semester && a.DepartmentID == DepartmentID && a.Level == Level && a.StudentID == StudentID && a.SessionID == SessionID && (a.EXAM + a.CA) > 39).Select(a => a.Subjects.SubjectValue).ToList();
            //return getTnup.Count == 0 ? 0 : getTnup.Sum();
            // var results = db.Results.AsQueryable();
            //double tnup = 0;
            //double tnupII = 0;
            //if (Semester == 2)
            //{
            //    var getSubjs = (from d in results where d.DepartmentID == DepartmentID && d.StudentID == StudentID && d.Level <= Level join f in db.Subjects on d.SubjectID equals f.SubjectID select new { d.CA, d.EXAM, d.SubjectID, f.SubjectValue }).ToList();
            //    foreach (var setSubjs in getSubjs)
            //    {
            //        double value = setSubjs.SubjectValue;
            //        if ((setSubjs.CA + setSubjs.EXAM) >= 40)
            //        {
            //            tnup = tnup + value;
            //        }
            //        else
            //        {
            //            tnup = tnup + 0;
            //        }
            //    }
            //    return tnup;
            //}
            //else
            //{
            //    if (Semester == 1 && Level == 100)
            //    {
            //        var getSubjs = (from d in results where d.DepartmentID == DepartmentID && d.SessionID == SessionID && d.StudentID == StudentID && d.Level == Level join f in db.Subjects on d.SubjectID equals f.SubjectID select new { d.CA, d.EXAM, d.SubjectID, f.SubjectValue }).ToList();
            //        foreach (var setSubjs in getSubjs)
            //        {
            //            double value = setSubjs.SubjectValue;
            //            if ((setSubjs.CA + setSubjs.EXAM) >= 40)
            //            {
            //                tnup = tnup + value;
            //            }
            //            else
            //            {
            //                tnup = tnup + 0;
            //            }
            //        }
            //        return tnup;
            //    }
            //    else
            //    {
            //        var getSubjs = (from d in results where d.DepartmentID == DepartmentID && d.SessionID == SessionID && d.StudentID == StudentID && d.Level < Level join f in db.Subjects on d.SubjectID equals f.SubjectID select new { d.CA, d.EXAM, d.SubjectID, f.SubjectValue }).ToList();

            //        foreach (var setSubjs in getSubjs)
            //        {
            //            double value = setSubjs.SubjectValue;
            //            if ((setSubjs.CA + setSubjs.EXAM) >= 40)
            //            {
            //                tnup = tnup + value;
            //            }
            //            else
            //            {
            //                tnup = tnup + 0;
            //            }

            //            var getSubjsII = (from d in results where d.DepartmentID == DepartmentID && d.SessionID == SessionID && d.StudentID == StudentID && d.Level == Level && d.Semester == Semester join f in db.Subjects on d.SubjectID equals f.SubjectID select new { d.CA, d.EXAM, d.SubjectID, f.SubjectValue }).ToList();
            //            foreach (var setSubjsII in getSubjsII)
            //            {
            //                if ((setSubjsII.CA + setSubjsII.EXAM) >= 40)
            //                {
            //                    tnupII = tnupII + value;
            //                }
            //                else
            //                {
            //                    tnupII = tnupII + 0;
            //                }
            //            }
            //        }
            //        return tnup + tnupII;
            //    }
            //}
        }

        public double CummDeptTcp(int Level, int StudentID, int Semester, int DepartmentID)
        {
            var results = db.Results.AsQueryable();
            double tcp = 0;
            double tcpII = 0;
            if (Semester == 2)
            {
                var getSubjs = (from d in results where d.DepartmentID == DepartmentID && d.StudentID == StudentID && d.Level <= Level join f in db.Subjects on d.SubjectID equals f.SubjectID select new { d.CA, d.EXAM, d.SubjectID, f.SubjectValue }).ToList();
                foreach (var setSubjs in getSubjs)
                {
                    double value = GradeValue(setSubjs.CA + setSubjs.EXAM) * setSubjs.SubjectValue;
                    tcp = tcp + value;
                }
                return tcp;
            }
            else
            {
                var getSubjs = (from d in results where d.DepartmentID == DepartmentID && d.StudentID == StudentID && d.Level < Level join f in db.Subjects on d.SubjectID equals f.SubjectID select new { d.CA, d.EXAM, d.SubjectID, f.SubjectValue }).ToList();
                foreach (var setSubjs in getSubjs)
                {
                    double value = GradeValue(setSubjs.CA + setSubjs.EXAM) * setSubjs.SubjectValue;
                    tcp = tcp + value;
                }
                var getSubjsII = (from d in results where d.DepartmentID == DepartmentID && d.StudentID == StudentID && d.Level == Level && d.Semester == Semester join f in db.Subjects on d.SubjectID equals f.SubjectID select new { d.CA, d.EXAM, d.SubjectID, f.SubjectValue }).ToList();

                foreach (var setSubjs in getSubjsII)
                {
                    double value = GradeValue(setSubjs.CA + setSubjs.EXAM) * setSubjs.SubjectValue;
                    tcpII = tcpII + value;
                }
                return tcp + tcpII;
            }
        }

        public double CummDeptTnup(int Level, int StudentID, int Semester, int DepartmentID)
        {
            var results = db.Results.AsQueryable();
            double tnup = 0;
            double tnupII = 0;
            if (Semester == 2)
            {
                var getSubjs = (from d in results where d.DepartmentID == DepartmentID && d.StudentID == StudentID && d.Level <= Level join f in db.Subjects on d.SubjectID equals f.SubjectID select new { d.CA, d.EXAM, d.SubjectID, f.SubjectValue }).ToList();
                foreach (var setSubjs in getSubjs)
                {
                    double value = setSubjs.SubjectValue;
                    if ((setSubjs.CA + setSubjs.EXAM) >= 40)
                    {
                        tnup = tnup + value;
                    }
                    else
                    {
                        tnup = tnup + 0;
                    }
                }
                return tnup;
            }
            else
            {
                if (Semester == 1 && Level == 100)
                {
                    var getSubjs = (from d in results where d.DepartmentID == DepartmentID && d.StudentID == StudentID && d.Level == Level join f in db.Subjects on d.SubjectID equals f.SubjectID select new { d.CA, d.EXAM, d.SubjectID, f.SubjectValue }).ToList();
                    foreach (var setSubjs in getSubjs)
                    {
                        double value = setSubjs.SubjectValue;
                        if ((setSubjs.CA + setSubjs.EXAM) >= 40)
                        {
                            tnup = tnup + value;
                        }
                        else
                        {
                            tnup = tnup + 0;
                        }
                    }
                    return tnup;
                }
                else
                {
                    var getSubjs = (from d in results where d.DepartmentID == DepartmentID && d.StudentID == StudentID && d.Level < Level join f in db.Subjects on d.SubjectID equals f.SubjectID select new { d.CA, d.EXAM, d.SubjectID, f.SubjectValue }).ToList();

                    foreach (var setSubjs in getSubjs)
                    {
                        double value = setSubjs.SubjectValue;
                        if ((setSubjs.CA + setSubjs.EXAM) >= 40)
                        {
                            tnup = tnup + value;
                        }
                        else
                        {
                            tnup = tnup + 0;
                        }

                        var getSubjsII = (from d in results where d.DepartmentID == DepartmentID && d.StudentID == StudentID && d.Level == Level && d.Semester == Semester join f in db.Subjects on d.SubjectID equals f.SubjectID select new { d.CA, d.EXAM, d.SubjectID, f.SubjectValue }).ToList();
                        foreach (var setSubjsII in getSubjsII)
                        {
                            if ((setSubjsII.CA + setSubjsII.EXAM) >= 40)
                            {
                                tnupII = tnupII + value;
                            }
                            else
                            {
                                tnupII = tnupII + 0;
                            }
                        }
                    }
                    return tnup + tnupII;
                }
            }
        }


        public double AllTnu(int Level, int StudentID, int Semester, int SessionID)
        {
            var results = db.Results.AsQueryable();
            double getTnu = 0;
            double getTnuII = 0;
            double igetTnu = 0;
            if (Semester == 2 && Level > 100)
            {
                var getDptID = (from d in results where d.StudentID == StudentID select d.DepartmentID).Distinct().ToList();
                var getValue = (from d in results where  d.StudentID == StudentID && d.Level <= Level && d.SessionID <= SessionID join f in db.Subjects on d.SubjectID equals f.SubjectID select f.SubjectValue).Sum();
                 getTnu = getValue + getTnu;
                //foreach (var setDeptID in getDptID)
                //{
                //    var getValue = (from d in results where d.DepartmentID == setDeptID && d.StudentID == StudentID && d.Level <= Level && d.SessionID == SessionID join f in db.Subjects on d.SubjectID equals f.SubjectID select f.SubjectValue).Sum();
                //    getTnu = getValue + getTnu;
                //}
                return getTnu;
            }
            else if (Semester == 2 && Level == 100)
            {
                var getDptID = (from d in results where d.StudentID == StudentID && Level == 100 && d.SessionID == SessionID select d.DepartmentID).Distinct().ToList();
                foreach (var setDeptID in getDptID)
                {
                    var getValue = (from d in results where d.DepartmentID == setDeptID && Level == 100 && d.StudentID == StudentID && d.SessionID == SessionID join f in db.Subjects on d.SubjectID equals f.SubjectID select f.SubjectValue).Sum();
                    //var getValue = (from d in results where d.DepartmentID == setDeptID && Level == 100 && d.StudentID == StudentID select d.TNU).Distinct().Sum(); 
                    getTnu = getValue + getTnu;
                }
                return getTnu;
            }

            else
            {
                if (Semester == 1 && Level == 100)
                {
                    var getDptID = (from d in results where d.StudentID == StudentID select d.DepartmentID).Distinct().ToList();
                    //get previous total years

                    //get current year semester
                    foreach (var setDeptID in getDptID)
                    {
                        var getValued = (from d in results where d.DepartmentID == setDeptID && d.StudentID == StudentID && d.Semester == Semester && d.Level == Level join f in db.Subjects on d.SubjectID equals f.SubjectID select f.SubjectValue).ToList();
                        double getValue = getValued == null ? 0 : getValued.Sum();
                        igetTnu = igetTnu + getValue;
                    }
                    return igetTnu;
                }
                else
                {
                    var getDptID = (from d in results where d.StudentID == StudentID select d.DepartmentID).Distinct().ToList();
                    //get previous total years
                    foreach (var setDeptID in getDptID)
                    {
                        var getValue = (from d in results where d.DepartmentID == setDeptID && d.StudentID == StudentID && d.Level < Level && d.Semester == 1 join f in db.Subjects on d.SubjectID equals f.SubjectID select f.SubjectValue).ToList();
                        double _getValue = getValue == null ? 0 : getValue.Sum();
                        getTnu = getTnu + _getValue;
                    }
                    foreach (var setDeptID in getDptID)
                    {
                        var getValue = (from d in results where d.DepartmentID == setDeptID && d.StudentID == StudentID && d.Level < Level && d.Semester == 2 join f in db.Subjects on d.SubjectID equals f.SubjectID select f.SubjectValue).ToList();
                        double _getValue = getValue == null ? 0 : getValue.Sum();
                        getTnuII = getTnuII + _getValue;
                    }

                    //get current year semester
                    foreach (var setDeptID in getDptID)
                    {
                        var getValue = (from d in results where d.DepartmentID == setDeptID && d.StudentID == StudentID && d.Semester == Semester && d.Level == Level join f in db.Subjects on d.SubjectID equals f.SubjectID select f.SubjectValue).ToList();
                        double _getValue = getValue == null ? 0 : getValue.Sum();
                        igetTnu = igetTnu + _getValue;
                    }
                    return getTnu + igetTnu + getTnuII;
                }
            }
        }
        //2
        public double AllTcp(int Level, int StudentID, int Semester)
        {
            var results = db.Results.AsQueryable();

            double tcp = 0;
            double tcpII = 0;
            double tcpIII = 0;
            if (Semester == 2)
            {

                var getDptID = (from d in results where d.StudentID == StudentID select d.DepartmentID).Distinct().ToList();
                foreach (var setDeptID in getDptID)
                {
                    var getSubjs = (from d in results where d.DepartmentID == setDeptID && d.StudentID == StudentID && d.Level <= Level && d.Semester == 1 join f in db.Subjects on d.SubjectID equals f.SubjectID select new { d.CA, d.EXAM, d.SubjectID, f.SubjectValue }).ToList();
                    foreach (var setSubjs in getSubjs)
                    {
                        double value = GradeValue(setSubjs.CA + setSubjs.EXAM) * setSubjs.SubjectValue;
                        tcp = tcp + value;
                    }
                    var getSubjsII = (from d in results where d.DepartmentID == setDeptID && d.StudentID == StudentID && d.Level <= Level && d.Semester == 2 join f in db.Subjects on d.SubjectID equals f.SubjectID select new { d.CA, d.EXAM, d.SubjectID, f.SubjectValue }).ToList();
                    foreach (var setSubjsII in getSubjsII)
                    {
                        double valueII = GradeValue(setSubjsII.CA + setSubjsII.EXAM) * setSubjsII.SubjectValue;
                        tcpII = tcpII + valueII;
                    }
                }
                return tcp + tcpII;
            }

            else
            {
                if (Semester == 1 && Level == 100)
                {
                    var getDptID = (from d in results where d.StudentID == StudentID select d.DepartmentID).Distinct().ToList();
                    //get current year semester
                    foreach (var setDeptID in getDptID)
                    {
                        var getSubjs = (from d in results where d.DepartmentID == setDeptID && d.StudentID == StudentID && d.Level == Level && d.Semester == Semester join f in db.Subjects on d.SubjectID equals f.SubjectID select new { d.CA, d.EXAM, d.SubjectID, f.SubjectValue }).ToList();
                        foreach (var setSubjs in getSubjs)
                        {
                            double value = GradeValue(setSubjs.CA + setSubjs.EXAM) * setSubjs.SubjectValue;
                            tcp = tcp + value;
                        }
                        /*   double getValue = (from d in results where d.DepartmentID == setDeptID && d.StudentID == StudentID && d.Semester == Semester && d.Level == Level select d.TCP).Distinct().Sum();
                           igetTcp = igetTcp + getValue;*/
                    }
                    return tcp;
                    //return igetTcp;
                }
                else
                {
                    var getDptID = (from d in results where d.StudentID == StudentID select d.DepartmentID).Distinct().ToList();
                    //get previous total years

                    foreach (var setDeptID in getDptID)
                    {
                        var getSubjs = (from d in results where d.DepartmentID == setDeptID && d.StudentID == StudentID && d.Level < Level && d.Semester == 1 join f in db.Subjects on d.SubjectID equals f.SubjectID select new { d.CA, d.EXAM, d.SubjectID, f.SubjectValue }).ToList();
                        foreach (var setSubjs in getSubjs)
                        {
                            double value = GradeValue(setSubjs.CA + setSubjs.EXAM) * setSubjs.SubjectValue;
                            tcp = tcp + value;
                        }
                        var getSubjsII = (from d in results where d.DepartmentID == setDeptID && d.StudentID == StudentID && d.Level < Level && d.Semester == 2 join f in db.Subjects on d.SubjectID equals f.SubjectID select new { d.CA, d.EXAM, d.SubjectID, f.SubjectValue }).ToList();
                        foreach (var setSubjsII in getSubjsII)
                        {
                            double valueII = GradeValue(setSubjsII.CA + setSubjsII.EXAM) * setSubjsII.SubjectValue;
                            tcpII = tcpII + valueII;
                        }

                        var getSubjsIII = (from d in results where d.DepartmentID == setDeptID && d.StudentID == StudentID && d.Level == Level && d.Semester == Semester join f in db.Subjects on d.SubjectID equals f.SubjectID select new { d.CA, d.EXAM, d.SubjectID, f.SubjectValue }).ToList();
                        foreach (var setSubjsIII in getSubjsIII)
                        {
                            double valueIII = GradeValue(setSubjsIII.CA + setSubjsIII.EXAM) * setSubjsIII.SubjectValue;
                            tcpIII = tcpIII + valueIII;
                        }
                    }
                    return tcp + tcpII + tcpIII;
                }
            }
        }

        public double AllTnup(int Level, int StudentID, int Semester)
        {
            var results = db.Results.AsQueryable();
            double tnup = 0;
            double tnupII = 0;
            double tnupIII = 0;

            if (Semester == 2)
            {
                var getDptID = (from d in results where d.StudentID == StudentID select d.DepartmentID).Distinct().ToList();
                foreach (var setDptID in getDptID)
                {
                    var getSubjs = (from d in results where d.DepartmentID == setDptID && d.StudentID == StudentID && d.Level <= Level join f in db.Subjects on d.SubjectID equals f.SubjectID select new { d.CA, d.EXAM, d.SubjectID, f.SubjectValue }).ToList();
                    foreach (var setSubjs in getSubjs)
                    {
                        double value = setSubjs.SubjectValue;
                        if ((setSubjs.CA + setSubjs.EXAM) >= 40)
                        {
                            tnup = tnup + value;
                        }
                        else
                        {
                            tnup = tnup + 0;
                        }
                    }
                }
                return tnup;
            }
            else
            {
                if (Semester == 1 && Level == 100)
                {
                    var getDptID = (from d in results where d.StudentID == StudentID select d.DepartmentID).Distinct().ToList();
                    foreach (var setDptID in getDptID)
                    {
                        //get current year semester
                        var getSubjs = (from d in results where d.DepartmentID == setDptID && d.StudentID == StudentID && d.Level == Level && d.Semester == 1 join f in db.Subjects on d.SubjectID equals f.SubjectID select new { d.CA, d.EXAM, d.SubjectID, f.SubjectValue }).ToList();
                        foreach (var setSubjs in getSubjs)
                        {
                            double value = setSubjs.SubjectValue;
                            if ((setSubjs.CA + setSubjs.EXAM) >= 40)
                            {
                                tnup = tnup + value;
                            }
                            else
                            {
                                tnup = tnup + 0;
                            }
                        }
                    }
                    return tnup;
                }
                else
                {
                    var getDptID = (from d in results where d.StudentID == StudentID select d.DepartmentID).Distinct().ToList();
                    foreach (var setDptID in getDptID)
                    {
                        var getSubjs = (from d in results where d.DepartmentID == setDptID && d.StudentID == StudentID && d.Level < Level join f in db.Subjects on d.SubjectID equals f.SubjectID select new { d.CA, d.EXAM, d.SubjectID, f.SubjectValue }).ToList();
                        foreach (var setSubjs in getSubjs)
                        {
                            double value = setSubjs.SubjectValue;
                            if ((setSubjs.CA + setSubjs.EXAM) >= 40)
                            {
                                tnup = tnup + value;
                            }
                            else
                            {
                                tnup = tnup + 0;
                            }
                        }

                        //check ooo

                        //var getSubjsII = (from d in results where d.DepartmentID == setDptID && d.StudentID == StudentID && d.Level < Level && d.Semester == 2 join f in db.Subjects on d.SubjectID equals f.SubjectID select new { d.CA, d.EXAM, d.SubjectID, f.SubjectValue }).ToList();
                        //foreach (var setSubjs in getSubjs)
                        //{
                        //    double value = setSubjs.SubjectValue;
                        //    if ((setSubjs.CA + setSubjs.EXAM) >= 40)
                        //    {
                        //        tnupII = tnupII + value;
                        //    }
                        //    else
                        //    {
                        //        tnupII = tnupII + 0;
                        //    }
                        //}
                        var getSubjsIII = (from d in results where d.DepartmentID == setDptID && d.StudentID == StudentID && d.Level == Level && d.Semester == Semester join f in db.Subjects on d.SubjectID equals f.SubjectID select new { d.CA, d.EXAM, d.SubjectID, f.SubjectValue }).ToList();
                        foreach (var setSubjsIII in getSubjsIII)
                        {
                            double value = setSubjsIII.SubjectValue;
                            if ((setSubjsIII.CA + setSubjsIII.EXAM) >= 40)
                            {
                                tnupIII = tnupIII + value;
                            }
                            else
                            {
                                tnupIII = tnupIII + 0;
                            }
                        }
                    }
                    return tnup + tnupIII;
                }
            }
        }
        //3
        public double PrevCummTcp(int Level, int StudentID, int Semester)
        {
            var results = db.Results.AsQueryable();
            double tcp = 0;
            double tcpII = 0;
            double tcpIII = 0;
            double getTcp = 0;
            double getTcpII = 0;
            double igetTcp = 0;
            var getDptID = (from d in results where d.StudentID == StudentID select d.DepartmentID).Distinct().ToList();
            //calculate the total of the last level

            if (Semester == 1 && Level == 100)
            {
                return 0;
            }

            else if (Semester == 1 && Level > 100)
            {
                foreach (var setDeptID in getDptID)
                {
                    var getSubjs = (from d in results where d.DepartmentID == setDeptID && d.StudentID == StudentID && d.Level < Level && d.Semester == 1 join f in db.Subjects on d.SubjectID equals f.SubjectID select new { d.CA, d.EXAM, d.SubjectID, f.SubjectValue }).ToList();
                    foreach (var setSubjs in getSubjs)
                    {
                        double value = GradeValue(setSubjs.CA + setSubjs.EXAM) * setSubjs.SubjectValue;
                        tcp = tcp + value;
                    }
                    var getSubjsII = (from d in results where d.DepartmentID == setDeptID && d.StudentID == StudentID && d.Level < Level && d.Semester == 2 join f in db.Subjects on d.SubjectID equals f.SubjectID select new { d.CA, d.EXAM, d.SubjectID, f.SubjectValue }).ToList();
                    foreach (var setSubjsII in getSubjsII)
                    {
                        double valueII = GradeValue(setSubjsII.CA + setSubjsII.EXAM) * setSubjsII.SubjectValue;
                        tcpII = tcpII + valueII;
                    }
                }
                return tcp + tcpII;
                //foreach (var setDeptID in getDptID)
                //{
                //    double getValue = (from d in results where d.DepartmentID == setDeptID && d.StudentID == StudentID && d.Level < Level && d.Semester == 1 select d.TCP).Distinct().Sum();
                //    getTcp = getTcp + getValue;
                //}
                //foreach (var setDeptID in getDptID)
                //{
                //    double getValue = (from d in results where d.DepartmentID == setDeptID && d.StudentID == StudentID && d.Level < Level && d.Semester == 2 select d.TCP).Distinct().Sum();
                //    getTcpII = getTcpII + getValue;
                //}
                //return (getTcp + getTcpII);
            }
            else
            {
                if (Level == 100 && Semester == 2)
                {
                    foreach (var setDeptID in getDptID)
                    {
                        var getSubjs = (from d in results where d.DepartmentID == setDeptID && d.StudentID == StudentID && d.Level == Level && d.Semester == 1 join f in db.Subjects on d.SubjectID equals f.SubjectID select new { d.CA, d.EXAM, d.SubjectID, f.SubjectValue }).ToList();
                        foreach (var setSubjs in getSubjs)
                        {
                            double value = GradeValue(setSubjs.CA + setSubjs.EXAM) * setSubjs.SubjectValue;
                            tcp = tcp + value;
                        }
                        /* double getValue = (from d in results where d.DepartmentID == setDeptID && d.StudentID == StudentID && d.Level == Level && d.Semester == 1 select d.TCP).Distinct().Sum();
                         getTcp = getTcp + getValue;*/
                    }
                    //return getTcp;
                    return tcp;
                }
                else
                {
                    foreach (var setDeptID in getDptID)
                    {
                        var getSubjs = (from d in results where d.DepartmentID == setDeptID && d.StudentID == StudentID && d.Level < Level && d.Semester == 1 join f in db.Subjects on d.SubjectID equals f.SubjectID select new { d.CA, d.EXAM, d.SubjectID, f.SubjectValue }).ToList();
                        foreach (var setSubjs in getSubjs)
                        {
                            double value = GradeValue(setSubjs.CA + setSubjs.EXAM) * setSubjs.SubjectValue;
                            tcp = tcp + value;
                        }
                        var getSubjsII = (from d in results where d.DepartmentID == setDeptID && d.StudentID == StudentID && d.Level < Level && d.Semester == 2 join f in db.Subjects on d.SubjectID equals f.SubjectID select new { d.CA, d.EXAM, d.SubjectID, f.SubjectValue }).ToList();
                        foreach (var setSubjsII in getSubjsII)
                        {
                            double valueII = GradeValue(setSubjsII.CA + setSubjsII.EXAM) * setSubjsII.SubjectValue;
                            tcpII = tcpII + valueII;
                        }
                        var getSubjsIII = (from d in results where d.DepartmentID == setDeptID && d.StudentID == StudentID && d.Level == Level && d.Semester == Semester join f in db.Subjects on d.SubjectID equals f.SubjectID select new { d.CA, d.EXAM, d.SubjectID, f.SubjectValue }).ToList();
                        foreach (var setSubjsIII in getSubjsIII)
                        {
                            double valueIII = GradeValue(setSubjsIII.CA + setSubjsIII.EXAM) * setSubjsIII.SubjectValue;
                            tcpIII = tcpIII + valueIII;
                        }
                    }

                    return tcp + tcpII + tcpIII;
                }
            }
        }

        //4
        public double PrevCummTnu(int Level, int StudentID, int Semester)
        {
            var results = db.Results.AsQueryable();
            double getTnu = 0;
            double getTnuII = 0;
            double getTnuIII = 0;
            double igetTnu = 0;
            var getDptID = (from d in results where d.StudentID == StudentID select d.DepartmentID).Distinct().ToList();

            if (Semester == 1 && Level == 100)
            {
                return 0.00;
            }
            //calculate the total of the last level
            else if (Semester == 1 && Level > 100)
            {
                foreach (var setDeptID in getDptID)
                {
                    double getValue = (from d in results where d.DepartmentID == setDeptID && d.StudentID == StudentID && d.Level < Level && d.Semester == 1 join f in db.Subjects on d.SubjectID equals f.SubjectID select f.SubjectValue).Sum();
                    getTnu = getTnu + getValue;
                }
                foreach (var setDeptID in getDptID)
                {
                    double getValue = (from d in results where d.DepartmentID == setDeptID && d.StudentID == StudentID && d.Level < Level && d.Semester == 2 join f in db.Subjects on d.SubjectID equals f.SubjectID select f.SubjectValue).Sum();
                    getTnuII = getTnuII + getValue;
                }
                return (getTnu + getTnuII);
            }
            else
            {
                if (Level == 100 && Semester == 2)
                {
                    foreach (var setDeptID in getDptID)
                    {
                        var getValue = (from d in results where d.DepartmentID == setDeptID && d.StudentID == StudentID && d.Level == Level && d.Semester == 1 join f in db.Subjects on d.SubjectID equals f.SubjectID select f.SubjectValue).ToList();

                        getTnu = getTnu + getValue.Sum();
                    }
                    return getTnu;
                }
                else
                {
                    //get previous total years
                    foreach (var setDeptID in getDptID)
                    {
                        double getValue = (from d in results where d.DepartmentID == setDeptID && d.StudentID == StudentID && d.Level < Level && d.Semester == 1 join f in db.Subjects on d.SubjectID equals f.SubjectID select f.SubjectValue).Sum();
                        getTnu = getTnu + getValue;
                    }
                    foreach (var setDeptID in getDptID)
                    {
                        double getValue = (from d in results where d.DepartmentID == setDeptID && d.StudentID == StudentID && d.Level < Level && d.Semester == 2 join f in db.Subjects on d.SubjectID equals f.SubjectID select f.SubjectValue).Sum();
                        getTnuII = getTnuII + getValue;
                    }

                    //current year
                    foreach (var setDeptID in getDptID)
                    {
                        double getValue = (from d in results where d.DepartmentID == setDeptID && d.StudentID == StudentID && d.Level == Level && d.Semester == 1 join f in db.Subjects on d.SubjectID equals f.SubjectID select f.SubjectValue).Sum();
                        getTnuIII = getTnuIII + getValue;
                    }
                    //get current year semester
                    //foreach (var setDeptID in getDptID)
                    //{
                    //    if (Semester == 1)
                    //    {
                    //        double getValue = (from d in results where d.DepartmentID == setDeptID && d.StudentID == StudentID && d.Semester == 2 && d.Level == Level select d.TNU).Distinct().Sum();
                    //        igetTnu = igetTnu + getValue;
                    //    }
                    //    else
                    //    {
                    //        double getValue = (from d in results where d.DepartmentID == setDeptID && d.StudentID == StudentID && d.Semester == 1 && d.Level == Level select d.TNU).Distinct().Sum();
                    //        igetTnu = igetTnu + getValue;
                    //    }

                    //}
                    return getTnu + getTnuII + getTnuIII;
                    //be wary
                }
            }
        }

        public double PrevCummTnup(int Level, int StudentID, int Semester)
        {
            var results = db.Results.AsQueryable();
            double tnup = 0;
            double tnupII = 0;
            double tnupIII = 0;
            var getDptID = (from d in results where d.StudentID == StudentID select d.DepartmentID).Distinct().ToList();
            if (Semester == 1 && Level == 100)
            {
                return 0.00;
            }
            if (Level == 100 && Semester == 2)
            {
                foreach (var setDeptID in getDptID)
                {
                    {
                        //get current year semester
                        var getSubjs = (from d in results where d.DepartmentID == setDeptID && d.StudentID == StudentID && d.Level == Level && d.Semester == 1 join f in db.Subjects on d.SubjectID equals f.SubjectID select new { d.CA, d.EXAM, d.SubjectID, f.SubjectValue }).ToList();
                        foreach (var setSubjs in getSubjs)
                        {
                            double value = setSubjs.SubjectValue;
                            if ((setSubjs.CA + setSubjs.EXAM) >= 40)
                            {
                                tnup = tnup + value;
                            }
                            else
                            {
                                tnup = tnup + 0;
                            }
                        }
                    }
                }
                return tnup;
            }

            //calculate the total of the last level
            if (Semester == 1 && Level > 100)
            {
                foreach (var setDeptID in getDptID)
                {
                    var getSubjs = (from d in results where d.DepartmentID == setDeptID && d.StudentID == StudentID && d.Level < Level && d.Semester == 1 join f in db.Subjects on d.SubjectID equals f.SubjectID select new { d.CA, d.EXAM, d.SubjectID, f.SubjectValue }).ToList();
                    foreach (var setSubjs in getSubjs)
                    {
                        double value = setSubjs.SubjectValue;
                        if ((setSubjs.CA + setSubjs.EXAM) >= 40)
                        {
                            tnup = tnup + value;
                        }
                        else
                        {
                            tnup = tnup + 0;
                        }
                    }
                    var getSubjsII = (from d in results where d.DepartmentID == setDeptID && d.StudentID == StudentID && d.Level < Level && d.Semester == 2 join f in db.Subjects on d.SubjectID equals f.SubjectID select new { d.CA, d.EXAM, d.SubjectID, f.SubjectValue }).ToList();
                    foreach (var setSubjsII in getSubjsII)
                    {
                        double value = setSubjsII.SubjectValue;
                        if ((setSubjsII.CA + setSubjsII.EXAM) >= 40)
                        {
                            tnupII = tnupII + value;
                        }
                        else
                        {
                            tnupII = tnupII + 0;
                        }
                    }
                }
                return tnup + tnupII;
            }

            if (Semester > 1 && Level > 100)
            {
                //get previous total years
                foreach (var setDeptID in getDptID)
                {
                    var getSubjs = (from d in results where d.DepartmentID == setDeptID && d.StudentID == StudentID && d.Level < Level && d.Semester == 1 join f in db.Subjects on d.SubjectID equals f.SubjectID select new { d.CA, d.EXAM, d.SubjectID, f.SubjectValue }).ToList();
                    foreach (var setSubjs in getSubjs)
                    {
                        double value = setSubjs.SubjectValue;
                        if ((setSubjs.CA + setSubjs.EXAM) >= 40)
                        {
                            tnup = tnup + value;
                        }
                        else
                        {
                            tnup = tnup + 0;
                        }
                    }

                    var getSubjsII = (from d in results where d.DepartmentID == setDeptID && d.StudentID == StudentID && d.Level < Level && d.Semester == 2 join f in db.Subjects on d.SubjectID equals f.SubjectID select new { d.CA, d.EXAM, d.SubjectID, f.SubjectValue }).ToList();
                    foreach (var setSubjsII in getSubjsII)
                    {
                        double value = setSubjsII.SubjectValue;
                        if ((setSubjsII.CA + setSubjsII.EXAM) >= 40)
                        {
                            tnupII = tnupII + value;
                        }
                        else
                        {
                            tnupII = tnupII + 0;
                        }
                    }
                    var getSubjsIII = (from d in results where d.DepartmentID == setDeptID && d.StudentID == StudentID && d.Level == Level && d.Semester == Semester join f in db.Subjects on d.SubjectID equals f.SubjectID select new { d.CA, d.EXAM, d.SubjectID, f.SubjectValue }).ToList();
                    foreach (var setSubjsIII in getSubjsIII)
                    {
                        double value = setSubjsIII.SubjectValue;
                        if ((setSubjsIII.CA + setSubjsIII.EXAM) >= 40)
                        {
                            tnupIII = tnupIII + value;
                        }
                        else
                        {
                            tnupIII = tnupIII + 0;
                        }
                    }
                }
                return tnup + tnupII + tnupIII;
            }
            else
            {
                return 0;
            }
        }

        public double CountStdByGrade(int SubjectID, double Semester, int SessionID, string GradeName)
        {
            var results = db.Results.AsQueryable();
            double getData = 0;
            //string GradeName= from d in db.Subjects where d.SubjectUnit==

            if (GradeName == "A")
            {
                getData = (from d in results where (d.EXAM + d.CA) >= 70 && d.SubjectID == SubjectID && d.Semester == Semester && d.SessionID == SessionID select d.StudentID).Distinct().Count();
                return getData;
            }
            else if (GradeName == "B")
            {
                getData = (from d in results where (d.EXAM + d.CA) >= 60 && (d.EXAM + d.CA) <= 69 && d.SubjectID == SubjectID && d.Semester == Semester && d.SessionID == SessionID select d.StudentID).Distinct().Count();
                return getData;
            }
            else if (GradeName == "C")
            {
                getData = (from d in results where (d.EXAM + d.CA) >= 50 && (d.EXAM + d.CA) <= 59 && d.SubjectID == SubjectID && d.Semester == Semester && d.SessionID == SessionID select d.StudentID).Distinct().Count();
                return getData;
            }
            else if (GradeName == "D")
            {
                getData = (from d in results where (d.EXAM + d.CA) >= 49 && (d.EXAM + d.CA) <= 45 && d.SubjectID == SubjectID && d.Semester == Semester && d.SessionID == SessionID select d.StudentID).Distinct().Count();
                return getData;
            }
            else if (GradeName == "E")
            {
                getData = (from d in results where (d.EXAM + d.CA) >= 40 && (d.EXAM + d.CA) <= 44 && d.SubjectID == SubjectID && d.Semester == Semester && d.SessionID == SessionID select d.StudentID).Distinct().Count();
                return getData;
            }
            else if (GradeName == "F")
            {
                getData = (from d in results where (d.EXAM + d.CA) <= 39 && d.SubjectID == SubjectID && d.Semester == Semester && d.SessionID == SessionID select d.StudentID).Distinct().Count();
                return getData;
            }
            return getData;

        }
        public double StudentByGrade(int SubjectID, double Semester, int SessionID)
        {
            var results = db.Results.AsQueryable();
            double getTotal = (from d in results where d.SubjectID == SubjectID && d.Semester == Semester && d.SessionID == SessionID select d.StudentID).Count();

            return getTotal;
        }
        public double InstructorCountBySubjReg(int SubjectID, double Semester, int SessionID)
        {
            var results = db.Results.AsQueryable();
            double getTotal = (from d in results
                               where d.SubjectID == SubjectID && d.Semester == Semester && d.SessionID == SessionID
                               join f in db.LecturerProfiles on d.SubjectID equals f.SubjectID
                               select f.LecturerID).Distinct().Count();
            return getTotal;
        }
        public double AllInstructorsReg(double Semester, int SessionID)
        {
            var results = db.Results.AsQueryable();
            double getTotal = (from d in results
                               where d.Semester == Semester && d.SessionID == SessionID
                               join f in db.LecturerProfiles on d.SubjectID equals f.SubjectID
                               select f.LecturerID).Distinct().Count();
            return getTotal;
        }
        public double AllLecturersCourses(int LecturerID, double Semester, int SessionID)
        {
            var results = db.Results.AsQueryable();
            double getTotal = (from d in db.LecturerProfiles where d.LecturerID == LecturerID select d.SubjectID).Distinct().Count();
            return getTotal;
        }
        public double StudentCountByCode(string SubjectCode, double Semester, int SessionID)
        {
            var results = db.Results.AsQueryable();
            List<int> allStudentID = (from d in db.Subjects
                                      where d.SubjectCode == SubjectCode
                                      join s in results on d.SubjectID equals s.SubjectID
                                      select s.StudentID).Distinct().ToList();
            List<int> allCurrentStd = (from d in results
                                       where d.Semester == Semester && d.SessionID == SessionID
                                       select d.StudentID).Distinct().ToList();
            var JoinThem = allStudentID.Union(allStudentID);
            return JoinThem.Count();
        }
        public double StudentCountByDeptID(int DeptID, double Semester, int SessionID)
        {
            var results = db.Results.AsQueryable();
            double getTotal = (from d in results
                               where d.Semester == Semester && d.SessionID == SessionID && d.DepartmentID == DeptID
                               select d.StudentID).Distinct().Count();
            return getTotal;
        }
        public double AllStudentsCount(double Semester, int SessionID)
        {
            var results = db.Results.AsQueryable();
            double getTotal = (from d in results
                               where d.Semester == Semester && d.SessionID == SessionID
                               select d.StudentID).Distinct().Count();
            return getTotal;
        }
        public string Grade(double cgpa)
        {
            string GradeValue = "";
            if (cgpa >= 4.50)
            {
                GradeValue = "DISTINCTION ";
            }
            else if (cgpa >= 3.50 && cgpa <= 4.49)
            {
                GradeValue = "CREDIT";
            }
            else if (cgpa >= 2.40 && cgpa <= 3.49)
            {
                GradeValue = "MERIT";
            }
            else if (cgpa >= 1.50 && cgpa <= 2.39)
            {
                GradeValue = "PASS";
            }
            else if (cgpa >= 1.00 && cgpa <= 1.49)
            {
                GradeValue = "LOW PASS";
            }
            else if (cgpa <= 0.99)
            {
                GradeValue = "FAIL";
            }
            else
            {
                GradeValue = "No grade";
            }
            return GradeValue;
        }

        public double cummdeptTnu(int Semester, int Level, int StudentID, int SessionID, int DeptID)
        {
            var results = db.Results.AsQueryable();
            var tnu = db.Results
                       .Where(a => a.Semester <= Semester && a.DepartmentID == DeptID && a.Level <= Level && a.StudentID == StudentID && a.SessionID == SessionID).Select(a => a.Subjects.SubjectValue).ToList();
            return tnu.Count == 0 ? 0 : tnu.Sum();
        }

        public double _cummdeptTnu(int Semester, int Level, int StudentID, int DeptID)
        {
            var results = db.Results.AsQueryable();
            var tnu = db.Results
                       .Where(a => a.Semester <= Semester && a.DepartmentID == DeptID && a.Level <= Level && a.StudentID == StudentID).Select(a => a.Subjects.SubjectValue).ToList();
            return tnu.Count == 0 ? 0 : tnu.Sum();
        }

        //fixed department result on 23.11.016
        public double CurrdeptTnu(int Semester, int Level, int StudentID, int SessionID, int DeptID)
        {
            var results = db.Results.AsQueryable();
            var getTnu = db.Results
                       .Where(a => a.Semester == Semester && a.DepartmentID == DeptID && a.StudentID == StudentID && a.SessionID == SessionID).Select(a => a.Subjects.SubjectValue).ToList();

            return getTnu.Count == 0 ? 0 : getTnu.Sum();
        }


        public double CurrdeptTcp(int Semester, int Level, int StudentID, int SessionID, int DeptID)
        {
            var results = db.Results.AsQueryable();
            var getTcp = db.Results
                       .Where(a => a.Semester == Semester && a.DepartmentID == DeptID && a.StudentID == StudentID && a.SessionID == SessionID).Select(a => new { a.CA, a.EXAM,a.Subjects.SubjectValue }).ToList();
            double tcp = 0;
            foreach (var item in getTcp)
            {
                tcp += GradeValue(item.CA + item.EXAM) *item.SubjectValue;
            }
            return tcp;
        }

        public double CurrdeptTnup(int Semester, int Level, int StudentID, int SessionID, int DeptID)
        {
            var results = db.Results.AsQueryable();
            var getTnup = db.Results
                                  .Where(a => a.Semester == Semester && a.DepartmentID == DeptID && a.StudentID == StudentID && a.SessionID == SessionID && (a.EXAM + a.CA) > 39).Select(a => a.Subjects.SubjectValue).ToList();
            return getTnup.Count == 0 ? 0 : getTnup.Sum();
        }
        //newFix remove session from curr
        public double _CurrdeptTnu(int Semester, int Level, int StudentID, int DeptID)
        {
            var results = db.Results.AsQueryable();
            var getTnu = db.Results
                       .Where(a => a.Semester == Semester && a.DepartmentID == DeptID && a.Level == Level && a.StudentID == StudentID).Select(a => a.Subjects.SubjectValue).ToList();

            return getTnu.Count == 0 ? 0 : getTnu.Sum();
        }
        public double _CurrdeptTcp(int Semester, int Level, int StudentID, int DeptID)
        {
            var results = db.Results.AsQueryable();
            var getTcp = db.Results
                       .Where(a => a.Semester == Semester && a.DepartmentID == DeptID && a.Level == Level && a.StudentID == StudentID).Select(a => new { a.CA, a.EXAM,a.Subjects.SubjectValue }).ToList();
            double tcp = 0;
            foreach (var item in getTcp)
            {
                tcp += GradeValue(item.CA + item.EXAM) * item.SubjectValue;
            }
            return tcp;
        }
        public double _CurrdeptTnup(int Semester, int Level, int StudentID, int DeptID)
        {
            var results = db.Results.AsQueryable();
            var getTnup = db.Results
                                  .Where(a => a.Semester == Semester && a.DepartmentID == DeptID && a.Level == Level && a.StudentID == StudentID && (a.EXAM + a.CA) > 39).Select(a => a.Subjects.SubjectValue).ToList();
            return getTnup.Count == 0 ? 0 : getTnup.Sum();
        }
        public string DeptGpa(int Semester, int Level, int StudentID, int SessionID, int DeptID)
        {
            return string.Format("{0:0.00}", CurrdeptTcp(Semester, Level, StudentID, SessionID, DeptID) / CurrdeptTnu(Semester, Level, StudentID, SessionID, DeptID));
        }
        public double PrevdeptTnu(int Semester, int Level, int StudentID, int SessionID, int DeptID)
        {
            var results = db.Results.AsQueryable();
            double tnu = 0;
            if (Semester == 1 && Level == 100)
                return tnu;
            else if (Semester == 2 && Level == 100)
            {
                tnu=CurrdeptTnu(1, Level, StudentID, SessionID, DeptID);
                //tnu = results.Where(a => a.DepartmentID == DeptID && a.SessionID == SessionID && a.StudentID == StudentID && a.Semester == 1).Count() == 0 ? 0 : results.Where(a => a.DepartmentID == DeptID && a.SessionID == SessionID && a.StudentID == StudentID).Select(a => a.Subjects.SubjectValue).Sum();
                return tnu;
            }

            //tnu = _CurrdeptTnu(1, 100, StudentID, DeptID);
            else if (Semester == 1 && Level > 100)
            {
                var getTnu = results.Where(a => a.DepartmentID == DeptID && a.Level < Level && a.StudentID == StudentID).Select(a => a.Subjects.SubjectValue).ToList();
                tnu = getTnu.Count == 0 ? 0 : getTnu.Sum();
                return tnu;
            }
            else if (Semester == 2 && Level > 100)
            {
                var getTnu = results
                                      .Where(a => a.DepartmentID == DeptID && a.Level < Level && a.StudentID == StudentID).Select(a => a.Subjects.SubjectValue).Count() == 0 ? 0 :
                                      results.Where(a => a.DepartmentID == DeptID && a.Level < Level && a.StudentID == StudentID).Select(a => a.Subjects.SubjectValue).Sum();
                var _getTnu = results
                                      .Where(a => a.Semester == 1 && a.DepartmentID == DeptID && a.Level == Level && a.StudentID == StudentID && a.SessionID == SessionID).Select(a => a.Subjects.SubjectValue).Count() == 0 ? 0 :
                                      results.Where(a => a.Semester == 1 && a.DepartmentID == DeptID && a.Level == Level && a.StudentID == StudentID).Select(a => a.Subjects.SubjectValue).Sum();
                tnu = getTnu + _getTnu;
                return tnu;
            }

            return tnu;
        }
        public double PrevdeptTcp(int Semester, int Level, int StudentID, int SessionID, int DeptID)
        {
            var results = db.Results.AsQueryable();
            double tcp = 0;

            if (Semester == 1 && Level == 100) return tcp;
            else if (Semester == 2 && Level == 100)
            {

                var getTcp = results.Where(a => a.DepartmentID == DeptID && a.StudentID == StudentID && a.SessionID == SessionID &&a.Semester==1).Select(a => new { a.CA, a.EXAM, a.Subjects.SubjectValue }).ToList();
                getTcp.ForEach(a =>
                {
                    tcp += GradeValue(a.CA + a.EXAM) * a.SubjectValue;
                });
                return tcp;
                //tcp = CurrdeptTcp(1, 100, StudentID, SessionID, DeptID);
            }

            else if (Semester == 1 && Level > 100)
            {
                var getTcp = results.Where(a => a.DepartmentID == DeptID && a.Level < Level && a.StudentID == StudentID).Select(a => new { a.CA, a.EXAM, a.Subjects.SubjectValue }).ToList();
                getTcp.ForEach(a =>
                {
                    tcp += GradeValue(a.CA + a.EXAM) * a.SubjectValue;
                });
                return tcp;
            }

            else if (Semester == 2 && Level > 100)
            {
                var getTcp = results
                                     .Where(a => a.DepartmentID == DeptID && a.Level < Level && a.StudentID == StudentID).Select(a => new {a.SubjectID, a.CA, a.EXAM, a.Subjects.SubjectValue }).ToList();

                var _getTcp = results
                                      .Where(a => a.Semester == 1 && a.DepartmentID == DeptID && a.Level == Level && a.StudentID == StudentID && a.SessionID == SessionID).Select(a => new { a.SubjectID, a.CA, a.EXAM, a.Subjects.SubjectValue }).ToList();

                getTcp.ForEach(a =>
                {
                    tcp += GradeValue(a.CA + a.EXAM) * a.SubjectValue;
                });
                _getTcp.ForEach(a =>
                {
                    tcp += GradeValue(a.CA + a.EXAM) * a.SubjectValue;
                });
                //tnu = getTnu + _getTnu;
                //return tnu;
            }

            return tcp;

        }
        public double PrevdeptTnup(int Semester, int Level, int StudentID, int SessionID, int DeptID)
        {
            var results = db.Results.AsQueryable();
            double tnup = 0;
            if (Semester == 1 && Level == 100)
                return tnup;
            else if (Semester == 2 && Level == 100)
            {
                tnup=CurrdeptTnup(1, Level, StudentID, SessionID, DeptID);
                //tnup = results.Where(a => a.DepartmentID == DeptID && a.SessionID == SessionID && a.Semester == 1 && a.StudentID == StudentID && (a.CA + a.EXAM) > 39).Count() == 0 ? 0 : results.Where(a => a.DepartmentID == DeptID && a.SessionID == SessionID && a.StudentID == StudentID && (a.CA + a.EXAM) > 39).Select(a => a.Subjects.SubjectValue).Sum();
                return tnup;
            }
            else if (Semester == 1 && Level > 100)
            {
                var getTnup = results.Where(a => a.DepartmentID == DeptID && a.Level < Level && a.StudentID == StudentID && (a.EXAM + a.CA) > 39).Select(a => a.Subjects.SubjectValue).ToList();
                tnup = getTnup.Count == 0 ? 0 : getTnup.Sum();
                return tnup;
            }
            else if (Semester == 2 && Level > 100)
            {
                var getTnup = results
                                      .Where(a => a.DepartmentID == DeptID && a.Level < Level && a.StudentID == StudentID && (a.EXAM + a.CA) > 39).Select(a => a.Subjects.SubjectValue).Count() == 0 ? 0 :
                                      results.Where(a =>  a.DepartmentID == DeptID && a.Level < Level && a.StudentID == StudentID && (a.EXAM + a.CA) > 39).Select(a => a.Subjects.SubjectValue).Sum();
                var _getTnup = results
                                      .Where(a => a.Semester == 1 && a.DepartmentID == DeptID && a.Level == Level && a.StudentID == StudentID && (a.EXAM + a.CA) > 39 && a.SessionID == SessionID).Select(a => a.Subjects.SubjectValue).Count() == 0 ? 0 :
                                      results.Where(a => a.Semester == 1 && a.DepartmentID == DeptID && a.Level == Level && a.StudentID == StudentID && (a.EXAM + a.CA) > 39).Select(a => a.Subjects.SubjectValue).Sum();
                tnup = getTnup + _getTnup;
                return tnup;
            }
            
            return tnup;
        }
        public string PrevdeptCgpa(int Semester, int Level, int StudentID, int SessionID, int DeptID)
        {
            return string.Format("{0:0.00}", PrevdeptTcp(Semester, Level, StudentID, SessionID, DeptID) / PrevdeptTnu(Semester, Level, StudentID, SessionID, DeptID));
        }
        public double cummTnu(int Semester, int Level, int StudentID, int SessionID, int DeptID)
        {
            double cummTnu = 0;
            cummTnu = PrevdeptTnu(Semester, Level, StudentID, SessionID, DeptID) + CurrdeptTnu(Semester, Level, StudentID, SessionID, DeptID);
            return cummTnu;
        }

        public double cummTnup(int Semester, int Level, int StudentID, int SessionID, int DeptID)
        {
            double cummTnup = 0;
            cummTnup = PrevdeptTnup(Semester, Level, StudentID, SessionID, DeptID) + CurrdeptTnup(Semester, Level, StudentID, SessionID, DeptID);
            return cummTnup;
        }

        public double cummTcp(int Semester, int Level, int StudentID, int SessionID, int DeptID)
        {
            double cummTcp = 0;
            cummTcp = PrevdeptTcp(Semester, Level, StudentID, SessionID, DeptID) + CurrdeptTcp(Semester, Level, StudentID, SessionID, DeptID);
            return cummTcp;
        }
        public string cummCgpa(int Semester, int Level, int StudentID, int SessionID, int DeptID)
        {
            return string.Format("{0:0.00}", cummTcp(Semester, Level, StudentID, SessionID, DeptID) / cummTnu(Semester, Level, StudentID, SessionID, DeptID));
        }
        //ended here

        public List<OutstandingModel> GetOutstanding(int StudentID, int SubjectComb)
        {
            var results = db.Results.AsQueryable();
            try
            {
                int CurrSession = db.Sessions.Where(a => a.CurrentSession == true).Select(a => a.SessionID).FirstOrDefault();
                int PrevSession = CurrSession - 1;

                //getAll students
                Model.Student std = db.Students.Where(a => a.StudentID == StudentID).Select(a => a).FirstOrDefault();
                //get prev Level
                double prevLevel = std.Level - 100;

                //get all results which has the prev level
                List<Model.Result> allResults = results.Where(a => a.Level == prevLevel).Select(a => a).ToList();
                List<Model.AllCombined> allComb = db.AllCombineds.Where(a => a.SpecialCase == false && a.CurricullumID==std.CurriculumID).Select(a => a).ToList();

                //getting level
                //double StudentLevel = allResults.Where(a => a.StudentID == StudentID)
                //                               .Select(a => a.Level).FirstOrDefault();
                //get studentSubComb
                int getSubComb = allResults.Where(a => a.StudentID == StudentID)
                                              .Select(a => a.SubjectCombinID)
                                              .FirstOrDefault();

                //get subject with that subject combination
                var getSubjects = allComb.Where(a => a.SubjectCombineID == getSubComb && a.Subjects.SubjectLevel == prevLevel.ToString() && a.CurricullumID==std.CurriculumID)
                                               .Select(a => a.SubjectID).ToList();

                //get student registered sub
                var getStudentSub = allResults.Where(a => a.StudentID == StudentID)
                                               .Select(a => a.SubjectID).ToList();
                //compare and get subjects that the person didn't register
                var newSubID = getSubjects.Except(getStudentSub).ToList();

                List<int> AllCombined = new List<int>();
                List<OutstandingModel> allModel = new List<OutstandingModel>();

                foreach (var item in newSubID)
                {
                    var AllCombinedID = allComb.Where(a => a.SubjectID == item && a.SubjectCombineID == getSubComb)
                                                .Select(a => a.AllCombinedID).FirstOrDefault();

                    var getdata = (from a in db.AllCombineds
                                   where a.AllCombinedID == AllCombinedID && a.CurricullumID==std.CurriculumID
                                   join j in db.SubjectCombinations on a.SubjectCombineID equals j.SubjectCombinID
                                   join d in db.Departments on a.DepartmentID equals d.DepartmantID
                                   join h in db.Subjects on a.SubjectID equals h.SubjectID
                                   select new OutstandingModel { SubjectCombinID = j.SubjectCombinID, SubjectCombinName = j.SubjectCombinName, DepartmantID = d.DepartmantID, SubjectCode = h.SubjectCode, SubjectID = h.SubjectID, SubjectName = h.SubjectName, SubjectValue = h.SubjectValue, SubjectUnit = h.SubjectUnit, SubjectLevel = h.SubjectLevel, Semester = h.Semester, Active = h.Active, SessionID = PrevSession }).FirstOrDefault();
                    allModel.Add(getdata);
                }
                return allModel.ToList();
                // }
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }

        }
        public List<OutstandingModel> GetOutstandingResult(int StudentID, int SubjectComb, int Level, int SessionID,int Semester)
        {
            var results = db.Results.AsQueryable();
            List<Model.Result> allResults;
            List<int> getSubjects;
            try
            {

                //get all results which has the prev level
                if (Semester == 1)               
                    allResults = db.Results.Where(a => a.SessionID == SessionID && a.StudentID == StudentID && a.Semester==Semester).Select(a => a).ToList();
                else
                    allResults = db.Results.Where(a => a.SessionID == SessionID && a.StudentID == StudentID).Select(a => a).ToList();

                var std = db.Students.Where(a => a.StudentID == StudentID).FirstOrDefault();
                List<Model.AllCombined> allComb = db.AllCombineds.Where(a => a.SpecialCase == false && a.CurricullumID== std.CurriculumID).Select(a => a).ToList();


                //get studentSubComb
                int getSubComb = allResults.Select(a => a.SubjectCombinID).FirstOrDefault();

                //get subject with that subject combination
                if (Semester == 1)
                     getSubjects = allComb.Where(a => a.SubjectCombineID == getSubComb && a.Subjects.SubjectLevel == Level.ToString() && a.Subjects.Semester==Semester.ToString())
                     .Select(a => a.SubjectID).ToList();
                else
                    getSubjects = allComb.Where(a => a.SubjectCombineID == getSubComb && a.Subjects.SubjectLevel == Level.ToString())
                    .Select(a => a.SubjectID).ToList();


                //get student registered sub
                var getStudentSub = allResults.Select(a => a.SubjectID).ToList();
                //compare and get subjects that the person didn't register
                var newSubID = getSubjects.Except(getStudentSub).ToList();

                List<int> AllCombined = new List<int>();
                List<OutstandingModel> allModel = new List<OutstandingModel>();

                foreach (var item in newSubID)
                {
                    var AllCombinedID = allComb.Where(a => a.SubjectID == item && a.SubjectCombineID == getSubComb && a.CurricullumID==std.CurriculumID)
                                                .Select(a => a.AllCombinedID).FirstOrDefault();

                    var getdata = (from a in db.AllCombineds
                                   where a.AllCombinedID == AllCombinedID && a.CurricullumID == std.CurriculumID
                                   join j in db.SubjectCombinations on a.SubjectCombineID equals j.SubjectCombinID
                                   join d in db.Departments on a.DepartmentID equals d.DepartmantID
                                   join h in db.Subjects on a.SubjectID equals h.SubjectID
                                   select new OutstandingModel { SubjectCombinID = j.SubjectCombinID, SubjectCombinName = j.SubjectCombinName, DepartmantID = d.DepartmantID, SubjectCode = h.SubjectCode, SubjectID = h.SubjectID, SubjectName = h.SubjectName, SubjectValue = h.SubjectValue, SubjectUnit = h.SubjectUnit, SubjectLevel = h.SubjectLevel, Semester = h.Semester, Active = h.Active }).FirstOrDefault();
                    allModel.Add(getdata);
                }
                return allModel.ToList();
                // }
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }

        }
        public List<Result> GetCarryOvers(int StudentID, int Semester, int SessionID, int Level, int SubjectCombinID)
        {
            List<Result> carryover = new List<Result>();
            var getResult = db.Results.Where(a => a.StudentID == StudentID && a.SubjectCombinID == SubjectCombinID).AsQueryable();
            if (Semester == 1 && Level > 100)
            {
                //first get current carryover
                var currCarr = getResult.Where(a => a.SessionID == SessionID && a.Semester == Semester && (a.CA + a.EXAM) <= 39).Select(a => a.ResultID).Distinct().ToList();
                var prevCarr = getResult.Where(a => a.Level < Level && (a.CA + a.EXAM) <= 39).Select(a => a.ResultID).Distinct().ToList();
                //var getCarr = currCarr.Intersect(prevCarr);
                foreach (var item in prevCarr)
                {
                    carryover.Add(getResult.Where(a => a.ResultID == item).FirstOrDefault());
                }
                return carryover;
            }
            else if (Semester == 2 && Level > 100)
            {
                return getResult.Where(a => a.Level <= Level && (a.CA + a.EXAM) <= 39).ToList();
            }
            else if (Semester == 2 && Level == 100)
                return getResult.Where(a => a.Level == Level && a.Level == Level && (a.CA + a.EXAM) <= 39).ToList();
            // return getResult.Where(a=>a.StudentID==StudentID && a.)
            return null;
        }
        public List<CarrryOverViewModel> CarryOver(int StudentID)
        {
            var getCarryOvers = from r in db.Results
                                where r.StudentID == StudentID                              
                                join d in db.CarryOvers on r.ResultID equals d.ResultID
                                join f in db.Departments on r.DepartmentID equals f.DepartmantID
                                join a in db.Subjects on r.SubjectID equals a.SubjectID
                                join s in db.SubjectCombinations on r.SubjectCombinID equals s.SubjectCombinID
                                select new CarrryOverViewModel {SubjectCombinID=s.SubjectCombinID,SubjectCombinName= s.SubjectCombinName,DepartmantID=f.DepartmantID,SubjectID=a.SubjectID,SubjectName=a.SubjectName,SubjectValue=a.SubjectValue,SubjectCode= a.SubjectCode,SubjectUnit= a.SubjectUnit,Semester= a.Semester,Ca=r.CA,Exam=r.EXAM };
            return getCarryOvers.ToList();
        }
        public IEnumerable<int> SubjectBySubjectCombination(int SubCombID,string Level)
        {
            return db.AllCombineds.Where(a => a.SubjectCombineID == SubCombID && a.Subjects.SubjectLevel==Level).Select(a => a.SubjectID);
        }
        public List<Result> GetCourseCarryOver(int SubCombID,int SessionID,string Level)
        {
            
            List<Result> res = new List<Result>();
            
            double _Level = double.Parse(Level);
            var result = db.Results.Where(a => a.SubjectCombinID == SubCombID && a.SessionID == SessionID && a.Level>_Level).AsQueryable();
            foreach (var item in SubjectBySubjectCombination(SubCombID,Level).ToList())
            {
                 res.AddRange(result.Where(a => a.SubjectID == item));
            }
            return res;
        }
        public List<String> GetCarryOverMatricNo(int SubCombID, int SessionID, string Level)
        {
            return GetCourseCarryOver(SubCombID, SessionID, Level).Select(a => a.MatricNo).ToList();
        }
        public Result GetResultByMatric(string MatricNo)
        {
            return db.Results.Where(a => a.MatricNo == MatricNo).FirstOrDefault();
        }
        public bool CheckBed(int StudentID)
        {
            var student = db.Students.Where(a => a.StudentID == StudentID && a.Level==300 && a.Major=="BED" && a.Minor=="BED").FirstOrDefault();
            if (student == null)
                return false;
            return true;
        }
        public void GenerateMatric()
        {
            var students = db.Students.AsQueryable();
          
            string newCurrent = (from d in db.Sessions where d.CurrentSession == true select d.SessionYear).FirstOrDefault();
            string year = newCurrent.Split('/')[0];
            var newStudents = students.Where(a => a.Level == 100 && a.Registered=="True" && a.MatricNo==null).OrderBy(a=>a.FacultyName).ThenBy(a=>a.Major).ThenBy(a=>a.Minor).ThenBy(a=>a.Surname).ToArray();
            var allstudents = new List<Model.Student>();
            for (int i = 0; i < newStudents.Count(); i++)
            {
                int _i = i + 1;
                string mat = year.Substring(2,2);
                string padd = _i.ToString().PadLeft(4, '0');
                string studentMatric = mat + "/" + padd;
                var _updatestudents = newStudents[i];
                _updatestudents.MatricNo = studentMatric;
                db.SaveChanges();
                int studentid = _updatestudents.StudentID;
                
                db.Results.Where(a => a.StudentID == studentid).Update(x => new Model.Result() { MatricNo=studentMatric});
            }
            
        }
        
        //public void AddUpdate(int StudentID,string SubCourse)
        //{
        //    if (CheckBed(StudentID) == true)
        //    {
        //        var student = db.Students.Where(a => a.StudentID == StudentID).FirstOrDefault();
        //        if(student!=null)
        //        {
        //            student.SubCourse = SubCourse;
        //            db.SaveChanges();
        //        }
        //    }
        //}
       
    }
}