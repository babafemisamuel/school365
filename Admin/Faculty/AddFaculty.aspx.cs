﻿using School365.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace School365.Admin
{
    public partial class Faculty : System.Web.UI.Page
    {
        StudentModel db=new StudentModel();
        int getAdminID = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (User.Identity.IsAuthenticated == false)
                Response.Redirect("../Administrator/UserLogin.aspx");
            getAdminID = int.Parse(User.Identity.Name);
            var getAuth = (from d in db.RoleAdmins where d.AdminID == getAdminID select d.RoleID).ToList();
            if (getAuth.Contains(5) != true)
            {
                Response.Redirect("../Administrator/Profile.aspx");
            }  

        }

        protected void InsertBtn_Click(object sender, EventArgs e)
        {

            var InsertFaculty = new Model.Faculty
            {
                FacultyName = facultyName.Text.ToUpper(),
                FacultyCode = facultyCode.Text.ToUpper(),
            };
            db.Facultys.Add(InsertFaculty);
            db.SaveChanges();
            Response.Write("DATA INSERTED");
            facultyName.Text = "";
            facultyCode.Text = "";
        }

        protected void OnRowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            allFaculty.EditIndex = -1;
        }

        protected void OnRowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            int FacultyID = int.Parse(allFaculty.DataKeys[e.RowIndex].Values[0].ToString());
            var UpdateDb = (from d in db.Facultys where d.FacultyID == FacultyID select d).First();
            db.Facultys.Remove(UpdateDb);
            db.SaveChanges();
        }

        protected void OnRowEditing(object sender, GridViewEditEventArgs e)
        {
            allFaculty.EditIndex = e.NewEditIndex;
        }

        protected void OnRowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            GridViewRow row = allFaculty.Rows[e.RowIndex];
            int FacultyID = Convert.ToInt32(allFaculty.DataKeys[e.RowIndex].Values[0]);
            string FacultyName = (row.FindControl("txtName") as TextBox).Text;
            string FacultyCode = (row.FindControl("txtCode") as TextBox).Text;
            var UpdateDb = (from d in db.Facultys where d.FacultyID == FacultyID select d).FirstOrDefault();
            UpdateDb.FacultyName = FacultyName;
            UpdateDb.FacultyCode = FacultyCode;
            db.SaveChanges();
            allFaculty.EditIndex = -1;
        }

        protected void OnRowDataBound(object sender, GridViewRowEventArgs e)
        {

        }
    }
}