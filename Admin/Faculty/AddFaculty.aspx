﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin_Layout.Master" AutoEventWireup="true" CodeBehind="AddFaculty.aspx.cs" Inherits="School365.Admin.Faculty" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="header" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="body" runat="server">
    <br />
    <div class="row">
        <div class="large-3 columns">
            <asp:TextBox ID="facultyName" runat="server" placeholder="Faculty Name"></asp:TextBox>
        </div>
        <div class="large-3 columns">
            <asp:TextBox ID="facultyCode" runat="server" placeholder="Faculty Code"></asp:TextBox>
        </div>
        <div class="large-3 columns">
            <asp:Button ID="InsertBtn" runat="server" Text="Insert" CssClass="tiny radius button bg-blue" OnClick="InsertBtn_Click"></asp:Button>
        </div>
        <div class="large-3 columns">
        </div>
    </div>
    <br />
    <div class="row">
        <div class="large-9 columns">
            <asp:GridView ID="allFaculty" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" DataSourceID="LinqDataSource1" OnRowDataBound="OnRowDataBound" OnRowEditing="OnRowEditing" OnRowCancelingEdit="OnRowCancelingEdit"
                OnRowUpdating="OnRowUpdating" OnRowDeleting="OnRowDeleting" EmptyDataText="No records has been added." DataKeyNames="FacultyID">
                <Columns>

                    <asp:TemplateField HeaderText="FacultyName" SortExpression="FacultyName">
                        <ItemTemplate>
                            <asp:Label ID="lblName" runat="server" Text='<%# Eval("FacultyName") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtName" runat="server" Text='<%# Eval("FacultyName") %>'></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="FacultyCode" SortExpression="FacultyCode">
                        <ItemTemplate>
                            <asp:Label ID="lblCode" runat="server" Text='<%# Eval("FacultyCode") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtCode" runat="server" Text='<%# Eval("FacultyCode") %>'></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>

                    <%--<asp:BoundField DataField="FacultyID" Visible="false" ReadOnly="True" SortExpression="FacultyID" />
                    <asp:BoundField DataField="FacultyName" HeaderText="FacultyName" ReadOnly="True" SortExpression="FacultyName" />
                    <asp:BoundField DataField="FacultyCode" HeaderText="FacultyCode" ReadOnly="True" SortExpression="FacultyCode" />--%>
                    <asp:CommandField ButtonType="Link" ShowEditButton="true" ShowDeleteButton="true" ItemStyle-Width="150" />
                </Columns>

            </asp:GridView>
            <asp:LinqDataSource ID="LinqDataSource1" runat="server" EnableUpdate="true" EnableDelete="true" EnableInsert="true" ContextTypeName="School365.Model.StudentModel" EntityTypeName="" OrderBy="FacultyCode" Select="new (FacultyID, FacultyName, FacultyCode)" TableName="Facultys">
            </asp:LinqDataSource>
        </div>
    </div>
</asp:Content>
