﻿using School365.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace School365.Admin.Audit
{
    public  class UserTracker
    {
        StudentModel db = new StudentModel();
        public void Track(string UserName, string Activity, string MethodName,string ClassName,string info)
        {
            
            AuditTrail aud = new AuditTrail();
            aud.Activity = Activity;
            aud.UserName = UserName;
            aud.MethodName = MethodName;
            aud.ClassName = ClassName;
            aud.Info = info;
            aud.DateTime = DateTime.Now;
            db.AuditTrail.Add(aud);
            db.SaveChanges();

        }
    }
}