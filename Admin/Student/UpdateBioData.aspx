﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin_Layout.Master" AutoEventWireup="true" CodeBehind="UpdateBioData.aspx.cs" Inherits="School365.Admin.Student.UpdateBioData" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="header" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="body" runat="server">

    <h2 class="btn btn-danger" style="color: white">JAMB DETAILS</h2>
    <div class="row">
        <asp:GridView ID="AllStdJamb" runat="server" AutoGenerateColumns="false" OnRowDataBound="AllStdJamb_RowDataBound">
            <Columns>
                <asp:TemplateField HeaderText="Select">
                    <ItemTemplate>
                        <asp:CheckBox ID="JambCrs" runat="server" />

                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="ID">
                    <ItemTemplate>
                        <asp:TextBox ID="JambDetailID" runat="server" Visible="true" Text='<%#Bind("JambDetailID") %>'> ></asp:TextBox>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="RegNo">
                    <ItemTemplate>
                        <asp:TextBox ID="RegNo" runat="server" Visible="true" Text='<%#Bind("RegNo") %>'> ></asp:TextBox>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Center">
                    <ItemTemplate>
                        <asp:TextBox ID="Center" runat="server" Text='<%#Bind("Center") %>'> ></asp:TextBox>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Year">
                    <ItemTemplate>
                        <asp:TextBox ID="Year" runat="server" Text='<%#Bind("Year") %>'> ></asp:TextBox>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="SubjectName">
                    <ItemTemplate>
                        <asp:Label ID="Subject" runat="server" Visible="false" Text='<%#Bind("Subject") %>'>> </asp:Label>
                        <asp:DropDownList ID="getSubj" runat="server"></asp:DropDownList>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Score">
                    <ItemTemplate>
                        <asp:TextBox ID="Score" runat="server" Visible="true" Text='<%#Bind("Score") %>'>> </asp:TextBox>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
        <asp:Button ID="Submit" runat="server" Text="Update Jamb Details" CssClass="btn btn-flat btn-info" Style="color: white" OnClick="Submit_Click" />
        <asp:Button ID="DeleteJamb" runat="server" Text="Delete Jamb Details" CssClass="btn  btn-danger" Style="color: white" OnClick="DeleteJamb_Click" />

    </div>
    <hr />
    <h2 class="btn btn-danger" style="color: white">OLEVEL DETAILS</h2>
    <asp:GridView ID="allWaecDetails" runat="server" AutoGenerateColumns="false" OnRowDataBound="allWaecDetails_RowDataBound">
        <Columns>
            <asp:TemplateField HeaderText="Select">
                <ItemTemplate>
                    <asp:CheckBox ID="WaecCrs" runat="server" />

                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="ID">
                <ItemTemplate>
                    <asp:TextBox ID="OlevelDetailsID" runat="server" Visible="true" Text='<%#Bind("OlevelDetailsID") %>'> ></asp:TextBox>
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="EXAM TYPE">
                <ItemTemplate>
                    <asp:TextBox ID="ExamType" runat="server" Visible="true" Text='<%#Bind("ExamType") %>'> ></asp:TextBox>
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="EXAM CENTER">
                <ItemTemplate>
                    <asp:TextBox ID="ExamCenter" runat="server" Text='<%#Bind("ExamCenter") %>'> ></asp:TextBox>
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="EXAM DATE">
                <ItemTemplate>
                    <asp:TextBox ID="ExamDate" runat="server" Text='<%#Bind("ExamDate") %>'>> </asp:TextBox>
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="CENTER NUMBER">
                <ItemTemplate>
                    <asp:TextBox ID="CenterNumber" runat="server" Visible="true" Text='<%#Bind("CenterNumber") %>'> ></asp:TextBox>
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="RegNo">
                <ItemTemplate>
                    <asp:TextBox ID="RegNo" runat="server" Text='<%#Bind("RegNo") %>'> ></asp:TextBox>
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="SubjectName">
                <ItemTemplate>
                    <asp:Label ID="SubjectName" runat="server" Visible="false" Text='<%#Bind("SubjectName") %>'>> </asp:Label>
                    <asp:DropDownList ID="getSubjNm" runat="server"></asp:DropDownList>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Grade">
                <ItemTemplate>
                    <asp:Label ID="Grade" runat="server" Visible="False" Text='<%#Bind("Grade") %>'>> </asp:Label>
                    <asp:DropDownList ID="allgrade" runat="server">
                       
                    </asp:DropDownList>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    <asp:Button ID="UpdateWaec" runat="server" Text="Update Waec Details" CssClass="btn btn-success" Style="color: white" OnClick="UpdateWaec_Click" />
    <asp:Button ID="DeleteWaec" runat="server" Text="Delete Waec Details" CssClass="btn btn-danger" Style="color: white" OnClick="DeleteWaec_Click" />

</asp:Content>
