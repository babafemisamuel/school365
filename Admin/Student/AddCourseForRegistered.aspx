﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin_Layout.Master" AutoEventWireup="true" CodeBehind="AddCourseForRegistered.aspx.cs" Inherits="School365.Admin.Student.AddCourseForRegistered" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="header" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="body" runat="server">
    <hr />

    <div class="row">
        <div class="columns large-4">
            <asp:TextBox ID="MatricNo" runat="server" placeholder="Matric No"></asp:TextBox>
        </div>
        <div class="columns large-4">
            <asp:DropDownList ID="allSessions" runat="server">
            </asp:DropDownList>
        </div>
        <div class="columns large-4">
            <asp:Button ID="GetStudents" runat="server" Text="Get Student" CssClass="radius button bg-light-green" OnClick="GetStudents_Click" />
        </div>
    </div>
    <hr />
    <div class="row">
        <div class="columns large-12">
            <asp:Button ID="DeleteData" runat="server" Text="Delete Registered" CssClass="radius button bg-red" OnClick="DeleteData_Click" />
            <asp:GridView ID="allSubjects" runat="server" AutoGenerateColumns="false">
                <Columns>
                    <asp:TemplateField HeaderText="Select">
                        <ItemTemplate>
                            <asp:CheckBox ID="Crs" runat="server" class="input-sm" />

                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="ResultID" />
                    <asp:BoundField DataField="MatricNo" HeaderText="Matric No" />
                    <asp:BoundField DataField="SubjectCode" HeaderText="Subject Code" />
                    <asp:BoundField DataField="SubjectLevel" HeaderText="Subject Level" />
                    <asp:BoundField DataField="SubjectValue" HeaderText="Subject Value" />
                    <asp:BoundField DataField="SubjectUnit" HeaderText="Subject Unit" />
                    <asp:BoundField DataField="EXAM" HeaderText="Exam Score" />
                    <asp:BoundField DataField="CA" HeaderText="CA Score" />
                </Columns>
            </asp:GridView>
        </div>
    </div>
    <div class="row">
        <div class="row">
            <div class="large-4 columns">
                <label id="subjectName">SubjectCode</label>
                <asp:DropDownList ID="allSubj" runat="server" DataSourceID="linqDatSource" DataTextField="SubjectCode" DataValueField="SubjectID">
                </asp:DropDownList>
                <asp:LinqDataSource ID="linqDatSource" runat="server" ContextTypeName="School365.Model.StudentModel" EntityTypeName="" OrderBy="SubjectCode" Select="new (SubjectID, SubjectCode)" TableName="Subjects">
                </asp:LinqDataSource>
            </div>
            <div class="large-4 columns">
                <label id="DeptName">Department Name</label>
                <asp:DropDownList ID="allDept" runat="server" DataSourceID="LinqDataSource1" DataTextField="DepartmentName" DataValueField="DepartmantID">
                </asp:DropDownList>
                <asp:LinqDataSource ID="LinqDataSource1" runat="server" ContextTypeName="School365.Model.StudentModel" EntityTypeName="" Select="new (DepartmantID, DepartmentName)" TableName="Departments">
                </asp:LinqDataSource>
            </div>
            <div class="large-4 columns">
                <asp:Button ID="AddStudent" runat="server" Text="Add Subjects" CssClass="radius button bg-light-green" OnClick="AddStudent_Click" />
            </div>
        </div>
    </div>
</asp:Content>
