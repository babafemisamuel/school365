﻿using School365.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace School365.Admin.Student
{
    public partial class UpdateBioData : System.Web.UI.Page
    {
        StudentModel db = new StudentModel();
        string StudentID = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            //Request.QueryString["UserValue"].ToString()
            if (!IsPostBack)
            {
                StudentID = Request.QueryString["UserValue"].ToString();
                StudentData(StudentID);
            }

        }
        public void StudentData(string StudentID)
        {
            var getJambDetail = (from d in db.JambDetails where d.StudentID.ToString() == StudentID.ToString() select d).Distinct().ToList();
            var getWaecDetail = (from d in db.OlevelDetails where d.StudentID.ToString() == StudentID.ToString() select d).Distinct().ToList();
            AllStdJamb.DataSource = getJambDetail;
            AllStdJamb.DataBind();
            allWaecDetails.DataSource = getWaecDetail;
            allWaecDetails.DataBind();

        }
        protected void UpdateWaec_Click(object sender, EventArgs e)
        {

            foreach (GridViewRow row in allWaecDetails.Rows)
            {
                TextBox id = (TextBox)row.FindControl("OlevelDetailsID");
                TextBox examType = (TextBox)row.FindControl("ExamType");
                TextBox examCenter = (TextBox)row.FindControl("ExamCenter");
                TextBox examDate = (TextBox)row.FindControl("ExamDate");
                TextBox centerNumber = (TextBox)row.FindControl("CenterNumber");
                TextBox RegNo = (TextBox)row.FindControl("RegNo");
                DropDownList SubjectName = (DropDownList)row.FindControl("getSubjNm");
                DropDownList Grade = (DropDownList)row.FindControl("allgrade");
                var UpdateStudent = (from d in db.OlevelDetails where d.OlevelDetailsID.ToString() == id.Text select d).FirstOrDefault();
                UpdateStudent.ExamType = examType.Text;
                UpdateStudent.ExamCenter = examCenter.Text;
                UpdateStudent.ExamDate = examDate.Text;
                UpdateStudent.CenterNumber = centerNumber.Text;
                UpdateStudent.RegNo = RegNo.Text;
                UpdateStudent.SubjectName = SubjectName.SelectedItem.Text;
                UpdateStudent.Grade = Grade.SelectedItem.Text;
                db.SaveChanges();
                //StudentData(StudentID);
                Response.Write("Updated");
            }
        }

        protected void Submit_Click(object sender, EventArgs e)
        {
            foreach (GridViewRow row in AllStdJamb.Rows)
            {
                TextBox id = (TextBox)row.FindControl("JambDetailID");
                TextBox year = (TextBox)row.FindControl("Year");
                TextBox center = (TextBox)row.FindControl("center");
                TextBox regNo = (TextBox)row.FindControl("RegNo");
                TextBox score = (TextBox)row.FindControl("Score");
                DropDownList SubjectName = (DropDownList)row.FindControl("getSubj");

                var UpdateStudent = (from d in db.JambDetails where d.JambDetailID.ToString() == id.Text select d).FirstOrDefault();
                UpdateStudent.Year = year.Text;
                UpdateStudent.Center = center.Text;
                UpdateStudent.RegNo = regNo.Text;
                UpdateStudent.Subject = SubjectName.SelectedItem.Text;
                UpdateStudent.Score = score.Text;
                db.SaveChanges();
                Response.Write("Data Changed");

            }
        }

        protected void AllStdJamb_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DropDownList getSubj = (e.Row.FindControl("getSubj") as DropDownList);
                string filePath = Server.MapPath("~/SecSubj.xml");
                using (DataSet ds = new DataSet())
                {
                    ds.ReadXml(filePath);
                    getSubj.DataSource = ds;
                    getSubj.DataTextField = "ListItem_Text";
                    getSubj.DataBind();
                }
                string Subject = (e.Row.FindControl("Subject") as Label).Text;
                getSubj.Items.FindByText(Subject).Selected = true;

                //SubjectName
            }

        }

        protected void allWaecDetails_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DropDownList getSubjNm = (e.Row.FindControl("getSubjNm") as DropDownList);
                string filePath = Server.MapPath("~/SecSubj.xml");
                using (DataSet ds = new DataSet())
                {
                    ds.ReadXml(filePath);
                    getSubjNm.DataSource = ds;
                    getSubjNm.DataTextField = "ListItem_Text";
                    getSubjNm.DataBind();
                }
                //getSubjNm
                string SubjectName = (e.Row.FindControl("SubjectName") as Label).Text;
                getSubjNm.Items.FindByText(SubjectName).Selected = true;

                List<string> all = new List<string>();
                
                string[] arr = { "A1", "B2", "B3", "C4", "C5", "C6", "D7", "E8", "F9" };
                List<Grade> grade = new List<Grade>();
                foreach (var item in arr)
                {
                    var _grade = new Grade();
                    _grade.name = item;
                    grade.Add(_grade);
                }

                DropDownList allgrade = (e.Row.FindControl("allgrade") as DropDownList);
                allgrade.DataSource = grade;
                allgrade.DataTextField = "name";
                allgrade.DataBind();

                string __grade= (e.Row.FindControl("Grade") as Label).Text;
                
                allgrade.Items.FindByText(__grade).Selected = true;

                //DropDownList grade = (e.Row.FindControl("grade") as DropDownList);
                //var getgrade = (from d in db.OlevelDetails where d.StudentID == StudentID.ToString() select d.Grade).Distinct().ToList();
                //grade.DataSource = getgrade;
                //grade.SelectedItem.Text = "Grade";
                //grade.DataBind();
                //SubjectName
            }
        }

        protected void DeleteJamb_Click(object sender, EventArgs e)
        {
            foreach (GridViewRow row in AllStdJamb.Rows)
            {
                if ((row.Cells[0].FindControl("JambCrs") as CheckBox).Checked)
                {
                    TextBox id = (TextBox)row.FindControl("JambDetailID");
                    var UpdateStudent = (from d in db.JambDetails where d.JambDetailID.ToString() == id.Text select d).FirstOrDefault();
                    db.JambDetails.Remove(UpdateStudent);
                    db.SaveChanges();
                    //StudentData(StudentID);
                    Response.Write("Jamb Data Deleted");
                }
            }
        }

        protected void DeleteWaec_Click(object sender, EventArgs e)
        {
            foreach (GridViewRow row in allWaecDetails.Rows)
            {
                if ((row.Cells[0].FindControl("WaecCrs") as CheckBox).Checked)
                {

                    TextBox id = (TextBox)row.FindControl("OlevelDetailsID");
                    var UpdateStudent = (from d in db.OlevelDetails where d.OlevelDetailsID.ToString() == id.Text select d).FirstOrDefault();
                    db.OlevelDetails.Remove(UpdateStudent);
                    db.SaveChanges();
                    //StudentData(StudentID);
                    Response.Write("Olevel Data Deleted");

                }
            }
        }
        public class Grade
        {
            public string name { get; set; }
        }
    }
}