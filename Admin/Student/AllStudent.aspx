﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin_Layout.Master" AutoEventWireup="true" CodeBehind="AllStudent.aspx.cs" Inherits="School365.Admin.Student.AllStudent" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="header" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="body" runat="server">

    
        <div class="large-12 columns">
            <asp:GridView ID="allStudents" runat="server" AutoGenerateColumns="False" DataSourceID="LinqDataSource1" AllowSorting="True" Width="1115px" AllowPaging="True" PageSize="15">
                <Columns>
                    <asp:BoundField DataField="StudentID" ReadOnly="True" Visible="false" SortExpression="StudentID" HeaderText="StudentID" />
                    <asp:BoundField DataField="MatricNo" HeaderText="MatricNo" ReadOnly="True" SortExpression="MatricNo" ControlStyle-Width="150" />
                    <asp:BoundField DataField="Surname" HeaderText="Surname" ReadOnly="True" SortExpression="Surname" />
                    <asp:BoundField DataField="Firstname" HeaderText="Firstname" ReadOnly="True" SortExpression="Firstname" />
                    <asp:BoundField DataField="Middlename" HeaderText="Middlename" ReadOnly="True" SortExpression="Middlename" />
                    <asp:BoundField DataField="PhoneNumberNextOfKin" HeaderText="Phone Number" ReadOnly="True" SortExpression="PhoneNumberNextOfKin" />
                    <asp:BoundField DataField="SOR" HeaderText="SOR" ReadOnly="True" SortExpression="SOR" />
                    <asp:BoundField DataField="Email" HeaderText="Email" ReadOnly="True" SortExpression="Email" />
                    <asp:BoundField DataField="Major" HeaderText="Major" ReadOnly="True" SortExpression="Major" />
                    <asp:BoundField DataField="Minor" HeaderText="Minor" ReadOnly="True" SortExpression="Minor" />
                    <asp:BoundField DataField="FacultyName" HeaderText="FacultyName" ReadOnly="True" SortExpression="FacultyName" />
                    <asp:BoundField DataField="LGA" HeaderText="LGA" ReadOnly="True" SortExpression="LGA" />
                    <asp:BoundField DataField="Gender" HeaderText="Gender" ReadOnly="True" SortExpression="Gender" />
                </Columns>

            </asp:GridView>
            <asp:LinqDataSource ID="LinqDataSource1" runat="server" ContextTypeName="School365.Model.StudentModel" EntityTypeName="" OrderBy="Surname" Select="new (StudentID, MatricNo, Surname, Firstname, Middlename, SOR, LGA, Gender, Email, Major, Minor, FacultyName, PhoneNumberNextOfKin)" TableName="Students">
            </asp:LinqDataSource>
        </div>
   

</asp:Content>
