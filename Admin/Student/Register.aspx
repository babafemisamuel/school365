﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="Register.aspx.cs" Inherits="School365.Admin.Student.Register1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="header" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="body" runat="server">
    <div class="row">
        <div class="large-10 columns">
            <asp:Button ID="submit" runat="server" Text="Submit" CssClass="button alert round" OnClick="submit_Click" />
            <asp:Button ID="CheckAll" runat="server" Text="Check All" CssClass="button success round" OnClick="CheckAll_Click" />
            <asp:GridView ID="allSubject" runat="server" AutoGenerateColumns="false">
                <Columns>
                    <asp:TemplateField HeaderText="Select">
                        <ItemTemplate>
                            <asp:CheckBox ID="Crs" runat="server" class="input-sm" />

                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="DepartmantID" />
                    <asp:BoundField DataField="DepartmentName" HeaderText="Department" />
                    <asp:BoundField DataField="SubjectID" />
                    <asp:BoundField DataField="SubjectName" HeaderText="Subject Name" />
                    <asp:BoundField DataField="SubjectCombinID" />
                    <asp:BoundField DataField="SubjectCombinName" HeaderText="Subject Combination" />
                    <asp:BoundField DataField="SubjectValue" HeaderText="Subject Value" />
                    <asp:BoundField DataField="SubjectUnit" HeaderText="Subject Unit" />
                </Columns>

            </asp:GridView>
        </div>
    </div>


</asp:Content>
