﻿using School365.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace School365.Admin.Student
{
    public partial class GenerateMatric : System.Web.UI.Page
    {
        //Need to put value as curent
        StudentModel db = new StudentModel();
        int getAdminID = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (User.Identity.IsAuthenticated == false)
                Response.Redirect("../Administrator/UserLogin.aspx");
            getAdminID = int.Parse(User.Identity.Name);
            var getAuth = (from d in db.RoleAdmins where d.AdminID == getAdminID select d.RoleID).ToList();
            if (getAuth.Contains(11) != true)
            {
                Response.Redirect("../Administrator/Profile.aspx");
            }  
            if (!IsPostBack)
            {
               
                //var getStudent = from d in db.Students where d.MatricNo == null && d.Registered=="True" orderby d.FacultyName ascending,d.Major ascending,d.Minor ascending,d.Surname ascending select new { d.StudentID, d.Firstname, d.Surname, d.MatricNo,d.FacultyName };
                //allStudents.DataSource = getStudent.ToList();
                //allStudents.DataBind();
            }
        }

        protected void matricGen_Click(object sender, EventArgs e)
        {
            Util util = new Util();
            util.GenerateMatric();

            var getStudent = from d in db.Students where d.Level==100 && d.Registered=="True" orderby d.FacultyName ascending,d.Major ascending,d.Minor ascending,d.Surname ascending select new { d.MatricNo, d.Firstname, d.Surname, d.Major, d.Minor, d.FacultyName };
            allStudents.DataSource = getStudent.ToList().OrderBy(a=>a.MatricNo);
            allStudents.DataBind();

            
        }
        public void GetStudentMatic()
        {
            var getStudent = from d in db.Students where d.Level == 100 && d.Registered == "True" orderby d.FacultyName ascending, d.Major ascending, d.Minor ascending, d.Surname ascending select new {d.MatricNo, d.Firstname, d.Surname,d.Major,d.Minor, d.FacultyName };
            allStudents.DataSource = getStudent.ToList().OrderBy(a => a.MatricNo);
            allStudents.DataBind();
        }

        protected void GetMatric_Click(object sender, EventArgs e)
        {
            GetStudentMatic();
        }
    }
}