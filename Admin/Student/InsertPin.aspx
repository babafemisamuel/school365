﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="InsertPin.aspx.cs" Inherits="School365.Admin.Student.InsertPin" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="header" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="body" runat="server">
    
     <hr />
    <asp:Button ID="submit" runat="server" Text="Submit" CssClass="button alert round" OnClick="submit_Click"/>
    <asp:GridView ID="allStudents" runat="server" AutoGenerateColumns="false" AllowPaging="True" OnPageIndexChanging="allStudents_PageIndexChanging" PageSize="50" Font-Size="Small" Width="826px">
        <Columns>             
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:TextBox ID="StudentID" runat="server" Visible="false"  Text='<%#Bind("StudentID") %>'> ></asp:TextBox>
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Surname">
                <ItemTemplate>
                    <asp:TextBox ID="Surname" runat="server" Visible="true" ReadOnly="true"  Text='<%#Bind("Surname") %>'> ></asp:TextBox>
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Firstname">
                <ItemTemplate>
                    <asp:TextBox ID="Firstname" runat="server" ReadOnly="true" Text='<%#Bind("Firstname") %>'> ></asp:TextBox>
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Othername">
                <ItemTemplate>
                    <asp:TextBox ID="Othername" runat="server" ReadOnly="true" Text='<%#Bind("Middlename") %>'>> </asp:TextBox>
                </ItemTemplate>
            </asp:TemplateField>

            
            <asp:TemplateField HeaderText="InvoiceNo">
                <ItemTemplate>
                    <asp:TextBox ID="InvoiceNo" runat="server" ReadOnly="true" Text='<%#Bind("InvoiceNumber") %>'>> </asp:TextBox>
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="MatricNo" ControlStyle-Width="150">
                <ItemTemplate>
                    <asp:TextBox ID="MatricNo" runat="server" ReadOnly="true" Text='<%#Bind("MatricNo") %>'>> </asp:TextBox>
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Pin" >
                <ItemTemplate>
                    <asp:TextBox ID="Password" runat="server" Text='<%#Bind("Password") %>'> </asp:TextBox>
                </ItemTemplate>
            </asp:TemplateField>

        </Columns>

    </asp:GridView>
</asp:Content>
