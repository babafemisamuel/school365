﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin_Layout.Master" AutoEventWireup="true" CodeBehind="ViewStudents.aspx.cs" Inherits="School365.Admin.Student.ViewStudents" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style2 {
            margin-top: 0px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="header" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="body" runat="server">
    <script type="text/javascript">
		
	</script>
    <div class="row">
        <div class="large-4 columns">
            <asp:TextBox ID="Keyword" runat="server" placeholder="Keyword"></asp:TextBox>
        </div>
        <div class="large-4 columns">
            <asp:Button ID="getStudents" runat="server" Text="Get Student" CssClass="tiny radius button bg-blue" OnClick="getStudents_Click" />
        </div>
        <div class="large-4 columns">
            <asp:Button ID="DeleteStd" runat="server" Text="Delete Students" CssClass="tiny radius button bg-red" OnClick="DeleteStd_Click" />
        </div>
    </div>

    <asp:GridView ID="AllStudents" runat="server" AutoGenerateColumns="false" AllowSorting="true" AllowPaging="true" OnPageIndexChanging="allStudents_PageIndexChanging" EmptyDataText="No Student Avaliable" CssClass="auto-style2" Width="826px">
        <Columns>
            <asp:TemplateField HeaderText="Select">
                <ItemTemplate>
                    <asp:CheckBox ID="Crs" runat="server" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemTemplate>
                    <%#Container.DataItemIndex+1 %>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="Surname" HeaderText="Surname" ReadOnly="true" SortExpression="Surname" />
            <asp:BoundField DataField="Firstname" HeaderText="Firstname" ReadOnly="true" SortExpression="Firstname" />
            <asp:BoundField DataField="Middlename" HeaderText="Middlename" ReadOnly="true" SortExpression="Middlename" />
            <asp:BoundField DataField="MatricNo" HeaderText="Matric Number" ReadOnly="true" SortExpression="MatricNo" />
            <asp:BoundField DataField="Gender" HeaderText="Gender" ReadOnly="true" SortExpression="Gender" />
            <asp:BoundField DataField="Major" HeaderText="Major" ReadOnly="true" SortExpression="Major" />
            <asp:BoundField DataField="Minor" HeaderText="Minor" ReadOnly="true" SortExpression="Minor" />
             <asp:BoundField DataField="StudentID" HeaderText="" ReadOnly="true" SortExpression="StudentID"  />
            <asp:HyperLinkField Text="Edit" DataNavigateUrlFields="StudentID" DataNavigateUrlFormatString="EditStudent.aspx?MatricNo={0}" />
        </Columns>
    </asp:GridView>

    <!--s/n, names,matric no, gender,level,major ,minor,edit and delete -->
</asp:Content>
