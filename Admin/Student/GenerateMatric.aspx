﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin_Layout.Master" AutoEventWireup="true" CodeBehind="GenerateMatric.aspx.cs" Inherits="School365.Admin.Student.GenerateMatric" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="body" runat="server">
    
     <hr />
    <div class="row">
       <div class="large-7">
           <asp:Button Text="Generate Matric" runat="server" ID="matricGen" CssClass="tiny radius button bg-blue" OnClick="matricGen_Click"/>
           <asp:Button Text="Get Matric" runat="server" ID="GetMatric" CssClass="tiny radius button bg-red" OnClick="GetMatric_Click"/>
        <asp:GridView ID="allStudents" runat="server" AutoGenerateColumns="false">
            <Columns>
                <asp:BoundField DataField="MatricNo" HeaderText="Matric No" />
               
                <asp:BoundField DataField="Firstname" HeaderText="FirstName" />
                <asp:BoundField DataField="Surname" HeaderText="LastName" />   
                <asp:BoundField DataField="Major" HeaderText="Major" />
                <asp:BoundField DataField="Minor" HeaderText="Minor" />
                
                 <asp:BoundField DataField="FacultyName" HeaderText="Faculty Name" />
            </Columns>
        </asp:GridView>
       </div>
    </div>
</asp:Content>
