﻿using School365.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace School365.Admin.Student
{
    public partial class StudentPerformance : System.Web.UI.Page
    {
        public StudentModel db = new StudentModel();
        public List<int> StudentID = new List<int>();
        public Util util = new Util();

        public List<Model.Student> allStudnet = new List<Model.Student>();
        public int SerialNo = 0;
        public int Semesters = 1;
        public int Levels =100;
        public int SessionsID = 4;
    
        int getAdminID = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            getAdminID = int.Parse(User.Identity.Name);
            var getAuth = (from d in db.RoleAdmins where d.AdminID == getAdminID select d.RoleID).ToList();
            if (getAuth.Contains(8) != true)
            {
                Response.Redirect("../Administrator/Profile.aspx");
            }
            allStudnet = (from d in db.Students select d).Distinct().OrderBy(a => a.MatricNo).ToList();
        }

        protected void getStds_Click(object sender, EventArgs e)
        {
            Semesters =int.Parse(AllSemester.SelectedItem.Text);
            //SubjectCombID = int.Parse(subComb.SelectedItem.Value);
            Levels = int.Parse(Level.SelectedItem.Text);

           // string [] _comb = subComb.SelectedItem.Text.Split('/');
            //string major = _comb[0];
            //string minor = _comb[1];

            allStudnet = (from d in db.Students select d).Distinct().OrderBy(a => a.MatricNo).OrderBy(a=>a.FacultyName).Take(20).ToList();
            SessionsID = int.Parse(Sessions.SelectedItem.Value);

        }
    }
}