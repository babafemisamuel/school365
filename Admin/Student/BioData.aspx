﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin_Layout.Master" AutoEventWireup="true" CodeBehind="BioData.aspx.cs" Inherits="School365.Admin.Student.BioData" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="header" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="body" runat="server">
    <hr />
    <h2>STUDENT DETAILS</h2>
    

    <hr />
    <h2>JAMB DETAILS</h2>
    <div class="row">
        <div class="column large-4">
            <asp:TextBox ID="RegNo" runat="server" placeholder="JAMB REGISTERATION NUMBER"></asp:TextBox>
            <br />
            <asp:TextBox ID="Center" runat="server" placeholder="CENTER NUMBER"></asp:TextBox>
            <br />
            <asp:TextBox ID="Year" runat="server" placeholder="YEAR"></asp:TextBox>
            <br />
        </div>
        <div class="column large-6">
            <asp:DropDownList ID="Subj1" runat="server">
                <asp:ListItem>Mathematics</asp:ListItem>
                <asp:ListItem>English Language</asp:ListItem>
                <asp:ListItem>Agricultural Science</asp:ListItem>
                <asp:ListItem>Applied Electricity</asp:ListItem>
                <asp:ListItem>Auto Mechanics</asp:ListItem>
                <asp:ListItem>Biology</asp:ListItem>
                <asp:ListItem>Building Construction</asp:ListItem>
                <asp:ListItem>Chemistry</asp:ListItem>
                <asp:ListItem>Christian Religious Knowledge</asp:ListItem>
                <asp:ListItem>Clothing and Textile</asp:ListItem>
                <asp:ListItem>Commerce</asp:ListItem>
                <asp:ListItem>Economics</asp:ListItem>
                <asp:ListItem>Electronics</asp:ListItem>
                <asp:ListItem>Financial Accounting</asp:ListItem>
                <asp:ListItem>Foods and Nutrition</asp:ListItem>
                <asp:ListItem>French</asp:ListItem>
                <asp:ListItem>Further Mathematics</asp:ListItem>
                <asp:ListItem>Geography</asp:ListItem>
                <asp:ListItem>Government</asp:ListItem>
                <asp:ListItem>Health Science</asp:ListItem>
                <asp:ListItem>History</asp:ListItem>
                <asp:ListItem>Home Management</asp:ListItem>
                <asp:ListItem>Islamic Studies</asp:ListItem>
                <asp:ListItem>Literature in English</asp:ListItem>
                <asp:ListItem>Metalwork</asp:ListItem>
                <asp:ListItem>Music</asp:ListItem>
                <asp:ListItem>Physical Education</asp:ListItem>
                <asp:ListItem>Physics</asp:ListItem>
                <asp:ListItem>Shorthand</asp:ListItem>
                <asp:ListItem>Technical Drawing</asp:ListItem>
                <asp:ListItem>Typewriting</asp:ListItem>
                <asp:ListItem>Visual Arts</asp:ListItem>
                <asp:ListItem>Woodwork</asp:ListItem>
            </asp:DropDownList>
            <br />
            <asp:DropDownList ID="Subj2" runat="server">
                <asp:ListItem>Mathematics</asp:ListItem>
                <asp:ListItem>English Language</asp:ListItem>
                <asp:ListItem>Agricultural Science</asp:ListItem>
                <asp:ListItem>Applied Electricity</asp:ListItem>
                <asp:ListItem>Auto Mechanics</asp:ListItem>
                <asp:ListItem>Biology</asp:ListItem>
                <asp:ListItem>Building Construction</asp:ListItem>
                <asp:ListItem>Chemistry</asp:ListItem>
                <asp:ListItem>Christian Religious Knowledge</asp:ListItem>
                <asp:ListItem>Clothing and Textile</asp:ListItem>
                <asp:ListItem>Commerce</asp:ListItem>
                <asp:ListItem>Economics</asp:ListItem>
                <asp:ListItem>Electronics</asp:ListItem>
                <asp:ListItem>Financial Accounting</asp:ListItem>
                <asp:ListItem>Foods and Nutrition</asp:ListItem>
                <asp:ListItem>French</asp:ListItem>
                <asp:ListItem>Further Mathematics</asp:ListItem>
                <asp:ListItem>Geography</asp:ListItem>
                <asp:ListItem>Government</asp:ListItem>
                <asp:ListItem>Health Science</asp:ListItem>
                <asp:ListItem>History</asp:ListItem>
                <asp:ListItem>Home Management</asp:ListItem>
                <asp:ListItem>Islamic Studies</asp:ListItem>
                <asp:ListItem>Literature in English</asp:ListItem>
                <asp:ListItem>Metalwork</asp:ListItem>
                <asp:ListItem>Music</asp:ListItem>
                <asp:ListItem>Physical Education</asp:ListItem>
                <asp:ListItem>Physics</asp:ListItem>
                <asp:ListItem>Shorthand</asp:ListItem>
                <asp:ListItem>Technical Drawing</asp:ListItem>
                <asp:ListItem>Typewriting</asp:ListItem>
                <asp:ListItem>Visual Arts</asp:ListItem>
                <asp:ListItem>Woodwork</asp:ListItem>
            </asp:DropDownList>
            <br />
            <asp:DropDownList ID="Subj3" runat="server">
                <asp:ListItem>Mathematics</asp:ListItem>
                <asp:ListItem>English Language</asp:ListItem>
                <asp:ListItem>Agricultural Science</asp:ListItem>
                <asp:ListItem>Applied Electricity</asp:ListItem>
                <asp:ListItem>Auto Mechanics</asp:ListItem>
                <asp:ListItem>Biology</asp:ListItem>
                <asp:ListItem>Building Construction</asp:ListItem>
                <asp:ListItem>Chemistry</asp:ListItem>
                <asp:ListItem>Christian Religious Knowledge</asp:ListItem>
                <asp:ListItem>Clothing and Textile</asp:ListItem>
                <asp:ListItem>Commerce</asp:ListItem>
                <asp:ListItem>Economics</asp:ListItem>
                <asp:ListItem>Electronics</asp:ListItem>
                <asp:ListItem>Financial Accounting</asp:ListItem>
                <asp:ListItem>Foods and Nutrition</asp:ListItem>
                <asp:ListItem>French</asp:ListItem>
                <asp:ListItem>Further Mathematics</asp:ListItem>
                <asp:ListItem>Geography</asp:ListItem>
                <asp:ListItem>Government</asp:ListItem>
                <asp:ListItem>Health Science</asp:ListItem>
                <asp:ListItem>History</asp:ListItem>
                <asp:ListItem>Home Management</asp:ListItem>
                <asp:ListItem>Islamic Studies</asp:ListItem>
                <asp:ListItem>Literature in English</asp:ListItem>
                <asp:ListItem>Metalwork</asp:ListItem>
                <asp:ListItem>Music</asp:ListItem>
                <asp:ListItem>Physical Education</asp:ListItem>
                <asp:ListItem>Physics</asp:ListItem>
                <asp:ListItem>Shorthand</asp:ListItem>
                <asp:ListItem>Technical Drawing</asp:ListItem>
                <asp:ListItem>Typewriting</asp:ListItem>
                <asp:ListItem>Visual Arts</asp:ListItem>
                <asp:ListItem>Woodwork</asp:ListItem>
            </asp:DropDownList>
            <br />
            <asp:DropDownList ID="Subj4" runat="server">
                <asp:ListItem>Mathematics</asp:ListItem>
                <asp:ListItem>English Language</asp:ListItem>
                <asp:ListItem>Agricultural Science</asp:ListItem>
                <asp:ListItem>Applied Electricity</asp:ListItem>
                <asp:ListItem>Auto Mechanics</asp:ListItem>
                <asp:ListItem>Biology</asp:ListItem>
                <asp:ListItem>Building Construction</asp:ListItem>
                <asp:ListItem>Chemistry</asp:ListItem>
                <asp:ListItem>Christian Religious Knowledge</asp:ListItem>
                <asp:ListItem>Clothing and Textile</asp:ListItem>
                <asp:ListItem>Commerce</asp:ListItem>
                <asp:ListItem>Economics</asp:ListItem>
                <asp:ListItem>Electronics</asp:ListItem>
                <asp:ListItem>Financial Accounting</asp:ListItem>
                <asp:ListItem>Foods and Nutrition</asp:ListItem>
                <asp:ListItem>French</asp:ListItem>
                <asp:ListItem>Further Mathematics</asp:ListItem>
                <asp:ListItem>Geography</asp:ListItem>
                <asp:ListItem>Government</asp:ListItem>
                <asp:ListItem>Health Science</asp:ListItem>
                <asp:ListItem>History</asp:ListItem>
                <asp:ListItem>Home Management</asp:ListItem>
                <asp:ListItem>Islamic Studies</asp:ListItem>
                <asp:ListItem>Literature in English</asp:ListItem>
                <asp:ListItem>Metalwork</asp:ListItem>
                <asp:ListItem>Music</asp:ListItem>
                <asp:ListItem>Physical Education</asp:ListItem>
                <asp:ListItem>Physics</asp:ListItem>
                <asp:ListItem>Shorthand</asp:ListItem>
                <asp:ListItem>Technical Drawing</asp:ListItem>
                <asp:ListItem>Typewriting</asp:ListItem>
                <asp:ListItem>Visual Arts</asp:ListItem>
                <asp:ListItem>Woodwork</asp:ListItem>
            </asp:DropDownList>
            <br />
        </div>
        <div class="column large-2">
            <asp:TextBox ID="Scr1" runat="server" placeholder="Score"></asp:TextBox>

            <asp:TextBox ID="Scr2" runat="server" placeholder="Score"></asp:TextBox>

            <asp:TextBox ID="Scr3" runat="server" placeholder="Score"></asp:TextBox>

            <asp:TextBox ID="Scr4" runat="server" placeholder="Score"></asp:TextBox>

        </div>
    </div>
    <hr />
    <h2>OLEVEL DETAILS</h2>
    <div class="row">
        <div class="column large-4">
            <asp:DropDownList ID="allExmTypr" runat="server">
                <asp:ListItem>NECO</asp:ListItem>
                <asp:ListItem>WAEC</asp:ListItem>
            </asp:DropDownList>

            <asp:TextBox ID="ExamCntr" runat="server" placeholder="EXAM CENTER"></asp:TextBox>
            <asp:TextBox ID="ExamYr" runat="server" placeholder="EXAM YEAR"></asp:TextBox>
            <asp:TextBox ID="CntrNo" runat="server" placeholder="CENTER NUMBER"></asp:TextBox>
            <asp:TextBox ID="ORegNo" runat="server" placeholder="REGISTERATION NUMBER"></asp:TextBox>
        </div>
        <div class="column large-6">
            <asp:DropDownList ID="oLeel1" runat="server">
            </asp:DropDownList>
            <asp:DropDownList ID="oLeel2" runat="server">
            </asp:DropDownList>  
            <asp:DropDownList ID="oLeel3" runat="server">
            </asp:DropDownList>  
            <asp:DropDownList ID="oLeel4" runat="server">
            </asp:DropDownList>  
            <asp:DropDownList ID="oLeel5" runat="server">
            </asp:DropDownList>  
            <asp:DropDownList ID="oLeel6" runat="server">
            </asp:DropDownList>  
            <asp:DropDownList ID="oLeel7" runat="server">
            </asp:DropDownList>  
            <asp:DropDownList ID="oLeel8" runat="server">
            </asp:DropDownList>  
            <asp:DropDownList ID="oLeel9" runat="server">
            </asp:DropDownList>                                  
        </div>
         <div class="column large-2">
              <asp:DropDownList ID="Grade1" runat="server">
                 <asp:ListItem>A1</asp:ListItem>
                 <asp:ListItem>B2</asp:ListItem>
                 <asp:ListItem>B3</asp:ListItem>
                 <asp:ListItem>C4</asp:ListItem>
                 <asp:ListItem>C5</asp:ListItem>
                 <asp:ListItem>C6</asp:ListItem>
                 <asp:ListItem>D7</asp:ListItem>
                 <asp:ListItem>E8</asp:ListItem>
                 <asp:ListItem>F9</asp:ListItem>
            </asp:DropDownList>   

             <asp:DropDownList ID="Grade2" runat="server">
                 <asp:ListItem>A1</asp:ListItem>
                 <asp:ListItem>B2</asp:ListItem>
                 <asp:ListItem>B3</asp:ListItem>
                 <asp:ListItem>C4</asp:ListItem>
                 <asp:ListItem>C5</asp:ListItem>
                 <asp:ListItem>C6</asp:ListItem>
                 <asp:ListItem>D7</asp:ListItem>
                 <asp:ListItem>E8</asp:ListItem>
                 <asp:ListItem>F9</asp:ListItem>
             </asp:DropDownList> 

              <asp:DropDownList ID="Grade3" runat="server">
                 <asp:ListItem>A1</asp:ListItem>
                 <asp:ListItem>B2</asp:ListItem>
                 <asp:ListItem>B3</asp:ListItem>
                 <asp:ListItem>C4</asp:ListItem>
                 <asp:ListItem>C5</asp:ListItem>
                 <asp:ListItem>C6</asp:ListItem>
                 <asp:ListItem>D7</asp:ListItem>
                 <asp:ListItem>E8</asp:ListItem>
                 <asp:ListItem>F9</asp:ListItem>
            </asp:DropDownList> 

              <asp:DropDownList ID="Grade4" runat="server">
                 <asp:ListItem>A1</asp:ListItem>
                 <asp:ListItem>B2</asp:ListItem>
                 <asp:ListItem>B3</asp:ListItem>
                 <asp:ListItem>C4</asp:ListItem>
                 <asp:ListItem>C5</asp:ListItem>
                 <asp:ListItem>C6</asp:ListItem>
                 <asp:ListItem>D7</asp:ListItem>
                 <asp:ListItem>E8</asp:ListItem>
                 <asp:ListItem>F9</asp:ListItem>
            </asp:DropDownList> 

              <asp:DropDownList ID="Grade5" runat="server">
                <asp:ListItem>A1</asp:ListItem>
                 <asp:ListItem>B2</asp:ListItem>
                 <asp:ListItem>B3</asp:ListItem>
                 <asp:ListItem>C4</asp:ListItem>
                 <asp:ListItem>C5</asp:ListItem>
                 <asp:ListItem>C6</asp:ListItem>
                 <asp:ListItem>D7</asp:ListItem>
                 <asp:ListItem>E8</asp:ListItem>
                 <asp:ListItem>F9</asp:ListItem>
            </asp:DropDownList> 

              <asp:DropDownList ID="Grade6" runat="server">
                 <asp:ListItem>A1</asp:ListItem>
                 <asp:ListItem>B2</asp:ListItem>
                 <asp:ListItem>B3</asp:ListItem>
                 <asp:ListItem>C4</asp:ListItem>
                 <asp:ListItem>C5</asp:ListItem>
                 <asp:ListItem>C6</asp:ListItem>
                 <asp:ListItem>D7</asp:ListItem>
                 <asp:ListItem>E8</asp:ListItem>
                 <asp:ListItem>F9</asp:ListItem>
            </asp:DropDownList> 

              <asp:DropDownList ID="Grade7" runat="server">
                <asp:ListItem>A1</asp:ListItem>
                 <asp:ListItem>B2</asp:ListItem>
                 <asp:ListItem>B3</asp:ListItem>
                 <asp:ListItem>C4</asp:ListItem>
                 <asp:ListItem>C5</asp:ListItem>
                 <asp:ListItem>C6</asp:ListItem>
                 <asp:ListItem>D7</asp:ListItem>
                 <asp:ListItem>E8</asp:ListItem>
                 <asp:ListItem>F9</asp:ListItem>
            </asp:DropDownList> 

              <asp:DropDownList ID="Grade8" runat="server">
                <asp:ListItem>A1</asp:ListItem>
                 <asp:ListItem>B2</asp:ListItem>
                 <asp:ListItem>B3</asp:ListItem>
                 <asp:ListItem>C4</asp:ListItem>
                 <asp:ListItem>C5</asp:ListItem>
                 <asp:ListItem>C6</asp:ListItem>
                 <asp:ListItem>D7</asp:ListItem>
                 <asp:ListItem>E8</asp:ListItem>
                 <asp:ListItem>F9</asp:ListItem>
            </asp:DropDownList> 

              <asp:DropDownList ID="Grade9" runat="server">
                <asp:ListItem>A1</asp:ListItem>
                 <asp:ListItem>B2</asp:ListItem>
                 <asp:ListItem>B3</asp:ListItem>
                 <asp:ListItem>C4</asp:ListItem>
                 <asp:ListItem>C5</asp:ListItem>
                 <asp:ListItem>C6</asp:ListItem>
                 <asp:ListItem>D7</asp:ListItem>
                 <asp:ListItem>E8</asp:ListItem>
                 <asp:ListItem>F9</asp:ListItem>
            </asp:DropDownList>                                
        </div>
       
    </div>

    <div class="row">
        <div class="columns large-6">
        </div>
        <div class="columns large-6">
            <asp:Button ID="Insert" runat="server" Text="Submit" CssClass="button" OnClick="Insert_Click" />
        </div>
    </div>
    

</asp:Content>
