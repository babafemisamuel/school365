﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin_Layout.Master" AutoEventWireup="true" CodeBehind="EditStudent.aspx.cs" Inherits="School365.Admin.Student.EditStudent" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="header" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="body" runat="server">
    <div class="row">
        <div class="large-4 columns">
            <asp:TextBox ID="Keyword" runat="server" placeholder="Keyword" ReadOnly="true"></asp:TextBox>
        </div>
        <div class="large-4 columns">
            <asp:Button ID="getStudents" runat="server" Text="Get Student" CssClass="tiny radius button bg-blue" OnClick="getStudents_Click" />
        </div>
        <div class="large-4 columns">
            <asp:FileUpload ID="getImage" runat="server" placeholder="Image Upload" />
        </div>
    </div>
   
    <fieldset>
        <legend>Update</legend>
        <asp:Label ID="key" runat="server" Text=" "></asp:Label>
        <div class="row">
            <div class="large-4 columns">
                <asp:TextBox ID="surname" runat="server" placeholder="Surname"></asp:TextBox>
            </div>
            <div class="large-4 columns">
                <asp:TextBox ID="firstname" runat="server" placeholder="Firstname"></asp:TextBox>
            </div>
            <div class="large-4 columns">
                <asp:TextBox ID="othername" runat="server" placeholder="Othername"></asp:TextBox>
            </div>
            <div class="large-4 columns">
                <asp:TextBox ID="matricText" runat="server" placeholder="Matric Text"></asp:TextBox>
            </div>
        </div>

        <div class="row">
            <div class="large-4 columns">
                <label>Country</label>
                <asp:DropDownList ID="country" runat="server">
                    <asp:ListItem>Nigeria</asp:ListItem>
                    <asp:ListItem>Others</asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="large-3 columns">
                <label>State Of Origin</label>
                <asp:DropDownList ID="sor" runat="server">
                    <asp:ListItem>Abia</asp:ListItem>
                    <asp:ListItem>Adamawa</asp:ListItem>
                    <asp:ListItem>Akwa-Ibom</asp:ListItem>
                    <asp:ListItem>Anambra</asp:ListItem>
                    <asp:ListItem>Abuja</asp:ListItem>
                    <asp:ListItem>Bauchi</asp:ListItem>
                    <asp:ListItem>Bayelsa</asp:ListItem>
                    <asp:ListItem>Benue</asp:ListItem>
                    <asp:ListItem>Borno</asp:ListItem>
                    <asp:ListItem>Cross River</asp:ListItem>
                    <asp:ListItem>Delta</asp:ListItem>
                    <asp:ListItem>Ebonyi</asp:ListItem>
                    <asp:ListItem>Edo</asp:ListItem>
                    <asp:ListItem>Ekiti</asp:ListItem>
                    <asp:ListItem>Enugu</asp:ListItem>
                    <asp:ListItem>Gombe</asp:ListItem>
                    <asp:ListItem>Imo</asp:ListItem>
                    <asp:ListItem>Jigawa</asp:ListItem>
                    <asp:ListItem>Kaduna</asp:ListItem>
                    <asp:ListItem>Kano</asp:ListItem>
                    <asp:ListItem>Katsina</asp:ListItem>
                    <asp:ListItem>Kebbi</asp:ListItem>
                    <asp:ListItem>Kogi</asp:ListItem>
                    <asp:ListItem>Kwara</asp:ListItem>
                    <asp:ListItem>Lagos</asp:ListItem>
                    <asp:ListItem>Nassarawa</asp:ListItem>
                    <asp:ListItem>Niger</asp:ListItem>
                    <asp:ListItem>Ogun</asp:ListItem>
                    <asp:ListItem>Ondo</asp:ListItem>
                    <asp:ListItem>Osun</asp:ListItem>
                    <asp:ListItem>Oyo</asp:ListItem>
                    <asp:ListItem>Plateau</asp:ListItem>
                    <asp:ListItem>Rivers</asp:ListItem>
                    <asp:ListItem>Sokoto</asp:ListItem>
                    <asp:ListItem>Taraba</asp:ListItem>
                    <asp:ListItem>Yobe</asp:ListItem>
                    <asp:ListItem>Zamfara</asp:ListItem>
                    <asp:ListItem>Others</asp:ListItem>
                </asp:DropDownList>
            </div>

            <div class="large-5 columns">
                <div class="row">
                    <label>LGA</label>
                    <div class="small-5 columns">
                        <asp:TextBox ID="lga" runat="server" placeholder="Local Government Area"></asp:TextBox>
                    </div>
                      <label>Propation</label>
                    <div class="small-7 columns">
                      
                        <asp:DropDownList ID="IsProbation" runat="server">
                            <asp:ListItem>True</asp:ListItem>
                            <asp:ListItem>False</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="large-4 columns">
                <asp:TextBox ID="email" runat="server" placeholder="Email Address"></asp:TextBox>
            </div>
            <div class="large-4 columns">
                <asp:TextBox ID="phoneNumber" runat="server" placeholder="Phone Number"></asp:TextBox>
            </div>
            <div class="large-4 columns">

                <asp:DropDownList ID="sex" runat="server">
                    <asp:ListItem>Male</asp:ListItem>
                    <asp:ListItem>Female</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>

        <div class="row">

            <div class="large-4 columns">
                <label>School</label>
                <asp:DropDownList ID="school" runat="server" placeholder="School">
                    <asp:ListItem Value="SCHOOL OF ARTS AND SOCIAL SCIENCES">SCHOOL OF ARTS AND SOCIAL SCIENCES</asp:ListItem>
                    <asp:ListItem Value="SCHOOL OF EDUCATION">SCHOOL OF EDUCATION</asp:ListItem>
                    <asp:ListItem Value="SCHOOL OF LANGUAGES">SCHOOL OF LANGUAGES</asp:ListItem>
                    <asp:ListItem Value="SCHOOL OF SCIENCES">SCHOOL OF SCIENCES</asp:ListItem>
                    <asp:ListItem Value="SCHOOL OF VOCATIONAL">SCHOOL OF VOCATIONAL</asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="large-4 columns">
                <label>Major</label>
                <asp:DropDownList ID="major" runat="server">
                    <asp:ListItem Value="AGE">AGE</asp:ListItem>
                    <asp:ListItem Value="ANF">ANF</asp:ListItem>
                    <asp:ListItem Value="ARB">ARB</asp:ListItem>
                    <asp:ListItem Value="BED">BED</asp:ListItem>
                    <asp:ListItem Value="BIO">BIO</asp:ListItem>
                    <asp:ListItem Value="CHE">CHE</asp:ListItem>
                    <asp:ListItem Value="CRS">CRS</asp:ListItem>
                    <asp:ListItem Value="CSC">CSC</asp:ListItem>
                    <asp:ListItem Value="ECE">ECE</asp:ListItem>
                    <asp:ListItem Value="ECO">ECO</asp:ListItem>
                    <asp:ListItem Value="EDU">EDU</asp:ListItem>
                    <asp:ListItem Value="ENG">ENG</asp:ListItem>
                    <asp:ListItem Value="FAA">FAA</asp:ListItem>
                    <asp:ListItem Value="FRE">FRE</asp:ListItem>
                    <asp:ListItem Value="GSE">GSE</asp:ListItem>
                    <asp:ListItem Value="HAU">HAU</asp:ListItem>
                    <asp:ListItem Value="HEC">HEC</asp:ListItem>
                    <asp:ListItem Value="IGBO">IGBO</asp:ListItem>
                    <asp:ListItem Value="ISC">ISC</asp:ListItem>
                    <asp:ListItem Value="ISS">ISS</asp:ListItem>
                    <asp:ListItem Value="MAT">MAT</asp:ListItem>
                    <asp:ListItem Value="MUS">MUS</asp:ListItem>
                    <asp:ListItem Value="PED">PED</asp:ListItem>
                    <asp:ListItem Value="PHE">PHE</asp:ListItem>
                    <asp:ListItem Value="PHY">PHY</asp:ListItem>
                    <asp:ListItem Value="POL">POL</asp:ListItem>
                    <asp:ListItem Value="SOS">SOS</asp:ListItem>
                    <asp:ListItem Value="THA">THA</asp:ListItem>
                    <asp:ListItem Value="YOR">YOR</asp:ListItem>
                </asp:DropDownList>
                <%--<asp:LinqDataSource ID="LinqDataSource2" runat="server" ContextTypeName="School365.Model.StudentModel" EntityTypeName="" Select="new (DepartmentCode)" TableName="Departments" OrderBy="DepartmentCode">
                </asp:LinqDataSource>--%>
            </div>

            <div class="large-4 columns">
                <label>Minor</label>
                <asp:DropDownList ID="minor" runat="server">
                     <asp:ListItem Value="AGE">AGE</asp:ListItem>
                    <asp:ListItem Value="ANF">ANF</asp:ListItem>
                    <asp:ListItem Value="ARB">ARB</asp:ListItem>
                    <asp:ListItem Value="BED">BED</asp:ListItem>
                    <asp:ListItem Value="BIO">BIO</asp:ListItem>
                    <asp:ListItem Value="CHE">CHE</asp:ListItem>
                    <asp:ListItem Value="CRS">CRS</asp:ListItem>
                    <asp:ListItem Value="CSC">CSC</asp:ListItem>
                    <asp:ListItem Value="ECE">ECE</asp:ListItem>
                    <asp:ListItem Value="ECO">ECO</asp:ListItem>
                    <asp:ListItem Value="EDU">EDU</asp:ListItem>
                    <asp:ListItem Value="ENG">ENG</asp:ListItem>
                    <asp:ListItem Value="FAA">FAA</asp:ListItem>
                    <asp:ListItem Value="FRE">FRE</asp:ListItem>
                    <asp:ListItem Value="GSE">GSE</asp:ListItem>
                    <asp:ListItem Value="HAU">HAU</asp:ListItem>
                    <asp:ListItem Value="HEC">HEC</asp:ListItem>
                    <asp:ListItem Value="IGBO">IGBO</asp:ListItem>
                    <asp:ListItem Value="ISC">ISC</asp:ListItem>
                    <asp:ListItem Value="ISS">ISS</asp:ListItem>
                    <asp:ListItem Value="MAT">MAT</asp:ListItem>
                    <asp:ListItem Value="MUS">MUS</asp:ListItem>
                    <asp:ListItem Value="PED">PED</asp:ListItem>
                    <asp:ListItem Value="PHE">PHE</asp:ListItem>
                    <asp:ListItem Value="PHY">PHY</asp:ListItem>
                    <asp:ListItem Value="POL">POL</asp:ListItem>
                    <asp:ListItem Value="SOS">SOS</asp:ListItem>
                    <asp:ListItem Value="THA">THA</asp:ListItem>
                    <asp:ListItem Value="YOR">YOR</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
      
        <hr />
        <h2>MORE DETAILS</h2>
        <div class="row">
        <div class="column large-8">
            <asp:TextBox ID="MotherName" runat="server" placeholder="Mother's Name"></asp:TextBox>
             <asp:TextBox ID="FormerFirstName" runat="server" placeholder="Former First Name"></asp:TextBox>
             <asp:TextBox ID="FormerMiddleName" runat="server" placeholder="Former Middle Name"></asp:TextBox>
             <asp:TextBox ID="FormerSurname" runat="server" placeholder="Former Surname Name"></asp:TextBox>
             <asp:TextBox ID="Saltn" runat="server" placeholder="Salutaion (Mr,Mrs,Dr, etc..)"></asp:TextBox>
            <asp:DropDownList ID="MaritalStatus" runat="server">
                <asp:ListItem>Single</asp:ListItem>
                <asp:ListItem>Married</asp:ListItem>
                <asp:ListItem>Divorced</asp:ListItem>
                <asp:ListItem>Complicated</asp:ListItem>
            </asp:DropDownList>
            <asp:DropDownList ID="Religion" runat="server">
                <asp:ListItem>Christianity</asp:ListItem>
                <asp:ListItem>Islam</asp:ListItem>
                <asp:ListItem>Other</asp:ListItem>
            </asp:DropDownList>
            <asp:TextBox ID="DateOfBirth" runat="server" placeholder="Date of Birth (Day/Month/Year)"></asp:TextBox>
            <asp:TextBox ID="PlaceOfBirth" runat="server" placeholder="Place Of Birth"></asp:TextBox>

            <asp:TextBox ID="HomeAddress" runat="server" placeholder="Home Address"></asp:TextBox>
            <asp:TextBox ID="StudentAddress" runat="server" placeholder="School Address"></asp:TextBox>
            <asp:TextBox ID="SponsorsName" runat="server" placeholder="Sponsors Name"></asp:TextBox>
            <asp:TextBox ID="SponsorsAddress" runat="server" placeholder="Sponsors Address"></asp:TextBox>
            <asp:TextBox ID="SponsorsPhoneNumber" runat="server" placeholder="SponsorsPhoneNumber"></asp:TextBox>

        </div>
            <div class="column large-4">
                <asp:Image ID="StudentImage" runat="server" />
            </div>
    </div>

        <div class="row">
            <div class="large-12 columns">
                <asp:Button ID="UpdateNew" runat="server" Text="Edit Student O'level " CssClass="tiny radius button bg-green" OnClick="UpdateNew_Click" />
                <asp:Button ID="AddNew" runat="server" Text="Add Student O'level" CssClass="tiny radius button bg-red" OnClick="AddNew_Click" />
                <asp:Button ID="Update" runat="server" Text="Update" CssClass="tiny radius button bg-blue" OnClick="submit_Click" />

            </div>
        </div>
    </fieldset>
</asp:Content>
