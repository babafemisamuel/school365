﻿using School365.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace School365.Admin.Student
{
    public partial class AddCourseForRegistered : System.Web.UI.Page
    {
        StudentModel db = new StudentModel();
        int getAdminID = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (User.Identity.IsAuthenticated == false)
                Response.Redirect("../Administrator/UserLogin.aspx");
            getAdminID = int.Parse(User.Identity.Name);
            var getAuth = (from d in db.RoleAdmins where d.AdminID == getAdminID select d.RoleID).ToList();
            if (getAuth.Contains(12) != true)
            {
                Response.Redirect("../Administrator/Profile.aspx");
            }  
            if (!IsPostBack)
            {
                var getSessionDetail = (from d in db.Sessions select d).Distinct().ToList();
                allSessions.DataTextField = "SessionYear";
                allSessions.DataValueField = "SessionID";
                allSessions.DataSource = getSessionDetail;
                allSessions.DataBind();               
            }
         
        }

        protected void GetStudents_Click(object sender, EventArgs e)
        {
            //|| d.Students.JambRegNo == MatricNo.Text.Trim() 
            var getAllSubjects = (from d in db.Results where d.MatricNo.Trim() == MatricNo.Text.Trim() && d.SessionID.ToString() == allSessions.SelectedItem.Value
                                  join f in db.Subjects on d.SubjectID equals f.SubjectID
                                  select new { d.MatricNo,f.SubjectCode,f.SubjectUnit,f.SubjectValue,f.SubjectLevel,d.ResultID,d.CA,d.EXAM}).OrderBy(a=>a.SubjectCode).ToList();
            if (getAllSubjects.Count()==0)
            {
                getAllSubjects = (from d in db.Results
                 where d.Students.JambRegNo == MatricNo.Text.Trim() && d.SessionID.ToString() == allSessions.SelectedItem.Value
                 join f in db.Subjects on d.SubjectID equals f.SubjectID
                 select new { d.MatricNo, f.SubjectCode, f.SubjectUnit, f.SubjectValue, f.SubjectLevel, d.ResultID, d.CA, d.EXAM }).OrderBy(a => a.SubjectCode).ToList();
            }
            allSubjects.DataSource = getAllSubjects;
            allSubjects.DataBind();
        }

        protected void DeleteData_Click(object sender, EventArgs e)
        {
            foreach (GridViewRow row in allSubjects.Rows)
            {
                if ((row.Cells[0].FindControl("Crs") as CheckBox).Checked)
                {
                    int ResultID =int.Parse(row.Cells[1].Text);
                    var Deletestudent = (from d in db.Results where d.MatricNo.Trim() == MatricNo.Text.Trim() || d.Students.JambRegNo==MatricNo.Text.Trim() && d.SessionID.ToString() == allSessions.SelectedItem.Value && d.ResultID == ResultID select d).First();
                    db.Results.Remove(Deletestudent);
                    db.SaveChanges();                 
                }
            }

        }

        protected void AddStudent_Click(object sender, EventArgs e)
        {
            var getStudent= (from d in db.Students where d.MatricNo.Trim()==MatricNo.Text.Trim() || d.JambRegNo==MatricNo.Text.Trim() select new {d.StudentID,d.Level,d.MatricNo}).FirstOrDefault();
            int SubjectID = int.Parse(allSubj.SelectedItem.Value);
            double getSubjectLevel=double.Parse(db.Subjects.Where(a => a.SubjectID == SubjectID).FirstOrDefault().SubjectLevel);
            double getSubjetValue = db.Subjects.Where(a => a.SubjectID == SubjectID).FirstOrDefault().SubjectValue;
            int Semester = int.Parse(db.Subjects.Where(a => a.SubjectID == SubjectID).FirstOrDefault().Semester);
            var getFromProfile = (from d in db.Results where d.MatricNo.Trim() == MatricNo.Text.Trim() || d.Students.JambRegNo==MatricNo.Text.Trim() select d.SubjectCombinID).FirstOrDefault();
            var InsertResult = new Result
                        {
                            MatricNo=getStudent.MatricNo,
                            StudentID = getStudent.StudentID,
                            SubjectCombinID =getFromProfile,
                            DepartmentID = int.Parse(allDept.SelectedItem.Value),
                            TNU= getSubjetValue,
                            SubjectID = int.Parse(allSubj.SelectedItem.Value),
                            Level = getSubjectLevel,                                                    
                            SessionID =int.Parse(allSessions.SelectedItem.Value),
                            Semester= Semester
            };
            db.Results.Add(InsertResult);
            db.SaveChanges();
            Response.Write("Subject Inserted");
        }
    }
}