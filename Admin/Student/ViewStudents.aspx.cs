﻿using School365.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace School365.Admin.Student
{
    public partial class ViewStudents : System.Web.UI.Page
    {
        StudentModel db = new StudentModel();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void getStudents_Click(object sender, EventArgs e)
        {
            StudentBind();
        }

        protected void DeleteStd_Click(object sender, EventArgs e)
        {
            foreach (GridViewRow row in AllStudents.Rows)
            {
                string StdMat = row.Cells[5].Text; 
                if ((row.Cells[0].FindControl("Crs") as CheckBox).Checked)
                {
                    var DeleteCourse = (from d in db.Students where d.MatricNo == StdMat  select d).FirstOrDefault();
                    db.Students.Remove(DeleteCourse);
                    db.SaveChanges();
                    Response.Write("Student Deleted");
                }

                if ((row.Cells[0].FindControl("Crs") as CheckBox).Checked)
                {
                    (row.Cells[0].FindControl("Crs") as CheckBox).Checked = false;
                }
            }
        }

        private void StudentBind()
        {
            var getStudents = (from d in db.Students
                               where d.MatricNo.Trim() == Keyword.Text ||
                                      
                                     d.JambRegNo== Keyword.Text.Trim() ||
                                   d.Surname.Trim().ToUpper() == Keyword.Text.Trim().ToUpper() ||
                                   d.Firstname.Trim().ToUpper() == Keyword.Text.Trim().ToUpper() ||
                                   d.Middlename.Trim().ToUpper() == Keyword.Text.Trim().ToUpper()
                               select new { d.Surname, d.Firstname, d.MatricNo, d.Middlename, d.Gender, d.Major, d.Minor,d.StudentID });
            AllStudents.DataSource = getStudents.OrderBy(a => a.MatricNo).ToList();
            AllStudents.DataBind();
        }

        //protected void AllStudents_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        //{
        //    AllStudents.PageIndex = e.NewPageIndex;
        //    StudentBind();
        //}
        protected void allStudents_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            AllStudents.PageIndex = e.NewPageIndex;
            StudentBind();

        }
       
    }
}