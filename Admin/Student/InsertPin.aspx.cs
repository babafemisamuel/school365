﻿using School365.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace School365.Admin.Student
{
    public partial class InsertPin : System.Web.UI.Page
    {
        StudentModel db = new StudentModel();
        int sn = 0;
        int getAdminID = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            getAdminID = int.Parse(User.Identity.Name);
            var getAuth = (from d in db.RoleAdmins where d.AdminID == getAdminID select d.RoleID).ToList();
            if (getAuth.Contains(11) != true)
            {
                Response.Redirect("../Administrator/Profile.aspx");
            }  
            if (!IsPostBack)
            {
                StudentBind();
            }
        }

        protected void submit_Click(object sender, EventArgs e)
        {
            foreach (GridViewRow row in allStudents.Rows)
            {
                
                TextBox pincode = (TextBox)row.FindControl("Password");
                TextBox id = (TextBox)row.FindControl("StudentID");
                //Response.Write(pincode.Text);
                //Response.Write(id.Text);

                var UpdatePin = (from d in db.Students where d.StudentID.ToString() == id.Text select d).First();
                UpdatePin.Password = pincode.Text;
                db.SaveChanges();
            }
        }

        protected void allStudents_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
           
        }

        protected void allStudents_PageIndexChanged(object sender, EventArgs e)
        {
            
        }

        protected void allStudents_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            allStudents.PageIndex = e.NewPageIndex;
            StudentBind();

        }
        private void StudentBind()
        {
            var getValue = from d in db.Students select new { d.StudentID, d.Surname, d.Firstname, d.Middlename, d.MatricNo, d.Password, d.InvoiceNumber };
            allStudents.DataSource = getValue.ToList();
            allStudents.DataBind();
        }
        
    }
}