﻿using School365.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace School365.Admin.Student
{
    public partial class BioData : System.Web.UI.Page
    {
        StudentModel db = new StudentModel();
        string newStudentID = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                newStudentID = Request.QueryString["UserValue"].ToString();
                string filePath = Server.MapPath("~/SecSubj.xml");
                using (DataSet ds = new DataSet())
                {
                    ds.ReadXml(filePath);
                    oLeel1.DataSource = ds;
                    oLeel1.DataTextField = "ListItem_Text";
                    oLeel1.DataBind();

                    oLeel2.DataSource = ds;
                    oLeel2.DataTextField = "ListItem_Text";
                    oLeel2.DataBind();

                    oLeel3.DataSource = ds;
                    oLeel3.DataTextField = "ListItem_Text";
                    oLeel3.DataBind();

                    oLeel4.DataSource = ds;
                    oLeel4.DataTextField = "ListItem_Text";
                    oLeel4.DataBind();

                    oLeel5.DataSource = ds;
                    oLeel5.DataTextField = "ListItem_Text";
                    oLeel5.DataBind();

                    oLeel6.DataSource = ds;
                    oLeel6.DataTextField = "ListItem_Text";
                    oLeel6.DataBind();

                    oLeel7.DataSource = ds;
                    oLeel7.DataTextField = "ListItem_Text";
                    oLeel7.DataBind();

                    oLeel8.DataSource = ds;
                    oLeel8.DataTextField = "ListItem_Text";
                    oLeel8.DataBind();

                    oLeel9.DataSource = ds;
                    oLeel9.DataTextField = "ListItem_Text";
                    oLeel9.DataBind();
                }
               
            }
            
        }

        protected void Insert_Click(object sender, EventArgs e)
        {
            var InsertData1 = new Model.JambDetail
            {
                RegNo = RegNo.Text,
                Center = Center.Text,
                Year = Year.Text,
                Subject = Subj1.SelectedItem.Text,
                Score = Scr1.Text,
                StudentID= int.Parse(newStudentID),
            };
            db.JambDetails.Add(InsertData1);
            db.SaveChanges();

            var InsertData2 = new Model.JambDetail
            {
                RegNo = RegNo.Text,
                Center = Center.Text,
                Year = Year.Text,
                Subject = Subj2.SelectedItem.Text,
                Score = Scr2.Text,
                StudentID = int.Parse(newStudentID),
            };
            db.JambDetails.Add(InsertData2);
            db.SaveChanges();

            var InsertData3 = new Model.JambDetail
            {
                RegNo = RegNo.Text,
                Center = Center.Text,
                Year = Year.Text,
                Subject = Subj3.SelectedItem.Text,
                Score = Scr3.Text,
                StudentID = int.Parse(newStudentID),
            };
            db.JambDetails.Add(InsertData3);
            db.SaveChanges();

            var InsertData4 = new Model.JambDetail
            {
                RegNo = RegNo.Text,
                Center = Center.Text,
                Year = Year.Text,
                Subject = Subj4.SelectedItem.Text,
                Score = Scr4.Text,
                StudentID = int.Parse(newStudentID),
            };
            db.JambDetails.Add(InsertData4);
            db.SaveChanges();
            //Response.Redirect("~/SecSubj.xml");
            var OInsertData1 = new Model.OlevelDetail
            {
                ExamType = allExmTypr.SelectedItem.Text,
                ExamCenter = ExamCntr.Text,
                ExamDate = ExamYr.Text,
                RegNo = ORegNo.Text,
                CenterNumber = CntrNo.Text,
                SubjectName = oLeel1.SelectedItem.Text,
                Grade=Grade1.SelectedItem.Text,
                StudentID = int.Parse(newStudentID)
            };
            db.OlevelDetails.Add(OInsertData1);
            db.SaveChanges();

            var OInsertData2 = new Model.OlevelDetail
            {
                ExamType = allExmTypr.SelectedItem.Text,
                ExamCenter = ExamCntr.Text,
                ExamDate = ExamYr.Text,
                RegNo = ORegNo.Text,
                CenterNumber = CntrNo.Text,
                SubjectName = oLeel2.SelectedItem.Text,
                Grade = Grade2.SelectedItem.Text,
                StudentID = int.Parse(newStudentID)
            };
            db.OlevelDetails.Add(OInsertData2);
            db.SaveChanges();

            var OInsertData3 = new Model.OlevelDetail
            {
                ExamType = allExmTypr.SelectedItem.Text,
                ExamCenter = ExamCntr.Text,
                ExamDate = ExamYr.Text,
                RegNo = ORegNo.Text,
                CenterNumber = CntrNo.Text,
                SubjectName = oLeel3.SelectedItem.Text,
                Grade = Grade3.SelectedItem.Text,
                StudentID = int.Parse(newStudentID)
            };
            db.OlevelDetails.Add(OInsertData3);
            db.SaveChanges();

            var OInsertData4 = new Model.OlevelDetail
            {
                ExamType = allExmTypr.SelectedItem.Text,
                ExamCenter = ExamCntr.Text,
                ExamDate = ExamYr.Text,
                RegNo = ORegNo.Text,
                CenterNumber = CntrNo.Text,
                SubjectName = oLeel4.SelectedItem.Text,
                Grade = Grade4.SelectedItem.Text,
                StudentID = int.Parse(newStudentID)
            };
            db.OlevelDetails.Add(OInsertData4);
            db.SaveChanges();

            var OInsertData5 = new Model.OlevelDetail
            {
                ExamType = allExmTypr.SelectedItem.Text,
                ExamCenter = ExamCntr.Text,
                ExamDate = ExamYr.Text,
                RegNo = ORegNo.Text,
                CenterNumber = CntrNo.Text,
                SubjectName = oLeel5.SelectedItem.Text,
                Grade = Grade5.SelectedItem.Text,
                StudentID = int.Parse(newStudentID)
            };
            db.OlevelDetails.Add(OInsertData5);
            db.SaveChanges();

            var OInsertData6 = new Model.OlevelDetail
            {
                ExamType = allExmTypr.SelectedItem.Text,
                ExamCenter = ExamCntr.Text,
                ExamDate = ExamYr.Text,
                RegNo = ORegNo.Text,
                CenterNumber = CntrNo.Text,
                SubjectName = oLeel6.SelectedItem.Text,
                Grade = Grade6.SelectedItem.Text,
                StudentID = int.Parse(newStudentID)
            };
            db.OlevelDetails.Add(OInsertData6);
            db.SaveChanges();

            var OInsertData7 = new Model.OlevelDetail
            {
                ExamType = allExmTypr.SelectedItem.Text,
                ExamCenter = ExamCntr.Text,
                ExamDate = ExamYr.Text,
                RegNo = ORegNo.Text,
                CenterNumber = CntrNo.Text,
                SubjectName = oLeel7.SelectedItem.Text,
                Grade = Grade7.SelectedItem.Text,
                StudentID = int.Parse(newStudentID)
            };
            db.OlevelDetails.Add(OInsertData7);
            db.SaveChanges();

            var OInsertData8 = new Model.OlevelDetail
            {
                ExamType = allExmTypr.SelectedItem.Text,
                ExamCenter = ExamCntr.Text,
                ExamDate = ExamYr.Text,
                RegNo = ORegNo.Text,
                CenterNumber = CntrNo.Text,
                SubjectName = oLeel8.SelectedItem.Text,
                Grade = Grade8.SelectedItem.Text,
                StudentID = int.Parse(newStudentID)
            };
            db.OlevelDetails.Add(OInsertData8);
            db.SaveChanges();

            var OInsertData9 = new Model.OlevelDetail
            {
                ExamType = allExmTypr.SelectedItem.Text,
                ExamCenter = ExamCntr.Text,
                ExamDate = ExamYr.Text,
                RegNo = ORegNo.Text,
                CenterNumber = CntrNo.Text,
                SubjectName = oLeel9.SelectedItem.Text,
                Grade = Grade9.SelectedItem.Text,
                StudentID = int.Parse(newStudentID)
            };
            db.OlevelDetails.Add(OInsertData9);
            db.SaveChanges();

            Response.Write("Data Inserted");
        }

        protected void oLeel1_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }
    }
}