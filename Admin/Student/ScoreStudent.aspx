﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin_Layout.Master" AutoEventWireup="true" CodeBehind="ScoreStudent.aspx.cs" Inherits="School365.Admin.Student.ScoreStudent" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="header" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="body" runat="server">
    <div>
    
    </div>
     <hr />
    <div class="row">
        <div class="large-4 columns">
        </div>
        <div class="large-4 columns">
            <label>Subject Combination</label>
            <asp:DropDownList ID="SubComb" runat="server">
            </asp:DropDownList>
            <asp:Button ID="getSubject" Text="Get Subjects" CssClass="tiny radius button bg-blue" runat="server" OnClick="getSubject_Click" />
        </div>
        <div class="large-4 columns">
            <label>Subjects</label>
            <asp:DropDownList ID="AllSubjects" runat="server">
            </asp:DropDownList>
            <asp:Button ID="ShowAllSubjects" Text="Show all students" CssClass="tiny radius button bg-light-green" runat="server" OnClick="ShowAllSubjects_Click" />
        </div>
        <div class="large-4 columns">
            <label>Year</label>
           <asp:DropDownList ID="AllSessions" runat="server" DataSourceID="LinqDataSource1" DataTextField="SessionYear" DataValueField="SessionID">
            </asp:DropDownList>
            
            <asp:LinqDataSource ID="LinqDataSource1" runat="server" ContextTypeName="School365.Model.StudentModel" EntityTypeName="" OrderBy="SessionYear" Select="new (SessionID, SessionYear)" TableName="Sessions">
            </asp:LinqDataSource>
            
        </div>
         <div class="large-4 columns">
             <asp:DropDownList ID="curr" runat="server">
                 
            </asp:DropDownList>
        </div>
    </div>
    <hr />
    <div class="row">
        <div class="column large-10">
            <asp:Button ID="insertScore" Text="Submit" CssClass="tiny radius button bg-red" runat="server" OnClick="insertScore_Click" />
            <asp:GridView ID="allStudents" runat="server" AutoGenerateColumns="false" AllowPaging="True" OnPageIndexChanging="allStudents_PageIndexChanging" PageSize="50">
                <Columns>

                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:TextBox ID="StudentID" runat="server" Visible="false" Text='<%#Bind("StudentID") %>'> ></asp:TextBox>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Surname">
                        <ItemTemplate>
                            <asp:TextBox ID="Surname" runat="server" Visible="true" ReadOnly="true" Text='<%#Bind("Surname") %>'> ></asp:TextBox>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Firstname">
                        <ItemTemplate>
                            <asp:TextBox ID="Firstname" runat="server" ReadOnly="true" Text='<%#Bind("Firstname") %>'> ></asp:TextBox>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="MatricNo" ControlStyle-Width="150">
                        <ItemTemplate>
                            <asp:TextBox ID="MatricNo" runat="server" ReadOnly="true" Text='<%#Bind("MatricNo") %>'> </asp:TextBox>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="CA">
                        <ItemTemplate>
                            <asp:TextBox ID="CA" runat="server" Text='<%#Bind("CA") %>'> </asp:TextBox>
                        </ItemTemplate>
                    </asp:TemplateField>

                     <asp:TemplateField HeaderText="EXAM">
                        <ItemTemplate>
                            <asp:TextBox ID="EXAM" runat="server" Text='<%#Bind("EXAM") %>'> </asp:TextBox>
                        </ItemTemplate>
                    </asp:TemplateField>

                </Columns>
                <PagerSettings Position="TopAndBottom" />
            </asp:GridView>
            <asp:Button ID="InsertScoreBtm" Text="Submit" CssClass="tiny radius button bg-red" runat="server" OnClick="insertScore_Click" />
        </div>
        <div class="large-2 column">
        </div>
    </div>
    <div class="row">     
        <div class="column large-10">            

        </div>
        <div class="column 2">

        </div>
        </div>


</asp:Content>
