﻿using School365.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using School365.Admin.Audit;

namespace School365.Admin.Student
{
    public partial class ScoreStudent : System.Web.UI.Page
    {
        StudentModel db = new StudentModel();
        int getAdminID = 0;
        UserTracker track = new UserTracker();
        protected void Page_Load(object sender, EventArgs e)
        {
            if(User.Identity.IsAuthenticated==false)
                Response.Redirect("../Administrator/UserLogin.aspx");
            getAdminID = int.Parse(User.Identity.Name);
            var getAuth = (from d in db.RoleAdmins where d.AdminID == getAdminID select d.RoleID).ToList();
            if (getAuth.Contains(12) != true)
            {
                Response.Redirect("../Administrator/Profile.aspx");
            }  
            if (!IsPostBack)
            {
                curr.DataSource = db.Curricullums.Select(a => new { a.CurricullumID, a.CurricullumName }).ToList();
                curr.DataTextField = "CurricullumName";
                curr.DataValueField = "CurricullumID";
                DataBind();

                SubComb.DataSource = (from d in db.SubjectCombinations select new { d.SubjectCombinName, d.SubjectCombinID }).Distinct().OrderBy(a=>a.SubjectCombinName).ToList();
                SubComb.DataTextField = "SubjectCombinName";
                SubComb.DataValueField = "SubjectCombinID";
                DataBind();

            }
            string getVal = System.Reflection.MethodBase.GetCurrentMethod().Name;
            string getClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.ToString();
            string LoggedName = db.Admin.Where(a => a.AdminID == getAdminID).Select(a => a.Username).FirstOrDefault();
            track.Track(LoggedName, "Accessed Score Students", getVal, getClass, "Score Students");
        }

        protected void getSubject_Click(object sender, EventArgs e)
        {
            var getSubjects = from d in db.AllCombineds 
                              where d.SubjectCombineID.ToString() == SubComb.SelectedItem.Value && d.CurricullumID.ToString()== curr.SelectedItem.Value
                              join f in db.Subjects on d.SubjectID equals f.SubjectID
                              select new { f.SubjectCode, f.SubjectID };
            AllSubjects.DataSource = getSubjects.OrderBy(a=>a.SubjectCode).ToList();
            AllSubjects.DataTextField = "SubjectCode";
            AllSubjects.DataValueField = "SubjectID";
            DataBind();

            string getVal = System.Reflection.MethodBase.GetCurrentMethod().Name;
            string getClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.ToString();
            string LoggedName = db.Admin.Where(a => a.AdminID == getAdminID).Select(a => a.Username).FirstOrDefault();
            track.Track(LoggedName, "Clicked"+ SubComb.SelectedItem.Text, getVal, getClass, "Score Students");
        }

        protected void ShowAllSubjects_Click(object sender, EventArgs e)
        {
            StudentDataBind();            
        }
        protected void insertScore_Click(object sender, EventArgs e)
        {
            //int getCurrentSession = (from d in db.Sessions where d.CurrentSession == true select d.SessionID).FirstOrDefault();
            foreach (GridViewRow row in allStudents.Rows)
            {
                TextBox CA = (TextBox)row.FindControl("CA");
                TextBox EXAM = (TextBox)row.FindControl("EXAM");
                TextBox id = (TextBox)row.FindControl("StudentID");
                var UpdateResult = (from d in db.Results where d.StudentID.ToString() == id.Text && d.SubjectID.ToString() == AllSubjects.SelectedItem.Value && d.SubjectCombinID.ToString() == SubComb.SelectedItem.Value && d.SessionID.ToString() == AllSessions.SelectedItem.Value select d).First();
                double _prevCa = UpdateResult.CA;
                double _prevExam = UpdateResult.EXAM;

                UpdateResult.CA = double.Parse(CA.Text);
                UpdateResult.EXAM = double.Parse(EXAM.Text);
                db.SaveChanges();
                if(double.Parse(CA.Text)!= _prevCa || double.Parse(EXAM.Text) != _prevExam)
                {
                    var std = db.Students.Where(a => a.StudentID.ToString() == id.Text).FirstOrDefault();
                    string getVal = System.Reflection.MethodBase.GetCurrentMethod().Name;
                    string getClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.ToString();
                    string LoggedName = db.Admin.Where(a => a.AdminID == getAdminID).Select(a => a.Username).FirstOrDefault();
                    if(double.Parse(CA.Text) != _prevCa)
                    {
                        string scoreadjusted = "adjusted ca from" + _prevCa + "to" + double.Parse(CA.Text);
                        string text = "Altered " + AllSubjects.SelectedItem.Text + " for " + std.Surname + " " + std.Firstname + " with matric number" + std.MatricNo + " ";
                        track.Track(LoggedName, text, getVal, getClass, "Score Students");
                    }
                    if (double.Parse(EXAM.Text) != _prevExam)
                    {
                        string scoreadjusted = "adjusted exam from" + _prevExam + "to" + double.Parse(EXAM.Text);
                        string text = "Altered " + AllSubjects.SelectedItem.Text + " for " + std.Surname + " " + std.Firstname + " with matric number" + std.MatricNo + " scoreadjusted ";
                        track.Track(LoggedName, text, getVal, getClass, "Score Students");
                    }

                    
                }
                
            }
            Response.Write("Data Inserted");
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            
        }

        protected void allStudents_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            allStudents.PageIndex = e.NewPageIndex;
            StudentDataBind();
        }
        private void StudentDataBind()
        {
            //int getCurrentSession = (from d in db.Sessions where d.CurrentSession == true select d.SessionID).FirstOrDefault();
            var getStudents = from d in db.Results
                              where d.SubjectID.ToString() == AllSubjects.SelectedItem.Value
                               && d.Students.CurriculumID.ToString()==curr.SelectedItem.Value
                              && d.SubjectCombinID.ToString() == SubComb.SelectedItem.Value
                              && d.SessionID.ToString()==AllSessions.SelectedItem.Value
                              join a in db.Students on d.StudentID equals a.StudentID
                              select new { a.StudentID, a.Firstname, a.Surname, a.MatricNo, d.CA, d.EXAM};
            if (getStudents != null)
            {
                allStudents.DataSource = getStudents.OrderBy(a=>a.MatricNo).ToList();
                allStudents.DataBind();
            }
            else
            {
                Response.Write("NO STUDENTS");
            }

        }

                    
    }
}