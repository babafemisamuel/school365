﻿using School365.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace School365.Admin.Student
{
    public partial class EditStudent : System.Web.UI.Page
    {
        StudentModel db = new StudentModel();
        int SubComb = 0;
        int StudentID = 0;
       static int StdID = 0;
        int getAdminID = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (User.Identity.IsAuthenticated == false)
                Response.Redirect("../Administrator/UserLogin.aspx");
            getAdminID = int.Parse(User.Identity.Name);
            var getAuth = (from d in db.RoleAdmins where d.AdminID == getAdminID select d.RoleID).ToList();
            if (getAuth.Contains(1) != true)
            {
                Response.Redirect("../Administrator/Profile.aspx");
            }

            if (!IsPostBack)
            {

                Keyword.Text = Request.QueryString["MatricNo"].ToString();
                if (Request.QueryString["MatricNo"].ToString() != null)
                {
                    int StudentID = int.Parse(Request.QueryString["MatricNo"].ToString());
                    var getStudents = (from d in db.Students
                                       where d.StudentID == StudentID ||
                                            d.JambRegNo == Keyword.Text ||
                                           d.Surname.Trim().ToUpper() == Keyword.Text.Trim().ToUpper() ||
                                           d.Firstname.Trim().ToUpper() == Keyword.Text.Trim().ToUpper() ||
                                           d.Middlename.Trim().ToUpper() == Keyword.Text.Trim().ToUpper()
                                       select d).FirstOrDefault();
                    if (getStudents == null)
                    {
                        Response.Write("No Data");
                    }
                    else
                    {
                        surname.Text = getStudents.Surname;
                        firstname.Text = getStudents.Firstname;
                        othername.Text = getStudents.Middlename;
                        matricText.Text = getStudents.MatricNo;
                        country.Text = getStudents.Nationality;
                        sor.Text = getStudents.SOR;
                        lga.Text = getStudents.LGA;
                        email.Text = getStudents.Email;
                        phoneNumber.Text = getStudents.PhoneNumber;
                        sex.Text = getStudents.Gender;
                        school.SelectedItem.Text = getStudents.FacultyName;
                        major.SelectedItem.Text = getStudents.Major;
                        minor.SelectedItem.Text = getStudents.Minor;

                        MotherName.Text = getStudents.MotherName;
                        FormerFirstName.Text = getStudents.FormerFirstame;
                        FormerMiddleName.Text = getStudents.FormerMiddlename;
                        FormerSurname.Text = getStudents.FormerSurname;
                        Saltn.Text = getStudents.Salution;
                        Religion.SelectedItem.Text = getStudents.Religion;
                        MaritalStatus.SelectedItem.Text = getStudents.MaritalStatus;
                        DateOfBirth.Text = getStudents.DateOfBirth;
                        PlaceOfBirth.Text = getStudents.PlaceOfBirth;
                        HomeAddress.Text = getStudents.HomeAddress;
                        StudentAddress.Text = getStudents.HomeAddress;
                        SponsorsName.Text = getStudents.SponsorsName;
                        SponsorsAddress.Text = getStudents.SponsorsAddress;
                        SponsorsPhoneNumber.Text = getStudents.SponsorsPhoneNumber;

                        if (getStudents.IsProbation==true)
                            IsProbation.Text = "True";
                        else
                            IsProbation.Text = "False";


                        StudentAddress.Text = getStudents.StudentAddress;
                        
                        StdID = getStudents.StudentID;
                        if (getStudents.StudentImage!=null)
                        {
                            StudentImage.ImageUrl = "data:image/jpeg;base64," + Convert.ToBase64String(getStudents.StudentImage);
                        }
                      
                        //incase
                        string subValue = major.Text + "/" + minor.Text;
                        SubComb = (from d in db.SubjectCombinations where d.SubjectCombinName == subValue select d.SubjectCombinID).FirstOrDefault();
                    }
                }
                else
                {
                    Keyword.Text = string.Empty;
                }
            }
            // Keyword.Text = Request.QueryString["MatricNo"].ToString();
        }

        protected void getStudents_Click(object sender, EventArgs e)
        {
            var getStudents = (from d in db.Students
                               where d.MatricNo.Trim() == Keyword.Text || d.StudentID.ToString() == Keyword.Text.Trim().ToUpper() ||
                                   d.Surname.Trim().ToUpper() == Keyword.Text.Trim().ToUpper() ||
                                   d.Firstname.Trim().ToUpper() == Keyword.Text.Trim().ToUpper() ||
                                   d.Middlename.Trim().ToUpper() == Keyword.Text.Trim().ToUpper()
                               select d).FirstOrDefault();
            if (getStudents == null)
            {
                Response.Write("No Data");
            }
            else
            {
                surname.Text = getStudents.Surname;
                firstname.Text = getStudents.Firstname;
                othername.Text = getStudents.Middlename;
                matricText.Text = getStudents.MatricNo;
                country.Text = getStudents.Nationality;
                sor.Text = getStudents.SOR;
                lga.Text = getStudents.LGA;
                email.Text = getStudents.Email;
                phoneNumber.Text = getStudents.PhoneNumber;
                sex.Text = getStudents.Gender;
                school.SelectedItem.Text = getStudents.FacultyName;
                major.SelectedItem.Text = getStudents.Major;
                minor.SelectedItem.Text = getStudents.Minor;
                SponsorsPhoneNumber.Text = getStudents.SponsorsPhoneNumber;
                SponsorsName.Text = getStudents.SponsorsName;
                SponsorsAddress.Text = getStudents.SponsorsAddress;

                if (getStudents.IsProbation == true)
                    IsProbation.Text = "True";
                else
                    IsProbation.Text = "False";

                //incase
                string subValue = major.Text + "/" + minor.Text;
                SubComb = (from d in db.SubjectCombinations where d.SubjectCombinName == subValue select d.SubjectCombinID).FirstOrDefault();
            }
            // Response.Write(SubComb);
        }
        protected void submit_Click(object sender, EventArgs e)
        {
            var UpdateStudent = (from d in db.Students
                                 where d.StudentID.ToString() == Keyword.Text.Trim()
                                 select d).FirstOrDefault();
            string FileName = " ";
            if (getImage.HasFile != null && getImage.PostedFile.ContentLength > 0)
            {
                FileName = getImage.FileName;
                byte[] ImageValue = getImage.FileBytes;
                UpdateStudent.ImageName = FileName;
                UpdateStudent.StudentImage = ImageValue;
                db.SaveChanges();
            }
            else
            {
                Response.Write("No Image is Selected");
            }

            var UpdateStudents = (from d in db.Students
                                  where d.StudentID.ToString() == Keyword.Text.Trim()
                                  select d).FirstOrDefault();
            UpdateStudents.Surname = surname.Text;
            UpdateStudents.Firstname = firstname.Text;
            UpdateStudents.Middlename = othername.Text;
            UpdateStudents.MatricNo = matricText.Text;

            if (IsProbation.SelectedItem.Text == "True")
                UpdateStudents.IsProbation = true;
            else
                UpdateStudents.IsProbation = false;
            UpdateStudents.Nationality = country.Text;
            UpdateStudents.SOR = sor.Text;
            UpdateStudents.LGA = lga.Text;
            UpdateStudents.Email = email.Text;
            UpdateStudents.PhoneNumber = phoneNumber.Text;
            UpdateStudents.Gender = sex.Text;
            UpdateStudents.FacultyName = school.SelectedItem.Text;
            UpdateStudents.Major = major.SelectedItem.Text;
            UpdateStudents.Minor = minor.SelectedItem.Text;

            UpdateStudents.MotherName = MotherName.Text;
            UpdateStudents.FormerFirstame = FormerFirstName.Text;
            UpdateStudents.FormerMiddlename = FormerMiddleName.Text;
            UpdateStudents.FormerSurname = FormerSurname.Text;
            UpdateStudents.Salution = Saltn.Text;
            UpdateStudents.Religion = Religion.SelectedItem.Text;
            UpdateStudents.MaritalStatus = MaritalStatus.SelectedItem.Text;
            UpdateStudents.DateOfBirth = DateOfBirth.Text;
            UpdateStudents.PlaceOfBirth = PlaceOfBirth.Text;
            UpdateStudents.StudentAddress = StudentAddress.Text;
            UpdateStudents.HomeAddress = HomeAddress.Text;
            UpdateStudents.SponsorsAddress = SponsorsAddress.Text;
            UpdateStudents.SponsorsName = SponsorsName.Text;
            UpdateStudents.SponsorsPhoneNumber = SponsorsPhoneNumber.Text;
           
            StudentID = UpdateStudents.StudentID;
            db.SaveChanges();

            var UpdateProfile = (from d in db.Results where d.StudentID == StudentID select d).FirstOrDefault();
            if (UpdateProfile != null)
            {
                UpdateProfile.MatricNo = matricText.Text;
                db.SaveChanges();
            }
            Response.Write("Data Updated");
        }

        protected void UpdateNew_Click(object sender, EventArgs e)
        {
            string parameter = "UserValue=" + StdID;
            Response.Redirect("UpdateBioData.aspx?"+parameter);
        }

        protected void AddNew_Click(object sender, EventArgs e)
        {
            string parameter = "UserValue=" + StdID;
            Response.Redirect("BioData.aspx?" + parameter);
        }
    }
}