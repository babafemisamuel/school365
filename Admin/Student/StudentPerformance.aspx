﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="StudentPerformance.aspx.cs" Inherits="School365.Admin.Student.StudentPerformance" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../../assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <link href="../../css/dataTables.bootstrap.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <%--<asp:DropDownList ID="allSchools" runat="server" DataSourceID="LinqDataSource1" DataTextField="FacultyName" DataValueField="FacultyID"></asp:DropDownList>
        <asp:LinqDataSource ID="LinqDataSource1" runat="server" ContextTypeName="School365.Model.StudentModel" EntityTypeName="" OrderBy="FacultyID" Select="new (FacultyID, FacultyName)" TableName="Facultys">
        </asp:LinqDataSource>--%>
        <%--<asp:DropDownList ID="subComb" runat="server" DataSourceID="LinqDataSource2" DataTextField="SubjectCombinName" DataValueField="SubjectCombinID">

        </asp:DropDownList>--%>

        <asp:DropDownList ID="Level" runat="server">
            <asp:ListItem>100</asp:ListItem>
            <asp:ListItem>200</asp:ListItem>
            <asp:ListItem>300</asp:ListItem>
            <asp:ListItem>400</asp:ListItem>
            <asp:ListItem>500</asp:ListItem>
            <asp:ListItem>600</asp:ListItem>
        </asp:DropDownList>
        <asp:DropDownList ID="AllSemester" runat="server">
            <asp:ListItem>1</asp:ListItem>
            <asp:ListItem>2</asp:ListItem>

        </asp:DropDownList>

        <asp:LinqDataSource ID="LinqDataSource2" runat="server" ContextTypeName="School365.Model.StudentModel" EntityTypeName="" Select="new (SubjectCombinID, SubjectCombinName)" TableName="SubjectCombinations" OrderBy="SubjectCombinName">
        </asp:LinqDataSource>

        <asp:DropDownList ID="Sessions" runat="server" DataSourceID="LinqDataSource1" DataTextField="SessionYear" DataValueField="SessionID">
        </asp:DropDownList>
        <asp:LinqDataSource ID="LinqDataSource1" runat="server" ContextTypeName="School365.Model.StudentModel" EntityTypeName="" OrderBy="SessionYear" Select="new (SessionID, SessionYear)" TableName="Sessions">
        </asp:LinqDataSource>
        <asp:Button ID="getStds" runat="server" Text="Get Students" OnClick="getStds_Click" />
        <table id="table" class="display" style="width: 100%; margin-top: 5px; border-top: 1px solid #aaa; border-bottom: 1px solid #aaa; font-size: x-small;" border="0" cellspacing="0">
            <thead>
                <tr>
                    <th style="border-bottom: 1px solid #aaa; border-top: 1px solid #aaa; border-left: 1px solid #aaa; border-right: 1px solid #aaa;" class="auto-style1" colspan="">S/N</th>
                    <th style="border-bottom: 1px solid #aaa; border-top: 1px solid #aaa; border-left: 1px solid #aaa; border-right: 1px solid #aaa;" class="auto-style1" colspan="">Matric No</th>
                    <th style="border-bottom: 1px solid #aaa; border-top: 1px solid #aaa; border-left: 1px solid #aaa; border-right: 1px solid #aaa;" class="auto-style1" colspan="">Surname</th>
                    <th style="border-bottom: 1px solid #aaa; border-top: 1px solid #aaa; border-left: 1px solid #aaa; border-right: 1px solid #aaa;" class="auto-style1" colspan="">Firstname</th>
                    <th style="border-bottom: 1px solid #aaa; border-top: 1px solid #aaa; border-left: 1px solid #aaa; border-right: 1px solid #aaa;" class="auto-style1" colspan="">Lastname</th>
                    <th style="border-bottom: 1px solid #aaa; border-top: 1px solid #aaa; border-left: 1px solid #aaa; border-right: 1px solid #aaa;" class="auto-style1" colspan="">Major</th>
                    <th style="border-bottom: 1px solid #aaa; border-top: 1px solid #aaa; border-left: 1px solid #aaa; border-right: 1px solid #aaa;" class="auto-style1" colspan="">Minor</th>
                    <th style="border-bottom: 1px solid #aaa; border-top: 1px solid #aaa; border-left: 1px solid #aaa; border-right: 1px solid #aaa;" class="auto-style1" colspan="">SubjectCombinations</th>
                    <th style="border-bottom: 1px solid #aaa; border-top: 1px solid #aaa; border-left: 1px solid #aaa; border-right: 1px solid #aaa;" class="auto-style1" colspan="">Faculty</th>
                    <th style="border-bottom: 1px solid #aaa; border-top: 1px solid #aaa; border-left: 1px solid #aaa; border-right: 1px solid #aaa;" class="auto-style1" colspan="">CGPA</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <%
                        foreach (var setStudentID in allStudnet)
                        {
                            var getStudent = (from d in db.Students where d.StudentID == setStudentID.StudentID select new { d.Surname, d.Firstname, d.Middlename, d.FacultyName, d.MatricNo, d.Major, d.Minor }).OrderBy(s => s.FacultyName).FirstOrDefault();
                            SerialNo = SerialNo + 1;
                            double cgpa = util.AllTcp(Levels, setStudentID.StudentID, Semesters) / util.AllTnu(Levels, setStudentID.StudentID, Semesters, SessionsID);
                            if (cgpa.ToString() == "NaN")
                                cgpa = 0;
                    %>
                    <td style="border-bottom: 1px solid #aaa; border-top: 1px solid #aaa; border-left: 1px solid #aaa; border-right: 1px solid #aaa;" class="auto-style1" colspan=""><%Response.Write(SerialNo); %></td>
                    <td style="border-bottom: 1px solid #aaa; border-top: 1px solid #aaa; border-left: 1px solid #aaa; border-right: 1px solid #aaa;" class="auto-style1" colspan=""><%Response.Write(getStudent.Surname); %></td>
                    <td style="border-bottom: 1px solid #aaa; border-top: 1px solid #aaa; border-left: 1px solid #aaa; border-right: 1px solid #aaa;" class="auto-style1" colspan=""><%Response.Write(getStudent.Firstname); %></td>
                    <td style="border-bottom: 1px solid #aaa; border-top: 1px solid #aaa; border-left: 1px solid #aaa; border-right: 1px solid #aaa;" class="auto-style1" colspan=""><%Response.Write(getStudent.Middlename); %></td>
                    <td style="border-bottom: 1px solid #aaa; border-top: 1px solid #aaa; border-left: 1px solid #aaa; border-right: 1px solid #aaa;" class="auto-style1" colspan=""><%Response.Write(getStudent.MatricNo); %></td>
                    <td style="border-bottom: 1px solid #aaa; border-top: 1px solid #aaa; border-left: 1px solid #aaa; border-right: 1px solid #aaa;" class="auto-style1" colspan=""><%Response.Write(getStudent.Major); %></td>
                    <td style="border-bottom: 1px solid #aaa; border-top: 1px solid #aaa; border-left: 1px solid #aaa; border-right: 1px solid #aaa;" class="auto-style1" colspan=""><%Response.Write(getStudent.Minor); %></td>
                    <td style="border-bottom: 1px solid #aaa; border-top: 1px solid #aaa; border-left: 1px solid #aaa; border-right: 1px solid #aaa;" class="auto-style1" colspan=""><%Response.Write(getStudent.Major + "/" + getStudent.Minor); %></td>
                    <td style="border-bottom: 1px solid #aaa; border-top: 1px solid #aaa; border-left: 1px solid #aaa; border-right: 1px solid #aaa;" class="auto-style1" colspan=""><%Response.Write(getStudent.FacultyName); %></td>
                    <td style="border-bottom: 1px solid #aaa; border-top: 1px solid #aaa; border-left: 1px solid #aaa; border-right: 1px solid #aaa;" class="auto-style1" colspan=""><%Response.Write(string.Format("{0:0.00}", cgpa)); %></td>
                </tr>
                <% } %>
            </tbody>
        </table>
    </form>
    <script src="../../assets/web/assets/jquery/jquery.min.js"></script>
    <script src="../../js/foundation/jquery.dataTables.min.js"></script>
    <script src="../../js/foundation/dataTables.bootstrap.min.js"></script>

    <script>
        $(document).ready(function () {
            $('#table').DataTable(
                {
                    "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]]
                });
        });
    </script>
</body>


</html>
