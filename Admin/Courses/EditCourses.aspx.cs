﻿using School365.Model;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace School365.Admin.Courses
{
    public partial class EditCourses : System.Web.UI.Page
    {
        StudentModel db = new StudentModel();
        public List<Subject> subject = new List<Subject>();
        int getAdminID = 0;
            
        protected void Page_Load(object sender, EventArgs e)
        {
            if (User.Identity.IsAuthenticated == false)
                Response.Redirect("../Administrator/UserLogin.aspx");
            getAdminID = int.Parse(User.Identity.Name);
            var getAuth = (from d in db.RoleAdmins where d.AdminID == getAdminID select d.RoleID).ToList();
            if (getAuth.Contains(3)!=true)
            {
                Response.Redirect("../Administrator/Profile.aspx");
            }           
        }
        protected void GetStudent_Click(object sender, EventArgs e)
        {
            DataBind();
            //d.SubjectCode.Substring(0,x)
        }


        protected void UpdateBtn_Click(object sender, EventArgs e)
        {           
            foreach (GridViewRow row in allSubjects.Rows)
            {
                TextBox SubjectID = (TextBox)row.FindControl("SubjectID");                
                TextBox SubjectName = (TextBox)row.FindControl("SubjectName");
                TextBox SubjectCode = (TextBox)row.FindControl("SubjectCode");
                TextBox SubjectValue = (TextBox)row.FindControl("SubjectValue");
                TextBox SubjectUnit = (TextBox)row.FindControl("SubjectUnit");
                TextBox SubjectLevel = (TextBox)row.FindControl("SubjectLevel");
                TextBox Semester = (TextBox)row.FindControl("Semester");
                TextBox Active = (TextBox)row.FindControl("Active");

                var UpdateSubject = db.Subjects.Where(a => a.SubjectID.ToString() == SubjectID.Text.ToString()).First();
                UpdateSubject.SubjectName = SubjectName.Text;
                UpdateSubject.SubjectCode = SubjectCode.Text;
                UpdateSubject.SubjectValue = double.Parse(SubjectValue.Text);
                UpdateSubject.SubjectUnit = SubjectUnit.Text;
                UpdateSubject.SubjectLevel = SubjectLevel.Text;
                UpdateSubject.Semester = Semester.Text;
                if (Active.Text.ToUpper()=="TRUE")
                {
                    UpdateSubject.Active = true;
                }
                else
                {
                    UpdateSubject.Active = false;
                }
                db.SaveChanges();
                Response.Write("Inserted");
            }
           
        }

        protected void Delete_Click(object sender, EventArgs e)
        {
            foreach (GridViewRow row in allSubjects.Rows)
            {
                if ((row.Cells[0].FindControl("Crs") as CheckBox).Checked)
                {
                    TextBox SubjectID = (TextBox)row.FindControl("SubjectID");   
                    var Deletestudent = (from d in db.Subjects where d.SubjectID.ToString() == SubjectID.Text select d).First();
                    db.Subjects.Remove(Deletestudent);
                    db.SaveChanges();                   
                }
                Response.Write("Deleted");
            }
        }

        protected void allSubjects_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            
        }

        protected void allSubjects_RowEditing(object sender, GridViewEditEventArgs e)
        {          
            DataBind();

        }

        protected void allSubjects_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {

        }

        protected void allSubjects_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {

        }

        protected void allSubjects_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {

        }
        public void DataBind()
        {
            string courseCode = CourseCode.Text.Trim().ToUpper();
            string[] getPrefix = Regex.Split(courseCode, " ");
            int x = getPrefix[0].Length;
            subject = (from d in db.Subjects where d.SubjectCode.Trim().ToUpper() == CourseCode.Text.Trim().ToUpper() || d.SubjectName.Trim().ToUpper() == CourseCode.Text.Trim() || d.SubjectUnit.ToString() == CourseCode.Text.Trim() || d.SubjectCode.Substring(0, x).ToUpper() == CourseCode.Text.Trim().ToUpper() select d).OrderBy(s => s.SubjectCode).ToList();
            allSubjects.DataSource = subject;
            allSubjects.DataBind();
        }
    }
}