﻿using School365.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

namespace School365.Admin.Courses
{
    public partial class AddCourses : System.Web.UI.Page
    {
        StudentModel db= new StudentModel();
        string sex = "";
        string InvoiceNo = "";
        int getAdminID = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (User.Identity.IsAuthenticated == false)
                Response.Redirect("../Administrator/UserLogin.aspx");
            getAdminID = int.Parse(User.Identity.Name);
            var getAuth = (from d in db.RoleAdmins where d.AdminID == getAdminID select d.RoleID).ToList();
            if (getAuth.Contains(3) !=true)
            {
                Response.Redirect("../Administrator/Profile.aspx");
            }

            //var LoggedName = (from d in db.Admin where d.AdminID.ToString() == getAdminID.ToString() select d.Username).FirstOrDefault();
            //string log = "UserName : " + LoggedName + "  " + " Date:" + DateTime.Now.ToString() + "Event : Courses Page";

            //string path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            //using (StreamWriter writer = new StreamWriter(path + @"\Logger.txt", true))
            //{
            //    writer.Write(log);
            //}
        }

        protected void submit_Click(object sender, EventArgs e)
        {
            //db = new StudentModel();
            //try
            //{
            

            string[] readCsv = System.IO.File.ReadAllLines(Server.MapPath("SchoolFile/EDUCATION.csv"));
            for (int i = 0; i < readCsv.Length; i++)
            {
                string[] members = readCsv[i].Split(',');
                InvoiceNo = System.Guid.NewGuid().ToString();
                if (members[4].ToString() == "M")
                {
                    sex = "MALE";
                }
                else if (members[4].ToString() == "F")
                {
                    sex = "FEMALE";
                }
                else
                {
                    sex = "MALE";
                }
                var InsertStudents = new Model.Student
                {
                    Surname = members[0].ToString(),
                    Firstname = members[1].ToString(),
                    Middlename = members[2].ToString(),
                    PhoneNumber = members[3].ToString(),
                    Gender =sex,
                    Nationality = "NIGERIA",
                    Major = members[5].ToString(),
                    Minor = members[6].ToString(),
                    PhoneNumberNextOfKin = members[7].ToString(),
                    SOR = members[8].ToString().ToUpper(),
                    FacultyName = "SCHOOL OF EDUCATION",
                    Registered = "False",
                    Level = 100,
                    StudentKey = InvoiceNo,                              
                };
                db.Students.Add(InsertStudents);
                db.SaveChanges();
                Response.Write("Data Inserted");
           
                }
        }

        protected void insertBtn_Click(object sender, EventArgs e)
        {
            db = new StudentModel();
            bool boolValue = false;
            if (Active.SelectedItem.Text == "TRUE")
            {
                boolValue = true;
            }
            else
            {
                boolValue = false;
            }
            var InsertSubject = new Subject
              {
                  SubjectName = SubjectName.Text.ToUpper(),
                  SubjectCode = SubjectCode.Text.ToUpper(),
                  SubjectUnit = SubjectUnit.SelectedItem.Text,
                  SubjectValue = double.Parse(SubjectValue.SelectedItem.Text),
                  Active = boolValue,
                  Semester = Semester.SelectedItem.Text,
                  SubjectLevel = SubjectLevel.SelectedItem.Text,
              };
            db.Subjects.Add(InsertSubject);
            db.SaveChanges();
            Response.Write("INSERTED");
            SubjectName.Text = "";
            SubjectCode.Text = "";
        }
    }
}