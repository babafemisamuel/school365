﻿using School365.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace School365.Admin.SubjectCombinations
{

    public partial class EditSubComb : System.Web.UI.Page
    {
        StudentModel db = new StudentModel();
        int getAdminID = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (User.Identity.IsAuthenticated == false)
                Response.Redirect("../Administrator/UserLogin.aspx");
            getAdminID = int.Parse(User.Identity.Name);
            var getAuth = (from d in db.RoleAdmins where d.AdminID == getAdminID select d.RoleID).ToList();

            var DeleteSubComb = (from d in db.AllCombineds where d.CurricullumID == null select d).ToList();
            db.AllCombineds.RemoveRange(DeleteSubComb);
            db.SaveChanges();

            if (getAuth.Contains(1) != true)
            {
                Response.Redirect("../Administrator/Profile.aspx");
            }

        }

        protected void getAllSubjects_Click(object sender, EventArgs e)
        {
            bindData();
        }

        protected void DeleteSUbject_Click(object sender, EventArgs e)
        {
            foreach (GridViewRow row in allSub.Rows)
            {
                if ((row.Cells[0].FindControl("Crs") as CheckBox).Checked == true)
                {
                    int SubjectID = int.Parse(row.Cells[7].Text);
                    int SubCombID = int.Parse(row.Cells[1].Text);

                    string CurriculumName = row.Cells[9].Text;
                    int CurriculumID = db.Curricullums.Where(a => a.CurricullumName == CurriculumName).FirstOrDefault().CurricullumID;


                    var DeleteSubComb = (from d in db.AllCombineds where d.SubjectID == SubjectID && d.SubjectCombineID == SubCombID && d.CurricullumID == CurriculumID select d).ToList();
                    db.AllCombineds.RemoveRange(DeleteSubComb);
                    db.SaveChanges();




                }
            }
        }
        private void bindData()
        {
            var getAllSubjects = (from d in db.AllCombineds
                                  where d.SubjectCombineID.ToString() == allSubComb.SelectedItem.Value && d.DepartmentID.ToString() == allDept.SelectedItem.Value
                                  join f in db.Departments on d.DepartmentID equals f.DepartmantID
                                  join g in db.SubjectCombinations on d.SubjectCombineID equals g.SubjectCombinID
                                  join h in db.Subjects on d.SubjectID equals h.SubjectID
                                  select new { d.SubjectCombineID, f.DepartmentCode, f.DepartmantID, g.SubjectCombinName, g.SubjectCombinID, h.SubjectCode, h.SubjectID, d.SpecialCase, d.Curricullums.CurricullumName }).OrderBy(a => a.SubjectCombinName).ToList();
            allSub.DataSource = getAllSubjects;
            allSub.DataBind();
        }

        protected void UpdateSubject_Click(object sender, EventArgs e)
        {
            foreach (GridViewRow row in allSub.Rows)
            {
                if ((row.Cells[0].FindControl("Crs") as CheckBox).Checked == true)
                {
                    int SubjectID = int.Parse(row.Cells[7].Text);
                    int SubCombID = int.Parse(row.Cells[1].Text);
                    var UpdateIndo = (from d in db.AllCombineds where d.SubjectID == SubjectID && d.SubjectCombineID == SubCombID select d).FirstOrDefault();
                    if (UpdateIndo.SpecialCase == false)
                    {
                        UpdateIndo.SpecialCase = true;
                    }
                    else
                    {
                        UpdateIndo.SpecialCase = false;
                    }
                  
                    db.SaveChanges();
                    Response.Write("Data Updated");

                }
            }
        }
    }
}