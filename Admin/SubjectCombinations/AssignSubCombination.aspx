﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin_Layout.Master" AutoEventWireup="true" CodeBehind="AssignSubCombination.aspx.cs" Inherits="School365.Admin.SubjectCombinations.AssignSubCombination" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="header" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="body" runat="server">
    <div class="row">
        <div class="large-3 columns">
            <label>Curriculum</label>
            <asp:DropDownList ID="curr" runat="server">
            </asp:DropDownList>
        </div>
        <div class="large-3 columns">
            <label>Subject Combination</label>
            <asp:DropDownList ID="SubComb" runat="server">
            </asp:DropDownList>
        </div>

        <div class="large-3 columns">
            <label>Departments</label>
            <asp:DropDownList ID="Dept" runat="server">
            </asp:DropDownList>
        </div>
        <div class="large-3 columns">
            <asp:Button ID="searchBtn" runat="server" CssClass="radius button bg-light-red" Text="Search Subjects" OnClick="searchBtn_Click" />
        </div>
    </div>

    <div class="row">
        <br />
        <div class="large-12 columns" role="grid">
            <asp:Button ID="submit" runat="server" CssClass=" radius button bg-light-green" Text="Submit" OnClick="submit_Click" />
            <asp:GridView runat="server" style="width:100%" ID="allSubjects" AutoGenerateColumns="false">
                <Columns>
                    <asp:TemplateField HeaderText="Select">
                        <ItemTemplate>
                            <asp:CheckBox ID="Crs" runat="server" class="input-sm" />
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:BoundField DataField="SubjectID" HeaderText="Subject ID" />
                    <asp:BoundField DataField="SubjectCode" HeaderText="SubjectCode" />
                    <asp:BoundField DataField="SubjectName" HeaderText="Subject Name" />
                    <asp:BoundField DataField="SubjectLevel" HeaderText="Subject Level" />
                </Columns>
            </asp:GridView>
        </div>
    </div>
</asp:Content>
