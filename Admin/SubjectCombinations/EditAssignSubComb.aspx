﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin_Layout.Master" AutoEventWireup="true" CodeBehind="EditAssignSubComb.aspx.cs" Inherits="School365.Admin.SubjectCombinations.EditSubComb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="header" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="body" runat="server">
    <div class="row">
        <div class="large-4 columns">
             <asp:DropDownList ID="allSubComb" runat="server" DataSourceID="LinqDataSource1" DataTextField="SubjectCombinName" DataValueField="SubjectCombinID" ></asp:DropDownList>
             <asp:LinqDataSource ID="LinqDataSource1" runat="server" ContextTypeName="School365.Model.StudentModel" EntityTypeName="" OrderBy="SubjectCombinName" Select="new (SubjectCombinID, SubjectCombinName)" TableName="SubjectCombinations">
             </asp:LinqDataSource>
        </div>
         <div class="large-4 columns">
             <asp:DropDownList ID="allDept" runat="server" DataSourceID="LinqDataSource2" DataTextField="DepartmentCode" DataValueField="DepartmantID" ></asp:DropDownList>            
             
             <asp:LinqDataSource ID="LinqDataSource2" runat="server" ContextTypeName="School365.Model.StudentModel" EntityTypeName="" OrderBy="DepartmentCode" Select="new (DepartmantID, DepartmentCode)" TableName="Departments">
             </asp:LinqDataSource>
             
        </div>
        <div class="large-4 columns">
            <asp:Button ID="getAllSubjects" runat="server" Text="Get Subjects" CssClass="radius button bg-light-green" OnClick="getAllSubjects_Click"/>
        </div>
    </div>
    <br />

    <div class="row">
        <div class="large-12">
            <asp:GridView ID="allSub" runat="server" >
                <Columns>
                     <asp:TemplateField HeaderText="Select Special Case">
                        <ItemTemplate>
                            <asp:CheckBox ID="Crs" runat="server" class="input-sm" />

                        </ItemTemplate>
                    </asp:TemplateField>
                     
                <%-- <asp:TemplateField HeaderText="Special Case">
                        <ItemTemplate>
                            <asp:CheckBox ID="SpecialCase" runat="server"  />
                        </ItemTemplate>
                    </asp:TemplateField>   --%>       
                </Columns>
            </asp:GridView>
        </div>
    </div>
    <br />
   <asp:Button ID="DeleteSUbject" runat="server" Text="Un Assign" CssClass="radius button bg-light-red" OnClick="DeleteSUbject_Click" />
    <asp:Button ID="UpdateSubject" runat="server" Text="Update" CssClass="radius button bg-light-green" OnClick="UpdateSubject_Click"  />


</asp:Content>
