﻿using School365.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace School365.Admin.SubjectCombinations
{
    public partial class AssignSubCombination : System.Web.UI.Page
    {
        StudentModel db = new StudentModel();
        int getAdminID = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (User.Identity.IsAuthenticated == false)
                Response.Redirect("../Administrator/UserLogin.aspx");
            getAdminID = int.Parse(User.Identity.Name);
            var getAuth = (from d in db.RoleAdmins where d.AdminID == getAdminID select d.RoleID).ToList();
            if (getAuth.Contains(1) != true)
            {
                Response.Redirect("../Administrator/Profile.aspx");
            }  
            if (!IsPostBack)
            {
                curr.DataSource = db.Curricullums.Select(a => new { a.CurricullumID, a.CurricullumName }).ToList();
                curr.DataTextField = "CurricullumName";
                curr.DataValueField = "CurricullumID";
                DataBind();

                SubComb.DataSource = (from d in db.SubjectCombinations select new { d.SubjectCombinName, d.SubjectCombinID }).Distinct().OrderBy(x=>x.SubjectCombinName).ToList();
                SubComb.DataTextField = "SubjectCombinName";
                SubComb.DataValueField = "SubjectCombinID";
                DataBind();

               Dept.DataSource = (from g in db.Departments select new { g.DepartmantID, g.DepartmentCode }).Distinct().OrderBy(x=>x.DepartmentCode).ToList();
               Dept.DataTextField = "DepartmentCode";
               Dept.DataValueField = "DepartmantID";
               DataBind();

               var setSubjects = (from d in db.Subjects select new { d.SubjectID,d.SubjectCode, d.SubjectName, d.SubjectLevel }).Distinct().ToList().OrderBy(o=>o.SubjectCode);
               allSubjects.DataSource = setSubjects.ToList();
               DataBind();
            }            
        }

        protected void submit_Click(object sender, EventArgs e)
        {
            foreach (GridViewRow row in allSubjects.Rows)
            {
                if ((row.Cells[0].FindControl("Crs") as CheckBox).Checked)
                {
                    var InsertAllCombined = new AllCombined
                    {
                        SubjectCombineID = int.Parse(SubComb.SelectedItem.Value),
                        SubjectID = int.Parse(row.Cells[1].Text),
                        DepartmentID = int.Parse(Dept.SelectedItem.Value),
                        CurricullumID=int.Parse(curr.SelectedItem.Value),
                                              
                    };
                    db.AllCombineds.Add(InsertAllCombined);
                    db.SaveChanges();
                }
            }

            foreach (GridViewRow item in allSubjects.Rows)
            {
                if ((item.Cells[0].FindControl("Crs") as CheckBox).Checked == true)
                {
                    (item.Cells[0].FindControl("Crs") as CheckBox).Checked = false;
                }
            }
            Response.Write("Data Inserted Successfully");
        }

        protected void searchBtn_Click(object sender, EventArgs e)
        {
            string deptName = Dept.SelectedItem.Text;
            string[] getPrefix = Regex.Split(deptName, " ");
            int x = getPrefix[0].Length;

            var setSubjects = (from d in db.Subjects where d.SubjectCode.Substring(0,x)==Dept.SelectedItem.Text select new { d.SubjectID, d.SubjectCode, d.SubjectName, d.SubjectLevel }).Distinct().ToList().OrderBy(o => o.SubjectCode);
            allSubjects.DataSource = setSubjects.ToList();
            DataBind();

            foreach (GridViewRow item in allSubjects.Rows)
            {
                if ((item.Cells[0].FindControl("Crs") as CheckBox).Checked == false)
                {
                    (item.Cells[0].FindControl("Crs") as CheckBox).Checked = true;
                }
            }
            
        }

      
         
    }
}