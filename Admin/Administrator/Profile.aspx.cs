﻿using School365.Admin.Audit;
using School365.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace School365.Admin.Administrator
{
    public partial class Profile : System.Web.UI.Page
    {
        StudentModel db = new StudentModel();
        int getAdminID = 0;
        UserTracker track = new UserTracker();
        protected void Page_Load(object sender, EventArgs e)
        {
            //if (!IsPostBack)
            //{

            //}
            if (User.Identity.IsAuthenticated == false)
            {
                Response.Write("You do not have access ......please contact admin");
            }
            if (User.Identity.Name == "")
                Response.Redirect("UserLogin.aspx");

            getAdminID = int.Parse(User.Identity.Name);

            int getSessionID = (from d in db.Sessions where d.CurrentSession == true select d.SessionID).First();
            int getTotalStundents = (from d in db.Results where d.SessionID == getSessionID select d.StudentID).Distinct().Count();
            int getDepartments = (from d in db.Departments select d.DepartmantID).Distinct().Count();
            int getFaculty = (from d in db.Facultys select d.FacultyID).Distinct().Count();
            int getCourses = (from d in db.Subjects select d.SubjectID).Distinct().Count();
            int getAdmin = (from d in db.Admin select d.AdminID).Distinct().Count();
            int getLecturers = (from d in db.Lecturers select d.LecturerID).Distinct().Count();

            StudentNo.Text=getTotalStundents.ToString();
            department.Text = getDepartments.ToString();
            faculty.Text = getFaculty.ToString();
            courses.Text = getCourses.ToString();
            Admin.Text = getAdmin.ToString();
            Lectures.Text = getLecturers.ToString();

            string getVal = System.Reflection.MethodBase.GetCurrentMethod().Name;
            string getClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.ToString();
            var LoggedName = (from d in db.Admin where d.AdminID.ToString() == getAdminID.ToString() select  d.Username).FirstOrDefault();
            track.Track(LoggedName, "Access his/her Profile", getVal, getClass, "User Profile");
           
        }

        protected void addUser_Click(object sender, EventArgs e)
        {
           
            var getAuth = (from d in db.RoleAdmins where d.AdminID == getAdminID select d.RoleID).ToList();
            if (getAuth.Contains(9))
            {
                string getVal = System.Reflection.MethodBase.GetCurrentMethod().Name;
                string getClass = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.ToString();
                string LoggedName = db.Admin.Where(a => a.AdminID == getAdminID).Select(a => a.Username).FirstOrDefault();
                track.Track(LoggedName, "Access his/her Profile", getVal, getClass, "User Profile");
                Response.Redirect("Add.aspx");
            }
            else
            {
                Response.Write("You do not have access ......please contact admin");
            }
            
        }
    }
}