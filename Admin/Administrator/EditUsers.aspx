﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin_Layout.Master" AutoEventWireup="true" CodeBehind="EditUsers.aspx.cs" Inherits="School365.Admin.Administrator.EditUsers" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="header" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="body" runat="server">
    <asp:GridView ID="alUsers" runat="server" width="100%" AutoGenerateColumns="false" AllowPaging="True"  PageSize="50" Font-Size="Small" >
        <Columns>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:TextBox ID="AdminID" runat="server" Visible="false" Text='<%#Bind("AdminID") %>'> ></asp:TextBox>
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Username">
                <ItemTemplate>
                    <asp:TextBox ID="Username" runat="server" Visible="true" ReadOnly="true" Text='<%#Bind("Username") %>'> ></asp:TextBox>
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Password">
                <ItemTemplate>
                    <asp:TextBox ID="Password" runat="server"  Text='<%#Bind("Password") %>'> ></asp:TextBox>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
   <asp:Button ID="updateInfo" runat="server" CssClass="tiny radius button bg-light-green" Text="Update" OnClick="updateInfo_Click" />
</asp:Content>
