﻿using School365.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace School365.Admin.Administrator
{
    public partial class EditUsers : System.Web.UI.Page
    {
        int Keyword = 0;
        StudentModel db = new StudentModel();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (User.Identity.IsAuthenticated == false)
                    Response.Redirect("Login.aspx");
                Keyword =int.Parse(User.Identity.Name);
                if (Request.QueryString["AdminID"] == null)
                   Response.Redirect("AllUsers");

                var getUser = db.Admin.Where(a => a.AdminID == Keyword).Select(a => a).ToList();
                alUsers.DataSource = getUser;
                alUsers.DataBind();
            }
           
        }

        protected void allStudents_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

        }

        protected void updateInfo_Click(object sender, EventArgs e)
        {
            foreach (GridViewRow row in alUsers.Rows)
            {
                TextBox password = (TextBox)row.FindControl("Password");
                TextBox AdminID = (TextBox)row.FindControl("AdminID");
                int admin=int.Parse(AdminID.Text);
                var getUser = db.Admin.Where(a => a.AdminID == admin).Select(a => a).First();
                getUser.Password = password.Text;
                db.SaveChanges();
                Response.Write("password updated");
            }
        }
    }
}