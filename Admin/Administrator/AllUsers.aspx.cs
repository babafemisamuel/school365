﻿using School365.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace School365.Admin.Administrator
{
    public partial class AllUsers : System.Web.UI.Page
    {
        StudentModel db = new StudentModel();
        protected void Page_Load(object sender, EventArgs e)
        {
           AllAdmin.DataSource=db.Admin.Select(a => new { a.AdminID,a.Username }).ToList();
           AllAdmin.DataBind();
        }
    }
}