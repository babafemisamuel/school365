﻿using School365.Model;
using School365.Students;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace School365.Admin.Administrator
{
    public partial class UserLogin : System.Web.UI.Page
    {
        StudentModel db = new StudentModel();
        private Model.Admin AdminAuth;
        School365.Students.StdGen user = new School365.Students.StdGen();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Login_Click(object sender, EventArgs e)
        {
            int id = user.AdminAuth(UserName.Text, Passsword.Text);
            
            if (id == 0)
            {
                errorDisplay.Text = "Username or Password is incorrect";
            }
            else
            {
                FormsAuthentication.SetAuthCookie(id.ToString(), false);
                FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(
                    1,
                    id.ToString(),
                    DateTime.Now,
                    DateTime.Now.AddMinutes(10),
                    false,
                    "admin"
                    );
                HttpCookie cookie = new HttpCookie(
                    FormsAuthentication.FormsCookieName,
                    FormsAuthentication.Encrypt(ticket));
                Response.Cookies.Add(cookie);
                //Response.Cookies["AdminID"].Value = id.ToString();
                // string parameter = "grab=" + AdminAuth.AdminID;
                Response.Redirect("Profile.aspx");
            }

        }
    }
}