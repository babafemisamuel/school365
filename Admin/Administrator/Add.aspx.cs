﻿using School365.Admin.Audit;
using School365.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace School365.Admin.Administrator
{
    public partial class Add : System.Web.UI.Page
    {
        StudentModel db = new StudentModel();
        UserTracker track = new UserTracker();
        protected void Page_Load(object sender, EventArgs e)
        {
            //Password.Text = Guid.NewGuid().ToString().Substring(0, new Guid().ToString().IndexOf("-"));

        }

        protected void InsertAdmin_Click(object sender, EventArgs e)
        {
            var InsertUser = new Model.Admin
            {
                Username = UserName.Text,
                Password = Password.Text,
                AdminKey = Guid.NewGuid().ToString().Substring(0, new Guid().ToString().IndexOf("-")),
            };
            db.Admin.Add(InsertUser);
            db.SaveChanges();

            var getUserName = (from d in db.Admin where d.Username == UserName.Text select d.AdminID).FirstOrDefault();

            foreach (GridViewRow rows in allRoles.Rows)
            {
                if ((rows.Cells[0].FindControl("Crs") as CheckBox).Checked)
                {
                    int RoleID = int.Parse(rows.Cells[1].Text);
                    var InsertRole = new Model.RoleAdmin
                    {
                        RoleID = RoleID,
                        AdminID = getUserName,
                    };
                    db.RoleAdmins.Add(InsertRole);
                    db.SaveChanges();
                }
            }
            Response.Write("User Inserted");
        }
    }
}