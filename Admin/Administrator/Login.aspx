﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="School365.Admin.Administrator.Login" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="header" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="body" runat="server">
         

    <div class="row">

        <fieldset>
            <legend>LOGIN</legend>
            <asp:Label ID="errorDisplay" runat="server" Text=" " Style="color: darkred; font-style: italic; font-size: small; font-weight: 700" />
            <div class="large-4 columns">
                <asp:TextBox ID="invoiceNum" runat="server" placeholder="User Name"></asp:TextBox>
                <asp:TextBox ID="pin" runat="server" TextMode="Password" placeholder="Password"></asp:TextBox>
                <asp:Button ID="submit" runat="server" CssClass="button" Style="float: right;" Text="Login" OnClick="submit_Click" />
            </div>
            <div class="large-4 columns">
                <asp:Image ID="LoginImage" runat="server" ImageUrl="~/Image/loginPic.jpg" />
            </div>
        </fieldset>
    </div>
</asp:Content>
