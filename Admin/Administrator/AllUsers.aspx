﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin_Layout.Master" AutoEventWireup="true" CodeBehind="AllUsers.aspx.cs" Inherits="School365.Admin.Administrator.AllUsers" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="header" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="body" runat="server">
      <asp:GridView ID="AllAdmin" runat="server" width="100%" AutoGenerateColumns="false" AllowSorting="true" AllowPaging="true" EmptyDataText="No User Avaliable">
        <Columns>
            <asp:TemplateField HeaderText="Select">
                <ItemTemplate>
                    <asp:CheckBox ID="Crs" runat="server" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemTemplate>
                    <%#Container.DataItemIndex+1 %>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="Username" HeaderText="Username" ReadOnly="true" SortExpression="Surname" />
            <asp:BoundField DataField="AdminID" Visible="false" HeaderText="UserID" ReadOnly="true" SortExpression="Surname" />            
            <asp:HyperLinkField Text="Edit" ControlStyle-CssClass="tiny radius button bg-light-green" DataNavigateUrlFields="AdminID" DataNavigateUrlFormatString="EditUsers.aspx?AdminID={0}" />
        </Columns>
    </asp:GridView>
</asp:Content>
