﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin_Layout.Master" AutoEventWireup="true" CodeBehind="Add.aspx.cs" Inherits="School365.Admin.Administrator.Add" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="header" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="body" runat="server">
    <div class="row">
        <div class="large-3 columns">
            <asp:TextBox ID="UserName" runat="server" placeholder="Username"></asp:TextBox>
        </div>

        <div class="large-3 columns">
            <asp:TextBox ID="Password" runat="server" placeholder="Password"></asp:TextBox>
        </div>
        <div class="large-3 columns">
           <asp:Button ID="InsertAdmin" runat="server" CssClass="tiny radius button bg-light-green" Text="Insert"  Style="float: left" OnClick="InsertAdmin_Click" />
        </div>
        <div class="large-3 columns">
            
        </div>
    </div>
    <div class="row">

        
        <div class="large-12 columns">
            <div>
                <asp:GridView ID="allRoles" runat="server" style="width:100%" AutoGenerateColumns="False" DataSourceID="LinqDataSource1">
                    <Columns>
                        <asp:TemplateField HeaderText="Select">
                            <ItemTemplate>
                                <asp:CheckBox ID="Crs" runat="server" class="input-sm" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="RoleID" Visible="false" HeaderText="RoleID" ReadOnly="True" SortExpression="RoleID" />
                        <asp:BoundField DataField="RoleName" HeaderText="Role Name" ReadOnly="True" SortExpression="RoleName" />
                    </Columns>
                </asp:GridView>
                <asp:LinqDataSource ID="LinqDataSource1" runat="server" ContextTypeName="School365.Model.StudentModel" EntityTypeName="" Select="new (RoleID, RoleName)" TableName="Roles">
                </asp:LinqDataSource>

            </div>
            
        </div>
    </div>

</asp:Content>
