﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin_Layout.Master" AutoEventWireup="true" CodeBehind="AllDepartments.aspx.cs" Inherits="School365.Admin.Departments.AllDepartments" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="header" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="body" runat="server">
     <div class="row">
        <div class="large-12">
            <asp:GridView ID="allDepts" style="width:100%" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" DataSourceID="LinqDataSource1">
                <Columns>
                    <asp:BoundField DataField="DepartmentName" HeaderText="Department Name" ReadOnly="True" SortExpression="DepartmentName" />
                    <asp:BoundField DataField="DepartmentCode" HeaderText="Department Code" ReadOnly="True" SortExpression="DepartmentCode" />
                </Columns>

            </asp:GridView>
            <asp:LinqDataSource ID="LinqDataSource1" runat="server" ContextTypeName="School365.Model.StudentModel" EntityTypeName="" OrderBy="DepartmentCode" Select="new (DepartmentName, DepartmentCode)" TableName="Departments">
            </asp:LinqDataSource>
        </div>
    </div>
</asp:Content>
