﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Lecturer/Lecturer.Master" AutoEventWireup="true" CodeBehind="LecturerProfile.aspx.cs" Inherits="School365.Lecturer.LecturerProfile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <hr />
    <asp:Label ID="CheckPassword" runat="server" Text=" " style="font-weight: 700; color: #FF0000; font-size: x-large" ></asp:Label>
    <div class="row">
       
        <div class=" large-4 columns">
            
        </div>
        <div class=" large-6 columns" style="float:right">
            <asp:Button ID="InsertScore" runat="server" CssClass="button small" Text="Insert Score" OnClick="InsertScore_Click"  />
            <asp:Button ID="PasswordChange" runat="server" CssClass="button small alert" Text="Change Password" OnClick="PasswordChange_Click" />
            <asp:Button ID="DownloadEMS" runat="server" CssClass="button small success" Text="Download EMS" OnClick="DownloadEMS_Click"  />

        </div>
    </div>
    <asp:Label ID="LecturerName" runat="server" Text=" " Style="font-family: 'Brush Script MT'; font-size: 103px;"></asp:Label>
    <br />

    <div class="row">
        <div class="large-6 columns" style="font-size: 103px; background-color: gainsboro; font-family: Castellar;">
            <p align="center">
                <asp:Label ID="SubjectNo" runat="server" Text=" " Style="font-size: 153px; font-family: Castellar;"></asp:Label>
            </p>
            <p align="center" style="font-family: 'Segoe UI'; font-size: x-large; color: black">Subjects</p>
        </div>
        <div class="large-6 columns">
        </div>
    </div>
</asp:Content>
