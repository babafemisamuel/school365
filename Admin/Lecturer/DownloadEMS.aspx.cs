﻿using School365.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace School365.Lecturer
{
    public partial class DownloadEMS : System.Web.UI.Page
    {
        public StudentModel db = new StudentModel();
        public List<int> getStudentsID = new List<int>();
        int getCurrentSessionID = 0;
        int getLecturerID = 0;
        int getSession = 0;
        public string subCode = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            getLecturerID = int.Parse(Request.QueryString["grab"].ToString());
            getSession = (from d in db.Sessions where d.CurrentSession == true select d.SessionID).FirstOrDefault();
            getCurrentSessionID = (from d in db.Sessions where d.CurrentSession == true select d.SessionID).FirstOrDefault();
            var getAllSubjectsDetail = (from d in db.LecturerProfiles where d.LecturerID == getLecturerID join f in db.Subjects on d.SubjectID equals f.SubjectID select new { f.SubjectID, f.SubjectCode }).ToList();
            getAllSubjects.DataSource = getAllSubjectsDetail;
            getAllSubjects.DataValueField = "SubjectID";
            getAllSubjects.DataTextField = "SubjectCode";
            getAllSubjects.DataBind();
            subCode = getAllSubjects.SelectedItem.Text;
        }

        protected void getStudents_Click(object sender, EventArgs e)
        {
            getStudentsID = (from d in db.Results
                             where d.SubjectID.ToString() == getAllSubjects.SelectedItem.Value && d.LecturerInserted == null
                             select d.StudentID).Distinct().ToList();
        }
    }
}