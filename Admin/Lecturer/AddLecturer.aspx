﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin_Layout.Master" AutoEventWireup="true" CodeBehind="AddLecturer.aspx.cs" Inherits="School365.Admin.Lecturer.AddLecturer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="header" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="body" runat="server">
    <hr />
    <div class="row">
        <div class="large-6 columns">
            
        </div>
        <div class="large-6 columns">
           <asp:Button ID="Edit" runat="server" CssClass="tiny radius button bg-blue" Text="Edit" OnClick="Edit_Click"/>
             <asp:Button ID="Assign" runat="server" CssClass="tiny radius button bg-black" Text="Assign" OnClick="Assign_Click" />
        </div>
    </div>
    <br />
   <div class="row">
       <div class="large-3 columns">
           <asp:TextBox ID="Surname" runat="server" placeholder="Surname"></asp:TextBox>
       </div>

        <div class="large-3 columns">
           <asp:TextBox ID="Firstname" runat="server" placeholder="Firstname"></asp:TextBox>
       </div>

        <div class="large-3 columns">
           <asp:TextBox ID="Othername" runat="server" placeholder="Othername"></asp:TextBox>
       </div>

        <div class="large-3 columns">
           <asp:TextBox ID="UserName" runat="server" placeholder="Username"></asp:TextBox>
       </div>
       <div class="large-3 columns">
           <asp:CheckBox ID="IsHod" runat="server"  /> <label>Hod</label>
       </div>
   </div>
    <div class="row">
        <div class="large-6">
            <asp:Button ID="InsertLecturer" runat="server" CssClass="tiny radius button bg-red" Text="Insert" style="float:right" OnClick="InsertLecturer_Click" />
        </div>
    </div>
</asp:Content>
