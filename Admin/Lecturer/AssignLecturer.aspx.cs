﻿using School365.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace School365.Admin.Lecturer
{
    public partial class AssingLecturer : System.Web.UI.Page
    {
        StudentModel db = new StudentModel();
        int getAdminID = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!IsPostBack)
            {
                if (User.Identity.IsAuthenticated == false)
                    Response.Redirect("../Administrator/UserLogin.aspx");
                getAdminID = int.Parse(User.Identity.Name);
                var getAuth = (from d in db.RoleAdmins where d.AdminID == getAdminID select d.RoleID).ToList();
                if (getAuth.Contains(2) != true)
                {
                    Response.Redirect("../Administrator/Profile.aspx");
                }
            }
        }

        protected void Assign_Click(object sender, EventArgs e)
        {
            foreach (GridViewRow row in allSubjects.Rows)
            {
                int SubjectID = int.Parse(row.Cells[1].Text);
                int LecturerID = int.Parse(getLecturers.SelectedItem.Value);
                if ((row.Cells[0].FindControl("Crs") as CheckBox).Checked)
                {
                    var Assign = new LecturerProfile
                    {
                        SubjectID = SubjectID,
                        LecturerID = LecturerID                       
                    };
                    db.LecturerProfiles.Add(Assign);
                    db.SaveChanges();
                    Response.Write("Assigned");
                }

                if ((row.Cells[0].FindControl("Crs") as CheckBox).Checked)
                {
                    (row.Cells[0].FindControl("Crs") as CheckBox).Checked = false;
                }
            }
        }

        protected void allSubjects_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            allSubjects.PageIndex = e.NewPageIndex;
            SubDataBind();
        }
       

        protected void Search_TextChanged(object sender, EventArgs e)
        {
            
        }
        private void SubDataBind()
        {
            var getAllSubjects = (from d in db.Subjects select new { d.SubjectID, d.SubjectCode, d.SubjectName }).OrderBy(a => a.SubjectCode).ToList();
            allSubjects.DataSource = getAllSubjects;
            allSubjects.DataBind();
        }

        protected void getStaff_Click(object sender, EventArgs e)
        {
            var getAllLecturer = (from d in db.Lecturers where d.Firstname == Search.Text || d.Surname == Search.Text || d.Middlename == Search.Text || d.UserName == Search.Text select new { d.LecturerID, d.UserName }).ToList();
            getLecturers.DataSource = getAllLecturer;
            getLecturers.DataTextField = "UserName";
            getLecturers.DataValueField = "LecturerID";
            getLecturers.DataBind();
            SubDataBind();
        }
    }
}