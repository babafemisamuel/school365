﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin_Layout.Master" AutoEventWireup="true" CodeBehind="EditLecturer.aspx.cs" Inherits="School365.Admin.Lecturer.EditLecturer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="header" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="body" runat="server">
    <hr />
    
    <div class="row">
        <div class="large-6 columns">
            <asp:TextBox ID="UserDetail" runat="server" placeholder="Search"></asp:TextBox>
            <asp:Button ID="getSubjects" runat="server" CssClass="button" Text="Get Subjects" OnClick="getSubjects_Click" />
        </div>        
    </div>   
    <div class="row">
       <div class="large-3 columns">
           <asp:TextBox ID="Surname" runat="server" placeholder="Surname"></asp:TextBox>
       </div>

        <div class="large-3 columns">
           <asp:TextBox ID="Firstname" runat="server" placeholder="Firstname"></asp:TextBox>
       </div>

        <div class="large-3 columns">
           <asp:TextBox ID="Othername" runat="server" placeholder="Othername"></asp:TextBox>
       </div>
        <div class="large-3 columns">
           <asp:TextBox ID="UserName" runat="server" placeholder="Username"></asp:TextBox>
       </div>
        <div class="large-3 columns">
           <asp:TextBox ID="Password" runat="server" placeholder="Password"></asp:TextBox>
       </div>
        
   </div>
    <div class="row">
        <div class="large-3 columns">
             <asp:TextBox ID="Email" runat="server" placeholder="EmailAddress"></asp:TextBox>
        </div>
        <div class="large-3 columns">
             <asp:TextBox ID="PhoneNumber" runat="server" placeholder="PhoneNumber"></asp:TextBox>
        </div>
        <div class="large-3 columns">
          <asp:Button ID="update" runat="server" Text="Update" CssClass="button success" OnClick="update_Click" />
       </div>
    </div>
    <hr />

    <div class="row">
        <div class="large-12 columns">
            <asp:GridView ID="allSubjects" runat="server">
                <Columns>
                    <asp:TemplateField HeaderText="Select">
                        <ItemTemplate>
                            <asp:CheckBox ID="Crs" runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
    </div>
    <div class="row">
        <div class="large-6">
            <asp:Button ID="DeleteCourse" runat="server" CssClass="button alert" Text="Delete Course" style="float:right" OnClick="DeleteCourse_Click"/>
        </div>
    </div>
</asp:Content>
