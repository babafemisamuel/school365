﻿using School365.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace School365.Admin.Lecturer
{
    public partial class EditLecturer : System.Web.UI.Page
    {
        StudentModel db = new StudentModel();
        static int getLecturerID = 0;
        int getAdminID = 0;
        string LecturerKey = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (User.Identity.IsAuthenticated == false)
                Response.Redirect("../Administrator/UserLogin.aspx");
            getAdminID = int.Parse(User.Identity.Name);
            
            var getAuth = (from d in db.RoleAdmins where d.AdminID == getAdminID select d.RoleID).ToList();
            if (getAuth.Contains(2) != true)
            {
                Response.Redirect("../Administrator/Profile.aspx");
            }
            if (!IsPostBack)
            {
                LecturerKey = Request.QueryString["LecturerKey"].ToString();
                getLecturerID = (from d in db.Lecturers where d.LecturerKey == LecturerKey select d.LecturerID).FirstOrDefault();
                var getAllSubjects = (from d in db.LecturerProfiles
                                      where d.LecturerID == getLecturerID
                                      join f in db.Subjects on d.SubjectID equals f.SubjectID
                                      join j in db.Lecturers on d.LecturerID equals j.LecturerID
                                      select new { d.SubjectID, f.SubjectCode, f.SubjectName, j.Firstname, j.Surname }).ToList();
                allSubjects.DataSource = getAllSubjects;
                allSubjects.DataBind();

                var getLecturerDetail = (from d in db.Lecturers where d.LecturerID == getLecturerID select new { d.Firstname, d.Middlename, d.Surname, d.Password, d.UserName }).FirstOrDefault();
                UserName.Text = getLecturerDetail.UserName;
                Firstname.Text = getLecturerDetail.Firstname;
                Surname.Text = getLecturerDetail.Surname;
                Othername.Text = getLecturerDetail.Middlename;
                Password.Text = getLecturerDetail.Password;
            }
        }

        protected void getSubjects_Click(object sender, EventArgs e)
        {
            
        }

        protected void update_Click(object sender, EventArgs e)
        {
            var UpdateStudent = (from d in db.Lecturers where d.LecturerID == getLecturerID select d).FirstOrDefault();
            if (UpdateStudent!=null)
            {
                UpdateStudent.Firstname = Firstname.Text;
                UpdateStudent.Surname = Surname.Text;
                UpdateStudent.Middlename = Othername.Text;
                UpdateStudent.UserName = UserName.Text;
                UpdateStudent.Password = Password.Text;
                UpdateStudent.Email = Email.Text;
                UpdateStudent.PhoneNumber = PhoneNumber.Text;
                db.SaveChanges();
            }
            else
            {
                Response.Write("Something is wrong");
            }
        }

        protected void DeleteCourse_Click(object sender, EventArgs e)
        {
            foreach (GridViewRow row in allSubjects.Rows)
            {
                int SubjectID = int.Parse(row.Cells[1].Text);               
                if ((row.Cells[0].FindControl("Crs") as CheckBox).Checked)
                {
                    var DeleteCourse = (from d in db.LecturerProfiles where d.SubjectID == SubjectID && d.LecturerID == getLecturerID select d).FirstOrDefault();
                    db.LecturerProfiles.Remove(DeleteCourse);
                    db.SaveChanges();                   
                    Response.Write("Deleted");
                }

                if ((row.Cells[0].FindControl("Crs") as CheckBox).Checked)
                {
                    (row.Cells[0].FindControl("Crs") as CheckBox).Checked = false;
                }
            }
        }
    }
}