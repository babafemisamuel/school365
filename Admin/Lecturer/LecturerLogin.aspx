﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Lecturer/Lecturer.Master" AutoEventWireup="true" CodeBehind="LecturerLogin.aspx.cs" Inherits="School365.Lecturer.LecturerLogin" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="row">

        <fieldset>
            <legend>LOGIN</legend>
            <asp:Label ID="errorDisplay" runat="server" Text=" " Style="color: darkred; font-style: italic; font-size: small; font-weight: 700" />
            <div class="large-5 columns">
                <asp:TextBox ID="UserName" runat="server" placeholder="User Name"></asp:TextBox>
                <asp:TextBox ID="Password" runat="server" TextMode="Password" placeholder="Password"></asp:TextBox>
                <asp:Button ID="submit" runat="server" CssClass="button" Style="float: right;" Text="Login" OnClick="submit_Click" />
            </div>
            <div class="large-5 columns">
                <asp:Image ID="LoginImage" runat="server" ImageUrl="~/Image/LecturersImg.jpg" />
            </div>
        </fieldset>
    </div>
</asp:Content>
