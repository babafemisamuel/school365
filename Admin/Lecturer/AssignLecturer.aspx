﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin_Layout.Master" AutoEventWireup="true" CodeBehind="AssignLecturer.aspx.cs" Inherits="School365.Admin.Lecturer.AssingLecturer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="header" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="body" runat="server">
    <hr />
    <div class="row">
        <div class="large-3 columns">
            <asp:TextBox ID="Search" runat="server" placeholder="Search"></asp:TextBox>
        </div>
        <div class="large-3 columns">
            <asp:Button ID="getStaff" runat="server" Text="Get Staff" CssClass="tiny radius button bg-blue" OnClick="getStaff_Click" />
        </div>
        <div class="large-3 columns">
            <asp:DropDownList ID="getLecturers" runat="server">
                
            </asp:DropDownList>
        </div>

        <div class="large-3 columns">
            <asp:Button ID="Assign" runat="server" CssClass="tiny radius button bg-black" Text="Assign" OnClick="Assign_Click" />
        </div>
    </div>
    <div class="row">
        <div class="large-12 columns">
            <asp:GridView ID="allSubjects" runat="server" AllowPaging="True" OnPageIndexChanging="allSubjects_PageIndexChanging" PageSize="50">
                <Columns>
                    <asp:TemplateField HeaderText="Select">
                        <ItemTemplate>
                            <asp:CheckBox ID="Crs" runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <PagerSettings Position="TopAndBottom" />
            </asp:GridView>
        </div>
    </div>
</asp:Content>
