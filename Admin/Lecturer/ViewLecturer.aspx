﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin_Layout.Master" AutoEventWireup="true" CodeBehind="ViewLecturer.aspx.cs" Inherits="School365.Admin.Lecturer.ViewLecturer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="header" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="body" runat="server">
    <div class="row">
        <div class="column large-4">
            <asp:TextBox ID="search" runat="server" placeholder="Search"></asp:TextBox>
        </div>
        <div class="column large-4">
            <asp:Button ID="getLecturer" runat="server" Text="Get Lecturer" CssClass="button info" OnClick="getLecturer_Click"></asp:Button>
        </div>
        <div class="column large-4">
            
        </div>
    </div>
    <div>
        <asp:GridView ID="AllLecturers" runat="server" AutoGenerateColumns="false" AllowSorting="true" AllowPaging="true">
        <Columns>
            <asp:TemplateField HeaderText="Select">
                <ItemTemplate>
                    <asp:CheckBox ID="Crs" runat="server" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemTemplate>
                    <%#Container.DataItemIndex+1 %>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="LecturerKey" HeaderText="Surname" ReadOnly="true" SortExpression="Surname" />
            <asp:BoundField DataField="Surname" HeaderText="Surname" ReadOnly="true" SortExpression="Surname" />
            <asp:BoundField DataField="Firstname" HeaderText="Firstname" ReadOnly="true" SortExpression="Firstname" />
            <asp:BoundField DataField="Middlename" HeaderText="Middlename" ReadOnly="true" SortExpression="Middlename" />
            <asp:BoundField DataField="Username" HeaderText="Username" ReadOnly="true" SortExpression="MatricNo" />           
            <asp:HyperLinkField Text="Edit" DataNavigateUrlFields="LecturerKey" DataNavigateUrlFormatString="EditLecturer.aspx?LecturerKey={0}" />
        </Columns>
    </asp:GridView>
    </div>
    
</asp:Content>
