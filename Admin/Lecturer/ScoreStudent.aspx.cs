﻿using School365.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace School365.Lecturer
{
    public partial class ScoreStudent : System.Web.UI.Page
    {
        StudentModel db = new StudentModel();
        int getCurrentSessionID = 0;
        int getLecturerID = 0;
        int getSession = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            getLecturerID = int.Parse(Request.QueryString["grab"].ToString());
            getSession = (from d in db.Sessions where d.CurrentSession == true select d.SessionID).FirstOrDefault();

            getCurrentSessionID = (from d in db.Sessions where d.CurrentSession == true select d.SessionID).FirstOrDefault();
            var getAllSubjectsDetail = (from d in db.LecturerProfiles where d.LecturerID == getLecturerID join f in db.Subjects on d.SubjectID equals f.SubjectID select new { f.SubjectID, f.SubjectCode }).ToList();
            getAllSubjects.DataSource = getAllSubjectsDetail;
            getAllSubjects.DataValueField = "SubjectID";
            getAllSubjects.DataTextField = "SubjectCode";
            getAllSubjects.DataBind();
        }

        protected void getStudents_Click(object sender, EventArgs e)
        {
            var getStudents = from d in db.Results
                              where d.SubjectID.ToString() == getAllSubjects.SelectedItem.Value
                              && d.SessionID == getSession && d.LecturerInserted != "TRUE"
                              join a in db.Students on d.StudentID equals a.StudentID
                              select new { a.StudentID, a.MatricNo, d.CA, d.EXAM };
            allStudents.DataSource = getStudents.ToList();
            allStudents.DataBind();
            //var getAllStudents= from d in db.Results where d.StudentID
        }

        protected void insertScore_Click(object sender, EventArgs e)
        {
            foreach (GridViewRow row in allStudents.Rows)
            {
                TextBox CA = (TextBox)row.FindControl("CA");
                TextBox EXAM = (TextBox)row.FindControl("EXAM");
                TextBox MatricNo = (TextBox)row.FindControl("MatricNo");

                var UpdateResult = (from d in db.Results where d.MatricNo == MatricNo.Text && d.SubjectID.ToString() == getAllSubjects.SelectedItem.Value select d).First();
                UpdateResult.CA = double.Parse(CA.Text);
                UpdateResult.EXAM = double.Parse(EXAM.Text);
                db.SaveChanges();
            }
            Response.Write("Data Inserted");
        }

        protected void Button1_Click(object sender, EventArgs e)
        {

            if (UploadFile.HasFile != null && UploadFile.PostedFile.ContentLength > 0)
            {
                string FileName = UploadFile.FileName;
                string ImgPath = " ../Upload/" + FileName;
                UploadFile.PostedFile.SaveAs(Server.MapPath(ImgPath));


                string[] readCsv = System.IO.File.ReadAllLines(Server.MapPath(ImgPath));

                try
                {
                    //using i=1 to remove headers 
                    for (int i = 1; i < readCsv.Length; i++)
                    {
                        string[] members = readCsv[i].Split(',');

                        double ca = double.Parse(members[1].ToString());
                        double exam = double.Parse(members[2].ToString());
                        if (members[1].ToString() == "")
                        {
                            ca = 0;
                        }
                        else if (members[2].ToString() == "")
                        {
                            exam = 0;
                        }
                        else if (members[0].ToString() == "")
                        {
                            Response.Write("NO MATRICNO");
                            break;
                        }
                        string MatricNo = members[0].ToString().ToUpper().Trim();
                        if (double.Parse(members[1].ToString()) <= 40 || double.Parse(members[2].ToString()) <= 60)
                        {
                            var UpdateResult = (from d in db.Results where d.MatricNo.ToUpper().Trim() == MatricNo && d.SubjectID.ToString() == getAllSubjects.SelectedItem.Value && d.LecturerInserted != "TRUE" select d).First();
                            UpdateResult.CA = ca;
                            UpdateResult.EXAM = exam;
                            UpdateResult.LecturerInserted = "TRUE";
                            db.SaveChanges();
                        }
                    }
                }
                catch (Exception ex)
                {
                    Response.Write(ex.Message);
                }
              
            }

        }
    }
}