﻿using School365.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace School365.Lecturer
{
    public partial class PasswordChange : System.Web.UI.Page
    {
        StudentModel db = new StudentModel();
        int getLecturerID = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            getLecturerID = int.Parse(Request.QueryString["grab"].ToString());
        }

        protected void InsertPassword_Click(object sender, EventArgs e)
        {
            int value = (from d in db.Lecturers where d.LecturerID == getLecturerID && d.Password == PrevPassword.Text select d.LecturerID).Count();
           if (value!=0)
           {
               var getLecturer = (from d in db.Lecturers where d.LecturerID == getLecturerID select d).FirstOrDefault();
               getLecturer.Password =NewPassword.Text;
               getLecturer.PasswordChanged = true;
               db.SaveChanges();
               Output.Text = "Password Changed";
             
           }
           else
           {
               Output.Text = "Password Changed";
           }

        }
    }
}