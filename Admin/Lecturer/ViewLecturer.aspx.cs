﻿using School365.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace School365.Admin.Lecturer
{
    public partial class ViewLecturer : System.Web.UI.Page
    {
        StudentModel db = new StudentModel();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void getLecturer_Click(object sender, EventArgs e)
        {
            var getAllLecturers = (from d in db.Lecturers where d.UserName.ToUpper() == search.Text.ToUpper()
                                  || d.Firstname.ToUpper() == search.Text.ToUpper()
                                  || d.Middlename.ToUpper() == search.Text.ToUpper()
                                  || d.Surname.ToUpper() == search.Text.ToUpper()
                                  select new {d.Firstname,d.Surname,d.Middlename,d.UserName,d.LecturerKey }).Distinct().ToList();
            AllLecturers.DataSource = getAllLecturers;
            AllLecturers.DataBind();



        }
    }
}