﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Lecturer/Lecturer.Master" AutoEventWireup="true" CodeBehind="ScoreStudent.aspx.cs" Inherits="School365.Lecturer.ScoreStudent" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <hr />
    <div class="row">
        <div class="large-4 columns">
        </div>
        <%--<a href="../Upload/WebForm1.aspx"></a>--%>
        <div class="large-4 columns">
            <label>Subjects</label>
            <asp:DropDownList ID="getAllSubjects" runat="server">
            </asp:DropDownList>
            <asp:Button ID="getSubjects" Text="Get Students" CssClass="button" runat="server" OnClick="getStudents_Click" />
        </div>
         <div class="large-4 columns">
            <asp:FileUpload ID="UploadFile" runat="server" />
            <asp:Button ID="Button1" Text="Upload Students" CssClass="button alert" runat="server" OnClick="Button1_Click" />
        </div>
    </div>
    <hr />
    <div class="row">
        <div class="column large-10">
            <asp:Button ID="insertScore" Text="Insert Score" CssClass="button" runat="server" OnClick="insertScore_Click" />
            <asp:GridView ID="allStudents" runat="server" AutoGenerateColumns="false">
                <Columns>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:TextBox ID="StudentID" runat="server" Visible="false" Text='<%#Bind("StudentID") %>'> ></asp:TextBox>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="MatricNo">
                        <ItemTemplate>
                            <asp:TextBox ID="MatricNo" runat="server" ReadOnly="true" Text='<%#Bind("MatricNo") %>'> </asp:TextBox>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="CA">
                        <ItemTemplate>
                            <asp:TextBox ID="CA" runat="server" Text='<%#Bind("CA") %>'> </asp:TextBox>
                        </ItemTemplate>
                    </asp:TemplateField>

                     <asp:TemplateField HeaderText="EXAM">
                        <ItemTemplate>
                            <asp:TextBox ID="EXAM" runat="server" Text='<%#Bind("EXAM") %>'> </asp:TextBox>
                        </ItemTemplate>
                    </asp:TemplateField>

                </Columns>
            </asp:GridView>

        </div>
        <div class="large-2 column">

        </div>
    </div>
</asp:Content>
