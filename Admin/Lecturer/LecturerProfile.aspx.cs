﻿using School365.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace School365.Lecturer
{
    public partial class LecturerProfile : System.Web.UI.Page
    {
        StudentModel db = new StudentModel();
        int CountStudent = 0;
        int getLecturerID = 0;
        int SessionLecturerID = 0;
      
        protected void Page_Load(object sender, EventArgs e)
        {
            getLecturerID = int.Parse(Request.QueryString["grab"].ToString());
            if (!IsPostBack)
            {                
               //SessionLecturerID = int.Parse(Session["LecturerID"].ToString());
                int getPasswordChanged = (from d in db.Lecturers where d.LecturerID == getLecturerID && d.PasswordChanged == true && d.PasswordChanged == true select d).Count();
                if (getPasswordChanged==0)
                {
                    CheckPassword.Text = "Please Change Your password";
                }
               
                var getLecturerDetails = (from d in db.Lecturers where d.LecturerID == getLecturerID  select d).FirstOrDefault();
                LecturerName.Text = getLecturerDetails.Surname + " " + getLecturerDetails.Firstname + " " + getLecturerDetails.Middlename;
                int LecturerID = getLecturerDetails.LecturerID;
                int getSubjectID = (from d in db.LecturerProfiles where d.LecturerID == LecturerID select d.SubjectID).Distinct().Count();
                SubjectNo.Text = getSubjectID.ToString();
            }
        }

        protected void InsertScore_Click(object sender, EventArgs e)
        {
            string parameter = "grab=" + getLecturerID;
            Response.Redirect("ScoreStudent.aspx?"+parameter);
        }

        protected void PasswordChange_Click(object sender, EventArgs e)
        {
            string parameter = "grab=" + getLecturerID;
            Response.Redirect("PasswordChange.aspx?" + parameter);
        }

        protected void DownloadEMS_Click(object sender, EventArgs e)
        {
            string parameter = "grab=" + getLecturerID;
            Response.Redirect("DownloadEMS.aspx?" + parameter);
        }
    }
}