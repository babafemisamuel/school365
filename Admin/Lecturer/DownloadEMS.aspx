﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Lecturer/Lecturer.Master" AutoEventWireup="true" CodeBehind="DownloadEMS.aspx.cs" Inherits="School365.Lecturer.DownloadEMS" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            width: 46px;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script src="../js/excellentexport.min.js"></script>
    <div class="row">
        <div class="large-4 columns">
            <label>Subjects</label>
            <asp:DropDownList ID="getAllSubjects" runat="server">
            </asp:DropDownList>
            <asp:Button ID="getSubjects" Text="Get Students" CssClass="button" runat="server" OnClick="getStudents_Click" />

            <a download='<%#getAllSubjects.SelectedItem.Text.ToString()%>' href="#" onclick="return ExcellentExport.csv(this, 'Table');" align="center">Download EMS</a>
            <br />

        </div>
       <%-- <asp:TextBox ID="Surname" runat="server" Visible="true" ReadOnly="true" Text='<%#Bind("Surname") %>'> ></asp:TextBox>--%>
        <div class="large-4 columns">
            
        </div>
    </div>

    <table id="Table" style="border: thin solid #000000" cellspacing="0">
        <tr style="border-bottom: 1px solid #aaa; border-top: 1px solid #aaa; border-left: 1px solid #aaa; border-right: 1px solid #aaa;">
            <th style="border-bottom: 1px solid #aaa; border-top: 1px solid #aaa; border-left: 1px solid #aaa; border-right: 1px solid #aaa;" class="auto-style13">Matric No</th>
            <th style="border-bottom: 1px solid #aaa; border-top: 1px solid #aaa; border-left: 1px solid #aaa; border-right: 1px solid #aaa;" class="auto-style13">CA</th>
            <th style="border-bottom: 1px solid #aaa; border-top: 1px solid #aaa; border-left: 1px solid #aaa; border-right: 1px solid #aaa;" class="auto-style13">EXAM</th>
        </tr>
        <%
            foreach (int setStudentsID in getStudentsID)
            {
                var getStudentDetail = (from d in db.Results where d.StudentID == setStudentsID select d).FirstOrDefault();
        %>
        <tr>
            <td style="border-bottom: 1px solid #aaa; border-top: 1px solid #aaa; border-left: 1px solid #aaa; border-right: 1px solid #aaa;" class="auto-style12"><%Response.Write(getStudentDetail.MatricNo); %></td>
            <td style="border-bottom: 1px solid #aaa; border-top: 1px solid #aaa; border-left: 1px solid #aaa; border-right: 1px solid #aaa;" class="auto-style12"></td>
            <td style="border-bottom: 1px solid #aaa; border-top: 1px solid #aaa; border-left: 1px solid #aaa; border-right: 1px solid #aaa;" class="auto-style12"></td>
            <%   } %>
        </tr>
    </table>

</asp:Content>
