﻿using School365.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace School365.Admin.Lecturer
{
    public partial class AddLecturer : System.Web.UI.Page
    {
        StudentModel db = new StudentModel();
        int getAdminID = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (User.Identity.IsAuthenticated == false)
                Response.Redirect("../Administrator/UserLogin.aspx");
            getAdminID = int.Parse(User.Identity.Name);
            var getAuth = (from d in db.RoleAdmins where d.AdminID == getAdminID select d.RoleID).ToList();
            if (getAuth.Contains(2) != true)
            {
                Response.Redirect("../Administrator/Profile.aspx");
            }  
        }

        protected void InsertLecturer_Click(object sender, EventArgs e)
        {
            bool Hod = false;
            if (IsHod.Checked==true)
            {
                Hod = true;
            }
            var InsertLecturer = new Model.Lecturer
            {
                Surname = Surname.Text,
                Firstname = Firstname.Text,
                Middlename = Othername.Text,
                UserName=UserName.Text,
                HOD= Hod,
                LecturerKey = System.Guid.NewGuid().ToString(),
                Password = "12345",
                PasswordChanged = false,
            };
            db.Lecturers.Add(InsertLecturer);
            db.SaveChanges();
            Response.Write("Data Inserted");
            Surname.Text = "";
            Firstname.Text = "";
            Othername.Text = "";
        }

        protected void Assign_Click(object sender, EventArgs e)
        {
            Response.Redirect("AssignLecturer.aspx");
        }

        protected void Edit_Click(object sender, EventArgs e)
        {
            Response.Redirect("EditLecturer.aspx");
        }
    }
}