﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EMS.aspx.cs" Inherits="School365.Admin.Results.EMS" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>EMS</title>
    <style type="text/css">
        .auto-style1 {            height: 66px;
            width: 76px;
        }

        .auto-style13 {
           
        }

        .auto-style19 {
            font-size: x-large;
        }
        .auto-style20 {
            text-decoration: underline;
        }
        .auto-style44 {
            
            
            width: 56px;
        }
        .auto-style65 {
           
           
            width: 136px;
        }
        .auto-style67 {
            
            
            width: 11px;
        }
        .auto-style88 {
            width: 82px;
            
        }
        .auto-style112 {
            font-size: medium;
        }
        .auto-style113 {
            width: 11px;
            }
        .auto-style114 {
            width: 136px;
            }
        .auto-style119 {
            width: 56px;
            }
        .auto-style123 {
            width: 82px;
            }
        .auto-style128 {
            width: 37px;
           
        }
        .auto-style130 {
            width: 36px;
            
        }
        .auto-style131 {
            width: 35px;
        }
        .auto-style132 {
            width: 35px;
           
        }
        .auto-style135 {
            width: 7px;
           
        }
        .auto-style136 {
            width: 7px;
            }
        .auto-style138 {
            width: 50px;
            }
        .auto-style139 {
          
            width: 50px;
        }
        .auto-style141 {
           
          
        }
        .auto-style142 {
            width: 36px;
        }
        .auto-style143 {
            width: 33px;
        }
        .auto-style144 {
            width: 37px;
            }
        .auto-style146 {
            width: 11px;
            height: 90px;
        }
        .auto-style147 {
            width: 136px;
            height: 90px;
        }
        .auto-style148 {
            width: 7px;
            height: 90px;
        }
        .auto-style149 {
            width: 56px;
            height: 90px;
        }
        .auto-style150 {
            width: 82px;
            height: 90px;
        }
        .auto-style151 {
           
            width: 33px;
            height: 90px;
        }
        .auto-style152 {
            width: 50px;
            height: 90px;
        }
        .auto-style153 {
            height: 90px;
        }
    </style>
</head>
<body style="font-family: 'Segoe UI'; font-size:small">
    <form id="form1" runat="server">
        <asp:DropDownList ID="subddlCode" runat="server" DataSourceID="LinqDataSource2" DataTextField="SubjectCode" DataValueField="SubjectID" OnSelectedIndexChanged="subddlCode_SelectedIndexChanged">
        </asp:DropDownList>
        <asp:LinqDataSource ID="LinqDataSource2" runat="server" ContextTypeName="School365.Model.StudentModel" EntityTypeName="" OrderBy="SubjectCode" Select="new (SubjectCode, SubjectID)" TableName="Subjects">
        </asp:LinqDataSource>       
        <asp:DropDownList ID="level" runat="server">
            <asp:ListItem>100</asp:ListItem>
            <asp:ListItem>200</asp:ListItem>
            <asp:ListItem>300</asp:ListItem>
        </asp:DropDownList>
        <asp:DropDownList ID="school" runat="server" DataSourceID="LinqDataSource1" DataTextField="FacultyName" DataValueField="FacultyID">
        </asp:DropDownList>
        <asp:LinqDataSource ID="LinqDataSource1" runat="server" ContextTypeName="School365.Model.StudentModel" EntityTypeName="" OrderBy="FacultyName" Select="new (FacultyID, FacultyName)" TableName="Facultys">
        </asp:LinqDataSource>
        <asp:TextBox ID="pageNo" runat="server" Text="2" placeholder="No of Pages/Print"></asp:TextBox>
        <asp:Button ID="getAllStudent" runat="server" Text="Get Students" OnClick="getAllStudent_Click" />
       <%
        int constant =int.Parse(pageNo.Text);
        int prev = constant;
        int init = 0;
          //var allStdID = (from d in getMatric select d.MatricNo).Distinct().OrderBy(a=>a).ToList();         
        int total = allStudentID.Count();
          int loopCount = (int)(total / constant);         
          for (int i = 0; i <= loopCount; i++)
          {
              var newAllStdID = allStudentID.Skip(init).Take(constant);   
              %> 
         <p style="height: 62px">
            &nbsp;<img alt="" class="auto-style1" src="Image2.PNG" />
        </p>
        
        <p align="center" style="height: 30px">
            <span class="auto-style19">FEDERAL COLLEGE OF EDUCATION</span>
        </p>
        <p align="center">
            &nbsp;
            P.M.B 2096,ABEOKUTA</p>
        <p align="center" class="auto-style20">
            <strong>NCE EXAM. MARK SHEET</strong></p>
        <p align="center">
            Course Lecturer: ______________________________________
        </p>
        <table cellspacing="0" >

            <tr>
                <td>SCHOOL : <%Response.Write(" " + school.SelectedItem.Text); %> &nbsp;&nbsp;&nbsp;SEMESTER :<%Response.Write(" " + Semester); %>   &nbsp;&nbsp;&nbsp; SESSION : <%Response.Write(" " + getSessionYear); %></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>COURSE TITLE :<%Response.Write(" " + SubjectName); %>  &nbsp;&nbsp;&nbsp; COURSE CODE :
                    <asp:Label ID="SubjectCode" runat="server"><%Response.Write(" " + subddlCode.SelectedItem.Text);%></asp:Label>
                    &nbsp;&nbsp;&nbsp; UNIT :
                    <asp:Label ID="SubjectUnit" runat="server"><%Response.Write(" " + SubjectUnits);%></asp:Label>
                    &nbsp;&nbsp;&nbsp; STATUS :
                    <asp:Label ID="Status" runat="server"><%Response.Write(" " + SubjectValue);%></asp:Label>
                </td>
            </tr>
        </table>
        <br />
         
        <table style="border: thin solid #000000; width: 100%;margin-left: 1%; margin-top: 30px;font-size:small; height: 146px;" cellspacing="0">
            <tr style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;">
                <th style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;" class="auto-style146"><span class="auto-style112">S/N</th>
                <th style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000; border-color: #000000;" class="auto-style147">Matric No</th>
                 <th style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;" class="auto-style148">SubjectComb</span></th>
                <%--<th style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;" class="auto-style13">Program</th>--%>
                <th colspan="4" style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;" class="auto-style153"><span class="auto-style112">CA</th>
                <th style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;" class="auto-style149"><span class="auto-style13">TOTAL C.A.</span><br class="auto-style13" />
                    <span class="auto-style13">(40%)</span></th>
                <th colspan="6" style="border: thin solid #000000" class="auto-style153">EXAMINATION SCORE</th>
                <th style="border: thin solid #000000" class="auto-style150"><span class="auto-style13">TOTAL 
                </span>
                    <br class="auto-style13" />
                    <span class="auto-style13">EXAM(60%)</span></th>
                <th style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;" class="auto-style151"><span class="auto-style13">TOTAL</span><br class="auto-style13" />
                    <span class="auto-style13">&nbsp;CA & 
                    </span>
                    <br class="auto-style13" />
                    <span class="auto-style13">EXAM(100%)</span></th>
                <th style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;" class="auto-style152"><span class="auto-style13">H.O.D</span><br class="auto-style13" />
                    <span class="auto-style13">&nbsp;EXTERNAL</span><br class="auto-style13" />
                    <span class="auto-style13">&nbsp;MODERATOR'S 
                    </span>
                    <br class="auto-style13" />
                    </span>
                    <span class="auto-style13"><span class="auto-style112">REMARK</span></span></th>
            </tr>

            <tr style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;">
                <th style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000; border-color: #000000;" class="auto-style113"></th>
                <th style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000; border-color: #000000;" class="auto-style114">
                    <h1 style="text-align: left"></h1>
                </th>
                <th style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;" class="auto-style136"></th>
                <%-- <th style="border: thin solid #000000" class="auto-style6"></th>--%>
                <th style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;" class="auto-style144"><span class="auto-style112">1</th>
                <th style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;" class="auto-style142">2</th>
                <th style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;" class="auto-style131">3</th>
                <th style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;" class="auto-style131">4</th>
                <th style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;" class="auto-style119"></th>
                <th style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;" class="auto-style142">1</th>
                <th style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;" class="auto-style142">2</th>
                <th style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;" class="auto-style131">3</th>
                <th style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;" class="auto-style142">4</th>
                <th style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;" class="auto-style142">5</th>
                <th style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;" class="auto-style142">6</span></th>
                <th style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;" class="auto-style123"></th>
                <th style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;" class="auto-style143"></th>
                <th style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;" class="auto-style138"></th>
            </tr>
            <%
                foreach (string setStudentID in newAllStdID)
                {

                    var getStudentID = (from d in db.Students where d.MatricNo == setStudentID select new { d.MatricNo,d.Major,d.Minor }).FirstOrDefault();
                   
                    SerialValue = SerialValue + 1;

            %>
           
            <tr>
                <td style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;" class="auto-style67"><span class="auto-style112"><%Response.Write(SerialValue); %></td>
                <td style="border: 1px solid #000000; text-align: left;" class="auto-style65"><%Response.Write(getStudentID.MatricNo);%></td>
                <td style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;" class="auto-style135"><%Response.Write(getStudentID.Major + "/" + getStudentID.Minor);%></span></td>
                <td style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;" class="auto-style128"></td>
                <%--<td style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;" class="auto-style5"></td>--%>
                <td style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;" class="auto-style130"></td>
                <td style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;" class="auto-style132"></td>
                <td style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;" class="auto-style132"></td>
                <td style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;" class="auto-style44"></td>
                <td style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;" class="auto-style130"></td>
                <td style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;" class="auto-style130"></td>
                <td style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;" class="auto-style132"></td>
                <td style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;" class="auto-style130"></td>
                <td style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;" class="auto-style130"></td>
                <td style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;" class="auto-style130"></td>
                <td style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;" class="auto-style88"></td>
                <td style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;" class="auto-style141"></td>
                <td style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;" class="auto-style139"></td>
                <%}              
                %>
            </tr>                      
        </table>
        <br />       

        <table align="left" style="font-size: small;">
            <tr>
                <td>NAME OF EXAMINER__________________________________&nbsp;&nbsp;&nbsp; SIGNATURE_________________________&nbsp;&nbsp;&nbsp; DATE_________________________<br />
                    <br />
                </td>
            </tr>
            <tr>
                <td>&nbsp;NAME OF HEAD OF DEPARTMENT______________________________&nbsp;&nbsp;&nbsp;&nbsp; SIGNATURE_________________________&nbsp;&nbsp;&nbsp;&nbsp; DATE____________________________<br />
                    <br />
                </td>
            </tr>
            <tr>
                <td>MODERATOR&#39;S NAME________________________________________&nbsp;&nbsp;&nbsp; SIGNATURE_________________________&nbsp;&nbsp;&nbsp;&nbsp; DATE_______________________<br />
                    <br />
                    <br />
                </td>
            </tr>
        </table>
         <p style="page-break-before:always"></p>    
       <%   
              init = init + constant;
              prev = prev + prev;                    
            %> 
          
         <%
             
          }                   
          %>
    </form>
    <p align="center">
    </p>
    <p align="center">
    </p>
    <p align="center">
    </p>

</body>
</html>
