﻿using School365.Model;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EntityFramework.Extensions;

namespace School365.Admin.Results
{
    public partial class Navigate : System.Web.UI.Page
    {
        StudentModel db = new StudentModel();
        public List<Model.Result> StdColl = new List<Model.Result>();
        public List<Model.Department> DptColl = new List<Model.Department>();
        public List<int> StudentID = new List<int>();
        public List<int> DeptID = new List<int>();
        int getAdminID = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (User.Identity.IsAuthenticated == false)
                Response.Redirect("../Administrator/UserLogin.aspx");
            getAdminID = int.Parse(User.Identity.Name);
            var getAuth = (from d in db.RoleAdmins where d.AdminID == getAdminID select d.RoleID).ToList();
            if (getAuth.Contains(7) != true)
            {
                Response.Redirect("../Administrator/Profile.aspx");
            }

            if (!IsPostBack)
            {
                curr.DataSource = db.Curricullums.Select(a => new { a.CurricullumID, a.CurricullumName }).ToList();
                curr.DataTextField = "CurricullumName";
                curr.DataValueField = "CurricullumID";
                DataBind();

                var getSubjectCombination = (from d in db.SubjectCombinations select d).OrderBy(a => a.SubjectCombinName).ToList();
                SubComb.DataValueField = "SubjectCombinID";
                SubComb.DataTextField = "SubjectCombinName";
                SubComb.DataSource = getSubjectCombination;
                SubComb.DataBind();
            }

        }

        protected void navigate_Click(object sender, EventArgs e)
        {
            Navigatation();
        }
        public void Navigatation()
        {
            int _pages = 10;
            int newPage = 0;
            newPage = int.TryParse(pages.Text, out _pages) == true ? _pages : 10;
            string parameter = "sumCombID=" + SubComb.SelectedItem.Value + "&semester=" + Semester.SelectedItem.Text + "&sessions=" + Sessions.SelectedItem.Value + "&level=" + Level.SelectedItem.Text + "&pages=" + newPage+ "&curriculumid="+curr.SelectedItem.Value;

            if(SubComb.SelectedItem.Text=="BED/BED" && Level.Text == "300")
            {
                parameter = parameter + "&resultpage=" + allResult.SelectedItem.Text;
                Response.Redirect("SubCourse.aspx?" + parameter);
            }
               

            if (allResult.SelectedItem.Text == "Detailed")
            {
                Response.Redirect("Detailed.aspx?" + parameter);
            }
            else if (allResult.SelectedItem.Text == "MasterMarkSheet")
            {
                Response.Redirect("Mastermarksheet.aspx?" + parameter);
            }
            else if (allResult.SelectedItem.Text == "Summary")
            {
                Response.Redirect("Summarys.aspx?" + parameter);
                //
            }
            else if (allResult.SelectedItem.Text == "DetailedII")
            {
                Response.Redirect("DetailedII.aspx?" + parameter);
            }
            else if(allResult.SelectedItem.Text == "SummaryII(Passed)")
            {
                Response.Redirect("SummaryII.aspx?" + parameter);
            }

        }
    }
}