﻿using School365.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace School365.Admin.Results
{
    public partial class SubCourse : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Submit_Click(object sender, EventArgs e)
        {
            StudentModel db = new StudentModel();
            string Level = Request.QueryString["level"].ToString();
            string Sessions = Request.QueryString["sessions"].ToString();
            string Semester = Request.QueryString["semester"].ToString();
            string constant = Request.QueryString["pages"].ToString();
            string curriculumid = Request.QueryString["curriculumid"].ToString();
            string resultpage = Request.QueryString["resultpage"].ToString();
            string subcourse =SubDepartment.SelectedItem.Value;   

         
            int SubCombID = db.SubjectCombinations.Where(a => a.SubjectCombinName == "BED/BED").FirstOrDefault().SubjectCombinID;

            string parameter = "sumCombID=" + SubCombID + "&semester=" + Semester + "&sessions=" + Sessions + "&level=" + Level + "&pages=" + constant + "&curriculumid=" + curriculumid+ "&subcourse="+ subcourse;
            if (resultpage == "Detailed")
            {
                Response.Redirect("Detailed.aspx?" + parameter);
            }
            else if (resultpage == "MasterMarkSheet")
            {
                Response.Redirect("Mastermarksheet.aspx?" + parameter);
            }
            else if (resultpage == "Summary")
            {
                Response.Redirect("Summarys.aspx?" + parameter);
                //
            }
            else if (resultpage == "DetailedII")
            {
                Response.Redirect("DetailedII.aspx?" + parameter);
            }
            else if (resultpage == "SummaryII(Passed)")
            {
                Response.Redirect("SummaryII.aspx?" + parameter);
            }
           
        }
    }
}