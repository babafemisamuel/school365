﻿using School365.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace School365.Admin.Results
{
    public partial class SummaryII : System.Web.UI.Page
    {
        public StudentModel db = new StudentModel();
        public List<Result> ResultData = new List<Result>();
        public List<int> getSubjectCombinationID = new List<int>();
        //public List<int> getStudentID = new List<int>();
        public List<Result> getAllResult = new List<Result>();
        public List<int> getSubjectID = new List<int>();
        public List<int> getAllSubjectID = new List<int>();
        public Util util = new Util();
        public int serialValue = 0;
        public string Semester = "";
        public string Sessions = "";
        public string Level = "";
        public int constant = 0;
        public int setSubID = 0;
        public int curriculumid = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            //pages = int.Parse(Request.QueryString["pages"].ToString());
            if (!IsPostBack)
            {
                Level = Request.QueryString["level"].ToString();
                Sessions = Request.QueryString["sessions"].ToString();
                Semester = Request.QueryString["semester"].ToString();
                constant = int.Parse(Request.QueryString["pages"].ToString());
                curriculumid = int.Parse(Request.QueryString["curriculumid"].ToString());
                //pages=pages - pages;
                string getSubID = Request.QueryString["sumCombID"].ToString();
               
                setSubID = int.Parse(getSubID);

                var getAllCombined = (from d in db.AllCombineds
                                      where d.SubjectCombineID == setSubID && d.CurricullumID == curriculumid
                                      join s in db.Subjects on d.SubjectID equals s.SubjectID
                                      select new { s.SubjectID, s.Semester, s.SubjectLevel }).Distinct().ToList();

                string getSubName = (from d in db.SubjectCombinations where d.SubjectCombinID == setSubID select d.SubjectCombinName).FirstOrDefault();
                string[] split = Regex.Split(getSubName, "/");
                string DeptCode = split[0].ToString();
                string DeptCodeI = split[1].ToString();
                string FacultyID = (from d in db.Departments where d.DepartmentCode == DeptCode select d.FacultyID).FirstOrDefault();

                school.Text = (from d in db.Facultys where d.FacultyID.ToString() == FacultyID select d.FacultyName).FirstOrDefault();
                program.Text = (from d in db.SubjectCombinations where d.SubjectCombinID == setSubID select d.SubjectCombinName).FirstOrDefault();
                session.Text = (from d in db.Sessions where d.SessionID.ToString() == Sessions select d.SessionYear).FirstOrDefault();
                StdLvl.Text = Level;

                if (Semester == "1")
                {
                    getSubjectID = (from d in getAllCombined where d.SubjectLevel == Level && d.Semester == Semester select d.SubjectID).Distinct().ToList();
                    //getStudentID = (from r in db.Results where r.SubjectCombinID == setSubID && r.Semester == 1 && r.SessionID.ToString() == Sessions && r.Level.ToString() == Level orderby r.MatricNo select r.StudentID).Distinct().ToList();
                    ResultData = (from r in db.Results where r.SubjectCombinID == setSubID && r.Semester == 1 && r.SessionID.ToString() == Sessions && r.Level.ToString() == Level select r).Distinct().OrderBy(r => r.MatricNo).ToList();

                }
                else
                {
                    getSubjectID = (from d in getAllCombined where d.SubjectLevel == Level && d.Semester == Semester select d.SubjectID).Distinct().ToList();
                    //getStudentID = (from r in db.Results where r.SubjectCombinID == setSubID && r.SessionID.ToString() == Sessions && r.Level.ToString() == Level select r.StudentID).Distinct().ToList();
                    ResultData = (from r in db.Results where r.SubjectCombinID == setSubID && r.Semester == 2 && r.SessionID.ToString() == Sessions && r.Level.ToString() == Level select r).Distinct().OrderBy(r => r.MatricNo).ToList();
                }
                //including carryovers
                getAllSubjectID = (from h in db.Results where h.SessionID.ToString() == Sessions && h.Level.ToString() == Level select h.SubjectID).OrderBy(h => h).ToList();


                // = (from d in db.AllCombineds where d.SubjectCombineID == setSubID select d.DepartmentID).Distinct().OrderByDescending(s => s).ToList();

                int DeptSubjI = (from d in db.Departments where d.DepartmentCode == DeptCode.ToString() select d.DepartmantID).FirstOrDefault();
                int DeptSubjII = (from d in db.Departments where d.DepartmentCode == DeptCodeI.ToString() select d.DepartmantID).FirstOrDefault();

                List<int> DataIDs = new List<int> { DeptSubjI, DeptSubjII, 7, 37};

                var InitSubCombID = (from d in db.AllCombineds where d.SubjectCombineID == setSubID && d.CurricullumID == curriculumid select d.DepartmentID).Distinct().ToList();
                var newData = (from d in db.AllCombineds where d.SubjectCombineID == setSubID select d.DepartmentID).Distinct().OrderByDescending(s => s).ToList();

                IEnumerable<int> orderedData = newData.OrderByDescending(
                item => Enumerable.Reverse(DataIDs).ToList().IndexOf(item));
                getSubjectCombinationID = orderedData.ToList();


                getAllResult = (from h in db.Results where h.SubjectCombinID == setSubID && h.SessionID.ToString() == Sessions && h.Level.ToString() == Level select h).AsEnumerable().ToList();


                // getStudentID = (from r in db.Results where r.SubjectCombinID == setSubID && r.SessionID.ToString()==Sessions && r.Level.ToString()==Level  select r.StudentID).Distinct().ToList();
                // getAllResult = (from h in db.Results where h.SubjectCombinID == setSubID select h).AsEnumerable().ToList();
            }
        }
    }
}