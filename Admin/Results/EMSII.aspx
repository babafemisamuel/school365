﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EMSII.aspx.cs" Inherits="School365.Admin.Results.EMSII" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>EMS II</title>
    <style type="text/css">
        .auto-style1 {
            width: 73px;
            height: 64px;
        }

        .auto-style10 {
            width: 6px;
            height: 27px;
        }

        .auto-style11 {
            width: 62px;
            height: 27px;
        }

        .auto-style12 {
            width: 37px;
            height: 27px;
        }

        .auto-style13 {
            width: 80px;
        }

        .auto-style14 {
            width: 122px;
            height: 27px;
        }

        .auto-style18 {
            text-decoration: underline;
        }

        .auto-style22 {
            width: 37px;
            height: 36px;
        }

        .auto-style23 {
            width: 122px;
            height: 36px;
        }

        .auto-style24 {
            width: 6px;
            height: 36px;
        }

        .auto-style28 {
            width: 62px;
            height: 36px;
        }

        .auto-style32 {
            width: 96px;
            height: 27px;
        }

        .auto-style37 {
            width: 122px;
            height: 41px;
            font-size: medium;
        }

        .auto-style38 {
            width: 30px;
            height: 27px;
        }

        .auto-style50 {
            width: 30px;
            height: 36px;
        }

        .auto-style54 {
            width: 91px;
            height: 27px;
        }

        .auto-style55 {
            width: 91px;
            height: 36px;
        }

        .auto-style56 {
            width: 37px;
            height: 41px;
        }
        .auto-style57 {
            width: 6px;
            height: 41px;
        }
        .auto-style61 {
            width: 62px;
            height: 41px;
        }
        .auto-style63 {
            width: 30px;
            height: 41px;
        }
        .auto-style67 {
            width: 91px;
            height: 41px;
        }
        .auto-style68 {
            width: 35px;
            height: 36px;
        }
        .auto-style69 {
            width: 35px;
            height: 41px;
        }
        .auto-style70 {
            width: 34px;
            height: 36px;
        }
        .auto-style71 {
            width: 34px;
            height: 41px;
        }
        .auto-style72 {
            width: 85px;
            height: 27px;
        }
        .auto-style73 {
            width: 85px;
            height: 36px;
        }
        .auto-style74 {
            width: 85px;
            height: 41px;
        }
        .auto-style75 {
            width: 36px;
            height: 36px;
        }
        .auto-style76 {
            width: 36px;
            height: 41px;
        }
        .auto-style77 {
            width: 80px;
            height: 27px;
        }
    </style>
</head>
<body style="font-family:'Segoe UI'">
    <form id="form1" runat="server">
        <asp:DropDownList ID="subjectCode" runat="server" DataSourceID="LinqDataSource1" DataTextField="SubjectCode" DataValueField="SubjectCode">
        </asp:DropDownList>
        <asp:LinqDataSource ID="LinqDataSource1" runat="server" ContextTypeName="School365.Model.StudentModel" EntityTypeName="" Select="new (SubjectCode)" TableName="Subjects" OrderBy="SubjectCode">
        </asp:LinqDataSource>
        <%--<asp:TextBox ID="subCode" runat="server" placeholder="Subject Code"></asp:TextBox>--%>
        <asp:DropDownList ID="level" runat="server">
            <asp:ListItem>100</asp:ListItem>
            <asp:ListItem>200</asp:ListItem>
            <asp:ListItem>300</asp:ListItem>
            <asp:ListItem>400</asp:ListItem>
            <asp:ListItem>500</asp:ListItem>
            <asp:ListItem>600</asp:ListItem>
        </asp:DropDownList>
        <asp:TextBox ID="pageNo" runat="server" Text="2" placeholder="No of Pages/Print"></asp:TextBox>
        <asp:Button ID="getAllStudent" runat="server" Text="Get Students" OnClick="getAllStudent_Click" />
         <%
            int constant = int.Parse(pageNo.Text);
            int prev = constant;
            int init = 0;
            var allStdID = (from d in AllStudentsMatric select d).Distinct().OrderBy(a=>a).AsQueryable();
            int total = allStdID.Count();
            int loopCount = (int)(total / constant);
            for (int i = 0; i <= loopCount; i++)
            {
                var newAllStdID = allStdID.Skip(init).Take(constant);   
        %>
        <p>
            &nbsp;<img alt="" class="auto-style1" src="Image2.PNG" />
        </p>
        <p align="center">
            <span style="font-size: x-large; font-weight: 700">FEDERAL COLLEGE OF EDUCATION 

            </span>

        </p>
        <p align="center">
            &nbsp;
            P.M.B 2096,ABEOKUTA
        </p>
        <p align="center">
            <span class="auto-style18"><strong>NCE EXAM. MARK SHEET</strong></span>

        </p>
        <hr />
        <p align="center">
            Course Lecturer: ______________________________________
        </p>
       
        <table cellspacing="0">
            <tr>
                <td>SUBJECT&nbsp; : <%Response.Write(" " + subjectCode.SelectedItem.Text); %> &nbsp;&nbsp;&nbsp;SEMESTER :<%Response.Write(" " + Semester); %>   &nbsp;&nbsp;&nbsp; SESSION : <%Response.Write(" " + getSessionYear); %></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>COURSE TITLE :<%Response.Write(" " + SubjectName); %>  &nbsp;&nbsp;&nbsp; COURSE CODE :
                    <asp:Label ID="Label1" runat="server"><%Response.Write(" " + subjectCode.SelectedItem.Text);%></asp:Label>
                    &nbsp;&nbsp;&nbsp; UNIT :
                    <asp:Label ID="SubjectUnit" runat="server"><%Response.Write(" " + SubjectUnits);%></asp:Label>
                    &nbsp;&nbsp;&nbsp; STATUS :
                    <asp:Label ID="Status" runat="server"><%Response.Write(" " + SubjectValue);%></asp:Label>
                </td>
            </tr>
        </table>
        <br />
        <table style="border: thin solid #000000; width: 90%;margin-left: 2%; margin-top: 30px;" cellspacing="0">
            <tr style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;">
                <th style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;" class="auto-style12">S/N</th>
                <th style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;" class="auto-style14">Matric No</th>
                <th style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;" class="auto-style10">SubjectComb</th>
                <th colspan="4" style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;" class="auto-style32">CA</th>
                <th style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;" class="auto-style11"><span class="auto-style13">TOTAL C.A.</span><br class="auto-style13" />
                    <span class="auto-style13">(40%)</span></th>
                <th colspan="6" style="border: thin solid #000000" class="auto-style77">EXAMINATION SCORE</th>
                <th style="border: thin solid #000000" class="auto-style72"><span class="auto-style13">TOTAL 
                </span>
                    <br class="auto-style13" />
                    <span class="auto-style13">EXAM(60%)</span></th>
                <th style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;" class="auto-style38"><span class="auto-style13">TOTAL</span><br class="auto-style13" />
                    <span class="auto-style13">&nbsp;CA & 
                    </span>
                    <br class="auto-style13" />
                    <span class="auto-style13">EXAM(100%)</span></th>
                <th style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;" class="auto-style54"><span class="auto-style13">H.O.D</span><br class="auto-style13" />
                    <span class="auto-style13">&nbsp;EXTERNAL</span><br class="auto-style13" />
                    <span class="auto-style13">&nbsp;MODERATOR'S 
                    </span>
                    <br class="auto-style13" />
                    <span class="auto-style13">REMARK</span></th>
            </tr>

            <tr style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;">
                <th style="border: 1px solid #000000; text-align: left;" class="auto-style22"></th>
                <th style="border: 1px solid #000000; text-align: left;" class="auto-style23"></th>
                <th style="border: thin solid #000000" class="auto-style24"></th>
                <th style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;" class="auto-style70">1</th>
                <th style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;" class="auto-style68">2</th>
                <th style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;" class="auto-style68">3</th>
                <th style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;" class="auto-style68">4</th>
                <th style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;" class="auto-style28"></th>
                <th style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;" class="auto-style68">1</th>
                <th style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;" class="auto-style68">2</th>
                <th style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;" class="auto-style68">3</th>
                <th style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;" class="auto-style68">4</th>
                <th style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;" class="auto-style75">5</th>
                <th style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;" class="auto-style68">6</th>
                <th style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;" class="auto-style73"></th>
                <th style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;" class="auto-style50"></th>
                <th style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;" class="auto-style55"></th>
            </tr>


            <%
                var getCurrentSession = db.Sessions.Where(a => a.CurrentSession == true).Select(a=>a.SessionID).FirstOrDefault();
                var result = db.Results.Where(a=>a.SessionID==getCurrentSession).AsQueryable();
                var subjectComb = db.SubjectCombinations.AsQueryable();
                foreach (string setAllStudentsMatric in newAllStdID)
                {
                    string getSubComb = (from d in result where d.MatricNo.Trim() == setAllStudentsMatric.Trim() join f in subjectComb on d.SubjectCombinID equals f.SubjectCombinID select f.SubjectCombinName).FirstOrDefault();
                    SerialValue = SerialValue + 1;
            %>
            <tr>
                <td style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;" class="auto-style56"><%Response.Write(SerialValue); %></td>
                <td style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;" class="auto-style37"><%Response.Write(setAllStudentsMatric); %></td>
                <td style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;" class="auto-style57"><%Response.Write(getSubComb); %></td>
                <td style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;" class="auto-style71"></td>
                <td style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;" class="auto-style69"></td>
                <td style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;" class="auto-style69"></td>
                <td style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;" class="auto-style69"></td>
                <td style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;" class="auto-style61"></td>
                <td style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;" class="auto-style69"></td>
                <td style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;" class="auto-style69"></td>
                <td style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;" class="auto-style69"></td>
                <td style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;" class="auto-style69"></td>
                <td style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;" class="auto-style76"></td>
                <td style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;" class="auto-style69"></td>
                <td style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;" class="auto-style74"></td>
                <td style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;" class="auto-style63"></td>
                <td style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;" class="auto-style67"></td>

                <%   } %>
            </tr>
        </table>
        <br />
       <table align="left" style="font-size: medium;">
            <tr>
                <td>NAME OF EXAMINER__________________________________&nbsp;&nbsp;&nbsp; SIGNATURE_________________________&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; DATE_________________________<br />
                    <br />
                </td>
            </tr>
            <tr>
                <td>&nbsp;NAME OF HEAD OF DEPARTMENT______________________________&nbsp;&nbsp;&nbsp;&nbsp; SIGNATURE_________________________&nbsp;&nbsp;&nbsp; DATE____________________________<br />
                    <br />
                </td>
            </tr>
            <tr>
                <td>MODERATOR&#39;S NAME________________________________________&nbsp;&nbsp;&nbsp; SIGNATURE_________________________&nbsp;&nbsp;&nbsp;&nbsp; DATE_______________________<br />
                    <br />
                    <br />
                </td>
            </tr>
        </table>
        <p style="page-break-before: always"></p>
        <%   
              init = init + constant;
              prev = prev + prev;                    
        %>
        <%             
          }                   
        %>
    </form>
</body>
</html>
