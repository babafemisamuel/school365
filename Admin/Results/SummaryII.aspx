﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SummaryII.aspx.cs" Inherits="School365.Admin.Results.SummaryII" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>SUMMARY</title>
    <style type="text/css">
        .auto-style2 {
            width: 67px;
            height: 53px;
        }

        .auto-style3 {
            text-align: center;
        }

        .auto-style4 {
            text-align: left;
        }

        .auto-style7 {
            font-size: x-small;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server" style="font-family: 'Segoe UI'">

        <div>

            <br />
        </div>
        <%
            int prev = constant;
            int init = 0;
            var allStdID = (from d in ResultData select new { d.StudentID, d.MatricNo }).Distinct().ToList();
            int total = allStdID.Count();
            int loopCount = (int)(total / constant);
            for (int i = 0; i <= loopCount; i++)
            {
                var newAllStdID = allStdID.Skip(init).Take(constant);
        %>
        <table align="center" style="width: 100%">
            <thead align="center">
                <tr>
                    <td>
                        <img alt="" class="auto-style2" src="Image2.PNG" align="left">
                        <p align="center"><strong>FEDERAL COLLEGE OF EDUCATION ABEOKUTA</strong></p>
                        <p align="center" class="auto-style3" style="font-size: small">NCE SUMMARY RESULT</p>
                        <p style="font-size: small" align="left">
                            <asp:Label ID="school" runat="server" Text=" " />
                            <span class="auto-style4">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PROGRAM :<asp:Label ID="program" runat="server" Text=" " />&nbsp;&nbsp;LEVEL :<asp:Label ID="StdLvl" runat="server" Text=" " />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SEMESTER:<%Response.Write(Semester); %>&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; </span>SESSION:&nbsp;&nbsp;
                                    <asp:Label ID="session" runat="server" Text=" "></asp:Label>
                            &nbsp;&nbsp;&nbsp;
                        </p>
                    </td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <span class="auto-style7">Date Printed&nbsp;<%Response.Write(DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString());  %></span></td>
                </tr>
            </tbody>
        </table>

        <table style="width: 100%; height: 100%; margin-top: 5px; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; border-left: 1px solid #000; font-size: 12px;" border="0" cellspacing="0">
            <tr>
                <th style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style1" colspan=""></th>
                <th style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style1" colspan=""></th>
                <th style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style1" colspan=""></th>
                <%foreach (int getID in getSubjectCombinationID)
                    {
                        var getDepartment = (from d in db.Departments where d.DepartmantID == getID select new { d.DepartmentCode, d.DepartmantID }).OrderByDescending(s => s.DepartmantID).FirstOrDefault();
                %>
                <th style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style1" colspan="4"><%Response.Write(getDepartment.DepartmentCode); %></th>
                <% }
                %>
                <%--<th style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style1" colspan="4">PREVIOUS</th>--%>
                <th style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style1" colspan="4">OVERALL</th>
                <th style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style1" colspan="4">REMARKS</th>
            </tr>

            <tr>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style1">S/N</td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style1">NAME</td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style1">MATRIC NO</td>

                <%foreach (int getID in getSubjectCombinationID)
                    {
                        var getDepartment = (from d in db.Departments where d.DepartmantID == getID select d.DepartmentCode).FirstOrDefault();
                %>
                <th style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style1" colspan="0"><%Response.Write("TCP"); %></th>
                <th style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style1" colspan="0"><%Response.Write("TNU"); %></th>
                <th style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style1" colspan="0"><%Response.Write("TNUP"); %></th>
                <th style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style1" colspan="0"><%Response.Write("CGPA"); %></th>
                <% }
                %>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style1">TCP</td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style1">TNU</td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style1">TNUP</td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style1">CGPA</td>


                <%--<td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style1">TCP</td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style1">TNU</td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style1">TNUP</td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style1">CGPA</td>--%>
            </tr>

            <tr>
                <%
                    foreach (var setStudentID in newAllStdID)
                    {

                        var getStudentDetails = (from d in getAllResult where d.StudentID == setStudentID.StudentID join r in db.Students on d.StudentID equals r.StudentID select new { r.Surname, r.Firstname, r.Middlename, r.StudentID, r.Level, d.MatricNo, d.CGPA, d.CTNU, d.CTNUP, d.CTCP }).FirstOrDefault();
                        double getPrevLvl = int.Parse(Level) - 100;
                        int getFailed = 0;
                        int _sessionid = int.Parse(Sessions);
                        //getFailed = util.GetCarryOvers(setStudentID.StudentID, int.Parse(Semester), int.Parse(Sessions), int.Parse(Level), setSubID).Select(a => a.SubjectID).ToList()==null?0:util.GetCarryOvers(setStudentID.StudentID, int.Parse(Semester), int.Parse(Sessions), int.Parse(Level), setSubID).Select(a => a.SubjectID).Count();
                        if (Semester == "1" && int.Parse(Level) == 100)
                        {
                            //getSubjectDetail = (from h in getAllResult where h.StudentID == setStudentID.StudentID && h.SessionID == SessionID && h.SubjectID == setSub.SubjectID && (h.CA + h.EXAM) <= 39 && h.Semester == 1 join d in db.Subjects on h.SubjectID equals d.SubjectID select new { d.SubjectCode, d.SubjectUnit, d.Semester, d.SubjectLevel, d.SubjectValue, h.CA, h.EXAM }).FirstOrDefault();
                            getFailed = (from d in db.Results where d.StudentID == setStudentID.StudentID && (d.CA + d.EXAM) <= 39 && d.Semester == 1 && d.SessionID == _sessionid select d.SubjectID).Count();
                        }
                        else if (Semester == "1" && int.Parse(Level) > 100)
                        {
                            int _num = 0;
                            var _failed = (from d in db.Results where d.StudentID == setStudentID.StudentID && (d.CA + d.EXAM) <= 39 && d.SessionID == _sessionid select new { d.SubjectID, d.Subjects.Semester, d.Subjects.SubjectLevel }).ToList();
                            // getSubjectDetail = (from h in db.Results where h.StudentID == setStudentID.StudentID && h.SessionID == SessionID && h.SubjectID == setSub.SubjectID && (h.CA + h.EXAM) <= 39 join d in db.Subjects on h.SubjectID equals d.SubjectID select new { d.SubjectCode, d.SubjectUnit, d.Semester, d.SubjectLevel, d.SubjectValue, h.CA, h.EXAM, }).FirstOrDefault();
                            foreach (var item in _failed)
                            {
                                if (item.SubjectLevel == Level && item.Semester == "2")
                                    _num = _num + 0;
                                else
                                    _num = _num + 1;
                            }
                            getFailed = _num;
                        }
                        //getFailed = (from d in db.Results where d.StudentID == setStudentID.StudentID && (d.CA + d.EXAM) <= 39 && d.Semester == 1 && d.SessionID == _sessionid select d.SubjectID).Count();

                        else
                        {
                            getFailed = (from d in db.Results where d.StudentID == setStudentID.StudentID && (d.CA + d.EXAM) <= 39 && d.SessionID == _sessionid select d.SubjectID).Count();
                        }

                        var getAllSubjectID = (from h in db.Results where h.SessionID.ToString() == Sessions && h.StudentID == setStudentID.StudentID select new { h.SubjectID, h.DepartmentID, h.Subjects.SubjectCode }).OrderBy(h => h.DepartmentID).OrderBy(h => h.SubjectCode).ToList();
                        
                %>
                <%
                    if (getFailed == 0)
                    {
                        serialValue = serialValue + 1;
                %>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style3"><%Response.Write(serialValue); %></td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000; text-transform: uppercase" class="auto-style4"><%Response.Write(getStudentDetails.Surname + " " + getStudentDetails.Firstname + " " + getStudentDetails.Middlename); %></td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000; text-transform: uppercase" class="auto-style3"><%Response.Write(getStudentDetails.MatricNo);%></td>
                <%foreach (int getID in getSubjectCombinationID)
                    {
                        var getDepartmentDetail = (from h in getAllResult where h.StudentID == setStudentID.StudentID && h.DepartmentID == getID join d in db.Departments on h.DepartmentID equals d.DepartmantID select new { h.TCP, h.TNU, h.TNUP, h.GPA, d.DepartmentCode }).FirstOrDefault();
                %>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style3"><%Response.Write(util.CummDeptTcp(int.Parse(Level), setStudentID.StudentID, int.Parse(Semester), getID)); %></td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style3"><%Response.Write(util.CummDeptTnu(int.Parse(Level), setStudentID.StudentID, int.Parse(Semester), getID, int.Parse(Sessions))); %></td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style3"><%Response.Write(util._CummDeptTnup(int.Parse(Level), setStudentID.StudentID, int.Parse(Semester), getID, int.Parse(Sessions))); %></td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style3"><%Response.Write(string.Format("{0:0.00}", util.CummDeptTcp(int.Parse(Level), setStudentID.StudentID, int.Parse(Semester), getID) / util.CummDeptTnu(int.Parse(Level), setStudentID.StudentID, int.Parse(Semester), getID, int.Parse(Sessions)))); %></td>
                <% }%>

                <%--<%if (getPrevLvl == 0 && Semester == "1")
                  {
                %>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style3"><%Response.Write("0"); %></td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style3"><%Response.Write("0"); %></td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style3"><%Response.Write("0");%></td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style3"><%Response.Write("0.00");%></td>
                <%}
                  else
                  {
                      var getPrevStudentDetails = (from d in db.Results where d.StudentID == setStudentID && d.Level.ToString() == Level join r in db.Students on d.StudentID equals r.StudentID select new { d.CGPA, d.CTNU, d.CTNUP, d.CTCP, d.Level }).FirstOrDefault();
                %>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style3"><%Response.Write(util.PrevCummTcp(int.Parse(Level), setStudentID, int.Parse(Semester))); %></td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style3"><%Response.Write(util.PrevCummTnu(int.Parse(Level), setStudentID, int.Parse(Semester))); %></td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style3"><%Response.Write(util.PrevCummTnup(int.Parse(Level), setStudentID, int.Parse(Semester)));%></td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style3"><%Response.Write(string.Format("{0:0.00}",util.PrevCummTcp(int.Parse(Level), setStudentID, int.Parse(Semester)) / util.PrevCummTnu(int.Parse(Level), setStudentID, int.Parse(Semester))));%></td>
                <% }
                %>--%>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style3"><%Response.Write(util.AllTcp(int.Parse(Level), setStudentID.StudentID, int.Parse(Semester))); %></td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style3"><%Response.Write(util.AllTnu(int.Parse(Level), setStudentID.StudentID, int.Parse(Semester), int.Parse(Sessions))); %></td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style3"><%Response.Write(util.AllTnup(int.Parse(Level), setStudentID.StudentID, int.Parse(Semester)));%></td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style3"><%Response.Write(string.Format("{0:0.00}", util.AllTcp(int.Parse(Level), setStudentID.StudentID, int.Parse(Semester)) / util.AllTnu(int.Parse(Level), setStudentID.StudentID, int.Parse(Semester), int.Parse(Sessions))));%></td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000;" class="auto-style1"></td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000" class="auto-style1">
                    <%var _outStanding = util.GetOutstandingResult(setStudentID.StudentID, setSubID, int.Parse(Level), int.Parse(Sessions), int.Parse(Semester)).Count(); %>
                    <%

                    %>

                    <%Response.Write(getFailed > 0 || _outStanding > 0 ? " F. " : "PASSED");%>

                    <%
                        foreach (var setSub in getAllSubjectID)
                        {
                            int SessionID = int.Parse(Sessions);
                            //var _getSubjectDetail = util.GetCarryOvers(setStudentID.StudentID, int.Parse(Semester), int.Parse(Sessions), int.Parse(Level), setSubID).ToList();
                            //var getSubjectDetail = (from x in _getSubjectDetail join d in db.Subjects on x.SubjectID equals d.SubjectID select new { d.SubjectCode, d.SubjectUnit, d.SubjectValue, x.CA, x.EXAM }).FirstOrDefault();
                            var getSubjectDetail = (from h in db.Results where h.StudentID == setStudentID.StudentID && h.SessionID == SessionID && h.SubjectID == setSub.SubjectID && (h.CA + h.EXAM) <= 39 join d in db.Subjects on h.SubjectID equals d.SubjectID select new { d.SubjectCode, d.SubjectUnit, d.Semester, d.SubjectLevel, d.SubjectValue, h.CA, h.EXAM }).FirstOrDefault();
                            if (getSubjectDetail != null)
                            {


                                var _showScore = util.CarryOver(setStudentID.StudentID).Where(a => a.SubjectCode == getSubjectDetail.SubjectCode).FirstOrDefault();
                                if (Semester == "1" && int.Parse(Level) == 100)
                                {
                                    getSubjectDetail = (from h in getAllResult where h.StudentID == setStudentID.StudentID && h.SessionID == SessionID && h.SubjectID == setSub.SubjectID && (h.CA + h.EXAM) <= 39 && h.Semester == 1 join d in db.Subjects on h.SubjectID equals d.SubjectID select new { d.SubjectCode, d.SubjectUnit, d.Semester, d.SubjectLevel, d.SubjectValue, h.CA, h.EXAM }).FirstOrDefault();
                                }
                                if (Semester == "1" && int.Parse(Level) > 100)
                                {

                                    getSubjectDetail = (from h in db.Results where h.StudentID == setStudentID.StudentID && h.SessionID == SessionID && h.SubjectID == setSub.SubjectID && (h.CA + h.EXAM) <= 39 join d in db.Subjects on h.SubjectID equals d.SubjectID select new { d.SubjectCode, d.SubjectUnit, d.Semester, d.SubjectLevel, d.SubjectValue, h.CA, h.EXAM, }).FirstOrDefault();
                                    if (getSubjectDetail != null)
                                    {
                                        if (getSubjectDetail.SubjectLevel == Level && getSubjectDetail.Semester == "2")
                                            getSubjectDetail = null;
                                    }

                                }

                    %>
                    <% 
                        if (_showScore != null && getSubjectDetail.SubjectCode == _showScore.SubjectCode && getSubjectDetail.CA == 0 && getSubjectDetail.EXAM == 0)
                        {
                            Response.Write(getSubjectDetail == null ? "" : "  " + getSubjectDetail.SubjectCode + " " + "(" + getSubjectDetail.SubjectValue + " " + getSubjectDetail.SubjectUnit + ")" + " , " + (_showScore.Ca + _showScore.Exam) + " | "); %>

                    <% }
                        else
                            Response.Write(getSubjectDetail == null ? "" : "  " + getSubjectDetail.SubjectCode + " " + "(" + getSubjectDetail.SubjectValue + " " + getSubjectDetail.SubjectUnit + ")" + " , " + (getSubjectDetail.CA + getSubjectDetail.EXAM) + " | ");
                    %>
                    <% }
                        }
                    %>

                    <%Response.Write(_outStanding > 0 ? "O/S " : "");%>
                    <%var outStanding = util.GetOutstandingResult(setStudentID.StudentID, setSubID, int.Parse(Level), int.Parse(Sessions), int.Parse(Semester)).ToList(); %>
                    <%foreach (var item in outStanding)
                        {
                    %>
                    <%Response.Write(item.SubjectCode + " " + "(" + "" + item.SubjectValue + " " + item.SubjectUnit + ")" + "|"); %>
                    <%}

                    %>
                </td>

                <%}

                %>
            </tr>
            <%} %>
        </table>
        <p style="page-break-before: always"></p>
        <%   
            init = init + constant;
            prev = prev + prev;
        %>
        <%             
            }
        %>
    </form>
</body>
</html>
