﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin_Layout.Master" AutoEventWireup="true" CodeBehind="Navigate.aspx.cs" Inherits="School365.Admin.Results.Navigate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="header" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="body" runat="server">
    <div class="row">
        <div class="large-4 columns">
            <asp:DropDownList ID="allResult" runat="server">
                <asp:ListItem>Detailed</asp:ListItem>
                 <asp:ListItem>DetailedII</asp:ListItem>
                <asp:ListItem>MasterMarkSheet</asp:ListItem>
                <asp:ListItem>Summary</asp:ListItem>
                <asp:ListItem>SummaryII(Passed)</asp:ListItem>
            </asp:DropDownList>
        </div>
        <div class="large-4 columns">
            <asp:DropDownList ID="SubComb" runat="server">
            </asp:DropDownList>
        </div>
        <div class="large-4 columns">
            <asp:DropDownList ID="Semester" runat="server">
                <asp:ListItem>1</asp:ListItem>
                <asp:ListItem>2</asp:ListItem>
            </asp:DropDownList>
        </div>
        </div>
    <div class="row">
        <div class="large-4 columns">
            <asp:DropDownList ID="Sessions" runat="server" DataSourceID="LinqDataSource1" DataTextField="SessionYear" DataValueField="SessionID">                
            </asp:DropDownList>
            <asp:LinqDataSource ID="LinqDataSource1" runat="server" ContextTypeName="School365.Model.StudentModel" EntityTypeName="" OrderBy="SessionYear" Select="new (SessionID, SessionYear)" TableName="Sessions">
            </asp:LinqDataSource>
        </div>
         <div class="large-4 columns">
             <asp:DropDownList ID="Level" runat="server">
                 <asp:ListItem>100</asp:ListItem>
                 <asp:ListItem>200</asp:ListItem>
                 <asp:ListItem>300</asp:ListItem>
                 <asp:ListItem>400</asp:ListItem>
                 <asp:ListItem>500</asp:ListItem>
                 <asp:ListItem>600</asp:ListItem>
            </asp:DropDownList>
        </div>
         <div class="large-4 columns">
             <asp:DropDownList ID="curr" runat="server">
                 
            </asp:DropDownList>
        </div>
        <div class="large-4">
            <asp:TextBox ID="pages" runat="server" placeholder="No of pages"></asp:TextBox>
        </div>
        <div class="large-4 columns">
            <asp:Button ID="navigate" runat="server" CssClass="tiny radius button bg-blue" Text="Get Result" OnClick="navigate_Click" />
        </div>
       
    </div>
</asp:Content>
