﻿using School365.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace School365.Admin.Results
{
   
    public partial class EMS : System.Web.UI.Page
    {
             
        public StudentModel db = new StudentModel();
        // public List<String> AllStudentsMatric = new List<String>();
        public List<String> getStudentMatric = new List<String>();
        public int SerialValue = 0;
        public int getCurrentSessionID = 0;
       
        public string getSessionYear = "";
        public string SubjectName = " ";
        public double SubjectUnits = 0;
        public string SubjectValue = " ";
        public string Semester = " ";
        public List<Result> getMatric = new List<Result>();
        public List<String> StudentID = new List<String>();

        //getting from d student class
        public List<string> allStudentID = new List<string>();

        

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void getAllStudent_Click(object sender, EventArgs e)
        {
            SubjectName = (from d in db.Subjects where d.SubjectCode == subddlCode.SelectedItem.Text select d.SubjectName).FirstOrDefault();
            SubjectUnits = (from d in db.Subjects where d.SubjectCode == subddlCode.SelectedItem.Text select d.SubjectValue).FirstOrDefault();
            SubjectValue = (from d in db.Subjects where d.SubjectCode == subddlCode.SelectedItem.Text select d.SubjectUnit).FirstOrDefault();
            Semester = (from d in db.Subjects where d.SubjectCode == subddlCode.SelectedItem.Text select d.Semester).FirstOrDefault();
            getSessionYear = (from d in db.Sessions where d.CurrentSession == true select d.SessionYear).FirstOrDefault();
            getStudentMatric = (from d in db.Students where d.FacultyName.Trim().ToUpper() == school.SelectedItem.Text.Trim().ToUpper() select d.MatricNo).Distinct().ToList();
            getCurrentSessionID = (from d in db.Sessions where d.CurrentSession == true select d.SessionID).FirstOrDefault();
            
            getMatric = (from d in db.Subjects where d.SubjectCode.Trim() == subddlCode.SelectedItem.Text.Trim() && d.SubjectLevel == level.SelectedItem.Text join f in db.Results on d.SubjectID equals f.SubjectID select f).ToList();                      
            
            StudentID = (from d in getMatric select d.MatricNo).Distinct().ToList();
            var getWithFalc = (from d in db.Students where d.FacultyName == school.SelectedItem.Text select d.MatricNo).Distinct().ToList();
            var results = db.Results.Where(a => a.SessionID == getCurrentSessionID);
            var getWithSubCode = (from d in results where d.SubjectID.ToString() == subddlCode.SelectedItem.Value select d.MatricNo).Distinct().ToList();
            
            allStudentID = getWithFalc.Intersect(getWithSubCode).OrderBy(a=>a).ToList();
           
            subddlCode.Visible = false;
            level.Visible = false;
            getAllStudent.Visible = false;
            school.Visible = false;
            pageNo.Visible = false;
        }

        protected void subddlCode_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }



}