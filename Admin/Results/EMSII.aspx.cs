﻿using School365.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace School365.Admin.Results
{
    public partial class EMSII : System.Web.UI.Page
    {
        public StudentModel db = new StudentModel();
        public List<String> AllStudentsMatric = new List<String>();
        public int SerialValue = 0;
        int getCurrentSessionID = 0;

        public static int pages = 2;
        public static int upper = pages;
        public static int lower = 0;


        public string SubjectName = " ";
        public double SubjectUnits = 0;
        public string SubjectValue = " ";
        public string Semester = " ";
        public string getSessionYear = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }
        protected void getAllStudent_Click(object sender, EventArgs e)
        {
            SubjectName = (from d in db.Subjects where d.SubjectCode == subjectCode.SelectedItem.Text.Trim() select d.SubjectName).FirstOrDefault();
            SubjectUnits = (from d in db.Subjects where d.SubjectCode == subjectCode.SelectedItem.Text.Trim() select d.SubjectValue).FirstOrDefault();
            SubjectValue = (from d in db.Subjects where d.SubjectCode == subjectCode.SelectedItem.Text.Trim() select d.SubjectUnit).FirstOrDefault();
            Semester = (from d in db.Subjects where d.SubjectCode == subjectCode.SelectedItem.Text.Trim() select d.Semester).FirstOrDefault();
            getSessionYear = (from d in db.Sessions where d.CurrentSession == true select d.SessionYear).FirstOrDefault();


            getCurrentSessionID = (from d in db.Sessions where d.CurrentSession == true select d.SessionID).FirstOrDefault();
            var results = db.Results.Where(a=>a.SessionID==getCurrentSessionID).AsQueryable();
            AllStudentsMatric = (from d in db.Subjects where d.SubjectCode.Trim() == subjectCode.SelectedItem.Text.Trim() && d.SubjectLevel == level.SelectedItem.Text join f in results on d.SubjectID equals f.SubjectID select f.MatricNo).OrderBy(a => a).ToList();
            
            subjectCode.Visible = false;
            level.Visible = false;
            getAllStudent.Visible = false;
            pageNo.Visible = false;
        }
    }
}