﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DetailedII.aspx.cs" Inherits="School365.Admin.Results.DetailedII" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>DETAILED</title>
    <style type="text/css">
        .auto-style2 {
            height: 97px;
            width: 117px;
        }

        @media print {
            tr.page-break {
                display: block;
                page-break-before: always;
            }
        }

        table tr.page-break {
            page-break-after: always;
        }

        .auto-style3 {
            font-size: medium;
        }

        .auto-style4 {
            font-size: small;
        }

        .auto-style5 {
            text-align: center;
        }

        .auto-style6 {
            text-align: left;
        }
    </style>
</head>
<body>
    <div id="tableWrap">
        <form id="form1" runat="server" style="font-family: 'Segoe UI'">

            <div>
            </div>

            <table align="center" style="width: 100%">
                <thead align="center">
                    <tr>
                        <td>
                            <img alt="" class="auto-style2" src="Image2.PNG" align="left">
                            <p align="center"><strong>FEDERAL COLLEGE OF EDUCATION ABEOKUTA</strong></p>
                            <p align="center" class="auto-style3" style="font-size: small">NCE DETAILED RESULT</p>
                            <p style="font-size: small" align="left">
                                SCHOOL:
                                    <asp:Label ID="school" runat="server" Text=" " />
                                <span class="auto-style4">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PROGRAM :<asp:Label ID="program" runat="server" Text=" " />&nbsp;&nbsp;LEVEL :<asp:Label ID="StdLvl" runat="server" Text=" " />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SEMESTER : <%Response.Write(Semester); %>&nbsp;&nbsp;</span>SESSION:&nbsp;&nbsp;
                                    <asp:Label ID="session" runat="server" Text=" "></asp:Label>
                            </p>
                        </td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <span class="auto-style7">Date Printed&nbsp;<%Response.Write(DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString());  %></span></td>
                    </tr>
                </tbody>
            </table>
            <%           
                int prev = constant;
                int init = 0;
                var allStdID = (from d in getStudentID select new { d.StudentID, d.MatricNo }).Distinct().OrderBy(a => a.MatricNo).ToList();
                int total = allStdID.Count();
                int loopCount = (int)(total / constant);
                for (int i = 0; i <= loopCount; i++)
                {
                    var newAllStdID = allStdID.Skip(init).Take(constant).Select(a => a.StudentID);
            %>

            <table style="width: 100%; height: 100%; margin-top: 5px; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; border-left: 1px solid #000; font-size: 12px;" border="0" cellspacing="0">

                <tr>
                    <th style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style1" colspan=""></th>
                    <th style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style1" colspan=""></th>
                    <th style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style1" colspan=""></th>
                    <%                
                        foreach (int setSub in getSubjectID)
                        {
                            var getSubjectDetail = (from d in getAllSubject where d.SubjectID == setSub select d).Distinct().FirstOrDefault();
                    %>
                    <th style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style1" colspan="1"><%Response.Write(getSubjectDetail == null ? "NA" : getSubjectDetail.SubjectCode + "[" + getSubjectDetail.SubjectValue + getSubjectDetail.SubjectUnit + "]");%></th>
                    <%  }%>
                    <%-- <th style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style1" colspan="4">PREVIOUS</th>--%>
                    <th style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style1" colspan="4">CURRENT</th>
                </tr>

                <tr>
                    <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5">S/N</td>
                    <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5">NAME</td>
                    <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5">MATRIC NO</td>
                    <%                
                        foreach (int setSub in getSubjectID)
                        {
                    %>
                    <%--<td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write("CA");%></td>
                    <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write("EX");%></td>--%>
                    <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write("TO");%></td>
                    <%--               <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write("GP");%></td>--%>
                    <%  
                        }%>

                    <%--                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5">TCP</td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5">TNU</td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5">TNUP</td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5">CGPA</td>--%>

                    <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5">TCP</td>
                    <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5">TNU</td>
                    <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5">TNUP</td>
                    <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5">CGPA</td>

                    <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5" colspan="2">REMARK</td>
                </tr>


                <tr>

                    <%
                        foreach (int setStudentID in newAllStdID)
                        {
                            var getStudentDetails = (from d in getAllResult where d.StudentID == setStudentID join r in db.Students on d.StudentID equals r.StudentID select new { r.Surname, r.Firstname, r.Middlename, r.StudentID, d.MatricNo, d.CGPA, d.CTNU, d.CTNUP, d.CTCP, d.Level }).FirstOrDefault();
                            double getPrevLvl = int.Parse(Level) - 100;
                            int getFailed = 0;
                            int _sessionid = int.Parse(Sessions);
                            //if (Semester == "1")
                            //{
                            //    getFailed = (from d in db.Results where d.StudentID == setStudentID && (d.CA + d.EXAM) <= 39 && d.Semester == 1 && d.SessionID == _sessionid select d.SubjectID).Count();
                            //}
                            //else
                            //{
                            //    getFailed = (from d in db.Results where d.StudentID == setStudentID && (d.CA + d.EXAM) <= 39 && d.SessionID == _sessionid select d.SubjectID).Count();
                            //}
                            if (Semester == "1" && int.Parse(Level) == 100)
                            {
                                getFailed = (from d in db.Results where d.StudentID == setStudentID && (d.CA + d.EXAM) <= 39 && d.Semester == 1 && d.SessionID == _sessionid select d.SubjectID).Count();
                            }
                            else if (Semester == "1" && int.Parse(Level) > 100)
                            {
                                int _num = 0;
                                var _failed = (from d in db.Results where d.StudentID == setStudentID && (d.CA + d.EXAM) <= 39 && d.Semester == 1 && d.SessionID == _sessionid select new { d.SubjectID, d.Subjects.Semester, d.Subjects.SubjectLevel }).ToList();
                                foreach (var item in _failed)
                                {
                                    if (item.SubjectLevel == Level && item.Semester == "2")
                                        _num = _num + 0;
                                    else
                                        _num = _num + 1;
                                }
                                getFailed = _num;
                            }
                            //getFailed = (from d in db.Results where d.StudentID == setStudentID.StudentID && (d.CA + d.EXAM) <= 39 && d.Semester == 1 && d.SessionID == _sessionid select d.SubjectID).Count();

                            else
                            {
                                getFailed = (from d in db.Results where d.StudentID == setStudentID && (d.CA + d.EXAM) <= 39 && d.SessionID == _sessionid select d.SubjectID).Count();
                            }

                            serialValue = serialValue + 1;
                    %>
                    <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(serialValue); %></td>
                    <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000; text-transform: uppercase" class="auto-style6"><%Response.Write(getStudentDetails.Surname + " " + getStudentDetails.Firstname + " " + getStudentDetails.Middlename); %></td>
                    <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000; text-transform: uppercase" class="auto-style1"><%Response.Write(getStudentDetails.MatricNo);%></td>

                    <%foreach (int setSub in getSubjectID)
                        {
                            var getSubjectDetail = (from h in getAllResult where h.StudentID == setStudentID && h.SubjectID == setSub join d in db.Subjects on h.SubjectID equals d.SubjectID select new { h.CA, h.EXAM, d.SubjectValue }).FirstOrDefault();
                    %>
                    <%--<td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(getSubjectDetail == null ? "NA" : getSubjectDetail.CA.ToString()); %></td>
                    <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(getSubjectDetail == null ? "NA" : getSubjectDetail.EXAM.ToString()); %></td>--%>
                    <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(getSubjectDetail == null ? "NA" : (getSubjectDetail.CA + getSubjectDetail.EXAM).ToString()); %></td>
                    <%--<td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(getSubjectDetail == null ? 0.00 : (util.GradeValue(getSubjectDetail.EXAM + getSubjectDetail.CA)) * getSubjectDetail.SubjectValue); %></td>--%>
                    <%}
                    %>

                    <%-- <%if (getPrevLvl == 0 && Semester == "1")
                  {
                %>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write("0"); %></td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write("0"); %></td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write("0");%></td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write("0.00");%></td>
                <%}
                  else
                  {
                      var getPrevStudentDetails = (from d in db.Results where d.StudentID == setStudentID && d.Level == getPrevLvl join r in db.Students on d.StudentID equals r.StudentID select new { d.CGPA, d.CTNU, d.CTNUP, d.CTCP, d.Level }).FirstOrDefault();
                %>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(util.PrevCummTcp(int.Parse(Level), setStudentID, int.Parse(Semester))); %></td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(util.PrevCummTnu(int.Parse(Level), setStudentID, int.Parse(Semester))); %></td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(util.PrevCummTnup(int.Parse(Level), setStudentID, int.Parse(Semester)));%></td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(string.Format("{0:0.00}", util.PrevCummTcp(int.Parse(Level), setStudentID, int.Parse(Semester)) / util.PrevCummTnu(int.Parse(Level), setStudentID, int.Parse(Semester))));%></td>
                <% }
                %>--%>
                    <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(util.AllTcp(int.Parse(Level), setStudentID, int.Parse(Semester))); %></td>
                    <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(util.AllTnu(int.Parse(Level), setStudentID, int.Parse(Semester), int.Parse(Sessions))); %></td>
                    <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(util.AllTnup(int.Parse(Level), setStudentID, int.Parse(Semester)));%></td>
                    <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(string.Format("{0:0.00}", util.AllTcp(int.Parse(Level), setStudentID, int.Parse(Semester)) / util.AllTnu(int.Parse(Level), setStudentID, int.Parse(Semester), int.Parse(Sessions))));%></td>
                    <td style="border-bottom: 1px solid #000; border-top: 1px solid #000;" class="auto-style1"></td>
                    <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-right: 1px solid #000;" class="auto-style1">
                        <%var _outStanding = util.GetOutstandingResult(setStudentID, setSubID, int.Parse(Level), int.Parse(Sessions), int.Parse(Semester)).Count(); %>
                        <%Response.Write(getFailed > 0 || _outStanding > 0 ? " F. " : "PASSED");%>
                        <%Response.Write(_outStanding > 0 ? "O/S " : "");%>
                        <%
                            foreach (int setSub in getAllSubID)
                            {
                                var getSubjectDetail = (from h in db.Results where h.StudentID == setStudentID && h.SubjectID == setSub && (h.CA + h.EXAM) <= 39 join d in db.Subjects on h.SubjectID equals d.SubjectID select new { d.SubjectCode, d.SubjectUnit, d.Semester, d.SubjectLevel, d.SubjectValue, h.CA, h.EXAM }).FirstOrDefault();
                                if (Semester == "1" && int.Parse(Level) == 100)
                                {
                                    getSubjectDetail = (from h in getAllResult where h.StudentID == setStudentID && h.SubjectID == setSub && (h.CA + h.EXAM) <= 39 && h.Semester == 1 join d in db.Subjects on h.SubjectID equals d.SubjectID select new { d.SubjectCode, d.SubjectUnit, d.Semester, d.SubjectLevel, d.SubjectValue, h.CA, h.EXAM }).FirstOrDefault();
                                }
                                if (Semester == "1" && int.Parse(Level) > 100)
                                {
                                    getSubjectDetail = (from h in db.Results where h.StudentID == setStudentID && h.SubjectID == setSub && (h.CA + h.EXAM) <= 39 join d in db.Subjects on h.SubjectID equals d.SubjectID select new { d.SubjectCode, d.SubjectUnit, d.Semester, d.SubjectLevel, d.SubjectValue, h.CA, h.EXAM, }).FirstOrDefault();
                                    if (getSubjectDetail != null)
                                    {
                                        if (getSubjectDetail.SubjectLevel == Level && getSubjectDetail.Semester == "2")
                                            getSubjectDetail = null;
                                    }
                                }
                        %>
                        <%Response.Write(getSubjectDetail == null ? "" : "  " + getSubjectDetail.SubjectCode + " " + "(" + getSubjectDetail.SubjectValue + " " + getSubjectDetail.SubjectUnit + ")" + " , " + (getSubjectDetail.CA + getSubjectDetail.EXAM) + " | "); %>
                        <% }
                        %>
                        <%var outStanding = util.GetOutstandingResult(setStudentID, setSubID, int.Parse(Level), int.Parse(Sessions), int.Parse(Semester)).ToList(); %>
                        <%foreach (var item in outStanding)
                            {
                        %>
                        <%Response.Write("  " + item.SubjectCode + " " + "(" + "" + item.SubjectValue + " " + item.SubjectUnit + ")" + "|"); %>
                        <%}

                        %>
                    </td>
                </tr>
                <% } %>
            </table>
            <hr />
            <table style="width: 100%; height: 100%; margin-top: 5px; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; border-left: 1px solid #000; font-size: 12px;" border="0" cellspacing="0">

                <tr>
                    <th style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style1" colspan=""></th>
                    <th style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style1" colspan=""></th>
                    <th style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style1" colspan=""></th>
                    <%                
                        foreach (int setSub in _getSubjectID)
                        {
                            var _getSubjectDetail = (from d in getAllSubject where d.SubjectID == setSub select d).Distinct().FirstOrDefault();
                    %>
                    <th style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style1" colspan="1"><%Response.Write(_getSubjectDetail == null ? "NA" : _getSubjectDetail.SubjectCode + "[" + _getSubjectDetail.SubjectValue + _getSubjectDetail.SubjectUnit + "]");%></th>
                    <%  }%>
                    <%-- <th style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style1" colspan="4">PREVIOUS</th>--%>
                    <th style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style1" colspan="4">CURRENT</th>
                </tr>

                <tr>
                    <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5">S/N</td>
                    <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5">NAME</td>
                    <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5">MATRIC NO</td>
                    <%                
                        foreach (int setSub in _getSubjectID)
                        {
                    %>
                    <%--                    <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write("CA");%></td>
                    <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write("EX");%></td>--%>
                    <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write("TO");%></td>
                    <%--                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write("GP");%></td>--%>
                    <%  
                        }%>

                    <%--                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5">TCP</td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5">TNU</td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5">TNUP</td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5">CGPA</td>--%>

                    <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5">TCP</td>
                    <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5">TNU</td>
                    <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5">TNUP</td>
                    <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5">CGPA</td>

                    <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5" colspan="2">REMARK</td>
                </tr>


                <tr>

                    <%
                        foreach (int setStudentID in newAllStdID)
                        {
                            var getStudentDetails = (from d in getAllResult where d.StudentID == setStudentID join r in db.Students on d.StudentID equals r.StudentID select new { r.Surname, r.Firstname, r.Middlename, r.StudentID, d.MatricNo, d.CGPA, d.CTNU, d.CTNUP, d.CTCP, d.Level }).FirstOrDefault();
                            double getPrevLvl = int.Parse(Level) - 100;
                            int getFailed = 0;

                            getFailed = util.GetCarryOvers(setStudentID, int.Parse(Semester), int.Parse(Sessions), int.Parse(Level), setSubID) == null ? 0 : util.GetCarryOvers(setStudentID, int.Parse(Semester), int.Parse(Sessions), int.Parse(Level), setSubID).Select(a => a.SubjectID).ToList() == null ? 0 : util.GetCarryOvers(setStudentID, int.Parse(Semester), int.Parse(Sessions), int.Parse(Level), setSubID).Select(a => a.SubjectID).Count();

                            //if (Semester == "1")
                            //{
                            //    getFailed = (from d in db.Results where d.StudentID == setStudentID && (d.CA + d.EXAM) <= 39 && d.Semester == 1 select d.SubjectID).Count();
                            //}
                            //else
                            //{
                            //    getFailed = (from d in db.Results where d.StudentID == setStudentID && (d.CA + d.EXAM) <= 39 select d.SubjectID).Count();
                            //}

                            // serialValue = serialValue + 1;
                            _serialValue = _serialValue + 1;

                    %>
                    <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(_serialValue); %></td>
                    <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style6"><%Response.Write(getStudentDetails.Surname + " " + getStudentDetails.Firstname + " " + getStudentDetails.Middlename); %></td>
                    <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style1"><%Response.Write(getStudentDetails.MatricNo);%></td>

                    <%foreach (int setSub in _getSubjectID)
                        {
                            var getSubjectDetail = (from h in getAllResult where h.StudentID == setStudentID && h.SubjectID == setSub join d in db.Subjects on h.SubjectID equals d.SubjectID select new { h.CA, h.EXAM, d.SubjectValue }).FirstOrDefault();
                    %>
                    <%--  <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(getSubjectDetail == null ? "NA" : getSubjectDetail.CA.ToString()); %></td>
                    <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(getSubjectDetail == null ? "NA" : getSubjectDetail.EXAM.ToString()); %></td>--%>
                    <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(getSubjectDetail == null ? "NA" : (getSubjectDetail.CA + getSubjectDetail.EXAM).ToString()); %></td>
                    <%--                    <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(getSubjectDetail == null ? 0.00 : (util.GradeValue(getSubjectDetail.EXAM + getSubjectDetail.CA)) * getSubjectDetail.SubjectValue); %></td>--%>
                    <%}
                    %>
                    <%-- <%if (getPrevLvl == 0 && Semester == "1")
                  {
                %>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write("0"); %></td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write("0"); %></td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write("0");%></td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write("0.00");%></td>
                <%}
                  else
                  {
                      var getPrevStudentDetails = (from d in db.Results where d.StudentID == setStudentID && d.Level == getPrevLvl join r in db.Students on d.StudentID equals r.StudentID select new { d.CGPA, d.CTNU, d.CTNUP, d.CTCP, d.Level }).FirstOrDefault();
                %>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(util.PrevCummTcp(int.Parse(Level), setStudentID, int.Parse(Semester))); %></td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(util.PrevCummTnu(int.Parse(Level), setStudentID, int.Parse(Semester))); %></td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(util.PrevCummTnup(int.Parse(Level), setStudentID, int.Parse(Semester)));%></td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(string.Format("{0:0.00}", util.PrevCummTcp(int.Parse(Level), setStudentID, int.Parse(Semester)) / util.PrevCummTnu(int.Parse(Level), setStudentID, int.Parse(Semester))));%></td>
                <% }
                %>--%>
                    <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(util.AllTcp(int.Parse(Level), setStudentID, int.Parse(Semester))); %></td>
                    <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(util.AllTnu(int.Parse(Level), setStudentID, int.Parse(Semester), int.Parse(Sessions))); %></td>
                    <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(util.AllTnup(int.Parse(Level), setStudentID, int.Parse(Semester)));%></td>
                    <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(string.Format("{0:0.00}", util.AllTcp(int.Parse(Level), setStudentID, int.Parse(Semester)) / util.AllTnu(int.Parse(Level), setStudentID, int.Parse(Semester), int.Parse(Sessions))));%></td>
                    <td style="border-bottom: 1px solid #000; border-top: 1px solid #000;" class="auto-style1"></td>
                    <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-right: 1px solid #000;" class="auto-style1">
                        <%var _outStanding = util.GetOutstandingResult(setStudentID, setSubID, int.Parse(Level), int.Parse(Sessions), int.Parse(Semester)).Count(); %>
                        <%Response.Write(getFailed > 0 || _outStanding > 0 ? " F. " : "PASSED");%>
                        
                        <%
                            int SessionID = int.Parse(Sessions);
                            foreach (int setSub in getAllSubID)
                            {
                                var _showScore = new School365.Model.CarrryOverViewModel();
                                //var _getSubjectDetail = util.GetCarryOvers(setStudentID.StudentID, int.Parse(Semester), int.Parse(Sessions), int.Parse(Level), setSubID).ToList();
                                //var getSubjectDetail = (from x in _getSubjectDetail join d in db.Subjects on x.SubjectID equals d.SubjectID select new { d.SubjectCode, d.SubjectUnit, d.SubjectValue, x.CA, x.EXAM }).FirstOrDefault();
                                var getSubjectDetail = (from h in db.Results where h.StudentID == setStudentID && h.SessionID == SessionID && h.SubjectID == setSub && (h.CA + h.EXAM) <= 39 join d in db.Subjects on h.SubjectID equals d.SubjectID select new { d.SubjectCode, d.SubjectUnit, d.Semester, d.SubjectLevel, d.SubjectValue, h.CA, h.EXAM }).FirstOrDefault();

                                if (getSubjectDetail == null)
                                {
                                    _showScore = null;
                                }
                                else
                                {
                                    _showScore = util.CarryOver(setStudentID).Where(a => a.SubjectCode == getSubjectDetail.SubjectCode).FirstOrDefault();
                                }

                                if (Semester == "1" && int.Parse(Level) == 100)
                                {
                                    getSubjectDetail = (from h in getAllResult where h.StudentID == setStudentID && h.SessionID == SessionID && h.SubjectID == setSub && (h.CA + h.EXAM) <= 39 && h.Semester == 1 join d in db.Subjects on h.SubjectID equals d.SubjectID select new { d.SubjectCode, d.SubjectUnit, d.Semester, d.SubjectLevel, d.SubjectValue, h.CA, h.EXAM }).FirstOrDefault();
                                }
                                if (Semester == "1" && int.Parse(Level) > 100)
                                {
                                    getSubjectDetail = (from h in db.Results where h.StudentID == setStudentID && h.SessionID == SessionID && h.SubjectID == setSub && (h.CA + h.EXAM) <= 39 join d in db.Subjects on h.SubjectID equals d.SubjectID select new { d.SubjectCode, d.SubjectUnit, d.Semester, d.SubjectLevel, d.SubjectValue, h.CA, h.EXAM, }).FirstOrDefault();
                                    if (getSubjectDetail != null)
                                    {
                                        if (getSubjectDetail.SubjectLevel == Level && getSubjectDetail.Semester == "2")
                                            getSubjectDetail = null;
                                    }

                                }

                        %>
                        <% 
                            if (_showScore != null && getSubjectDetail.SubjectCode == _showScore.SubjectCode && getSubjectDetail.CA == 0 && getSubjectDetail.EXAM == 0)
                            {
                                Response.Write(0 == null ? "" : "  " + getSubjectDetail.SubjectCode + " " + "(" + getSubjectDetail.SubjectValue + " " + getSubjectDetail.SubjectUnit + ")" + " , " + (_showScore.Ca + _showScore.Exam) + " | "); %>

                        <% }
                            else
                                Response.Write(getSubjectDetail == null ? "" : "  " + getSubjectDetail.SubjectCode + " " + "(" + getSubjectDetail.SubjectValue + " " + getSubjectDetail.SubjectUnit + ")" + " , " + (getSubjectDetail.CA + getSubjectDetail.EXAM) + " | ");
                        %>
                        <% }
                        %>
                        <%Response.Write(_outStanding > 0 ? "O/S " : "");%>
                        <%var outStanding = util.GetOutstandingResult(setStudentID, setSubID, int.Parse(Level), int.Parse(Sessions), int.Parse(Semester)).ToList(); %>


                        <%foreach (var item in outStanding)
                            {
                        %>
                        <%Response.Write(" " + item.SubjectCode + " " + "(" + "" + item.SubjectValue + " " + item.SubjectUnit + ")" + "|"); %>
                        <%}

                        %>
                    </td>
                </tr>
                <% } %>
            </table>


            <% 

            %>
            <p style="page-break-before: always"></p>
            <%   
                init = init + constant;
                prev = prev + prev;
            %>
            <%             
                }
            %>
        </form>
    </div>
    <div class="col-lg-3">
        <input type="button" id="DownloadExcel" value="Download Excel" class="btn btn-sm btn-danger" />
    </div>
    <script src="../../js/jquery.js"></script>
    <script type="text/javascript">
        $('#DownloadExcel').click(function () {
            var url = 'data:application/vnd.ms-excel,' + encodeURIComponent($('#tableWrap').html())
            location.href = url
            return false
        })
    </script>
</body>
</html>
