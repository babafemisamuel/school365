﻿using School365.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace School365.Admin.Results
{
    public partial class Mastermarksheet1 : System.Web.UI.Page
    {
        public StudentModel db = new StudentModel();
        public List<int> getSubjectID = new List<int>();
        public List<Subject> getAllSubject = new List<Subject>();
        public List<Result> getAllResult = new List<Result>();
        public List<Result> getStudentID = new List<Result>();
        public List<Result> getAllCarryOver = new List<Result>();
        public List<Result> carrOverResult = new List<Result>();
        public int serialValue = 0;
        public string Sessions = " ";
        public string Semester = "";
        public int constant = 0;
        public string Level = "";
        public int setSubID = 0;
        public Util util = new Util();
        public int curriculumid = 0;

        StringBuilder StrHtmlGenerate = new StringBuilder();
        StringBuilder StrExport = new StringBuilder();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                constant = int.Parse(Request.QueryString["pages"].ToString());
                session.Text = (from d in db.Sessions where d.CurrentSession == true select d.SessionYear).FirstOrDefault();
                string getSubID = Request.QueryString["sumCombID"].ToString();
                Level = Request.QueryString["level"].ToString();
                Sessions = Request.QueryString["sessions"].ToString();
                Semester = Request.QueryString["semester"].ToString();
                curriculumid = int.Parse(Request.QueryString["curriculumid"].ToString());
                setSubID = int.Parse(getSubID);

                var getAllCombined = (from d in db.AllCombineds 
                                      where d.SubjectCombineID == setSubID && d.CurricullumID == curriculumid
                                      join s in db.Subjects on d.SubjectID equals s.SubjectID
                                      select new { s.SubjectID, s.Semester, s.SubjectLevel, d.DepartmentID }).Distinct().OrderByDescending(s => s.DepartmentID).ToList();

                string getSubName = (from d in db.SubjectCombinations where d.SubjectCombinID == setSubID select d.SubjectCombinName).FirstOrDefault();
                string[] split = Regex.Split(getSubName, "/");
                string DeptCode = split[0].ToString();
                string FacultyID = (from d in db.Departments where d.DepartmentCode == DeptCode select d.FacultyID).FirstOrDefault();

                school.Text = (from d in db.Facultys where d.FacultyID.ToString() == FacultyID select d.FacultyName).FirstOrDefault();
                program.Text = (from d in db.SubjectCombinations where d.SubjectCombinID == setSubID select d.SubjectCombinName).FirstOrDefault();
                session.Text = (from d in db.Sessions where d.SessionID.ToString() == Sessions select d.SessionYear).FirstOrDefault();
                StdLvl.Text = Level;


                if (Semester == "1")
                {
                    getSubjectID = (from d in getAllCombined where d.SubjectLevel == Level && d.Semester == Semester select d.SubjectID).ToList();
                    getAllSubject = (from d in db.Subjects where d.SubjectLevel == Level select d).AsEnumerable().ToList();
                    getStudentID = (from r in db.Results where r.SubjectCombinID == setSubID && r.Semester == 1 && r.SessionID.ToString() == Sessions && r.Level.ToString() == Level select r).Distinct().ToList();
                }
                else
                {
                    //updated semester
                    getSubjectID = (from d in getAllCombined where d.SubjectLevel == Level && d.Semester == Semester select d.SubjectID).Distinct().ToList();
                    getAllSubject = (from d in db.Subjects where d.SubjectLevel == Level select d).AsEnumerable().ToList();
                    getStudentID = (from r in db.Results.OrderBy(a=>a.MatricNo) where r.SubjectCombinID == setSubID && r.Semester == 2 && r.SessionID.ToString() == Sessions && r.Level.ToString() == Level select r).Distinct().ToList();
                }

                getAllResult = (from h in db.Results where h.SubjectCombinID == setSubID && h.SessionID.ToString() == Sessions && h.Level.ToString() == Level select h).AsEnumerable().ToList();

                carrOverResult = util.GetCourseCarryOver(setSubID,int.Parse(Sessions), Level);
            }

        }


    }
}