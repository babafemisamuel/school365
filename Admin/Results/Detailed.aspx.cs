﻿using School365.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace School365.Admin.Results
{
    public partial class MasterMarkSheet : System.Web.UI.Page
    {
        public StudentModel db = new StudentModel();
        public List<int> getSubjectID = new List<int>();
        public List<int> _getSubjectID = new List<int>();
        public List<int> getAllSubID = new List<int>();
        public List<Subject> getAllSubject = new List<Subject>();
        public List<Subject> _getAllSubject = new List<Subject>();
        public List<Result> carrOverResult = new List<Result>();

        public Util util = new Util();


        //public List<AllCombined> getAllCombinedSubject = new List<AllCombined>();
        public List<Result> getAllResult = new List<Result>();
        public List<Result> getStudentID = new List<Result>();

        public int serialValue = 0;
        public int _serialValue = 0;
        public int StudentCount = 0;
        public string Semester = "";
        public string Level = "";
        public int constant = 0;
        public string Sessions = "";
        public int setSubID = 0;
        public int curriculumid = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                session.Text = (from d in db.Sessions where d.CurrentSession == true select d.SessionYear).FirstOrDefault();
                string getSubID = Request.QueryString["sumCombID"].ToString();
                Level = Request.QueryString["level"].ToString();
                Sessions = Request.QueryString["sessions"].ToString();
                Semester = Request.QueryString["semester"].ToString();
                constant = int.Parse(Request.QueryString["pages"].ToString());
                curriculumid= int.Parse(Request.QueryString["curriculumid"].ToString());

                setSubID = int.Parse(getSubID);

                var getAllCombined = (from d in db.AllCombineds 
                                      where d.SubjectCombineID == setSubID && d.CurricullumID== curriculumid
                                      join s in db.Subjects on d.SubjectID equals s.SubjectID
                                      select new { d.DepartmentID, s.SubjectID, s.Semester, s.SubjectLevel }).Distinct().OrderByDescending(x => x.DepartmentID).ToList();

                var _getAllWithoutConstant = getAllCombined.Where(a => a.DepartmentID != 7 && a.DepartmentID != 8).Select(a => a).ToList();

                var getConstantOnly = getAllCombined.Where(a => a.DepartmentID == 7 || a.DepartmentID == 8).Select(a => a).ToList();



                string getSubName = (from d in db.SubjectCombinations where d.SubjectCombinID == setSubID select d.SubjectCombinName).FirstOrDefault();
                string[] split = Regex.Split(getSubName, "/");
                string DeptCode = split[0].ToString();
                string FacultyID = (from d in db.Departments where d.DepartmentCode == DeptCode select d.FacultyID).FirstOrDefault();

                school.Text = (from d in db.Facultys where d.FacultyID.ToString() == FacultyID select d.FacultyName).FirstOrDefault();
                program.Text = (from d in db.SubjectCombinations where d.SubjectCombinID == setSubID select d.SubjectCombinName).FirstOrDefault();
                session.Text = (from d in db.Sessions where d.SessionID.ToString() == Sessions select d.SessionYear).FirstOrDefault();
                StdLvl.Text = Level;



                if (Semester == "1")
                {
                    getSubjectID = (from d in _getAllWithoutConstant where d.SubjectLevel == Level && d.Semester == Semester select d.SubjectID).Distinct().ToList();
                    getAllSubject = (from d in db.Subjects where d.SubjectLevel == Level && d.Semester == Semester select d).AsEnumerable().ToList();

                    _getSubjectID = (from d in getConstantOnly where d.SubjectLevel == Level && d.Semester == Semester select d.SubjectID).Distinct().ToList();
                    getAllSubID = (from d in getAllCombined where d.SubjectLevel == Level && d.Semester == Semester select d.SubjectID).Distinct().ToList();
                    //_getAllSubject = (from d in db.Subjects where d.SubjectLevel == Level && d.Semester == Semester select d).AsEnumerable().ToList();
                }
                else
                {

                    getSubjectID = (from d in _getAllWithoutConstant where d.SubjectLevel == Level && d.Semester == Semester select d.SubjectID).Distinct().ToList();
                    getAllSubject = (from d in db.Subjects where d.SubjectLevel == Level && d.Semester == Semester select d).AsEnumerable().ToList();

                    _getSubjectID = (from d in getConstantOnly where d.SubjectLevel == Level && d.Semester == Semester select d.SubjectID).Distinct().ToList();
                    _getAllSubject = (from d in db.Subjects where d.SubjectLevel == Level select d).AsEnumerable().ToList();
                    getAllSubID = (from d in getAllCombined where d.SubjectLevel == Level select d.SubjectID).Distinct().ToList();
                }

                StudentCount = (from r in db.Results where r.SubjectCombinID == setSubID && r.SessionID.ToString() == Sessions select r.StudentID).Distinct().Count();

                getAllResult = (from h in db.Results.OrderBy(a=>a.MatricNo) where h.SubjectCombinID == setSubID && h.SessionID.ToString() == Sessions && h.Level.ToString() == Level select h).AsEnumerable().ToList();

                getStudentID = (from r in db.Results.OrderBy(a=>a.MatricNo) where r.SubjectCombinID == setSubID && r.SessionID.ToString() == Sessions && r.Level.ToString() == Level select r).Distinct().ToList();
                carrOverResult = util.GetCourseCarryOver(setSubID, int.Parse(Sessions), Level);
            }
        }
    }
}