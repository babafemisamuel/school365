﻿using School365.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace School365.Admin.Results
{
    public partial class Summarys : System.Web.UI.Page
    {
       
        public StudentModel db = new StudentModel();
        public List<Result> ResultData = new List<Result>();
        public List<int> getSubjectCombinationID = new List<int>();
        
        public List<Result> getAllResult = new List<Result>();
        public List<int> getSubjectID = new List<int>();
        public List<int> getAllSubjectID = new List<int>();
        public List<Result> carrOverResult = new List<Result>();
        public Util util= new Util();
        public int serialValue = 0;
        public string Semester = "";
        public string Sessions = "";
        public string Level = "";
        public int constant = 0;
        public int setSubID = 0;
        public int curriculumid = 0;
        public string subcourse = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            //pages = int.Parse(Request.QueryString["pages"].ToString());
            if (!IsPostBack)
            {
                if (Request.QueryString["subcourse"] != null)
                    subcourse = Request.QueryString["subcourse"].ToString();

                Level = Request.QueryString["level"].ToString();
                Sessions = Request.QueryString["sessions"].ToString();
                Semester = Request.QueryString["semester"].ToString();
                constant = int.Parse(Request.QueryString["pages"].ToString());
                curriculumid = int.Parse(Request.QueryString["curriculumid"].ToString());

                //pages=pages - pages;
                string getSubID = Request.QueryString["sumCombID"].ToString();
                 setSubID = int.Parse(getSubID);
                
                var getAllCombined = (from d in db.AllCombineds 
                                      where d.SubjectCombineID == setSubID && d.CurricullumID == curriculumid
                                      join s in db.Subjects on d.SubjectID equals s.SubjectID
                                      select new { s.SubjectID, s.Semester, s.SubjectLevel }).Distinct().ToList();

                string getSubName = (from d in db.SubjectCombinations where d.SubjectCombinID == setSubID select d.SubjectCombinName).FirstOrDefault();
                string[] split = Regex.Split(getSubName, "/");
                string DeptCode = split[0].ToString();
                string DeptCodeI= split[1].ToString();
                string FacultyID = (from d in db.Departments where d.DepartmentCode == DeptCode select d.FacultyID).FirstOrDefault();

                school.Text = (from d in db.Facultys where d.FacultyID.ToString() == FacultyID select d.FacultyName).FirstOrDefault();
                program.Text = (from d in db.SubjectCombinations where d.SubjectCombinID == setSubID select d.SubjectCombinName).FirstOrDefault();
                session.Text = (from d in db.Sessions where d.SessionID.ToString() == Sessions select d.SessionYear).FirstOrDefault();
                StdLvl.Text = Level;
                double _level = double.Parse(Level);

                string _getyear = session.Text.Split('/')[0].Substring(2,2);

                if (Semester == "1")
                {
                    getSubjectID = (from d in getAllCombined where d.SubjectLevel == Level && d.Semester == Semester select d.SubjectID).Distinct().ToList();
                    //getStudentID = (from r in db.Results where r.SubjectCombinID == setSubID && r.Semester == 1 && r.SessionID.ToString() == Sessions && r.Level.ToString() == Level orderby r.MatricNo select r.StudentID).Distinct().ToList();
                    if(subcourse=="")
                        ResultData = (from r in db.Results where r.SubjectCombinID == setSubID && r.Semester == 1 && r.SessionID.ToString() == Sessions && r.Students.Level== _level select r).Distinct().OrderBy(r=>r.MatricNo).ToList();
                    else
                        ResultData = (from r in db.Results where r.SubjectCombinID == setSubID && r.Semester == 1 && r.SessionID.ToString() == Sessions && r.Students.Level == _level && r.Students.SubCourse==subcourse select r).Distinct().OrderBy(r => r.MatricNo).ToList();


                }
                else
                {

                    getSubjectID = (from d in getAllCombined where d.SubjectLevel == Level && d.Semester == Semester select d.SubjectID).Distinct().ToList();
                    //getStudentID = (from r in db.Results where r.SubjectCombinID == setSubID && r.SessionID.ToString() == Sessions && r.Level.ToString() == Level select r.StudentID).Distinct().ToList();
                    if (subcourse == "")
                        ResultData = (from r in db.Results where r.SubjectCombinID == setSubID && r.Semester == 2 && r.SessionID.ToString() == Sessions && r.Students.Level== _level select r).Distinct().OrderBy(r => r.MatricNo).ToList();
                    else
                        ResultData = (from r in db.Results where r.SubjectCombinID == setSubID && r.Semester == 2 && r.SessionID.ToString() == Sessions && r.Students.Level == _level && r.Students.SubCourse==subcourse select r).Distinct().OrderBy(r => r.MatricNo).ToList();
                }
                //including carryovers
                getAllSubjectID = (from h in db.Results where h.SessionID.ToString() == Sessions && h.Level.ToString() == Level select h.SubjectID).OrderBy(h => h).ToList();


                // = (from d in db.AllCombineds where d.SubjectCombineID == setSubID select d.DepartmentID).Distinct().OrderByDescending(s => s).ToList();

                int DeptSubjI = (from d in db.Departments where d.DepartmentCode == DeptCode.ToString() select d.DepartmantID).FirstOrDefault();
                int DeptSubjII = (from d in db.Departments where d.DepartmentCode == DeptCodeI.ToString() select d.DepartmantID).FirstOrDefault();

                List<int> DataIDs = new List<int>();
                if (int.Parse(Level) >= 300)
                {
                    DataIDs = new List<int> { DeptSubjI, DeptSubjII, 7, 37 };
                }
                else
                {
                    DataIDs = new List<int> { DeptSubjI, DeptSubjII, 7};
                }
                

                var InitSubCombID = (from d in db.AllCombineds where d.SubjectCombineID == setSubID select d.DepartmentID).Distinct().ToList();
                var newData= (from d in db.AllCombineds where d.SubjectCombineID == setSubID && d.CurricullumID == curriculumid select d.DepartmentID).Distinct().OrderByDescending(s => s).ToList();

                //the lenght of the array
              
                //int[] alluserid = newData.ToArray();
                
                IEnumerable<int> orderedData = newData.OrderByDescending(
                item => Enumerable.Reverse(DataIDs).ToList().IndexOf(item));
                getSubjectCombinationID = orderedData.ToList();
                if (getSubjectCombinationID.Count != 0)
                {
                    int getval = getSubjectCombinationID.FindIndex(a => a == 37);
                    if (getval > 0)
                    {
                        int lastval = getSubjectCombinationID.Last();
                        int count = getSubjectCombinationID.Count();
                        getSubjectCombinationID.RemoveAt(getval);
                        getSubjectCombinationID.Add(37);
                    }
                   
                }
                if (getSubjectCombinationID.Contains(38) == true)
                {
                    getSubjectCombinationID.Remove(38);
                }
                if (int.Parse(Level) < 300)
                {
                    getSubjectCombinationID.Contains(37);
                    getSubjectCombinationID.Remove(37);
                }
                


                getAllResult = (from h in db.Results where h.SubjectCombinID == setSubID && h.SessionID.ToString() == Sessions && h.Level.ToString() == Level select h).AsEnumerable().ToList();

                carrOverResult = util.GetCourseCarryOver(setSubID, int.Parse(Sessions), Level);

               
            }                     
        }
    }
}