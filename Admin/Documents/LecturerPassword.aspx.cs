﻿using School365.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace School365.Admin.Documents
{
    public partial class LecturerPassword : System.Web.UI.Page
    {
        public StudentModel db = new StudentModel();
        public List<int> LecturerID = new List<int>();
        public int SerialNo = 0;
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void getLecturer_Click(object sender, EventArgs e)
        {
            if (lecturer.SelectedItem.Text== "Password Changed")
            {
                LecturerID = (from d in db.Lecturers where d.PasswordChanged == true select d.LecturerID).ToList();
                LecturerText.Text = "ALL LECTURER THAT HAVE CHANGED THIER PASSWORD";
            }
            else
            {
                LecturerID = (from d in db.Lecturers where d.PasswordChanged == false select d.LecturerID).ToList();
                LecturerText.Text = "ALL LECTURER THAT HAVE NOT CHANGED THIER PASSWORD";
            }
            getLecturer.Visible = false;
            lecturer.Visible = false;
        }
    }
}