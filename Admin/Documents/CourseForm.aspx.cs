﻿using School365.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace School365.Admin.Documents
{
    public partial class CourseForm : System.Web.UI.Page
    {
        public StudentModel db = new StudentModel();
        public string value = "";
        public List<int> StudentID = new List<int>();
        public int sn = 0;
        public int snI = 0;
        public double totalUnit = 0;
        public double totalUnitII = 0;
        public string SessionYear = "";
        public double StudentLevel = 0;
        int getAdminID = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            getAdminID = int.Parse(User.Identity.Name);
            var getAuth = (from d in db.RoleAdmins where d.AdminID == getAdminID select d.RoleID).ToList();
            if (getAuth.Contains(8) != true)
            {
                Response.Redirect("../Administrator/Profile.aspx");
            }

        }

        protected void getCourse_Click(object sender, EventArgs e)
        {
            // value = Request.QueryString["grab"].ToString();

            SessionYear = (from d in db.Sessions where d.SessionID.ToString() == allSession.SelectedItem.Value select d.SessionYear).FirstOrDefault();
            StudentID = (from d in db.Students where d.MatricNo.Trim().ToUpper() == Mat.Text.Trim().ToUpper() join f in db.Results on d.StudentID equals f.StudentID select d.StudentID).ToList();
            StudentLevel= (from d in db.Students where d.MatricNo.Trim().ToUpper() == Mat.Text.Trim().ToUpper() join f in db.Results on d.StudentID equals f.StudentID select d.Level).FirstOrDefault();
            var studentDetailCheck = (from d in db.Students where d.MatricNo.Trim().ToUpper() == Mat.Text.Trim().ToUpper() select d).ToList();

            if (studentDetailCheck.Count != 0)
            {
                var studentDetail = studentDetailCheck.FirstOrDefault();
                names.Text = studentDetail == null ? " " : studentDetail.Surname.ToUpper() + " " + studentDetail.Firstname.ToUpper() + " " + studentDetail.Middlename.ToUpper();
                email.Text = studentDetail == null ? " " : studentDetail.Email;
                country.Text = studentDetail == null ? " " : studentDetail.Nationality;
                gender.Text = studentDetail == null ? " " : studentDetail.Gender;
                state.Text = studentDetail == null ? " " : studentDetail.SOR;
                phoneNo.Text = studentDetail == null ? " " : studentDetail.PhoneNumber;
                lga.Text = studentDetail == null ? " " : studentDetail.LGA;
                program.Text = studentDetail == null ? " " : studentDetail.Major + "/" + studentDetail.Minor;
                level.Text = studentDetail == null ? " " : studentDetail.Level.ToString();
                matricno.Text = studentDetail == null ? " " : studentDetail.MatricNo;
                if (studentDetail.ImageName == null)
                {
                    //studentImage.ImageUrl = " ";
                }
                else
                {
                    studentImage.ImageUrl = "data:image/jpeg;base64," + Convert.ToBase64String(studentDetail.StudentImage);
                }
                allSession.Visible = false;
                Mat.Visible = false;
                getCourse.Visible = false;
            }



        }
    }
}