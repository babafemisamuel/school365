﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="StudentListByLevel.aspx.cs" Inherits="School365.Admin.Documents.StudentListByLevel" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body style="font-family: 'Segoe UI'">
    <form id="form1" runat="server">
        <div>
            <asp:DropDownList ID="Session" runat="server" DataSourceID="LinqDataSource1" DataTextField="SessionYear" DataValueField="SessionID">
            </asp:DropDownList>
            &nbsp;&nbsp;&nbsp;
        <asp:DropDownList ID="Semesters" runat="server">
            <asp:ListItem>1</asp:ListItem>
            <asp:ListItem>2</asp:ListItem>
        </asp:DropDownList>
            &nbsp;&nbsp;
        <asp:Button ID="GetGrades" runat="server" Text="Get All Students" OnClick="GetGrades_Click" />
            <asp:LinqDataSource ID="LinqDataSource1" runat="server" ContextTypeName="School365.Model.StudentModel" EntityTypeName="" OrderBy="SessionYear" Select="new (SessionID, SessionYear)" TableName="Sessions">
            </asp:LinqDataSource>

        </div>
        <table align="center">
            <tr>
                <td colspan="2">
                    <h2>FEDERAL COLLEGE OF EDUCATION, ABEOKUTA </h2>
                </td>
                <td></td>
            </tr>
            <tr>
                <td>
                    <h4>&nbsp;&nbsp;&nbsp; List of All Instructors by Departments</h4>
                </td>
                <td>
                    <h4>FCE - <%Response.Write(Semesters.SelectedItem.Text + "st SEMESTER " + Session.SelectedItem.Text); %></h4>
                </td>
            </tr>
        </table>
        <table style="width: 100%; margin-top: 5px; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; border-left: 1px solid #000; font-size: 12px;" border="0" cellspacing="0">
            <tr>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5">LEVEL</td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5">NO OF STD</td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5">PERCENTAGE</td>
            </tr>
            <%
                var getAllLevelRegistered = (from d in db.Results where d.Semester == Semester && d.SessionID == SessionID select d.Level).Distinct().ToList();
                double StudentCount = (from d in db.Results where d.Semester == Semester && d.SessionID == SessionID select d.StudentID).Distinct().Count();
                foreach (var item in getAllLevelRegistered)
                {
                    var NoOfStuudents = (from d in db.Results where d.Semester == Semester && d.SessionID == SessionID select d.StudentID).Distinct().Count();
            %>
            <tr>
                <%-- <td><% %></td>--%>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(item); %></td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(NoOfStuudents); %></td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(String.Format("{0:0.00}",(NoOfStuudents * 100 / StudentCount))); %> &#37;</td>
            </tr>
            <%}
            %>
            <tr>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"></td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write("Total No " + StudentCount); %></td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"></td>
            </tr>
        </table>

    </form>
</body>
</html>
