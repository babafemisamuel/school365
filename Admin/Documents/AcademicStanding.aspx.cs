﻿using School365.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace School365.Admin.Documents
{
    public partial class AcademicStanding : System.Web.UI.Page
    {
        public StudentModel db = new StudentModel();
        public List<int> StudentID = new List<int>();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void InsertBtn_Click(object sender, EventArgs e)
        {
            StudentID = (from d in db.Results
                         where d.MatricNo.Trim() == StudentMatric.Text.Trim()
                         select d.StudentID).Distinct().ToList();
        }
    }
}