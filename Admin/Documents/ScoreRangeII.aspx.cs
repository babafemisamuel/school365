﻿using School365.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace School365.Admin.Documents
{
    public partial class ScoreRangeII : System.Web.UI.Page
    {
        StudentModel db = new StudentModel();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void UsrSubmit_Click(object sender, EventArgs e)
        {
            double _def = 0;
            int sessionID = int.Parse(UsrSession.SelectedItem.Value);
            double level = double.Parse(Level.SelectedItem.Value);
            int Semster = int.Parse(Semester.SelectedItem.Value);
            double subComb = double.Parse(SubjectComb.SelectedItem.Value);
            double score1 = double.TryParse(Score1.Text, out _def) == true ? _def : 0;
            double score2 = double.TryParse(Score2.Text, out _def) == true ? _def : 0;
            var getScore = db.Results
                .Where(a => a.SessionID == sessionID && a.Level == level && a.Semester == Semster && a.SubjectID == subComb && (a.CA + a.EXAM) >= score1 && (a.CA + a.EXAM) <= score2).Select(a => new { a.MatricNo, a.Students.Surname, a.Students.Firstname, a.SubjectCombinations.SubjectCombinName, a.Subjects.SubjectCode, a.CA, a.EXAM }).OrderBy(a => a.MatricNo).ToList();
            allScore.DataSource = getScore;
            allScore.DataBind();
            UsrSession.Visible = false;
            Level.Visible = false;
            SubjectComb.Visible = false;
            Score1.Visible = false;
            Score2.Visible = false;
            Semester.Visible = false;
            UsrSubmit.Visible = false;

        }

        protected void allScore_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            //if (e.Row.RowType == DataControlRowType.DataRow)
            //{
            //    double _def = 0;
            //    double score1 = double.TryParse(Score1.Text, out _def) == true ? _def : 0;
            //    double score2 = double.TryParse(Score2.Text, out _def) == true ? _def : 0;
            //    double ca = double.TryParse(e.Row.Cells[3].Text,out _def) == true ? _def : 0;
            //    double exam = double.TryParse(e.Row.Cells[4].Text, out _def) == true ? _def : 0;
            //    e.Row.Cells[5].Text = (ca + exam).ToString();
            //}
        }
    }
}