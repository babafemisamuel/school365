﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LecturerCourses.aspx.cs" Inherits="School365.Admin.Documents.Lecturer_Courses" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body style="font-family:'Segoe UI'">
    <p align="center">
        <h2>Lecturers and their courses</h2>
    </p>
    <form id="form1" runat="server">
        <div>
            <table style="width: 100%; margin-top: 5px; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; border-left: 1px solid #000; font-size: 12px;" border="0" cellspacing="0">
                <tr>
                    <th style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style1" colspan="">Lecture Names</th>
                    <th style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style1" colspan="">Subject Names</th>

                </tr>

                <%
                    foreach (var item in Lecturers)
                    {
                        var LecturerNames = Lecturers.Where(a => a.LecturerID == item.LecturerID).Select(a => new { a.Firstname,a.Middlename,a.Surname }).FirstOrDefault();
                        var getSubjects = LecturersProfiles.Where(a => a.LecturerID == item.LecturerID).Select(a => a.Subjects.SubjectCode);
                        //var getSubjectName = Subjects.Where(a => a.SubjectID == item).Select(a => a.SubjectCode).FirstOrDefault();


                        //var allLecturer = (from d in LecturersProfiles
                        //                   where d.SubjectID == item
                        //                   select new { d.LecturerID, d.Lectures.Firstname, d.Lectures.Surname }).ToList();
                        //join h in Lecturers on d.LecturerID equals h.LecturerID


                %>
                <tr>
                    <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style1" colspan=""><%Response.Write(LecturerNames.Surname+" "+ LecturerNames.Firstname+" "+LecturerNames.Middlename); %></td>
                    <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style1" colspan="">
                        <%
                        foreach (var setLecturer in getSubjects)
                            {
                        %>

                        <%Response.Write(setLecturer == null ? " " : setLecturer+ ", "); %>

                        <%}

                        %>
                    </td>
                </tr>
                <%

                    }
                %>
            </table>
        </div>
    </form>
</body>
</html>
