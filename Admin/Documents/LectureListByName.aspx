﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LectureListByName.aspx.cs" Inherits="School365.Admin.Documents.LectureListByName" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:DropDownList ID="Session" runat="server" DataSourceID="LinqDataSource1" DataTextField="SessionYear" DataValueField="SessionID">
            </asp:DropDownList>
            &nbsp;&nbsp;&nbsp;
        <asp:DropDownList ID="Semesters" runat="server">
            <asp:ListItem>1</asp:ListItem>
            <asp:ListItem>2</asp:ListItem>
        </asp:DropDownList>
            &nbsp;&nbsp;
        <asp:Button ID="GetGrades" runat="server" Text="Get Lecturers" OnClick="GetGrades_Click" />
            <asp:LinqDataSource ID="LinqDataSource1" runat="server" ContextTypeName="School365.Model.StudentModel" EntityTypeName="" OrderBy="SessionYear" Select="new (SessionID, SessionYear)" TableName="Sessions">
            </asp:LinqDataSource>

        </div>
        <table align="center">
            <tr>
                <td colspan="2">
                    <h2>FEDERAL COLLEGE OF EDUCATION, ABEOKUTA </h2>
                </td>
                <td></td>
            </tr>
            <tr>
                <td>
                    <h4>&nbsp;&nbsp;&nbsp; List of All Instructors by Departments</h4>
                </td>
                <td>
                    <h4>FCE - <%Response.Write(Semesters.SelectedItem.Text + "st SEMESTER " + Session.SelectedItem.Text); %></h4>
                </td>
            </tr>
        </table>
       <table style="width: 100%; margin-top: 5px; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; border-left: 1px solid #000; font-size: 12px;" border="0" cellspacing="0">
           <tr>
 <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5">LECTURER NAMES</td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5">TOTAL COURSES</td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5">PERCENTAGE</td>                
           </tr> 
           
           <%
                var GetAllLecturersProfile = (from d in db.LecturerProfiles join f in db.Lecturers on d.LecturerID equals f.LecturerID select new { d.LecturerID, f.Firstname, f.Middlename, f.Surname }).Distinct().OrderBy(a=>a.Surname).ToList();
                foreach (var item in GetAllLecturersProfile)
                {%>
            <tr>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(item.Surname.ToUpper() + " " + item.Firstname.ToUpper() + " " + item.Middlename.ToUpper()); %></td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(School365.Util.AllLecturersCourses(item.LecturerID, Semester, SessionID)); %></td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(string.Format("{0:0.00}",(School365.Util.AllLecturersCourses(item.LecturerID, Semester, SessionID) *100/School365.Util.AllInstructorsReg(Semester, SessionID)))); %>&#37;</td>                
            </tr>
            <%}
               
            %>
            <tr><td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write("Total :" + School365.Util.AllInstructorsReg(Semester, SessionID)); %></td></tr>
        </table>
    </form>
</body>
</html>
