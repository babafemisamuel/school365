﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="StudentListByGender.aspx.cs" Inherits="School365.Admin.Documents.StudentListByGender" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <div>
                <asp:DropDownList ID="Session" runat="server" DataSourceID="LinqDataSource1" DataTextField="SessionYear" DataValueField="SessionID">
                </asp:DropDownList>
                &nbsp;&nbsp;&nbsp;
        <asp:DropDownList ID="Semesters" runat="server">
            <asp:ListItem>1</asp:ListItem>
            <asp:ListItem>2</asp:ListItem>
        </asp:DropDownList>
                &nbsp;&nbsp;
        <asp:Button ID="GetGrades" runat="server" Text="Get Data" OnClick="GetGrades_Click" Style="height: 26px" />
                <asp:LinqDataSource ID="LinqDataSource1" runat="server" ContextTypeName="School365.Model.StudentModel" EntityTypeName="" OrderBy="SessionYear" Select="new (SessionID, SessionYear)" TableName="Sessions">
                </asp:LinqDataSource>

                <table align="center" style="font-family:'Times New Roman'">
            <tr>
                <td colspan="2">
                    <h2 class="auto-style2">FEDERAL COLLEGE OF EDUCATION, ABEOKUTA </h2>
                </td>
                <td></td>
            </tr>
            <tr>
                <td class="auto-style1">
                    <h5>&nbsp;&nbsp;&nbsp; List Of All Students By Major/Gender</h5>
                </td>
                <td class="auto-style1">
                    <h5>FCE - <%Response.Write(Semesters.SelectedItem.Text + " SEMESTER " + Session.SelectedItem.Text); %></h5>
                </td>
            </tr>
        </table>
            </div>
            <%
                double AllStudents = 0;
                var allSubComb = (from d in db.Results
                                  where d.SessionID == SessionID && d.Semester == Semester
                                  join s in db.SubjectCombinations on d.SubjectCombinID equals s.SubjectCombinID
                                  select new { d.SubjectCombinID, s.SubjectCombinName }).Distinct().OrderBy(a=>a.SubjectCombinName).ToList();
            %>
        </div>
        <%
            foreach (var item in allSubComb)
            {
                 AllStudents = util.AllStudentsCount(Semester, SessionID);
                double FeMaleTotal = (School365.NewUtil.AllStdByGender(SessionID, item.SubjectCombinID, Semester, "FEMALE"));
                double MaleTotal = (School365.NewUtil.AllStdByGender(SessionID, item.SubjectCombinID, Semester, "MALE"));
                %>
        <h4><u><%Response.Write(item.SubjectCombinName); %></u></h4>
         <table style="font-family:'Times New Roman'; width:100%; font-size:13px" align="center">                    
            <tr>
                <td>GENDER</td>
                <td>NO. OF STUDENT</td>
                <td>PERCENTAGE</td>
            </tr>
            <tr>
                <td style="border-bottom:1px solid #000;"></td>
                <td style="border-bottom:1px solid #000;"></td>
                <td style="border-bottom:1px solid #000;"></td>
            </tr>
            <tr>
                <td align="left">FEMALE</td>            
                <td align="left"><%Response.Write(School365.NewUtil.AllStdByGender(SessionID,item.SubjectCombinID,Semester,"FEMALE")); %></td>
                <td><%Response.Write(string.Format("{0:0.00}", School365.NewUtil.AllStdByGender(SessionID,item.SubjectCombinID,Semester,"FEMALE") * 100/AllStudents)); %>&#37;</td>
            </tr>
              
            <tr>
                <td align="left">MALE</td>
                <td align="left"><%Response.Write(School365.NewUtil.AllStdByGender(SessionID,item.SubjectCombinID,Semester,"MALE")); %></td>
                  <td><%Response.Write(string.Format("{0:0.00}", School365.NewUtil.AllStdByGender(SessionID,item.SubjectCombinID,Semester,"MALE") * 100/AllStudents)); %>&#37;</td>
            </tr>
             <tr>
                 <td></td>
                 <td></td>
                 <td>SubTotal&nbsp; - <%Response.Write(FeMaleTotal+MaleTotal); %></td>
             </tr>

        </table>
       
            <%}

             %>
        <p align="center">Full Total: <%Response.Write(AllStudents); %></p>
    </form>
</body>
</html>
