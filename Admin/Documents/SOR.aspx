﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SOR.aspx.cs" Inherits="School365.Admin.Documents.SOR" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 163px;
        }

        .auto-style2 {
            width: 681px;
        }

        .auto-style3 {
            width: 90px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:DropDownList ID="Session" runat="server" DataSourceID="LinqDataSource1" DataTextField="SessionYear" DataValueField="SessionID">
            </asp:DropDownList>
            &nbsp;&nbsp;&nbsp;
        <asp:DropDownList ID="Semesters" runat="server">
            <asp:ListItem>1</asp:ListItem>
            <asp:ListItem>2</asp:ListItem>
        </asp:DropDownList>
            <asp:DropDownList runat="server" ID="Level"></asp:DropDownList>
            &nbsp;&nbsp;
            <asp:TextBox ID="MatricNo" runat="server" placeholder="Matric No"></asp:TextBox>
            <asp:Button ID="GetGrades" runat="server" Text="Get All Students" OnClick="GetGrades_Click" />
            <asp:LinqDataSource ID="LinqDataSource1" runat="server" ContextTypeName="School365.Model.StudentModel" EntityTypeName="" OrderBy="SessionYear" Select="new (SessionID, SessionYear)" TableName="Sessions">
            </asp:LinqDataSource>

            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
        </div>
        <%
            var StudentDetails = (from d in db.Students where d.MatricNo == MatricNo.Text select new { d.Surname, d.Firstname, d.Middlename, d.Major, d.Minor }).FirstOrDefault();
        %>
        <%
            if (StudentDetails != null)
            {
        %>

        <table style="width: 100%; font-family: 'Times New Roman'; font-size: 12px">
            <tr>
                <td class="auto-style1">STUDENT NUMBER</td>
                <td class="auto-style2"><%Response.Write(MatricNo.Text); %></td>
                <td class="auto-style3">SESSION</td>
                <td><%Response.Write(Session.SelectedItem.Text); %></td>

            </tr>
            <tr>
                <td class="auto-style1">NAME</td>
                <td class="auto-style2"><%Response.Write(StudentDetails.Surname + " " + StudentDetails.Firstname + " " + StudentDetails.Firstname); %></td>
                <td class="auto-style3">SEMESTER</td>
                <td><%Response.Write(Semesters.Text); %></td>
            </tr>
            <tr>
                <td class="auto-style1">SUBJECT COMBINATION</td>
                <td class="auto-style2"><%Response.Write(StudentDetails.Major + "/" + StudentDetails.Minor); %></td>
                <td class="auto-style3">LEVEL</td>
                <td><%Response.Write(Level.Text); %></td>
            </tr>
        </table>
        <% }
        %>
        <br />
        <table style="font-family: 'Times New Roman'; width: 100%; margin-top: 5px; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; border-left: 1px solid #000; font-size: 16px;" border="0" cellspacing="0">
            <tr>
                <td colspan="6" align="center">CUMMULATIVE</td>
                <%-- <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>--%>
                <td align="center">OUTSTANDING SUBJECT(S)</td>

            </tr>
            <tr>
                <td></td>
                <td>CTCP</td>
                <td>CTNU</td>
                <td>CTNUP</td>
                <td>CGPA</td>
                <td>GRADE</td>
                <td></td>
                <%-- <td></td>--%>
            </tr>
            <%
                int Levels = int.Parse(Level.SelectedItem.Text);
                int getStudentID = (from d in db.Results
                                    where d.MatricNo == MatricNo.Text
                                    && d.Level == Levels
                                    && d.SessionID == SessionID
                                    && d.MatricNo == MatricNo.Text
                                    && d.Semester == Semester
                                    select d.StudentID).FirstOrDefault();

                var getAllDept = (from d in db.Results
                                  where d.MatricNo == MatricNo.Text
                                  && d.Level == Levels
                                  && d.SessionID == SessionID
                                  && d.MatricNo == MatricNo.Text
                                  && d.Semester == Semester
                                  join s in db.Departments
                                  on d.DepartmentID equals s.DepartmantID
                                  select new { s.DepartmentCode, s.DepartmantID, s.DepartmentName, d.StudentID }).Distinct().OrderBy(a => a.DepartmentCode).ToList();
            %>
            <%
                foreach (var item in getAllDept)
                {
                    double getFailed = (from d in db.Results
                                        where d.StudentID == item.StudentID
                                        && d.DepartmentID == item.DepartmantID
                                        && d.SessionID == SessionID
                                        && d.Semester == Semester
                                        && d.Level == Levels
                                        && d.EXAM + d.CA <= 39
                                        select d.SubjectID).Count();
                    double cgpa = double.Parse(String.Format("{0:0.00}", util.CummDeptTcp(Levels, item.StudentID, Semester, item.DepartmantID) / util.CummDeptTnu(Levels, item.StudentID, Semester, item.DepartmantID, SessionID)));
            %>
            <tr>
                <td><%Response.Write(item.DepartmentName); %></td>
                <td><%Response.Write(util.CummDeptTcp(Levels, item.StudentID, Semester, item.DepartmantID)); %></td>
                <td><%Response.Write(util.CummDeptTnu(Levels, item.StudentID, Semester, item.DepartmantID, SessionID)); %></td>
                <td><%Response.Write(util.CummDeptTnup(Levels, item.StudentID, Semester, item.DepartmantID)); %></td>
                <td><%Response.Write(String.Format("{0:0.00}", util.CummDeptTcp(Levels, item.StudentID, Semester, item.DepartmantID) / util.CummDeptTnu(Levels, item.StudentID, Semester, item.DepartmantID, SessionID))); %></td>
                <td><%Response.Write(util.Grade(cgpa)); %></td>
                <td align="center"><%Response.Write(getFailed); %></td>
            </tr>
            <%
                }

            %>
            <% 
                double _cgpa = double.Parse(String.Format("{0:0.00}", util.AllTcp(Levels, getStudentID, Semester) / util.AllTnu(Levels, getStudentID, Semester, SessionID)));
                double _getFailed = (from d in db.Results
                                     where d.StudentID == getStudentID
                                     && d.SessionID == SessionID
                                     && d.Semester == Semester
                                     && d.Level == Levels
                                     && d.EXAM + d.CA <= 39
                                     select d.SubjectID).Count();
            %>
            <tr>
                <td><%Response.Write("OVERALL PERFOMANCE"); %></td>
                <td><%Response.Write(util.AllTcp(Levels, getStudentID, Semester)); %></td>
                <td><%Response.Write(util.AllTnu(Levels, getStudentID, Semester, SessionID)); %></td>
                <td><%Response.Write(util.AllTnup(Levels, getStudentID, Semester)); %></td>
                <td><%Response.Write(String.Format("{0:0.00}", util.AllTcp(Levels, getStudentID, Semester) / util.AllTnu(Levels, getStudentID, Semester, SessionID))); %></td>
                <td><%Response.Write(util.Grade(_cgpa)); %></td>
                <td align="center"><%Response.Write(_getFailed); %></td>
            </tr>
        </table>
        <br />
        <br />
        <table style="width: 100%; font-family: 'Times New Roman'; font-size: 12px">
            <tr>
                <td>CUMMULATIVE TOTAL CREDIT POINTS</td>
                <td align="right">CGPA</td>
                <td align="right">GRADE</td>
                <td align="right">LETTER</td>
            </tr>
            <tr>
                <td>CUMMULATIVE TOTAL NUMBER OF UNITS</td>
                <td>4.50-5.00</td>
                <td>DISTINCTION</td>
                <td>A</td>
            </tr>
            <tr>
                <td>CUMMULATIVE GRADE POINT AVERAGE</td>
                <td>3.50-4.49</td>
                <td>CREDIT</td>
                <td>B</td>

            </tr>
            <tr>
                <td>CUMMULATIVE TOTAL NUMBER OF UNIT PASSED</td>
                <td>2.40-3.49</td>
                <td>MERIT</td>
                <td>C</td>
            </tr>
            <tr>
                <td></td>
                <td>1.50-2.39</td>
                <td>PASS</td>
                <td>D</td>
            </tr>
            <tr>
                <td></td>
                <td>1.00-1.49</td>
                <td>PASS</td>
                <td>E</td>
            </tr>
            <tr>
                <td></td>
                <td>0.00-0.99</td>
                <td>FAIL</td>
                <td>F</td>
            </tr>

        </table>
    </form>
    <br />
    <br />
    <br />
    <br />
    <br />
    <p align="center">-----------------------------------------------------</p>
    <p align="center">REGISTRAR</p>
</body>
</html>
