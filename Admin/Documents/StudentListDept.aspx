﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="StudentListDept.aspx.cs" Inherits="School365.Admin.Documents.StudentListDept" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>

    <form id="form1" runat="server">

        <asp:DropDownList ID="allDept" runat="server" DataSourceID="LinqDataSource1" DataTextField="DepartmentName" DataValueField="DepartmantID" Height="16px">
        </asp:DropDownList>
        <asp:LinqDataSource ID="LinqDataSource1" runat="server" ContextTypeName="School365.Model.StudentModel" EntityTypeName="" Select="new (DepartmentName, DepartmantID)" TableName="Departments">
        </asp:LinqDataSource>

        <br />
        <div>
            <%
                int DeptID = int.Parse(allDept.SelectedItem.Value);
                var getState = (from d in db.Results where d.DepartmentID ==DeptID  select d.Level).Distinct().ToList();
                foreach (var item in getState)
                {
                    var getRegData = (from d in db.Students
                                      where d.Level == item
                                      select d.SOR).Distinct().ToList();
            %>

            <table>
                <tr>
                    <td>STATE</td>
                    <td>No of States</td>
                    <td>Male</td>
                    <td>Female</td>
                    <td>Percentage</td>
                </tr>


                <%   
                    foreach (var items in getRegData)
                    {

                %>
                <tr>
                <td><%Response.Write(items); %></td>

                </tr>

                <% }

                    }
                %>

                <tr>
                </tr>

            </table>
        </div>
    </form>
</body>
</html>
