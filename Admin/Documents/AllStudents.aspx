﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AllStudents.aspx.cs" Inherits="School365.Admin.Documents.AllStudents" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <%--<asp:DropDownList ID="allSchools" runat="server" DataSourceID="LinqDataSource1" DataTextField="FacultyName" DataValueField="FacultyID"></asp:DropDownList>
        <asp:LinqDataSource ID="LinqDataSource1" runat="server" ContextTypeName="School365.Model.StudentModel" EntityTypeName="" OrderBy="FacultyID" Select="new (FacultyID, FacultyName)" TableName="Facultys">
        </asp:LinqDataSource>--%>
        <asp:DropDownList ID="major" runat="server" DataSourceID="LinqDataSource2" DataTextField="DepartmentCode" DataValueField="DepartmentCode">

        </asp:DropDownList>
        <asp:DropDownList ID="minor" runat="server" DataSourceID="LinqDataSource2" DataTextField="DepartmentCode" DataValueField="DepartmentCode">

        </asp:DropDownList>
        <asp:DropDownList ID="Level" runat="server">
            <asp:ListItem>100</asp:ListItem>
            <asp:ListItem>200</asp:ListItem>
            <asp:ListItem>300</asp:ListItem>
            <asp:ListItem>400</asp:ListItem>
            <asp:ListItem>500</asp:ListItem>
            <asp:ListItem>600</asp:ListItem>
        </asp:DropDownList>
        <asp:LinqDataSource ID="LinqDataSource2" runat="server" ContextTypeName="School365.Model.StudentModel" EntityTypeName="" Select="new (DepartmentName, DepartmentCode)" TableName="Departments">
        </asp:LinqDataSource>
        <asp:Button ID="getStds" runat="server" Text="Get Students" OnClick="getStds_Click" />
        <table style="width: 100%; margin-top: 5px; border-top: 1px solid #aaa; border-bottom: 1px solid #aaa; font-size: x-small;" border="0" cellspacing="0">
            <tr>
                <th style="border-bottom: 1px solid #aaa; border-top: 1px solid #aaa; border-left: 1px solid #aaa; border-right: 1px solid #aaa;" class="auto-style1" colspan="">S/N</th>
                <th style="border-bottom: 1px solid #aaa; border-top: 1px solid #aaa; border-left: 1px solid #aaa; border-right: 1px solid #aaa;" class="auto-style1" colspan="">Surname</th>
                <th style="border-bottom: 1px solid #aaa; border-top: 1px solid #aaa; border-left: 1px solid #aaa; border-right: 1px solid #aaa;" class="auto-style1" colspan="">Firstname</th>
                <th style="border-bottom: 1px solid #aaa; border-top: 1px solid #aaa; border-left: 1px solid #aaa; border-right: 1px solid #aaa;" class="auto-style1" colspan="">Lastname</th>
                <th style="border-bottom: 1px solid #aaa; border-top: 1px solid #aaa; border-left: 1px solid #aaa; border-right: 1px solid #aaa;" class="auto-style1" colspan="">Matric No</th>
                               <th style="border-bottom: 1px solid #aaa; border-top: 1px solid #aaa; border-left: 1px solid #aaa; border-right: 1px solid #aaa;" class="auto-style1" colspan="">Sex</th>
                <th style="border-bottom: 1px solid #aaa; border-top: 1px solid #aaa; border-left: 1px solid #aaa; border-right: 1px solid #aaa;" class="auto-style1" colspan="">State Of Origin</th>
                <th style="border-bottom: 1px solid #aaa; border-top: 1px solid #aaa; border-left: 1px solid #aaa; border-right: 1px solid #aaa;" class="auto-style1" colspan="">SubjectCombinations</th>
                <th style="border-bottom: 1px solid #aaa; border-top: 1px solid #aaa; border-left: 1px solid #aaa; border-right: 1px solid #aaa;" class="auto-style1" colspan="">Faculty</th>
            </tr>
            <tr>
                <%
                    foreach (int setStudentID in StudentID)
                    {
                        var getStudent = (from d in db.Students where d.StudentID == setStudentID select new { d.Surname, d.Firstname, d.Middlename, d.FacultyName, d.MatricNo, d.Major, d.Minor,d.SOR,d.Gender }).OrderBy(s => s.FacultyName).FirstOrDefault();
                        SerialNo = SerialNo + 1;
                %>
                <td style="border-bottom: 1px solid #aaa; border-top: 1px solid #aaa; border-left: 1px solid #aaa; border-right: 1px solid #aaa;text-transform:uppercase;" class="auto-style1" colspan=""><%Response.Write(SerialNo); %></td>
                <td style="border-bottom: 1px solid #aaa; border-top: 1px solid #aaa; border-left: 1px solid #aaa; border-right: 1px solid #aaa;text-transform:uppercase;" class="auto-style1" colspan=""><%Response.Write(getStudent.Surname); %></td>
                <td style="border-bottom: 1px solid #aaa; border-top: 1px solid #aaa; border-left: 1px solid #aaa; border-right: 1px solid #aaa;text-transform:uppercase;" class="auto-style1" colspan=""><%Response.Write(getStudent.Firstname); %></td>
                <td style="border-bottom: 1px solid #aaa; border-top: 1px solid #aaa; border-left: 1px solid #aaa; border-right: 1px solid #aaa;text-transform:uppercase;" class="auto-style1" colspan=""><%Response.Write(getStudent.Middlename); %></td>
                <td style="border-bottom: 1px solid #aaa; border-top: 1px solid #aaa; border-left: 1px solid #aaa; border-right: 1px solid #aaa;" class="auto-style1" colspan=""><%Response.Write(getStudent.MatricNo); %></td>
                                <td style="border-bottom: 1px solid #aaa; border-top: 1px solid #aaa; border-left: 1px solid #aaa; border-right: 1px solid #aaa;" class="auto-style1" colspan=""><%Response.Write(getStudent.Gender); %></td>
                <td style="border-bottom: 1px solid #aaa; border-top: 1px solid #aaa; border-left: 1px solid #aaa; border-right: 1px solid #aaa;" class="auto-style1" colspan=""><%Response.Write(getStudent.SOR); %></td>
                <td style="border-bottom: 1px solid #aaa; border-top: 1px solid #aaa; border-left: 1px solid #aaa; border-right: 1px solid #aaa;" class="auto-style1" colspan=""><%Response.Write(getStudent.Major + "/" + getStudent.Minor); %></td>
                <td style="border-bottom: 1px solid #aaa; border-top: 1px solid #aaa; border-left: 1px solid #aaa; border-right: 1px solid #aaa;" class="auto-style1" colspan=""><%Response.Write(getStudent.FacultyName); %></td>
            </tr>
            <% } %>
        </table>
    </form>
</body>
</html>
