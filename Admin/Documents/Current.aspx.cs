﻿using School365.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace School365.Admin.Documents
{
    public partial class Current : System.Web.UI.Page
    {
        public StudentModel db = new StudentModel();
        public List<String> StdID = new List<String>();
        public int SerialNo = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var getStud = (from d in db.Results select d.Level).Distinct().ToList();
                level.DataSource = getStud;
                level.DataBind();
            }
            
        }

        protected void getStudents_Click(object sender, EventArgs e)
        {
            level.Visible = false;
            session.Visible = false;
            check.Visible = false;
            getStudents.Visible = false;
            string val = level.SelectedItem.Text;
            if (check.SelectedItem.Value== "Registered")
            {
                StdID = (from d in db.Results
                         where d.Level.ToString() == level.SelectedItem.Text
                         && d.SessionID.ToString() == session.SelectedItem.Value
                         select d.MatricNo).OrderBy(d => d).Distinct().ToList();
                registered.Text = "REGISTERED STUDENTS";
            }
            else
            {
                var getFromStd= (from d in db.Students
                                 where d.Level.ToString() == level.SelectedItem.Text
                                 select d.MatricNo).OrderBy(d => d).Distinct().ToList();
                var getFromResult= (from d in db.Results
                                    where d.Level.ToString() == level.SelectedItem.Text
                                    && d.SessionID.ToString() == session.SelectedItem.Value
                                    select d.MatricNo).OrderBy(d => d).Distinct().ToList();

                StdID = getFromStd.Except(getFromResult).ToList();
                registered.Text = "UNREGISTERED STUDENTS";
            }
           
        }
    }
}