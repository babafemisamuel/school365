﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CourseForm.aspx.cs" Inherits="School365.Admin.Documents.CourseForm" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style3 {
            width: 220px;
        }

        .auto-style5 {
            width: 252px;
        }

        .auto-style6 {
            width: 85px;
            height: 64px;
        }

        .auto-style7 {
            font-size: small;
            font-weight: bold;
            width: 201px;
        }

        .auto-style8 {
            font-size: small;
        }

        .auto-style9 {
            width: 290px;
            font-size: small;
            height: 41px;
        }

        .auto-style10 {
            font-size: small;
            font-weight: bold;
            width: 292px;
        }

        .auto-style11 {
            width: 201px;
        }

        .auto-style12 {
            font-weight: normal;
            font-size: small;
        }

        .auto-style13 {
            width: 199px;
        }

        .auto-style14 {
            width: 200px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:TextBox ID="Mat" runat="server" placeholder="Matric No"></asp:TextBox>
            <asp:DropDownList ID="allSession" runat="server" DataSourceID="LinqDataSource1" DataTextField="SessionYear" DataValueField="SessionID"></asp:DropDownList>
            <asp:LinqDataSource ID="LinqDataSource1" runat="server" ContextTypeName="School365.Model.StudentModel" EntityTypeName="" OrderBy="SessionYear" Select="new (SessionID, SessionYear)" TableName="Sessions">
            </asp:LinqDataSource>
            <asp:Button ID="getCourse" runat="server" Text="Get Course Form" OnClick="getCourse_Click" />
            <p>
                <img alt="" class="auto-style6" src="../../Image/Image2.PNG" />
            </p>
            <p align="center" style="font-size: x-large">FEDERAL COLLEGE OF EDUCATION,OSIELE, ABEOKUTA</p>
            <p align="center" style="font-size: large">Course Registration Form</p>
            <p align="center" style="font-size: medium"><%Response.Write(allSession.SelectedItem.Text); %> First Semester</p>
        </div>
        <br />
        <table>
            <tr>
                <td colspan="4" style="text-align: center; border: double">STUDENT MATRIC NO :
                    <asp:Label ID="matricno" Text=" " runat="server"></asp:Label></td>
                <td>
                    <asp:Image ID="studentImage" runat="server" CssClass="mbr-figure__img" Width="100px" Height="100px" /></td>
            </tr>
            <tr>
                <td class="auto-style10">NAMES&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Label ID="names" runat="server" Text=""></asp:Label><td class="auto-style11">
                        <td>Date Printed : <%Response.Write(DateTime.Now.ToString("MMM dd, yyyy")); %> </td>
            </tr>
            <tr>
                <td class="auto-style10">COUNTRY&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Label ID="country" runat="server" Text="" /></td>
                <td class="auto-style7">GENDER&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Label ID="gender" runat="server" Text="" /></td>

            </tr>
            <tr>
                <td class="auto-style10">STATE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Label ID="state" runat="server" Text="" /></td>
                <td class="auto-style7">PHONE NO&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Label ID="phoneNo" runat="server" Text="" /></td>

            </tr>
            <tr>
                <td class="auto-style10">L/GOVT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Label ID="lga" runat="server" Text="" /></td>
                <td class="auto-style7">EMAIL&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Label ID="email" runat="server" Text="" /></td>
            </tr>
            <tr>
                <td class="auto-style10">PROGRAM&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Label ID="program" runat="server" Text="" /></td>
                <td class="auto-style7">LEVEL&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Label ID="level" runat="server" Text="" /></td>
            </tr>


        </table>


        <%
            if (StudentLevel < 300)
            {%>
        <table style="width: 100%; margin-top: 5px; border-top: 1px solid #aaa; border-bottom: 1px solid #aaa; font-size: x-small; font-weight: 700;" border="0" cellspacing="0">
            <tr>
                <th style="border-bottom: 1px solid #aaa; border-top: 1px solid #aaa; border-left: 1px solid #aaa; border-right: 1px solid #aaa;">S/N</th>
                <th style="border-bottom: 1px solid #aaa; border-top: 1px solid #aaa; border-left: 1px solid #aaa; border-right: 1px solid #aaa;">Course Code</th>
                <th style="border-bottom: 1px solid #aaa; border-top: 1px solid #aaa; border-left: 1px solid #aaa; border-right: 1px solid #aaa;">Course Title</th>
                <th style="border-bottom: 1px solid #aaa; border-top: 1px solid #aaa; border-left: 1px solid #aaa; border-right: 1px solid #aaa;">Cr</th>
                <th style="border-bottom: 1px solid #aaa; border-top: 1px solid #aaa; border-left: 1px solid #aaa; border-right: 1px solid #aaa;">St</th>
                <th style="border-bottom: 1px solid #aaa; border-top: 1px solid #aaa; border-left: 1px solid #aaa; border-right: 1px solid #aaa;">Course Lecturer</th>
            </tr>
            <% 
                int SessionID = int.Parse(allSession.SelectedItem.Value);
                var result = (from d in db.Results where d.StudentID == StudentID.FirstOrDefault() && d.SessionID == SessionID join f in db.Subjects on d.SubjectID equals f.SubjectID select new { f.SubjectID, f.SubjectCode, f.SubjectName, f.SubjectValue, f.Semester, f.SubjectUnit }).AsEnumerable().ToList();
                var resultSemester = (from d in result where d.Semester == "1" select new { d.SubjectID, d.SubjectCode, d.SubjectName, d.SubjectValue, d.Semester, d.SubjectUnit }).AsEnumerable().OrderBy(a => a.SubjectCode).ToList();

                foreach (var setResult in resultSemester)
                {
                    sn += 1;
                    var LecturerNames = (from d in db.LecturerProfiles where d.SubjectID == setResult.SubjectID join f in db.Lecturers on d.LecturerID equals f.LecturerID select new { f.Surname, f.Firstname, f.Middlename }).FirstOrDefault();
                    totalUnit += setResult.SubjectValue;
            %>
            <tr>
                <td style="border-bottom: 1px solid #aaa; border-top: 1px solid #aaa; border-left: 1px solid #aaa; border-right: 1px solid #aaa;"><%Response.Write(sn); %></td>
                <td style="border-bottom: 1px solid #aaa; border-top: 1px solid #aaa; border-left: 1px solid #aaa; border-right: 1px solid #aaa;"><%Response.Write(setResult.SubjectCode); %></td>
                <td style="border-bottom: 1px solid #aaa; border-top: 1px solid #aaa; border-left: 1px solid #aaa; border-right: 1px solid #aaa;"><%Response.Write(setResult.SubjectName.ToUpper()); %></td>
                <td style="border-bottom: 1px solid #aaa; border-top: 1px solid #aaa; border-left: 1px solid #aaa; border-right: 1px solid #aaa;"><%Response.Write(setResult.SubjectValue); %></td>
                <td style="border-bottom: 1px solid #aaa; border-top: 1px solid #aaa; border-left: 1px solid #aaa; border-right: 1px solid #aaa;"><%Response.Write(setResult.SubjectUnit); %></td>
                <td style="border-bottom: 1px solid #aaa; border-top: 1px solid #aaa; border-left: 1px solid #aaa; border-right: 1px solid #aaa;"><%Response.Write(LecturerNames == null ? "NA" : LecturerNames.Surname + " " + LecturerNames.Firstname + " " + LecturerNames.Middlename); %></td>

            </tr>
            <%}
            %>
        </table>

        <table>
            <tr>
                <th style="font-size: small" class="auto-style5">Total number of units registered :<%Response.Write(totalUnit);%><br />
                </th>
            </tr>
            <tr>
                <th class="auto-style5">&nbsp;</th>
                <th class="auto-style13">&nbsp;</th>
                <th class="auto-style14">&nbsp;</th>


            </tr>


            <tr>
                <th class="auto-style3">&nbsp;</th>

                <th class="auto-style5">&nbsp;</th>
                <th class="auto-style5">&nbsp;</th>

            </tr>
        </table>

        <p style="page-break-before: always"></p>


        <div>
            <p>
                <img alt="" class="auto-style6" src="../../Image/Image2.PNG" />
            </p>
            <p align="center" style="font-size: x-large">FEDERAL COLLEGE OF EDUCATION, ABEOKUTA</p>
            <p align="center" style="font-size: large">Course Registration Form</p>
            <%-- <p align="center" style="font-size: medium"><%Response.Write(SessionYear); %> Second Semester</p>--%>
        </div>
        <br />
        <table style="width: 100%; margin-top: 5px; border-top: 1px solid #aaa; border-bottom: 1px solid #aaa; font-size: x-small; font-weight: 700;" border="0" cellspacing="0">
            <tr>
                <th style="border-bottom: 1px solid #aaa; border-top: 1px solid #aaa; border-left: 1px solid #aaa; border-right: 1px solid #aaa;">S/N</th>
                <th style="border-bottom: 1px solid #aaa; border-top: 1px solid #aaa; border-left: 1px solid #aaa; border-right: 1px solid #aaa;">Course Code</th>
                <th style="border-bottom: 1px solid #aaa; border-top: 1px solid #aaa; border-left: 1px solid #aaa; border-right: 1px solid #aaa;">Course Title</th>
                <th style="border-bottom: 1px solid #aaa; border-top: 1px solid #aaa; border-left: 1px solid #aaa; border-right: 1px solid #aaa;">Cr</th>
                <th style="border-bottom: 1px solid #aaa; border-top: 1px solid #aaa; border-left: 1px solid #aaa; border-right: 1px solid #aaa;">St</th>
                <th style="border-bottom: 1px solid #aaa; border-top: 1px solid #aaa; border-left: 1px solid #aaa; border-right: 1px solid #aaa;">Course Lecturer</th>
            </tr>
            <% 
                var resultII = (from d in db.Results where d.StudentID == StudentID.FirstOrDefault() && d.SessionID == SessionID join f in db.Subjects on d.SubjectID equals f.SubjectID select new { f.SubjectID, f.SubjectCode, f.SubjectName, f.SubjectValue, f.Semester, f.SubjectUnit }).AsEnumerable().ToList();
                var resultSemesterII = (from d in resultII where d.Semester == "2" select new { d.SubjectID, d.SubjectCode, d.SubjectName, d.SubjectValue, d.Semester, d.SubjectUnit }).AsEnumerable().OrderBy(a => a.SubjectCode).ToList();

                foreach (var setResult in resultSemesterII)
                {
                    snI += 1;
                    var LecturerNames = (from d in db.LecturerProfiles where d.SubjectID == setResult.SubjectID join f in db.Lecturers on d.LecturerID equals f.LecturerID select new { f.Surname, f.Firstname, f.Middlename }).FirstOrDefault();
                    totalUnitII += setResult.SubjectValue;
            %>
            <tr>
                <td style="border-bottom: 1px solid #aaa; border-top: 1px solid #aaa; border-left: 1px solid #aaa; border-right: 1px solid #aaa;"><%Response.Write(snI); %></td>
                <td style="border-bottom: 1px solid #aaa; border-top: 1px solid #aaa; border-left: 1px solid #aaa; border-right: 1px solid #aaa;"><%Response.Write(setResult.SubjectCode); %></td>
                <td style="border-bottom: 1px solid #aaa; border-top: 1px solid #aaa; border-left: 1px solid #aaa; border-right: 1px solid #aaa;"><%Response.Write(setResult.SubjectName); %></td>
                <td style="border-bottom: 1px solid #aaa; border-top: 1px solid #aaa; border-left: 1px solid #aaa; border-right: 1px solid #aaa;"><%Response.Write(setResult.SubjectValue); %></td>
                <td style="border-bottom: 1px solid #aaa; border-top: 1px solid #aaa; border-left: 1px solid #aaa; border-right: 1px solid #aaa;"><%Response.Write(setResult.SubjectUnit); %></td>
                <td style="border-bottom: 1px solid #aaa; border-top: 1px solid #aaa; border-left: 1px solid #aaa; border-right: 1px solid #aaa;"><%Response.Write(LecturerNames == null ? "NA" : LecturerNames.Surname + " " + LecturerNames.Firstname + " " + LecturerNames.Middlename); %></td>

            </tr>
            <%}
            %>
        </table>


        <table>
            <tr>
                <th class="auto-style9">Total number of units registered :<%Response.Write(totalUnitII);%><br />
                </th>
            </tr>
            <tr>
                <th class="auto-style5">
                    <span class="auto-style12"><strong>...................................................</strong></span><br class="auto-style8" />
                    <br class="auto-style8" />
                    <span class="auto-style8">H.O.D (EDU) Sign/Date</span><br class="auto-style8" />
                </th>
                <th class="auto-style13"><span class="auto-style8">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    .......................................................</span><br class="auto-style8" />
                    <br class="auto-style8" />
                    <span class="auto-style8">&nbsp;H.O.D Sign/Date&nbsp;</span><br class="auto-style8" />
                </th>
                <th class="auto-style14"><span class="auto-style8">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ..................................................&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>
                    <br class="auto-style8" />
                    <br class="auto-style8" />
                    <span class="auto-style8">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;School Officer/Date</span><br class="auto-style8" />
                </th>


            </tr>


            <tr>
                <th class="auto-style3"><span class="auto-style8">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br />
                    <br />
                    &nbsp;..................................................</span><br class="auto-style8" />
                    <span class="auto-style8">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;H.O.D (GSE) Sign/Date</span><br class="auto-style8" />
                    <br />
                    <br />
                </th>

                <th class="auto-style5"><span class="auto-style8">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ..................................................</span><br class="auto-style8" />
                    <br class="auto-style8" />
                    <span class="auto-style8">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; H.O.D Sign/Date</span><br class="auto-style8" />
                </th>
                <th class="auto-style5"><span class="auto-style8">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;..................................................</span><br class="auto-style8" />
                    <br class="auto-style8" />
                    <span class="auto-style8">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Dean&#39;s Sign/Date&nbsp;&nbsp;&nbsp; </span>
                    <br class="auto-style8" />
                </th>

            </tr>
        </table>
        <hr style="height: -12px" />

        <%}
            else
            {%>
        <table style="width: 100%; margin-top: 5px; border-top: 1px solid #aaa; border-bottom: 1px solid #aaa; font-size: x-small; font-weight: 700;" border="0" cellspacing="0">
            <tr>
                <th style="border-bottom: 1px solid #aaa; border-top: 1px solid #aaa; border-left: 1px solid #aaa; border-right: 1px solid #aaa;">S/N</th>
                <th style="border-bottom: 1px solid #aaa; border-top: 1px solid #aaa; border-left: 1px solid #aaa; border-right: 1px solid #aaa;">Course Code</th>
                <th style="border-bottom: 1px solid #aaa; border-top: 1px solid #aaa; border-left: 1px solid #aaa; border-right: 1px solid #aaa;">Course Title</th>
                <th style="border-bottom: 1px solid #aaa; border-top: 1px solid #aaa; border-left: 1px solid #aaa; border-right: 1px solid #aaa;">Cr</th>
                <th style="border-bottom: 1px solid #aaa; border-top: 1px solid #aaa; border-left: 1px solid #aaa; border-right: 1px solid #aaa;">St</th>
                <th style="border-bottom: 1px solid #aaa; border-top: 1px solid #aaa; border-left: 1px solid #aaa; border-right: 1px solid #aaa;">Course Lecturer</th>
            </tr>
            <% 
                int SessionID = int.Parse(allSession.SelectedItem.Value);
                var result = (from d in db.Results where d.StudentID == StudentID.FirstOrDefault() && d.SessionID == SessionID join f in db.Subjects on d.SubjectID equals f.SubjectID select new { f.SubjectID, f.SubjectCode, f.SubjectName, f.SubjectValue, f.Semester, f.SubjectUnit }).AsEnumerable().ToList();
                var resultSemester = (from d in result select new { d.SubjectID, d.SubjectCode, d.SubjectName, d.SubjectValue, d.Semester, d.SubjectUnit }).AsEnumerable().OrderBy(a => a.SubjectCode).ToList();

                foreach (var setResult in resultSemester)
                {
                    sn += 1;
                    var LecturerNames = (from d in db.LecturerProfiles where d.SubjectID == setResult.SubjectID join f in db.Lecturers on d.LecturerID equals f.LecturerID select new { f.Surname, f.Firstname, f.Middlename }).FirstOrDefault();
                    totalUnit += setResult.SubjectValue;
            %>
            <tr>
                <td style="border-bottom: 1px solid #aaa; border-top: 1px solid #aaa; border-left: 1px solid #aaa; border-right: 1px solid #aaa;"><%Response.Write(sn); %></td>
                <td style="border-bottom: 1px solid #aaa; border-top: 1px solid #aaa; border-left: 1px solid #aaa; border-right: 1px solid #aaa;"><%Response.Write(setResult.SubjectCode); %></td>
                <td style="border-bottom: 1px solid #aaa; border-top: 1px solid #aaa; border-left: 1px solid #aaa; border-right: 1px solid #aaa;"><%Response.Write(setResult.SubjectName.ToUpper()); %></td>
                <td style="border-bottom: 1px solid #aaa; border-top: 1px solid #aaa; border-left: 1px solid #aaa; border-right: 1px solid #aaa;"><%Response.Write(setResult.SubjectValue); %></td>
                <td style="border-bottom: 1px solid #aaa; border-top: 1px solid #aaa; border-left: 1px solid #aaa; border-right: 1px solid #aaa;"><%Response.Write(setResult.SubjectUnit); %></td>
                <td style="border-bottom: 1px solid #aaa; border-top: 1px solid #aaa; border-left: 1px solid #aaa; border-right: 1px solid #aaa;"><%Response.Write(LecturerNames == null ? "NA" : LecturerNames.Surname + " " + LecturerNames.Firstname + " " + LecturerNames.Middlename); %></td>

            </tr>
            <%}
            %>
        </table>

        <table>
            <tr>
                <th style="font-size: small" class="auto-style5">Total number of units registered :<%Response.Write(totalUnit);%><br />
                </th>
            </tr>
            <tr>
                <th class="auto-style5">&nbsp;</th>
                <th class="auto-style13">&nbsp;</th>
                <th class="auto-style14">&nbsp;</th>


            </tr>


            <tr>
                <th class="auto-style3">&nbsp;</th>

                <th class="auto-style5">&nbsp;</th>
                <th class="auto-style5">&nbsp;</th>

            </tr>
        </table>

        <%}
        %>
    </form>
</body>
</html>
