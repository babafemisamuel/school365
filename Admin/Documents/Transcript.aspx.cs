﻿using School365.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace School365.Admin.Documents
{
    public partial class Transcript1 : System.Web.UI.Page
    {
        public StudentModel db = new StudentModel();
        public string StudentFullName = "";
        public string SubComb = "";
        public int Sess = 0;
        public Util util = new Util();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void GetDetails_Click(object sender, EventArgs e)
        {
            var StudentDetails = (from d in db.Students
                                  where d.MatricNo == MatricNo.Text
                                  select d).FirstOrDefault();

            StudentFullName = StudentDetails.Surname + " " + StudentDetails.Firstname + " " + StudentDetails.Middlename;
            SubComb = StudentDetails.Major + "/" + StudentDetails.Minor;
            Sess = int.Parse(UsrSession.SelectedItem.Value);
            MatricNo.Visible = false;
            GetDetails.Visible = false;
            UsrSession.Visible = false;

        }
    }
}