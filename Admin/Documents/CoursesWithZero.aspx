﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CoursesWithZero.aspx.cs" Inherits="School365.Admin.Documents.CoursesWithZero" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <h1>Courses that score haven't been inserted </h1>
            <asp:GridView ID="allCourses" runat="server">
            </asp:GridView>
        </div>
    </form>
</body>
</html>
