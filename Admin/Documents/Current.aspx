﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Current.aspx.cs" Inherits="School365.Admin.Documents.Current" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body style="font-family:'Segoe UI'">
    <form id="form1" runat="server">
    <div>
    <asp:DropDownList ID="level" runat="server">
    </asp:DropDownList>
        <asp:DropDownList ID="session" runat="server" DataSourceID="LinqDataSource1" DataTextField="SessionYear" DataValueField="SessionID">
        </asp:DropDownList>
        <asp:LinqDataSource ID="LinqDataSource1" runat="server" ContextTypeName="School365.Model.StudentModel" EntityTypeName="" OrderBy="SessionYear" Select="new (SessionYear, SessionID)" TableName="Sessions">
        </asp:LinqDataSource>
        <asp:DropDownList ID="check" runat="server">
            <asp:ListItem>Registered</asp:ListItem>
            <asp:ListItem>UnRegistered</asp:ListItem>
        </asp:DropDownList>
        <asp:Button ID="getStudents" runat="server" Text="Get Students" OnClick="getStudents_Click" />
    </div>
        <div>
            <asp:Label ID="registered" runat="server" style="font-size: xx-large" align="center"></asp:Label>
        </div>
        <table style="border: thin solid #000000; width: 100%;margin-left: 1%; margin-top: 30px;font-size:small; height: 146px;" cellspacing="0">
            <tr style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;">
                <td style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;" class="auto-style146">S/N</td>
                <td style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;" class="auto-style146">MATRIC NO</td>
                <td style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;" class="auto-style146">Surname</td>
                <td style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;" class="auto-style146">Firstname</td>
                <td style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;" class="auto-style146">Middlename</td>

                <td style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;" class="auto-style146">Sex</td>
                <td style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;" class="auto-style146">State Of Origin</td>
                <td style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;" class="auto-style146">1st Comb</td>
                <td style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;" class="auto-style146">2nd Comb</td>
                <td style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;" class="auto-style146">School</td>
            </tr>
            <tr>
                <%foreach (string getStdID in StdID)
                    {
                        SerialNo = SerialNo + 1;                        
                           var getStudentDetails=(from d in db.Students where d.MatricNo==getStdID
                                                  select new {d.MatricNo,d.Surname,d.Firstname,d.Middlename,d.Gender,d.SOR,d.Major,d.Minor,d.FacultyName}).FirstOrDefault();
                                              
                %>
                <td style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;"><%Response.Write(SerialNo); %></td>
                <td style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;"><%Response.Write(getStudentDetails.MatricNo); %></td>
                <td style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;"><%Response.Write(getStudentDetails.Surname); %></td>
                <td style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;"><%Response.Write(getStudentDetails.Firstname); %></td>
                <td style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;"><%Response.Write(getStudentDetails.Middlename); %></td>

                <td style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;"><%Response.Write(getStudentDetails.Gender); %></td>
                <td style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;"><%Response.Write(getStudentDetails.SOR); %></td>
                <td style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;"><%Response.Write(getStudentDetails.Major); %></td>
                <td style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;"><%Response.Write(getStudentDetails.Minor); %></td>
                <td style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;"><%Response.Write(getStudentDetails.FacultyName); %></td>
            </tr>
            <%}
            %>
        </table>
    </form>
</body>
</html>
