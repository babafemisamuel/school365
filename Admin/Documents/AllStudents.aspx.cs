﻿using School365.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace School365.Admin.Documents
{
    public partial class AllStudents : System.Web.UI.Page
    {
       public StudentModel db = new StudentModel();
        public List<int> StudentID = new List<int>();
        public int SerialNo = 0;
        int getAdminID = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            getAdminID = int.Parse(User.Identity.Name);
            var getAuth = (from d in db.RoleAdmins where d.AdminID == getAdminID select d.RoleID).ToList();
            if (getAuth.Contains(8) != true)
            {
                Response.Redirect("../Administrator/Profile.aspx");
            }  
            StudentID = (from d in db.Students select d.StudentID).Distinct().OrderBy(a=>a).ToList();
        }

        protected void getStds_Click(object sender, EventArgs e)
        {
            StudentID = (from d in db.Students where d.Major==major.SelectedItem.Value && d.Minor==minor.SelectedItem.Value && d.Level.ToString()==Level.Text select d.StudentID).Distinct().ToList();
        }
    }
}