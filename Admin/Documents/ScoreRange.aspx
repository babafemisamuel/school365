﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ScoreRange.aspx.cs" Inherits="School365.Admin.Documents.ScoreRange" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body style="font-family: 'Segoe UI'">
    <form id="form1" runat="server">
        <div>
            <asp:TextBox ID="Score1" runat="server" placeholder="First Score"></asp:TextBox>

            <asp:TextBox ID="Score2" runat="server" placeholder="Second Score"></asp:TextBox>
            <asp:DropDownList runat="server" ID="SubjectComb" DataSourceID="LinqDataSource1" DataTextField="SubjectCombinName" DataValueField="SubjectCombinID">
            </asp:DropDownList>
            <asp:LinqDataSource ID="LinqDataSource1" runat="server" ContextTypeName="School365.Model.StudentModel" EntityTypeName="" OrderBy="SubjectCombinName" Select="new (SubjectCombinID, SubjectCombinName)" TableName="SubjectCombinations">
            </asp:LinqDataSource>
            <asp:DropDownList ID="Level" runat="server">
                <asp:ListItem Value="100">
                100
                </asp:ListItem>
                <asp:ListItem Value="200">
                200
                </asp:ListItem>
                <asp:ListItem Value="300">
                300
                </asp:ListItem>
                <asp:ListItem Value="300+">
                300+
                </asp:ListItem>
            </asp:DropDownList>
            <asp:DropDownList ID="Semester" runat="server">
                <asp:ListItem Value="1">
                1 Semester
                </asp:ListItem>
                <asp:ListItem Value="2">
                2 Semester
                </asp:ListItem>
                
            </asp:DropDownList>
            <asp:DropDownList ID="UsrSession" runat="server" DataSourceID="LinqDataSource2" DataTextField="SessionYear" DataValueField="SessionID">
            </asp:DropDownList>
            <asp:LinqDataSource ID="LinqDataSource2" runat="server" ContextTypeName="School365.Model.StudentModel" EntityTypeName="" OrderBy="SessionYear" Select="new (SessionID, SessionYear)" TableName="Sessions">
            </asp:LinqDataSource>

           
            <asp:Button runat="server" ID="UsrSubmit" Text="Submit" OnClick="UsrSubmit_Click" />
        </div>
        <br />
        <div align="center">
            <asp:GridView runat="server" Width="100%" Height="100%" ID="allScore" Style="font-size: small; text-align: center;" OnRowDataBound="allScore_RowDataBound">
            </asp:GridView>
        </div>

    </form>
</body>
</html>
