﻿using School365.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace School365.Admin.Documents
{
    public partial class SubjectList : System.Web.UI.Page
    {
        public StudentModel db = new StudentModel();
        public Util util = new Util();
        public int SessionID = 0;
        public double Semester = 0;
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void GetGrades_Click(object sender, EventArgs e)
        {
            SessionID = int.Parse(Session.SelectedItem.Value);
            Semester = double.Parse(Semesters.SelectedItem.Text);
            Session.Visible = false;
            Semesters.Visible = false;
            GetGrades.Visible = false;
        }
    }
}