﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UserResult.aspx.cs" Inherits="School365.Admin.Documents.UserResult" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Studennt Result Slip</title>
    <style type="text/css">
        .auto-style1 {
            font-size: x-large;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:TextBox ID="MatricNo" runat="server" placeholder="MatricNo"></asp:TextBox>
        &nbsp;&nbsp;&nbsp;<asp:DropDownList ID="UsrSession" runat="server" DataSourceID="LinqDataSource1" DataTextField="SessionYear" DataValueField="SessionID"></asp:DropDownList>
        <asp:LinqDataSource ID="LinqDataSource1" runat="server" ContextTypeName="School365.Model.StudentModel" EntityTypeName="" OrderBy="SessionYear" Select="new (SessionID, SessionYear)" TableName="Sessions">
        </asp:LinqDataSource>
        <asp:Button ID="GetDetails" runat="server" Text="Get Transcript" OnClick="GetDetails_Click" />

        <table align="center">
            <tr>
                <td align="center" class="auto-style1"><strong>FEDERAL COLLEGE OF EDUCATION, ABEOKUTA</strong></td>
            </tr>
            <tr>
                <td align="center" class="auto-style1"><strong>RESULT SLIP</strong></td>
            </tr>
        </table>

        <div>
            <%
                string mat = MatricNo.Text;
                var getAllSessionID = (from d in db.Results where d.MatricNo == MatricNo.Text && d.SessionID <= Sess select d.SessionID).Distinct().OrderBy(a=>a).ToList();
                int Level =(int)db.Students.Where(a=>a.MatricNo==mat).Select(a=>a.Level).FirstOrDefault();
                int PrevLevel = Level - 100;
                int UsrLevel = 100;
                %>
            
            <%
                foreach (var item in getAllSessionID)
                {
                    string getSessionYear = (from d in db.Sessions where d.SessionID == item select d.SessionYear).FirstOrDefault();
                    var getResult = (from d in db.Results
                                     where d.MatricNo == MatricNo.Text && d.SessionID == item && d.Semester == 1
                                     join s in db.Subjects on d.SubjectID equals s.SubjectID
                                     select new { d.StudentID, s.SubjectCode, s.SubjectName, s.SubjectUnit, s.SubjectValue, d.CA, d.EXAM, d.Level }).OrderBy(a => a.SubjectCode).ToList();
                    var getResultII = (from d in db.Results
                                       where d.MatricNo == MatricNo.Text && d.SessionID == item && d.Semester == 2
                                       join s in db.Subjects on d.SubjectID equals s.SubjectID
                                       select new { d.StudentID, s.SubjectCode, s.SubjectName, s.SubjectUnit, s.SubjectValue, d.CA, d.EXAM, d.Level }).OrderBy(a => a.SubjectCode).ToList();
                   
                    %>
            <table align="center" style="width: 100%; font-family: 'Times New Roman'; font-size: 14px">
                <tr>
                    <td><strong>MATRIC NO :<%Response.Write(MatricNo.Text); %> </td>
                     
                </tr>
                <tr>
                    <td>STUDENT NAME :<%Response.Write(StudentFullName); %> </td>
                     <td></td>
                </tr>
                
                <tr>
                    <td>SUB/COMB : <%Response.Write(SubComb); %></td>
                    <td></td>
                </tr>
                <tr>
                  
                    <td> Level : <%Response.Write(UsrLevel); UsrLevel =UsrLevel+100;%> </td>
                    </strong>
                    <td></td>
                   
                </tr>
            </table>
          <hr />
            <table align="center" style="width: 100%; font-family: 'Times New Roman'; font-size: 12px">
                 <p align="left">SESSION : <%Response.Write(getSessionYear);%></p>
                <p align="left">SEMESTER: FIRST</p>
                <tr align="left">
                    <th>NO</th>                   
                    <th>TITLE</th>
                    <th>UNIT</th>
                    <th>STATUS</th>
                    <th>GRADE</th>
                    <th>REMARK</th>

                </tr>
                <%
                    foreach (var setResult in getResult)
                    {
                        double ta = setResult.CA + setResult.EXAM;
                        string GradeName = School365.Util.GradeName(ta);
                        string Remark = GradeName == "F" ? "FAILED" : "PASSED";
                %>
                <tr>
                    <td><%Response.Write(setResult.SubjectCode); %></td>
                    <td><%Response.Write(setResult.SubjectName.ToUpper()); %></td>
                    <td><%Response.Write(setResult.SubjectValue); %></td>
                    <td><%Response.Write(setResult.SubjectUnit); %></td>
                    <td><%Response.Write(GradeName); %></td>
                    <td><%Response.Write(Remark); %></td>

                </tr>
                <%}
                %>
            </table>
            <hr />
            <table align="center" style="width: 100%; font-family: 'Times New Roman'; font-size: 12px">
                <p align="left">SESSION : <%Response.Write(getSessionYear);%></p>
                <p align="left">SEMESTER: SECOND</p>
               
                 <tr align="left">
                    <th>NO</th>                   
                    <th>TITLE</th>
                    <th>UNIT</th>
                    <th>STATUS</th>
                    <th>GRADE</th>
                    <th>REMARK</th>

                </tr>
                <%
                    foreach (var setResult in getResultII)
                    {
                        double ta = setResult.CA + setResult.EXAM;
                        string GradeName = School365.Util.GradeName(ta);
                        string Remark = GradeName == "F" ? "FAILED" : "PASSED";
                %>
                <tr>
                    <td><%Response.Write(setResult.SubjectCode); %></td>
                    <td><%Response.Write(setResult.SubjectName.ToUpper()); %></td>
                    <td><%Response.Write(setResult.SubjectValue); %></td>
                    <td><%Response.Write(setResult.SubjectUnit); %></td>
                    <td><%Response.Write(GradeName); %></td>
                    <td><%Response.Write(Remark); %></td>

                </tr>
                <%}
                %>
            </table>
            <%  }

            %>
        </div>
        <br />
        <table style="width: 100%; margin-top: 5px; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; border-left: 1px solid #000; font-size: 12px;" border="0" cellspacing="0">
            <tr>
                <td style="border-top: 1px solid #000; border-left: 1px solid #000; border-right:1px solid #000;" class="auto-style5">SUBJECT COMBINATION </td>
                <td style="border-top: 1px solid #000; border-right: 1px solid #000; border-left:1px solid #000;"" class="auto-style5">CODE</td>
                <td colspan="4" align="center" style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5">PREVIOUS</td>
                <td colspan="4" align="center" style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5">CURRENT</td>   
                <td colspan="4" align="center" style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5">CUMMULATIVE</td>
                 <td align="center" style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"></td>                          
            </tr>
            <tr>
                <td style="border-bottom: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"></td>
                <td style="border-bottom: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"></td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5">TCP</td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5">TNU</td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5">TNUP</td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5">GPA</td>

                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5">TCP</td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5">TNU</td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5">TNUP</td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5">GPA</td>

                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5">CTCP</td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5">CTNU</td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5">CTNUP</td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5">CGPA</td>
                <td align="center" style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5">GRADE</td>
            </tr>
            <%

                var getAllDept = (from d in db.Results
                                  where d.MatricNo == MatricNo.Text
                                  join s in db.Departments on d.DepartmentID equals s.DepartmantID
                                  select new { d.StudentID, s.DepartmantID, s.DepartmentName, s.DepartmentCode }).Distinct().OrderBy(a => a.DepartmentCode).ToList();

                foreach (var item in getAllDept)
                {
                    double cgpa =double.Parse(School365.Util.cummCgpa(1, Level, item.StudentID, Sess, item.DepartmantID));
                    %>
            <tr>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(item.DepartmentName); %></td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(item.DepartmentCode); %></td>

                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(School365.NewUtil.DeptTcp(Sess, PrevLevel, 2, item.DepartmantID, item.StudentID)); %></td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(School365.NewUtil.DeptTnu(Sess, PrevLevel, 2, item.DepartmantID, item.StudentID)); %></td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(School365.NewUtil.DeptTnup(Sess, PrevLevel, 2, item.DepartmantID, item.StudentID)); %></td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(School365.NewUtil.DeptGpa(Sess, PrevLevel, 2, item.DepartmantID, item.StudentID)); %></td>

                <%--                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(School365.NewUtil.PrevDeptTcp(Sess, Level, 1, item.DepartmantID, item.StudentID)); %></td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(School365.NewUtil.PrevDepTnu(Sess, Level, 1, item.DepartmantID, item.StudentID)); %></td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(School365.NewUtil.PrevDepTnup(Sess, Level, 1, item.DepartmantID, item.StudentID)); %></td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(School365.NewUtil.PrevCgpa(Sess, Level, 1, item.DepartmantID, item.StudentID)); %></td> --%>

                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(School365.NewUtil.DeptTcp(Sess, Level, 2, item.DepartmantID, item.StudentID)); %></td>
                 <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(School365.NewUtil.DeptTnu(Sess, Level, 2, item.DepartmantID, item.StudentID)); %></td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(School365.NewUtil.DeptTnup(Sess, Level, 2, item.DepartmantID, item.StudentID)); %></td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(School365.NewUtil.DeptGpa(Sess, Level, 2, item.DepartmantID, item.StudentID)); %></td>
                
                
                
                 <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(School365.Util.cummTcp(2,Level, item.StudentID,Sess, item.DepartmantID)); %></td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(School365.Util.cummdeptTnu(2,Level, item.StudentID,Sess, item.DepartmantID)); %></td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(School365.Util.cummTnup(2,Level, item.StudentID,Sess, item.DepartmantID)); %></td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(School365.Util.cummCgpa(2,Level, item.StudentID,Sess, item.DepartmantID)); %></td> 
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(School365.Util.Grade(cgpa)); %></td>
            </tr>
            <%  } %>
        </table>
        <br />
        <table style="width: 20%; font-family: 'Times New Roman'; font-size: 12px">
            <tr>
                <td>OUTSTANDING SUBJECTS FAILED</td>
                <td>NO.</td>
                <td>CR.</td>
            </tr>
            <%
                var DepartmentID = (from d in db.Results
                                    where d.MatricNo == MatricNo.Text
                                    join s in db.Departments on d.DepartmentID equals s.DepartmantID
                                    select new { d.DepartmentID, s.DepartmentCode }).Distinct().ToList();
                int LastSessionID = (from d in db.Results where d.MatricNo == MatricNo.Text select d.SessionID).OrderByDescending(a => a).FirstOrDefault();
                int TotalOut = 0;
                  %>
            
            <%
                foreach (var item in DepartmentID)
                {
                    var getFailed = (from d in db.Results
                                     where d.MatricNo == MatricNo.Text && d.DepartmentID == item.DepartmentID && d.SessionID == LastSessionID
                     && (d.CA + d.EXAM) <= 39
                     && d.SessionID == LastSessionID
                                     join s in db.Subjects on d.SubjectID equals s.SubjectID
                                     select new { d.SubjectID, s.SubjectValue }).ToList();
                    int TotalFailed = getFailed.Select(a => a.SubjectID).Count();
                    double TotalFailedValue = getFailed.Sum(a => a.SubjectValue);
                    TotalOut += TotalFailed;

                    %>
            <tr>
                <td><%Response.Write(item.DepartmentCode); %></td>
                <td><%Response.Write(TotalFailed); %></td>
                <td><%Response.Write(TotalFailedValue); %></td>                
            </tr>

               <% }
                    %>
            <tr>
                
                <td>TOTAL SUBJECTS OUTSTANDING </td>
                <td><%Response.Write(TotalOut); %></td>
            </tr>

        </table>
      <%--  <table style="width: 100%; font-family: 'Times New Roman'; font-size: 12px">
            <tr>
                <td>CUMMULATIVE TOTAL CREDIT POINTS</td>
                <td align="right">CGPA</td>
                <td align="right">GRADE</td>
                <td align="right">LETTER</td>
            </tr>
            <tr>
                <td>CUMMULATIVE TOTAL NUMBER OF UNITS</td>
                <td>4.50-5.00</td>
                <td>DISTINCTION</td>
                <td>A</td>
            </tr>
            <tr>
                <td>CUMMULATIVE GRADE POINT AVERAGE</td>
                <td>3.50-4.49</td>
                <td>CREDIT</td>
                <td>B</td>

            </tr>
            <tr>
                <td>CUMMULATIVE TOTAL NUMBER OF UNIT PASSED</td>
                <td>2.40-3.49</td>
                <td>MERIT</td>
                <td>C</td>
            </tr>
            <tr>
                <td></td>
                <td>1.50-2.39</td>
                <td>PASS</td>
                <td>D</td>
            </tr>
            <tr>
                <td></td>
                <td>1.00-1.49</td>
                <td>PASS</td>
                <td>E</td>
            </tr>
            <tr>
                <td></td>
                <td>0.00-0.99</td>
                <td>FAIL</td>
                <td>F</td>
            </tr>

        </table>--%>
    </form>
    <br />
    <br />
    <br />   
    <p align="center">-----------------------------------------------------</p>
    <p align="center">REGISTRAR</p>


</body>
</html>