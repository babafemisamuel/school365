﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="StudentListSubComb.aspx.cs" Inherits="School365.Admin.Documents.StudentListSubComb" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            height: 9px;
        }
        .auto-style2 {
            height: 22px;
        }
    </style>
</head>
<body>

    <form id="form1" runat="server">

        <div>
            <asp:DropDownList ID="Session" runat="server" DataSourceID="LinqDataSource1" DataTextField="SessionYear" DataValueField="SessionID">
            </asp:DropDownList>
            &nbsp;&nbsp;&nbsp;
        <asp:DropDownList ID="Semesters" runat="server">
            <asp:ListItem>1</asp:ListItem>
            <asp:ListItem>2</asp:ListItem>
        </asp:DropDownList>
            &nbsp;&nbsp;
        <asp:Button ID="GetGrades" runat="server" Text="Get Data" OnClick="GetGrades_Click" />
            <asp:LinqDataSource ID="LinqDataSource1" runat="server" ContextTypeName="School365.Model.StudentModel" EntityTypeName="" OrderBy="SessionYear" Select="new (SessionID, SessionYear)" TableName="Sessions">
            </asp:LinqDataSource>

        </div>
        <table align="center" style="font-family:'Times New Roman'">
            <tr>
                <td colspan="2">
                    <h2 class="auto-style2">FEDERAL COLLEGE OF EDUCATION, ABEOKUTA </h2>
                </td>
                <td></td>
            </tr>
            <tr>
                <td class="auto-style1">
                    <h5>&nbsp;&nbsp;&nbsp; List of students registered</h5>
                </td>
                <td class="auto-style1">
                    <h5>FCE - <%Response.Write(Semesters.SelectedItem.Text + "st SEMESTER " + Session.SelectedItem.Text); %></h5>
                </td>
            </tr>
        </table>
        <div>

            <%
                var allSubComb = (from d in db.Results
                                  where d.SessionID == SessionID && d.Semester == Semester
                                  join s in db.SubjectCombinations on d.SubjectCombinID equals s.SubjectCombinID
                                  select new { d.SubjectCombinID, s.SubjectCombinName }).Distinct().ToList();
            %>
            <table style="width: 100%; margin-top: 5px; border-top: 1px solid #aaa; border-bottom: 1px solid #aaa; font-size: x-small;font-family:'Segoe UI' font-weight: 700;" border="0" cellspacing="0">
                <tr>
                    <td style="border-bottom: 1px solid #aaa; border-top: 1px solid #aaa; border-left: 1px solid #aaa; border-right: 1px solid #aaa;">MAJOR</td>
                    <td style="border-bottom: 1px solid #aaa; border-top: 1px solid #aaa; border-left: 1px solid #aaa; border-right: 1px solid #aaa;">NO OF STUDENTS</td>
                    <td style="border-bottom: 1px solid #aaa; border-top: 1px solid #aaa; border-left: 1px solid #aaa; border-right: 1px solid #aaa;">PERCENTAGE</td>
                </tr>
                <%
                    foreach (var item in allSubComb)
                    {
                %>
                <tr>
                    <td style="border-bottom: 1px solid #aaa; border-top: 1px solid #aaa; border-left: 1px solid #aaa; border-right: 1px solid #aaa;"><%Response.Write(item.SubjectCombinName); %></td>
                    <td style="border-bottom: 1px solid #aaa; border-top: 1px solid #aaa; border-left: 1px solid #aaa; border-right: 1px solid #aaa;"><%Response.Write(School365.NewUtil.AllStdBySubComb(SessionID, item.SubjectCombinID, Semester)); %></td>
                    <td style="border-bottom: 1px solid #aaa; border-top: 1px solid #aaa; border-left: 1px solid #aaa; border-right: 1px solid #aaa;"><%Response.Write(String.Format("{0:0.00}", School365.NewUtil.AllStdBySubComb(SessionID, item.SubjectCombinID, Semester) / util.AllStudentsCount(Semester, SessionID))); %>&#37;</td>
                </tr>
                <%}
                %>
                <tr>
                    <td style="border-bottom: 1px solid #aaa; border-top: 1px solid #aaa; border-left: 1px solid #aaa; border-right: 1px solid #aaa;"></td>
                    <td style="border-bottom: 1px solid #aaa; border-top: 1px solid #aaa; border-left: 1px solid #aaa; border-right: 1px solid #aaa;">TOTAL</td>
                    <td style="border-bottom: 1px solid #aaa; border-top: 1px solid #aaa; border-left: 1px solid #aaa; border-right: 1px solid #aaa;"><%Response.Write(util.AllStudentsCount(Semester, SessionID)); %></td>
                </tr>

            </table>
        </div>
    </form>
</body>
</html>
