﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GradeByCourse.aspx.cs" Inherits="School365.Admin.Documents.GradeByCourse" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body style="font-family:'Segoe UI'">
    <form id="form1" runat="server">
        <div>
            <asp:DropDownList ID="Session" runat="server" DataSourceID="LinqDataSource1" DataTextField="SessionYear" DataValueField="SessionID">
            </asp:DropDownList>
            &nbsp;&nbsp;&nbsp;
        <asp:DropDownList ID="Semesters" runat="server">
            <asp:ListItem>1</asp:ListItem>
            <asp:ListItem>2</asp:ListItem>
        </asp:DropDownList>
            &nbsp;&nbsp;
        <asp:Button ID="GetGrades" runat="server" Text="Get Grades" OnClick="GetGrades_Click" />
            <asp:LinqDataSource ID="LinqDataSource1" runat="server" ContextTypeName="School365.Model.StudentModel" EntityTypeName="" OrderBy="SessionYear" Select="new (SessionID, SessionYear)" TableName="Sessions">
            </asp:LinqDataSource>

        </div>
        <table align="center">
            <tr>
                <td colspan="2"><h2>FEDERAL COLLEGE OF EDUCATION, ABEOKUTA </h2></td>
                <td></td>
            </tr>
            <tr>                
                <td><h4>&nbsp;&nbsp;&nbsp; List Of Grades by Courses(Registered Students)</h4></td>
                <td><h4>FCE - <%Response.Write(Semesters.SelectedItem.Text + "st SEMESTER " + Session.SelectedItem.Text); %></h4></td>
            </tr>
        </table>
        <h3>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </h3>
        
        <% 

            var getAllCoursesRegistered = (from d in db.Results
                                           where d.SessionID == SessionID && d.Semester == Semester
                                           join s in db.Subjects on d.SubjectID equals s.SubjectID
                                           select new { d.SubjectID, s.SubjectCode, s.SubjectName }).Distinct().OrderBy(a=>a.SubjectCode).ToList();
        %>
        <%
            foreach (var item in getAllCoursesRegistered)
            {

        %>
        <h3><%Response.Write(item.SubjectCode.ToUpper() + " " + item.SubjectName.ToUpper()); %></h3>
        <table style="width: 100%; margin-top: 5px; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; border-left: 1px solid #000; font-size: 12px;" border="0" cellspacing="0">
            <tr>
                <th>GRADE</th>
                <th>NO OF STUDENTS</th>
                <th>Percentage</th>
            </tr>
            <tr>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5">A</td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(util.CountStdByGrade(item.SubjectID, Semester, SessionID, "A")); %></td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(string.Format("{0:0.00}", ((util.CountStdByGrade(item.SubjectID, Semester, SessionID, "A") / util.StudentByGrade(item.SubjectID, Semester, SessionID)) * 100)));%> &#37;</td>
            </tr>
            <tr>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5">B</td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(util.CountStdByGrade(item.SubjectID, Semester, SessionID, "B")); %></td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(string.Format("{0:0.00}", ((util.CountStdByGrade(item.SubjectID, Semester, SessionID, "B") / util.StudentByGrade(item.SubjectID, Semester, SessionID)) * 100))); %> &#37;</td>
            </tr>
            <tr>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5">C</td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(util.CountStdByGrade(item.SubjectID, Semester, SessionID, "C")); %></td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(string.Format("{0:0.00}", ((util.CountStdByGrade(item.SubjectID, Semester, SessionID, "C") / util.StudentByGrade(item.SubjectID, Semester, SessionID)) * 100))); %> &#37;</td>
            </tr>
            <tr>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5">D</td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(util.CountStdByGrade(item.SubjectID, Semester, SessionID, "D")); %></td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(string.Format("{0:0.00}", ((util.CountStdByGrade(item.SubjectID, Semester, SessionID, "D") / util.StudentByGrade(item.SubjectID, Semester, SessionID)) * 100))); %> &#37;</td>
            </tr>
            <tr>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5">E</td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(util.CountStdByGrade(item.SubjectID, Semester, SessionID, "E")); %></td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(string.Format("{0:0.00}", ((util.CountStdByGrade(item.SubjectID, Semester, SessionID, "E") / util.StudentByGrade(item.SubjectID, Semester, SessionID)) * 100))); %> &#37;</td>
            </tr>
            <tr>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5">F</td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(util.CountStdByGrade(item.SubjectID, Semester, SessionID, "F")); %></td>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(string.Format("{0:0.00}", ((util.CountStdByGrade(item.SubjectID, Semester, SessionID, "F") / util.StudentByGrade(item.SubjectID, Semester, SessionID)) * 100))); %> &#37;</td>
            </tr>
            <tr>
                <td style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"></td>
                <td style="border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5"><%Response.Write(util.StudentByGrade(item.SubjectID, Semester, SessionID)); %></td>
                <td style="border-left: 1px solid #000; border-right: 1px solid #000;" class="auto-style5">1.00 &#37;</td>
            </tr>
        </table>
        <hr />
        <%}
        %>
    </form>
</body>
</html>
