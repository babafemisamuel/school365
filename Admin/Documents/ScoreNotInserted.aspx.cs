﻿using School365.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace School365.Admin.Documents
{
    public partial class ScoreNotInserted : System.Web.UI.Page
    {
        StudentModel db = new StudentModel();
        public List<int> getSubjectsID = new List<int>();
        public List<Model.Lecturer> Lecturers = new List<Model.Lecturer>();
        public List<Model.Subject> Subjects = new List<Subject>();
        public List<LecturerProfile> LecturersProfiles = new List<LecturerProfile>();
        protected void Page_Load(object sender, EventArgs e)
        {
            int getCurrent = db.Sessions.Where(a => a.CurrentSession == true).Select(a => a.SessionID).FirstOrDefault();
            getSubjectsID = db.Results.Where(a => a.SessionID == getCurrent && a.LecturerInserted != "TRUE").Select(a => a.SubjectID).Distinct().ToList();
            LecturersProfiles = db.LecturerProfiles.Select(a => a).ToList();
            Lecturers = db.Lecturers.Select(a => a).ToList();
            Subjects = db.Subjects.Select(a => a).OrderBy(a => a.SubjectCode).ToList();

        }
    }
}