﻿using School365.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace School365.Admin.Documents
{
    public partial class Transcript : System.Web.UI.Page
    {
        public StudentModel db = new StudentModel();
        public int SessionID = 0;
        public int AllLevel = 0;
        public int Semester = 0;
        public string StudentFullName = "";
        public string SubComb = "";
        public Util util = new Util();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var Level = (from d in db.Results select d.Level).Distinct().ToList();
                Levels.DataSource = Level;
                Levels.DataBind();
            }


        }

        protected void GetGrades_Click(object sender, EventArgs e)
        {
            SessionID = int.Parse(Session.SelectedItem.Value);
            Semester = int.Parse(Semesters.SelectedItem.Text);
            AllLevel = int.Parse(Levels.Text);
            Session.Visible = false;
            Semesters.Visible = false;
            GetGrades.Visible = false;
            Levels.Visible = false;
            GetGrades.Visible = false;
            MatricNo.Visible = false;
            var StudentDetails = (from d in db.Students
                                  where d.MatricNo == MatricNo.Text
                                  select d).FirstOrDefault();

            StudentFullName = StudentDetails.Surname + " " + StudentDetails.Firstname + " " + StudentDetails.Middlename;
            SubComb = StudentDetails.Major + "/" + StudentDetails.Minor;
            MatricNo.Visible = false;
            //GetDetails.Visible = false;
        }

    }
}