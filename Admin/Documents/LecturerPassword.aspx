﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LecturerPassword.aspx.cs" Inherits="School365.Admin.Documents.LecturerPassword" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Lecturer Password</title>
</head>
<body style="font-family: 'Segoe UI'">
    <form id="form1" runat="server">
        <div>
            <asp:DropDownList ID="lecturer" runat="server">
                <asp:ListItem>Password Changed</asp:ListItem>
                <asp:ListItem>Password Unchange</asp:ListItem>
            </asp:DropDownList>
            <asp:Button ID="getLecturer" runat="server" Text="Get Lecturer" OnClick="getLecturer_Click" />
            <asp:Label ID="LecturerText" runat="server" style="font-size: xx-large"></asp:Label>
        </div>
        <table style="width: 100%; margin-top: 5px; border-top: 1px solid #000000; border-bottom: 1px solid #000000; font-size: x-small; font-weight: 700;" border="0" cellspacing="0">
            <tr>
                <td style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;" class="auto-style1" colspan="">S/N</td>
                <td style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;" class="auto-style1" colspan="">Surname</td>
                <td style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;" class="auto-style1" colspan="">Firstname</td>
                <td style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;" class="auto-style1" colspan="">Middlename</td>
            </tr>
            <tr>
                <%
                    foreach (int setLecturerID in LecturerID)
                    {
                        var getLecturerName = (from d in db.Lecturers where d.LecturerID == setLecturerID select new { d.Firstname, d.Middlename, d.Surname }).FirstOrDefault();
                        SerialNo = SerialNo + 1;
                %>
                <td style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;" class="auto-style1" colspan=""><%Response.Write(SerialNo); %></td>
                <td style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;" class="auto-style1" colspan=""><%Response.Write(getLecturerName.Surname); %></td>
                <td style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;" class="auto-style1" colspan=""><%Response.Write(getLecturerName.Middlename); %></td>
                <td style="border-bottom: 1px solid #000000; border-top: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000;" class="auto-style1" colspan=""><%Response.Write(getLecturerName.Firstname); %></td>
            </tr>
            <%}
            %>
        </table>
    </form>
</body>
</html>
