﻿using School365.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace School365.Admin.Documents
{
    public partial class CoursesWithZero : System.Web.UI.Page
    {
        StudentModel db = new StudentModel();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var getCourses = (from d in db.Results
                                  where d.CA == 0 && d.EXAM == 0
                                  join f in db.Subjects on d.SubjectID equals f.SubjectID
                                  select new { f.SubjectCode, f.SubjectName, f.SubjectUnit }).Distinct().ToList();
                allCourses.DataSource = getCourses;
                allCourses.DataBind();
            }
        }
    }
}