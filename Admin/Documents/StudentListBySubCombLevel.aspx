﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="StudentListBySubCombLevel.aspx.cs" Inherits="School365.Admin.Documents.StudentListBySubCombLevel" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            text-align: center;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:DropDownList ID="Session" runat="server" DataSourceID="LinqDataSource1" DataTextField="SessionYear" DataValueField="SessionID">
            </asp:DropDownList>
            &nbsp;&nbsp;&nbsp;
        <asp:DropDownList ID="Semesters" runat="server">
            <asp:ListItem>1</asp:ListItem>
            <asp:ListItem>2</asp:ListItem>
        </asp:DropDownList>
            &nbsp;&nbsp;
        <asp:Button ID="GetGrades" runat="server" Text="Get Data" OnClick="GetGrades_Click" Style="height: 26px" />
            <asp:LinqDataSource ID="LinqDataSource1" runat="server" ContextTypeName="School365.Model.StudentModel" EntityTypeName="" OrderBy="SessionYear" Select="new (SessionID, SessionYear)" TableName="Sessions">
            </asp:LinqDataSource>

            <table align="center" style="font-family: 'Times New Roman'">
                <tr>
                    <td colspan="2">
                        <h2 class="auto-style2">FEDERAL COLLEGE OF EDUCATION, ABEOKUTA </h2>
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td class="auto-style1">
                        <h5>&nbsp;&nbsp;&nbsp; List Of All Students By Major/Levels</h5>
                    </td>
                    <td class="auto-style1">
                        <h5>FCE - <%Response.Write(Semesters.SelectedItem.Text + " SEMESTER " + Session.SelectedItem.Text); %></h5>
                    </td>

                </tr>
            </table>

            <%
                double AllStudents = 0;
                var allSubComb = (from d in db.Results
                                  where d.SessionID == SessionID && d.Semester == Semester
                                  join s in db.SubjectCombinations on d.SubjectCombinID equals s.SubjectCombinID
                                  select new { d.SubjectCombinID, s.SubjectCombinName }).Distinct().OrderBy(a => a.SubjectCombinName).ToList();
            %>
        </div>
        <%
            foreach (var item in allSubComb)
            {
                AllStudents = util.AllStudentsCount(Semester, SessionID);
                var getAllLevel = (from d in db.Results
                                   where d.SessionID == SessionID && d.Semester == Semester select d.Level).Distinct().ToList();
                %>
        <table style="font-family:'Times New Roman'; width:100%; font-size:15px" align="center">
            <tr>
                <td>LEVEL</td>
                <td>NO. OF STUDENT</td>
                <td>PERCENTAGE</td>
            </tr>
            <tr>
                <td style="border-bottom:1px solid #000;"></td>
                <td style="border-bottom:1px solid #000;"></td>
                <td style="border-bottom:1px solid #000;"></td>
            </tr>
            <u><%Response.Write(item.SubjectCombinName); %></u>
            <%
                double total = 0;
                foreach (var setAllLevel in getAllLevel)
                {
                    total += School365.NewUtil.AllStudentDeptLevel(SessionID, item.SubjectCombinID, Semester, setAllLevel);
                    %>
            <tr>               
                <td class="auto-style1"><%Response.Write(setAllLevel); %></td>
                <td class="auto-style1"><%Response.Write(School365.NewUtil.AllStudentDeptLevel(SessionID,item.SubjectCombinID,Semester,setAllLevel)); %></td>
                <td class="auto-style1"><%Response.Write(string.Format("{0:0.00}",School365.NewUtil.AllStudentDeptLevel(SessionID,item.SubjectCombinID,Semester,setAllLevel) *100/AllStudents)); %>&#37;</td>
            </tr>
                <%}

                 %>
           <tr>
               <td class="auto-style1"></td>
               <td class="auto-style1"></td>
               <td class="auto-style1">SubTotal :-   <%Response.Write(total); %></td>
           </tr>
        </table>
        <%}
        %>
        <p align="center">Full Total:- <%Response.Write(AllStudents); %></p>
    </form>
</body>
</html>
