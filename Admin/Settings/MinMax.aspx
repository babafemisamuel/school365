﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin.Master_Layout" AutoEventWireup="true" CodeBehind="MinMax.aspx.cs" Inherits="School365.Admin.Settings.MinMax" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="header" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="body" runat="server">
    <br />
    <hr />
    <div class="row">

        <div class="large-2 columns">
            <label>MINIMUM</label>
            <asp:TextBox ID="Mini" runat="server" placeholder="Minimum" TextMode="Number"></asp:TextBox>
        </div>
        <div class="large-2 columns">
            <label>MAXIMUM</label>
            <asp:TextBox ID="Maxi" runat="server" placeholder="Maximum" TextMode="Number"></asp:TextBox>
        </div>
        <div class="large-2 columns">
            <label>SEMESTER</label>
            <asp:DropDownList runat="server" ID="Semester">
                <asp:ListItem>1</asp:ListItem>
                <asp:ListItem>2</asp:ListItem>
            </asp:DropDownList>
        </div>
        <div class="large-2 columns">
            <label>LEVEL</label>
            <asp:DropDownList runat="server" ID="LEVEL">
                <asp:ListItem>100</asp:ListItem>
                <asp:ListItem>200</asp:ListItem>
                <asp:ListItem>300</asp:ListItem>
            </asp:DropDownList>
        </div>
        <div class="large-3 columns">
            <label>Subject Combinations</label>
            <asp:DropDownList ID="AllSubjectComb" runat="server" DataSourceID="LinqDataSource1" DataTextField="SubjectCombinName" DataValueField="SubjectCombinID"></asp:DropDownList>
            <asp:LinqDataSource ID="LinqDataSource1" runat="server" ContextTypeName="School365.Model.StudentModel" EntityTypeName="" OrderBy="SubjectCombinName" Select="new (SubjectCombinName, SubjectCombinID)" TableName="SubjectCombinations">
            </asp:LinqDataSource>
        </div>

    </div>
    <p align="center">
        <asp:Button ID="UsrSubmit" runat="server" Text="Add" CssClass="button small" OnClick="UsrSubmit_Click" />
         <asp:Button ID="updateBtn" runat="server" Text="Update" CssClass="button dark small" OnClick="updateBtn_Click" />
    </p>
    

    <div class="row">
        <div class="large-2"></div>
        <div class="large-10">
            <asp:GridView ID="allMaxMin" runat="server" AutoGenerateColumns="false" Width="826px">
                <Columns>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:TextBox ID="MinMaxID" runat="server" Visible="false" Text='<%#Bind("MinMaxID") %>'> ></asp:TextBox>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Subject Combination">
                        <ItemTemplate>
                            <asp:TextBox ID="SubjectCombinName" runat="server" Text='<%#Bind("SubjectCombinName") %>'> ></asp:TextBox>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:TextBox ID="SubjectCombinID" runat="server" Visible="false" Text='<%#Bind("SubjectCombinID") %>'> ></asp:TextBox>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Minimum">
                        <ItemTemplate>
                            <asp:TextBox ID="Minimum" runat="server" Text='<%#Bind("Minimum") %>'> ></asp:TextBox>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Maximum">
                        <ItemTemplate>
                            <asp:TextBox ID="Maximum" runat="server" Text='<%#Bind("Maximum") %>'>> </asp:TextBox>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Semester">
                        <ItemTemplate>
                            <asp:TextBox ID="Semester" runat="server" Text='<%#Bind("Semester") %>'>> </asp:TextBox>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Level">
                        <ItemTemplate>
                            <asp:TextBox ID="Level" runat="server" Text='<%#Bind("Level") %>'>> </asp:TextBox>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
    </div>


</asp:Content>
