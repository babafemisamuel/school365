﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin_Layout.Master" AutoEventWireup="true" CodeBehind="Session.aspx.cs" Inherits="School365.Admin.Settings.Session" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="header" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="body" runat="server">
    <br />
    <div class="row">
        <div class="columns large-4">
            <asp:TextBox ID="sessionYear" runat="server" placeholder="Session Year"></asp:TextBox>
        </div>
        <div class="columns large-4" style="float: left">
            <asp:Button ID="InsertBtn" runat="server" Text="Insert Session" CssClass="tiny radius button bg-light-blue" OnClick="InsertBtn_Click" />
        </div>
    </div>
    <hr />
    <div class="row">
       

        <div class="columns large-12">
             <asp:Button ID="UpdateSession"  runat="server" Text="Assign Session" CssClass="radius button bg-red " OnClick="UpdateSession_Click" align="center"  />
            <br />
            <asp:GridView runat="server" style="width:100%" ID="allSession" AutoGenerateColumns="false">
                <Columns>
                    <asp:TemplateField HeaderText="Select Current">
                        <ItemTemplate>
                            <asp:RadioButton ID="selectBtn" runat="server" onclick="javascript:CheckOtherIsCheckedByGVID(this);" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="SessionID" />
                    <asp:BoundField DataField="SessionYear" HeaderText="Session Year" />
                    <asp:BoundField DataField="CurrentSession" HeaderText="Current Session" />
                </Columns>
            </asp:GridView>
        </div>
    </div>


    <script type="text/javascript">
        function CheckOtherIsCheckedByGVID(rb) {
            var isChecked = rb.checked;
            var row = rb.parentNode.parentNode;

            var currentRdbID = rb.id;
            parent = document.getElementById("<%= allSession.ClientID %>");
            var items = parent.getElementsByTagName('input');

            for (i = 0; i < items.length; i++) {
                if (items[i].id != currentRdbID && items[i].type == "radio") {
                    if (items[i].checked) {
                        items[i].checked = false;
                    }
                }
            }
        }
    </script>
</asp:Content>
