﻿using School365.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace School365.Admin.Settings
{
    public partial class MinMax : System.Web.UI.Page
    {
        StudentModel db = new StudentModel();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                AllStudents();
            }
        }

        protected void UsrSubmit_Click(object sender, EventArgs e)
        {
            int defaultNum = 10;
            int defaultLevel = 100;
            int defaultSemester = 1;
            int SubjectComb = int.Parse(AllSubjectComb.SelectedItem.Value);
            int min = int.TryParse(Mini.Text, out defaultNum) == true ? defaultNum : 10;
            int max = int.TryParse(Maxi.Text, out defaultNum) == true ? defaultNum : 40;
            int Level = int.TryParse(LEVEL.SelectedItem.Text, out defaultLevel) == true ? defaultLevel : 100;
            int Semester = int.TryParse(LEVEL.SelectedItem.Text, out defaultSemester) == true ? defaultSemester : 1;
            var InsertMinMax = new Model.MinMax
            {
                Maximum = max,
                Minimum = min,
                Level = Level,
                Semester = Semester,
                SubjectCombinID = SubjectComb
            };
            db.MinMaxs.Add(InsertMinMax);
            db.SaveChanges();
            Mini.Text = "";
            Maxi.Text = "";
            AllStudents();
            Response.Write("Data Inserted");

        }
        private void AllStudents()
        {
            var getAllData = (from d in db.MinMaxs join a in db.SubjectCombinations on d.SubjectCombinID equals a.SubjectCombinID select new { a.SubjectCombinID, a.SubjectCombinName, d.Semester, d.MinMaxID, d.Minimum, d.Maximum, d.Level }).ToList().OrderBy(a => a.SubjectCombinName);
            allMaxMin.DataSource = getAllData;
            allMaxMin.DataBind();
        }

        protected void updateBtn_Click(object sender, EventArgs e)
        {
            foreach (GridViewRow row in allMaxMin.Rows)
            {

                TextBox MinMaxID = (TextBox)row.FindControl("MinMaxID");
                TextBox SubjectCombinName = (TextBox)row.FindControl("SubjectCombinName");
                TextBox Minimum = (TextBox)row.FindControl("Minimum");
                TextBox Maximum = (TextBox)row.FindControl("Maximum");
                TextBox Semester = (TextBox)row.FindControl("Semester");
                TextBox Level = (TextBox)row.FindControl("Level");

                int minMaxID = int.Parse(MinMaxID.Text);
                string subComb = SubjectCombinName.Text.ToUpper();
                int max = int.Parse(Maximum.Text);
                int min = int.Parse(Minimum.Text);
                int semester = int.Parse(Semester.Text);
                int level = int.Parse(Level.Text);

                int getSubComb = db.SubjectCombinations.Where(a => a.SubjectCombinName == subComb).Select(a => a.SubjectCombinID).FirstOrDefault();
                if (getSubComb == 0)                  
                Response.Redirect("MinMax.aspx");

                var UpdateMinMax = db.MinMaxs.Where(a => a.MinMaxID == minMaxID).Select(a => a).FirstOrDefault();
                UpdateMinMax.Minimum = min;
                UpdateMinMax.Maximum = max;
                UpdateMinMax.Semester = semester;
                UpdateMinMax.Level = level;
                UpdateMinMax.SubjectCombinID = getSubComb;
                db.SaveChanges();
            }
        }
    }
}