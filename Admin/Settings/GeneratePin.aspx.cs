﻿using School365.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace School365.Admin.Settings
{
    public partial class GeneratePin : System.Web.UI.Page
    {
        StudentModel db = new StudentModel();
        public List<Pin> GetAllPin= new List<Model.Pin>();
        int getAdminID = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (User.Identity.IsAuthenticated == false)
                Response.Redirect("../Administrator/UserLogin.aspx");
            getAdminID = int.Parse(User.Identity.Name);
            var getAuth = (from d in db.RoleAdmins where d.AdminID == getAdminID select d.RoleID).ToList();
            if (getAuth.Contains(10) != true)
            {
                Response.Redirect("../Administrator/Profile.aspx");
            }
            GetPin();
        }
        public void GetPin()
        {
            GetAllPin = db.Pins.ToList();
        }

        protected void Pin_Click(object sender, EventArgs e)
        {
            int defaultNo = 50; int usrPin = 0;int defaultpin=6;
            int NoOdigits = int.TryParse(NoOfdigits.Text, out defaultpin) == true ? defaultpin : 6;
            int _AmountOfLogin= int.TryParse(AmountOfLogin.Text, out defaultpin) == true ? defaultpin : 6;
            usrPin = int.TryParse(TotalNo.Text, out defaultNo) == true ? defaultNo : 50;
            for (int i = 0; i < usrPin; i++)
            {
                var insertPin = new Pin
                {
                    PinKey = Guid.NewGuid().ToString().Replace("-", "").Substring(0, NoOdigits),
                    Counter = 0,
                    AmountOfLogin = _AmountOfLogin
                };
                db.Pins.Add(insertPin);
                db.SaveChanges();
            }
            Response.Write("Password Generated");
            GetPin();

        }

        protected void PasswordResert_Click(object sender, EventArgs e)
        {
            List<Pin> userPins = db.Pins.Select(a => a).ToList();
            db.Pins.RemoveRange(userPins);
            db.SaveChanges();
            Response.Write("Password Reserted");
            GetPin();
        }
    }
}