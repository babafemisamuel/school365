﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Logger.aspx.cs" Inherits="School365.Admin.Settings.Logger" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Logger</title>

    <link href="../../assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <link href="../../css/dataTables.bootstrap.min.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table id="#table">
                <thead>
                    <tr>
                        <th>UserName</th>
                        <th>Activity</th>
                        <th>Info</th>
                        <th>Date</th>
                    </tr>
                </thead>
                <tbody>
                    <%
                        foreach (var item in audit)
                        {%>
                    <tr>
                        <td><%Response.Write(item.UserName); %></td>
                        <td><%Response.Write(item.Activity); %></td>
                        <td><%Response.Write(item.Info); %></td>
                        <td><%Response.Write(item.DateTime); %></td>
                    </tr>
                    <%}

                    %>
                    <tr>
                    </tr>
                </tbody>
            </table>
        </div>
    </form>
</body>
</html>
<script src="../../assets/web/assets/jquery/jquery.min.js"></script>
<script src="../../js/foundation/jquery.dataTables.min.js"></script>

<script src="../../js/foundation/dataTables.bootstrap.min.js"></script>

<script>
    $(document).ready(function () {
        $('#table').DataTable(
            {
                "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]]
            });
    });
</script>
