﻿using School365.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace School365.Admin.Settings
{
    public partial class SchoolLogger : System.Web.UI.Page
    {
        StudentModel db = new StudentModel();
        protected void Page_Load(object sender, EventArgs e)
        {
            allLog.DataSource = db.AuditTrail.Select(a=> new { a.UserName,a.Info,a.Activity,a.DateTime}).OrderBy(a => a.DateTime).ToList();
            allLog.DataBind();
        }
    }
}