﻿using School365.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace School365.Admin.Settings
{
    public partial class Logger : System.Web.UI.Page
    {
        StudentModel db = new StudentModel();
        public List<AuditTrail> audit = new List<AuditTrail>();
        protected void Page_Load(object sender, EventArgs e)
        {
            audit = db.AuditTrail.ToList();
        }
    }
}