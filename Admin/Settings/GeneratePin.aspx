﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin_Layout.Master" AutoEventWireup="true" CodeBehind="GeneratePin.aspx.cs" Inherits="School365.Admin.Settings.GeneratePin" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="header" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="body" runat="server">
    <br />
    <br />
    <br />
    <hr />
    <div class="row">
        <div class="large-3 columns">
            <asp:TextBox ID="TotalNo" runat="server" placeholder="Total No Of Pin" />

        </div>
        <div class="large-3 columns">
            <asp:TextBox ID="NoOfdigits" runat="server" placeholder="No Of Digits" />
        </div>
        <div class="large-3 columns">
            <asp:TextBox ID="AmountOfLogin" runat="server" placeholder="Amount of Login " />
        </div>
        <div class="large-3 columns">
            <asp:Button ID="Pin" runat="server" Text="Generate Pin" CssClass="button" OnClick="Pin_Click" />
        </div>
        <div class="large-4 columns">
            <asp:Button ID="PasswordResert" runat="server" Text="Reset Password" CssClass="button info" OnClick="PasswordResert_Click" />
        </div>
    </div>
    <div class="row">
        <table id="myTable" style="width: 100%">
            <thead>
                <tr>
                    <td>S/N</td>
                    <td>Pin Key</td>
                    <td>Status</td>
                </tr>

            </thead>
            <tbody>

                <%
                    int sn = 1;
                    foreach (var item in GetAllPin)
                    {
                        
                %>
                <tr>
                    <td><%Response.Write(sn); %></td>
                    <td><%Response.Write(item.PinKey); %></td>

                    <td><%Response.Write(item.Counter);%></td>
                </tr>
                <%
                        sn=sn+1;
                    }

                %>
            </tbody>
        </table>
    </div>
</asp:Content>
