﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin_Layout.Master" AutoEventWireup="true" CodeBehind="ScorePreview.aspx.cs" Inherits="School365.Admin.Settings.ScorePreview" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="header" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="body" runat="server">
    <div class="row">
        <div class="column large-4">
            <asp:TextBox ID="matric" runat="server" placeholder="Matric No"></asp:TextBox>
        </div>
        <div class="column large-4">
            <asp:DropDownList ID="Semesters" runat="server">
                <asp:ListItem Value="1">1 SEMESTER</asp:ListItem>
                <asp:ListItem Value="2">2 SEMESTER</asp:ListItem>
            </asp:DropDownList>
        </div>
        <div class="column large-4">
            <asp:DropDownList ID="Level" runat="server">
                <asp:ListItem Value="100">100 LEVEL</asp:ListItem>
                <asp:ListItem Value="200">200 LEVEL</asp:ListItem>
                <asp:ListItem Value="300">300 LEVEL</asp:ListItem>
            </asp:DropDownList>
        </div>
       
        <div class="column large-4">
            <asp:DropDownList ID="allSession" runat="server" DataSourceID="LinqDataSource1" DataTextField="SessionYear" DataValueField="SessionID">

            </asp:DropDownList>
            <asp:LinqDataSource ID="LinqDataSource1" runat="server" ContextTypeName="School365.Model.StudentModel" EntityTypeName="" OrderBy="SessionYear" Select="new (SessionID, SessionYear)" TableName="Sessions">
            </asp:LinqDataSource>
        </div>
        <div class="column large-4">
            <asp:Button ID="getSubj" runat="server" Text="Get Subjects" CssClass="button" OnClick="getSubj_Click" />
        </div>
        <div class="column large-4">
            <asp:Button ID="UpStd" runat="server" Text="Update Students" CssClass="button" OnClick="UpStd_Click" />
        </div>
    </div>

    <div class="row">

        <div class="column large-10">
            <asp:GridView ID="allStudents" runat="server" AutoGenerateColumns="false" Font-Size="Small" Width="826px">
                <Columns>
                    <asp:TemplateField HeaderText="Surname">
                        <ItemTemplate>
                            <asp:TextBox ID="Surname" runat="server" Visible="true" ReadOnly="true" Text='<%#Bind("Surname") %>'> ></asp:TextBox>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Firstname">
                        <ItemTemplate>
                            <asp:TextBox ID="Firstname" runat="server" ReadOnly="true" Text='<%#Bind("Firstname") %>'> ></asp:TextBox>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Othername">
                        <ItemTemplate>
                            <asp:TextBox ID="Othername" runat="server" ReadOnly="true" Text='<%#Bind("Middlename") %>'>> </asp:TextBox>
                        </ItemTemplate>
                    </asp:TemplateField>


                    <asp:TemplateField HeaderText="MatricNo" ControlStyle-Width="150">
                        <ItemTemplate>
                            <asp:TextBox ID="MatricNo" runat="server" ReadOnly="true" Text='<%#Bind("MatricNo") %>'>> </asp:TextBox>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="SubjectCode" ControlStyle-Width="100">
                        <ItemTemplate>
                            <asp:TextBox ID="SubjectCode" runat="server" ReadOnly="true" Text='<%#Bind("SubjectCode") %>'>> </asp:TextBox>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="CA">
                        <ItemTemplate>
                            <asp:TextBox ID="CA" runat="server" Text='<%#Bind("CA") %>'> </asp:TextBox>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="EXAM">
                        <ItemTemplate>
                            <asp:TextBox ID="EXAM" runat="server" Text='<%#Bind("EXAM") %>'> </asp:TextBox>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:TextBox ID="SubjectID" runat="server" Visible="false" Text='<%#Bind("SubjectID") %>'> </asp:TextBox>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>

            </asp:GridView>
        </div>
        <div class="column large-1">
        </div>

    </div>

</asp:Content>
