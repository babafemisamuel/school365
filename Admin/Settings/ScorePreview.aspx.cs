﻿using School365.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace School365.Admin.Settings
{
    public partial class ScorePreview : System.Web.UI.Page
    {
        StudentModel db = new StudentModel();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (User.Identity.IsAuthenticated == false)
                Response.Redirect("../Administrator/UserLogin.aspx");
            int getAdminID = int.Parse(User.Identity.Name);
            var getAuth = (from d in db.RoleAdmins where d.AdminID == getAdminID select d.RoleID).ToList();
            if (getAuth.Contains(10) != true)
            {
                Response.Redirect("../Administrator/Profile.aspx");
            }
        }

        protected void getSubj_Click(object sender, EventArgs e)
        {
            //int getCurrentSession= (from d in db.Sessions where d.CurrentSession==true select d.SessionID).FirstOrDefault();
            int Session = int.Parse(allSession.SelectedItem.Value);
            double _Level = double.Parse(Level.SelectedItem.Value);
            int _Semester = int.Parse(Semesters.SelectedItem.Value);
            var getSubject = from d in db.Results
                             where d.MatricNo.Trim() == matric.Text.Trim().ToUpper() && d.SessionID == Session
                             && d.Level==_Level && d.Semester== _Semester
                             join f in db.Students on d.StudentID equals f.StudentID
                             join g in db.Subjects on d.SubjectID equals g.SubjectID
                             select new { f.Firstname, f.Surname, f.Middlename, g.SubjectCode, d.CA, d.EXAM,d.MatricNo,d.SubjectID };
            allStudents.DataSource = getSubject.ToList().OrderBy(a=>a.SubjectCode);
            allStudents.DataBind();                            
        }

        protected void UpStd_Click(object sender, EventArgs e)
        {
            int Session = int.Parse(allSession.SelectedItem.Value);
            double _Level = double.Parse(Level.SelectedItem.Value);
            int _Semester = int.Parse(Semesters.SelectedItem.Value);
            foreach (GridViewRow row in allStudents.Rows)
            {
                TextBox CA = (TextBox)row.FindControl("CA");
                TextBox EXAM = (TextBox)row.FindControl("EXAM");
                TextBox Matric = (TextBox)row.FindControl("Matric");
                TextBox SubjectID = (TextBox)row.FindControl("SubjectID");

                var UpdateResult = (from d in db.Results where d.MatricNo.Trim() == Matric.Text.Trim() && d.SubjectID.ToString() == SubjectID.Text && d.SessionID == Session && d.Level==_Level && d.Semester==_Semester select d).FirstOrDefault();
                UpdateResult.CA = double.Parse(CA.Text);
                UpdateResult.EXAM = double.Parse(EXAM.Text);
                db.SaveChanges();
            }
        }
    }
}