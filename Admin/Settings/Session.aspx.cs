﻿using School365.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EntityFramework.Extensions;

namespace School365.Admin.Settings
{
    public partial class Session : System.Web.UI.Page
    {
        StudentModel db = new StudentModel();
        int getAdminID = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (User.Identity.IsAuthenticated == false)
                Response.Redirect("../Administrator/UserLogin.aspx");
            getAdminID = int.Parse(User.Identity.Name);
            var getAuth = (from d in db.RoleAdmins where d.AdminID == getAdminID select d.RoleID).ToList();
            if (getAuth.Contains(10) != true)
            {
                Response.Redirect("../Administrator/Profile.aspx");
            }
            if (!IsPostBack)
            {
                var getSession = (from d in db.Sessions select d).ToList();
                allSession.DataSource = getSession;
                allSession.DataBind();
            }
        }

        //insert new Session
        protected void InsertBtn_Click(object sender, EventArgs e)
        {
            var InsertSession = new Model.Session
            {
                SessionYear = sessionYear.Text,
                CurrentSession = false,
            };
            db.Sessions.Add(InsertSession);
            db.SaveChanges();
            sessionYear.Text = "";
            var getSession = (from d in db.Sessions select d).ToList();
            allSession.DataSource = getSession;
            allSession.DataBind();
        }

        private void UpdateStudentYear()
        {
            //int getCurrentID = (from d in db.Sessions where d.CurrentSession == true select d.SessionID).FirstOrDefault();

            var years = db.Results.Select(a => a.Level).Distinct().ToList();
            foreach (var item in years)
            {
                //var students=db.Results.Where(a => a.SessionID == getCurrentID).Select(a => a.Students);
                db.Students.Where(a => a.Level == item && a.Registered!= "False").Update(x => new Model.Student() { Level = item + 100 ,Registered= "False" });
            }

            //var StudentDetails = (from d in db.Results where d.SessionID == getCurrentID join f in db.Students on d.StudentID equals f.StudentID select new { d.StudentID, d.Level, f.Registered }).Distinct().ToList();
            //foreach (var setStudentDetails in StudentDetails)
            //{
            //    double newLevel = setStudentDetails.Level + 100;
            //    var UpdateStudent = (from d in db.Students where d.StudentID == setStudentDetails.StudentID select d).FirstOrDefault();
            //    UpdateStudent.Level = newLevel;
            //    UpdateStudent.Registered = "False";
            //    db.SaveChanges();
            //}
        }
        private void InsertCarryOver()
        {
           // List<int> carryOvers = (from d in db.Results where (d.CA + d.EXAM) <= 39 select d.ResultID).ToList();
            //db.CarryOvers.AddRange<>
           
            try
            {
                db.Configuration.AutoDetectChangesEnabled = false;
                var getCarryOvers = (from d in db.Results where (d.CA + d.EXAM) <= 39 select d.ResultID).AsQueryable();

                List<CarryOver> carryOver = new List<CarryOver>();
                foreach (var item in getCarryOvers)
                {
                    CarryOver _carryOver = new CarryOver();
                    _carryOver.ResultID = item;
                    carryOver.Add(_carryOver);
                }
                db.CarryOvers.AddRange(carryOver);
                
                //foreach (var setCarryOvers in getCarryOvers)
                //{
                    //var InsertCarryOvers = new CarryOver
                    //{
                    //    ResultID = setCarryOvers,
                    //};
                    //db.CarryOvers.Add(InsertCarryOvers);
                    //db.SaveChanges();
                //}
               // db.SaveChanges();
            }
            finally
            {
                db.Configuration.AutoDetectChangesEnabled = true;
            }

        }

        private void InsertMatric()
        {
            string NewCurrent = (from d in db.Sessions where d.CurrentSession == true select d.SessionYear).FirstOrDefault();
            string[] stringValue = NewCurrent.Split('/');
            var InsertStudent = new Model.Student
            {
                Firstname = "Sample",
                Middlename = "Sample",
                Surname = "Sample",
                MatricNo = stringValue[1].ToString().Substring(2, 2) + "/0000",
                StudentKey = System.Guid.NewGuid().ToString(),
                SOR = "ABIA",
            };
            db.Students.Add(InsertStudent);
            db.SaveChanges();
        }

        protected void UpdateSession_Click(object sender, EventArgs e)
        {
            UpdateStudentYear();
            InsertCarryOver();
           // GetOutstanding();

            var getCurrent = (from d in db.Sessions where d.CurrentSession == true select d).FirstOrDefault();
            getCurrent.CurrentSession = false;
            db.SaveChanges();
            foreach (GridViewRow row in allSession.Rows)
            {
                if ((row.Cells[0].FindControl("selectBtn") as RadioButton).Checked)
                {
                    int sessionID = int.Parse(row.Cells[1].Text);
                    var UpdateStudent = (from d in db.Sessions where d.SessionID == sessionID select d).First();
                    UpdateStudent.CurrentSession = true;
                    db.SaveChanges();
                }
            }
            InsertMatric();
            ClearPin();
            var all = (from d in db.LecturerProfiles select d);
            //db.LecturerProfiles.RemoveRange(all);
            //db.SaveChanges();

            Response.Write("Session Inserted");
        }
        private void GetOutstanding()
        {
            try
            {
                int CurrSession = db.Sessions.Where(a => a.CurrentSession == true).Select(a => a.SessionID).FirstOrDefault();

                var getStudents = db.Results.Where(a => a.SessionID == CurrSession)
                                            .Select(a => a.StudentID)
                                            .Distinct().ToList();
                var allResults = db.Results.Where(a => a.SessionID == CurrSession).Select(a => a).ToList();
                var allComb = db.AllCombineds.Select(a => a).ToList();
                foreach (var setStudents in getStudents)
                {
                    //getting level
                    double StudentLevel = allResults.Where(a => a.StudentID == setStudents)
                                               .Select(a => a.Level).FirstOrDefault();
                    //get studentSubComb
                    int getSubComb = allResults.Where(a => a.StudentID == setStudents)
                                               .Select(a => a.SubjectCombinID)
                                               .FirstOrDefault();

                    //get subject with that subject combination
                    var getSubjects = allComb.Where(a => a.SubjectCombineID == getSubComb && a.Subjects.SubjectLevel == StudentLevel.ToString())
                                                .Select(a => a.SubjectID).ToList();

                    //get student registered sub
                    var getStudentSub = allResults.Where(a => a.StudentID == setStudents)
                                                   .Select(a => a.SubjectID).ToList();
                    //compare and get subjects that the person didn't register
                    var newSubID = getSubjects.Except(getStudentSub).ToList();

                    foreach (var item in newSubID)
                    {
                        var AllCombinedID = allComb.Where(a => a.SubjectID == item && a.SubjectCombineID == getSubComb)
                                                    .Select(a => a.AllCombinedID).FirstOrDefault();
                        var InsertOutstanding = new Outstanding
                        {
                            StudentID = setStudents,
                            AllCombinedID = AllCombinedID,
                            SessionID=CurrSession,
                            Registered=false
                        };
                        db.OutStandings.Add(InsertOutstanding);
                        db.SaveChanges();
                    }

                }
            }
            catch (Exception)
            {
                throw;
            }

        }
        private void ClearPin()
        {
            var getPin = db.Pins.Select(a => a);
            db.Pins.RemoveRange(getPin);
        }
    }
}