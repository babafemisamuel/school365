﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Admin_Layout.Master" AutoEventWireup="true" CodeBehind="Curricullum.aspx.cs" Inherits="School365.Admin.Curricullum.Curricullum" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="header" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="body" runat="server">


  
    <div class="row">
        <div class="large-1 columns">
             
        </div>
        <div class="large-6 columns">
            <label> Curricullum Name</label>
            <asp:TextBox ID="CurricullumText"  runat="server" placeholder="Curricullum Name"></asp:TextBox>
          
            <asp:Button ID="AddCurricullum" runat="server" CssClass="tiny radius button bg-blue" Text="Add Curriculum" OnClick="AddCurricullum_Click" style="left: 0px; top: 1px" />             
        </div>
          <div class="large-2 columns">
              <br />
            <asp:CheckBox ID="check" runat="server"/> <label for="check">: Current</label>
        </div>
        <div class="large-1 columns">
           
        </div>

       
    </div>
       <div class="row">
            <asp:GridView runat="server" ID="curr" AutoGenerateColumns="False" DataSourceID="LinqDataSource1" Width="100%">
                <Columns>
                    <asp:BoundField DataField="CurricullumName" HeaderText="CurricullumName" ReadOnly="True" SortExpression="CurricullumName" />
                    <asp:CheckBoxField DataField="IsCurrent" HeaderText="IsCurrent" ReadOnly="True" SortExpression="IsCurrent" />
                </Columns>


            </asp:GridView>
            <asp:LinqDataSource ID="LinqDataSource1" runat="server" ContextTypeName="School365.Model.StudentModel" EntityTypeName="" Select="new (CurricullumName, IsCurrent)" TableName="Curricullums">
            </asp:LinqDataSource>
        </div>
</asp:Content>
