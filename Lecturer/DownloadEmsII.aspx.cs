﻿using School365.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace School365.Lecturer
{
    public partial class DownloadEmsII : System.Web.UI.Page
    {
        public StudentModel db = new StudentModel();
        public List<int> getStudentsID = new List<int>();
        int getCurrentSessionID = 0;
        int getLecturerID = 0;
        int getSession = 0;
        public List<StudentResult> stds = new List<StudentResult>();

        //public string SubjectName = "";
        public string subCode = "";
        public string newSubName = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            getLecturerID = int.Parse(User.Identity.Name);
            getSession = (from d in db.Sessions where d.CurrentSession == true select d.SessionID).FirstOrDefault();
            if (!IsPostBack)
            {
                int getPasswordChanged = (from d in db.Lecturers where d.LecturerID == getLecturerID && d.PasswordChanged == true select d).Count();
                if (getPasswordChanged == 0)
                {
                    Response.Redirect("PasswordChange.aspx");
                    //CheckPassword.Text = "Please Change Your password";
                }
                getCurrentSessionID = (from d in db.Sessions where d.CurrentSession == true select d.SessionID).FirstOrDefault();
                var getAllSubjectsDetail = (from d in db.LecturerProfiles where d.LecturerID == getLecturerID join f in db.Subjects on d.SubjectID equals f.SubjectID select new { f.SubjectID, f.SubjectCode }).ToList();
                getAllSubjects.DataSource = getAllSubjectsDetail;
                getAllSubjects.DataValueField = "SubjectID";
                getAllSubjects.DataTextField = "SubjectCode";
                getAllSubjects.DataBind();
            }
        }

        protected void emsDownload_Click(object sender, EventArgs e)
        {
        //    Response.Clear();
        //    Response.Buffer = true;
        //    string filedata = "attachment;filename=" + getAllSubjects.SelectedItem.Text.Replace(" ", string.Empty) + ".csv";
        //    Response.AddHeader("content-disposition", filedata);
        //    Response.Charset = "";
        //    Response.ContentType = "application/text";
        //    StringBuilder sBuilder = new System.Text.StringBuilder();
        //    for (int index = 0; index < allStudents.Columns.Count; index++)
        //    {
        //        sBuilder.Append(allStudents.Columns[index].HeaderText + ',');
        //    }
        //    sBuilder.Append("\r\n");
        //    for (int i = 0; i < allStudents.Rows.Count; i++)
        //    {
        //        for (int k = 0; k < allStudents.HeaderRow.Cells.Count; k++)
        //        {
        //            sBuilder.Append(allStudents.Rows[i].Cells[k].Text.Replace(",", "") + ",");
        //        }
        //        sBuilder.Append("\r\n");
        //    }
        //    Response.Output.Write(sBuilder.ToString());
        //    Response.Flush();
        //    Response.End();
        }

        protected void getStudents_Click(object sender, EventArgs e)
        {
         
            var getStudentFaculty = (from d in db.Students where d.FacultyName == allFaculty.SelectedItem.Text select d.MatricNo).Distinct().ToList(); 
            var getStudents = (from d in db.Results
                               where d.SubjectID.ToString() == getAllSubjects.SelectedItem.Value && d.SessionID== getSession
                               && d.EXAM == 0 && d.CA == 0
                               join f in db.Subjects on d.SubjectID equals f.SubjectID
                               select new { d.MatricNo, d.CA, d.EXAM, d.CTCP }).Distinct().OrderBy(a => a.MatricNo).ToList();

            var getNewStd = (from d in db.Results 
                             where d.SubjectID.ToString() == getAllSubjects.SelectedItem.Value && d.SessionID == getSession
                             && d.EXAM == 0 && d.CA == 0
                             join f in db.Subjects on d.SubjectID equals f.SubjectID
                             select d.MatricNo).Distinct().OrderBy(a => a).ToList();

            var getIntersect = getStudentFaculty.Intersect(getNewStd).OrderBy(a => a);

            (from d in getIntersect
             join f in getStudents on d.ToString() equals f.MatricNo
             select new { f.MatricNo, f.CA, f.EXAM }).Distinct().ToList().ForEach(a =>
             {
                 StudentResult std = new StudentResult();
                 std.Ca = a.CA;
                 std.Exam = a.EXAM;
                 std.MatricNo = a.MatricNo;
                 stds.Add(std);
             });

          
        }
    }
}