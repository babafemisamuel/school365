﻿using School365.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace School365.Lecturer
{
    public partial class LecturerLogin : System.Web.UI.Page
    {
        StudentModel db = new StudentModel();
        private Model.Lecturer AdminAuth;
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Redirect("Login.aspx");
        }

        protected void submit_Click(object sender, EventArgs e)
        {
            AdminAuth = ((from d in db.Lecturers where d.UserName == UserName.Text && d.Password == Password.Text select d).Count() > 0) ? (from d in db.Lecturers where d.UserName == UserName.Text && d.Password == Password.Text select d).FirstOrDefault() : null;
            // AdminAuth = (db.Admin.Where(u => u.Username == invoiceNum.Text && u.Password == pin.Text).Count() > 0) ? db.Admin.FirstOrDefault(u => u.Username == invoiceNum.Text && u.Password == pin.Text) : null;
            if (AdminAuth == null)
            {
                errorDisplay.Text = "Username or Password is incorrect";
            }
            else
            {
                Response.Cookies["LecturerID"].Value = AdminAuth.LecturerID.ToString();
                string parameter = "grab=" + AdminAuth.LecturerID;               
                Response.Redirect("LecturerProfile.aspx");
            }
        }
    }
}