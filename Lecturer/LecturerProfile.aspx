﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Lecturer/Lecturers.Master" AutoEventWireup="true" CodeBehind="LecturerProfile.aspx.cs" Inherits="School365.Lecturer.LecturerProfile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <hr />
    <asp:Label ID="CheckPassword" runat="server" Text=" " Style="font-weight: 700; color: #FF0000; font-size: x-large"></asp:Label>
    <div class="row">

        <div class="col-lg-6">
        </div>
        <div class="col-lg-6 columns">
            <asp:Button ID="InsertScore" runat="server" CssClass="btn btn-outline btn-success" Text="Insert Score" OnClick="InsertScore_Click" />
            <asp:Button ID="DownloadEMS" runat="server" CssClass="btn btn-outline btn-info" Text="Download EMS" OnClick="DownloadEMS_Click" />
            <asp:Button ID="PasswordChange" runat="server" CssClass="btn btn-outline btn-danger" Text="Change Password" OnClick="PasswordChange_Click" />
        </div>
    </div>
    <div class="row">
        <div class="col-lg-3">
        </div>
        <div class="col-lg-6">
            <asp:Label ID="LecturerName" runat="server" Text=" " Style="font-family: 'Brush Script MT'; font-size: 103px;"></asp:Label>
            <br />
        </div>
        <div class="col-lg-3">
        </div>
    </div>

    <div class="row">
        <div class="col-lg-3"></div>
        
        <div class="col-lg-6" style="font-size: 103px; background-color: gainsboro; font-family: Castellar;">
            <p align="center">
                <asp:Label ID="SubjectNo" runat="server" Text=" " Style="font-size: 153px; font-family: Castellar;"></asp:Label>
            </p>
            <p align="center" style="font-family: 'Segoe UI'; font-size: x-large; color: black">Subjects</p>
        </div>
        <div class="col-lg-3">
        </div>
    </div>
</asp:Content>
