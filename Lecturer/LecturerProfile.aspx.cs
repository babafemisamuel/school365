﻿using School365.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace School365.Lecturer
{
    public partial class LecturerProfile : System.Web.UI.Page
    {
        StudentModel db = new StudentModel();
        int CountStudent = 0;
        int getLecturerID = 0;
      
        protected void Page_Load(object sender, EventArgs e)
        {
            getLecturerID = int.Parse(User.Identity.Name);
            //getLecturerID = int.Parse(Request.QueryString["grab"].ToString());
            if (!IsPostBack)
            {
                int getPasswordChanged = (from d in db.Lecturers where d.LecturerID == getLecturerID && d.PasswordChanged == true select d).Count();
                if (getPasswordChanged==0)
                {
                    Response.Redirect("PasswordChange.aspx");
                    //CheckPassword.Text = "Please Change Your password";
                }
               
                var getLecturerDetails = (from d in db.Lecturers where d.LecturerID == getLecturerID select d).FirstOrDefault();
                LecturerName.Text = getLecturerDetails.Surname + " " + getLecturerDetails.Firstname + " " + getLecturerDetails.Middlename;
                int LecturerID = getLecturerDetails.LecturerID;
                int getSubjectID = (from d in db.LecturerProfiles where d.LecturerID == LecturerID select d.SubjectID).Distinct().Count();
                SubjectNo.Text = getSubjectID.ToString();
            }
        }

        protected void InsertScore_Click(object sender, EventArgs e)
        {            
            Response.Redirect("ScoreStudent.aspx");
        }

        protected void PasswordChange_Click(object sender, EventArgs e)
        {           
            Response.Redirect("PasswordChange.aspx");
        }

        protected void DownloadEMS_Click(object sender, EventArgs e)
        {
            Response.Redirect("DownloadEMS.aspx");
        }
    }
}