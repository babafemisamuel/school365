﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="School365.Lecturer.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Lecturer Login</title>
    <link href="../Students/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
    <!-- animation CSS -->
    <link href="../Students/css/animate.css" rel="stylesheet" />
    <!-- Custom CSS -->
    <link href="../Students/css/lecturerstyle.css" rel="stylesheet" />
    <!-- color CSS -->
    <link href="../Students/css/colors/default.css" id="theme" rel="stylesheet" />

</head>
<body style="background-image:url(../Image/Lecture.png)">
   

    <div class="preloader">
        <div class="cssload-speeding-wheel"></div>
    </div>
    <section id="wrapper" class="login-register">
        <div class="login-box">
            <div class="white-box">
                <form runat="server" class="form-horizontal form-material" id="loginform">
                    <h3 class="box-title m-b-20">Sign In</h3>
                    <asp:Label ID="errorDisplay" runat="server" ></asp:Label>
                    <div class="form-group ">
                        <div class="col-xs-12">
                            <asp:TextBox ID="UserName" runat="server" CssClass="form-control" placeholder="Username" required=""></asp:TextBox>

                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <asp:TextBox ID="Password" TextMode="Password" CssClass="form-control" runat="server" placeholder="Password" required=""></asp:TextBox>

                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <div class="checkbox checkbox-primary pull-left p-t-0">
                            </div>
                            <a href="javascript:void(0)" id="to-recover" class="text-dark pull-right"><i class="fa fa-lock m-r-5"></i>Forgot pwd?</a>
                        </div>
                    </div>
                    <div class="form-group text-center m-t-20">
                        <div class="col-xs-12">
                            <%--<asp:Button ID="Log" runat="server" Text="Log in" OnClick="Login_Click"/>--%>
                            <asp:Button ID="UserLogin" runat="server" class="btn btn-info  btn-block text-uppercase waves-effect waves-light" Text="Log in" OnClick="UserLogin_Click" />
                        </div>
                    </div>

                   
                </form>

            </div>
        </div>
    </section>

    <script src="../Students/plugins/bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../Students/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- Menu Plugin JavaScript -->
    <script src="../Students/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>

    <!--slimscroll JavaScript -->
    <script src="../Students/js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="../Students/js/waves.js"></script>
 
    <!-- Custom Theme JavaScript -->
    <script src="../Students/js/custom.min.js"></script>
    <!--Style Switcher -->
    <script src="../Students/plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
</body>
</html>