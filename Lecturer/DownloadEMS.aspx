﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Lecturer/Lecturers.Master" AutoEventWireup="true" CodeBehind="DownloadEMS.aspx.cs" Inherits="School365.Lecturer.DownloadEMS" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            width: 46px;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script src="../js/excellentexport.min.js"></script>
    <div class="row">
        <div class=" col-lg-4"></div>
        <div class="col-lg-4 ">
            <label>Subjects</label>
            <asp:DropDownList ID="getAllSubjects" CssClass="form-control" runat="server">
            </asp:DropDownList>
            <br />
            <div class="row">
                <div class=" col-lg-4"></div>

                <div class=" col-lg-4">
                    <asp:Button ID="getSubjects" Text="Get Students" CssClass="btn  btn-outline btn-info" runat="server" OnClick="getStudents_Click" />
                </div>

                <div class=" col-lg-4">
                     
                <input type="button" id="DownloadExcel" value="Download Excel" class="export" />
  
                    <asp:Button ID="emsDownload" Visible="false" Text="Download" CssClass="btn btn-outline btn-danger" runat="server" OnClick="emsDownload_Click" />
                </div>

            </div>

            <br />
        </div>
        <%-- <asp:TextBox ID="Surname" runat="server" Visible="true" ReadOnly="true" Text='<%#Bind("Surname") %>'> ></asp:TextBox>--%>
        <div class="col-lg-4 ">
        </div>
    </div>
   <%-- <div class="row">
        <div class="col-lg-2"></div>
        <div class="col-lg-8">
            <asp:GridView ID="allStudents" runat="server" CssClass="table table-bordered table-hover table-striped" align="center" EmptyDataText="No Student Available">
            </asp:GridView>
        </div>
        <div class="col-lg-2"></div>
    </div>--%>
    <div class="row">
        <div class="col-lg-2"></div>
        <div class="col-lg-8">
           <div id="tableWrap">
            <table id="table" class="table table-bordered table-hover table-striped">
                <thead>
                    <tr>
                        <th>Matric Number</th>

                        <th>CA</th>
                        <th>EXAM</th>
                        <th>Total</th>
                    </tr>
                </thead>
                <tbody>
                    <%foreach (var item in AllMatric)
                        {
                            int SessionID = db.Sessions.Where(a => a.CurrentSession == true).FirstOrDefault().SessionID;
                            var students = (from d in db.Results
                                            where SessionID==SessionID
                                            where d.MatricNo == item && d.SubjectID.ToString() == getAllSubjects.SelectedItem.Value
                                            && d.EXAM == 0 && d.CA == 0
                                            join f in db.Subjects on d.SubjectID equals f.SubjectID
                                            select new { d.MatricNo, d.CA, d.EXAM, d.CTCP }).Distinct().OrderBy(a => a.MatricNo).FirstOrDefault();
                    %>
                    <tr>
                        <td><%Response.Write(students.MatricNo); %></td>
                        <td><%Response.Write(students.CA); %></td>
                        <td><%Response.Write(students.EXAM); %></td>
                        <td><%Response.Write(students.CA + students.EXAM); %></td>

                    </tr>
                    <%  }

                    %>
                </tbody>

            </table>
           </div>

        </div>
        <div class="col-lg-2"></div>

    </div>
</asp:Content>
