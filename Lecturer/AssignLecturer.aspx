﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Lecturer/Lecturers.Master" AutoEventWireup="true" CodeBehind="AssignLecturer.aspx.cs" Inherits="School365.Lecturer.AssignLecturer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <hr />
    <div class="row">

        <div class="col-lg-2"></div>
        <div class="col-lg-10">
            <div class="row">
                <div class="col-lg-3">
                    <asp:TextBox ID="Search" runat="server" CssClass="form-control" placeholder="Search"></asp:TextBox>
                </div>
                <div class="col-lg-3">
                    <asp:Button ID="getStaff" runat="server" Text="Get Staff" CssClass="btn btn-success" OnClick="getStaff_Click" />
                </div>
                <div class="col-lg-3">
                    <asp:DropDownList ID="getLecturers" runat="server" CssClass="form-control">
                    </asp:DropDownList>
                </div>

                <div class="col-lg-3">
                    <asp:Button ID="Assign" runat="server" CssClass="btn btn-info" Text="Assign" OnClick="Assign_Click" />
                </div>
            </div>
            <div class="row">
                <hr />
                <asp:GridView ID="allSubjects" CssClass="table table-bordered table-hover table-striped" runat="server" AllowPaging="True" OnPageIndexChanging="allSubjects_PageIndexChanging" PageSize="50">
                    <Columns>
                        <asp:TemplateField HeaderText="Select">
                            <ItemTemplate>
                                <asp:CheckBox ID="Crs" runat="server"  />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <PagerSettings Position="TopAndBottom" />
                </asp:GridView>

            </div>
        </div>
        
    </div>
    <hr />

</asp:Content>
