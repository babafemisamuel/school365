﻿using School365.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace School365.Lecturer
{
    public partial class ScoreStudent : System.Web.UI.Page
    {
        StudentModel db = new StudentModel();
        int getCurrentSessionID = 0;
        int getLecturerID = 0;
        int getSession = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            getLecturerID = int.Parse(User.Identity.Name);
            int getPasswordChanged = (from d in db.Lecturers where d.LecturerID == getLecturerID && d.PasswordChanged == true select d).Count();
            if (getPasswordChanged == 0)
            {
                Response.Redirect("PasswordChange.aspx");
                //CheckPassword.Text = "Please Change Your password";
            }

            getSession = (from d in db.Sessions where d.CurrentSession == true select d.SessionID).FirstOrDefault();
            if (!IsPostBack)
            {
               
                getCurrentSessionID = (from d in db.Sessions where d.CurrentSession == true select d.SessionID).FirstOrDefault();
               
                    var getAllSubjectsDetail = (from d in db.LecturerProfiles where d.LecturerID == getLecturerID join f in db.Subjects on d.SubjectID equals f.SubjectID select new { f.SubjectID, f.SubjectCode }).ToList();
                    getAllSubjects.DataSource = getAllSubjectsDetail;
                    getAllSubjects.DataValueField = "SubjectID";
                    getAllSubjects.DataTextField = "SubjectCode";
                    getAllSubjects.DataBind();
                
              
            }
            

        }

        protected void getStudents_Click(object sender, EventArgs e)
        {
            var getStudents = from d in db.Results
                              where d.SubjectID.ToString() == getAllSubjects.SelectedItem.Value
                              && d.SessionID == getSession 
                              && d.CA==0 && d.EXAM==00
                              join a in db.Students on d.StudentID equals a.StudentID
                              select new { a.StudentID, a.MatricNo, d.CA, d.EXAM };
            allStudents.DataSource = getStudents.OrderBy(a=>a.MatricNo).ToList();
            allStudents.DataBind();
            //var getAllStudents= from d in db.Results where d.StudentID
        }

        protected void insertScore_Click(object sender, EventArgs e)
        {
            foreach (GridViewRow row in allStudents.Rows)
            {
                TextBox CA = (TextBox)row.FindControl("CA");
                TextBox EXAM = (TextBox)row.FindControl("EXAM");
                TextBox MatricNo = (TextBox)row.FindControl("MatricNo");

                var UpdateResult = (from d in db.Results where d.MatricNo == MatricNo.Text && d.SubjectID.ToString() == getAllSubjects.SelectedItem.Value select d).First();
                UpdateResult.CA = double.Parse(CA.Text);
                UpdateResult.EXAM = double.Parse(EXAM.Text);
                UpdateResult.LecturerInserted = "TRUE";
                db.SaveChanges();
            }
            Response.Write("Data Inserted");
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            if (UploadFile.HasFile != null && UploadFile.PostedFile.ContentLength > 0)
            {
                string FileName = UploadFile.FileName;
                
                string ImgPath = " ../Upload/" + FileName;
                FileInfo file = new FileInfo(Server.MapPath(ImgPath));
                string FileExt = Path.GetExtension(ImgPath);
                if (FileExt == ".csv") { 
                if (file.Exists)
                {
                    file.Delete();
                    UploadFile.PostedFile.SaveAs(Server.MapPath(ImgPath));
                }
                else
                {
                    UploadFile.PostedFile.SaveAs(Server.MapPath(ImgPath));
                }            
                string parameter = "ImgPath=" + ImgPath;
                Response.Redirect("ScorePreview.aspx?"+parameter);
                }
                Response.Write("Please check your file extension. make sure it is a csv file");
            }

        }
       
    }
}