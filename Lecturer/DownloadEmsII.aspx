﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Lecturer/Lecturers.Master" AutoEventWireup="true" CodeBehind="DownloadEmsII.aspx.cs" Inherits="School365.Lecturer.DownloadEmsII" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            width: 46px;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script src="../js/excellentexport.min.js"></script>
    <div class="row">
        <div class="col-lg-3"></div>
        <div class="col-lg-4">
            <label>Subjects</label>
            <asp:DropDownList ID="getAllSubjects" CssClass="form-control" runat="server">
            </asp:DropDownList>
            <br />
            <div class="row">
                <div class="col-lg-4">
                    <asp:Button ID="getSubjects" Text="Get Students" CssClass="btn btn-outline btn-danger" runat="server" OnClick="getStudents_Click" />
                </div>
                <div class="col-lg-4">
                </div>
                <div class="col-lg-4">
                    <input type="button" id="DownloadExcel" value="Download Excel" class="export" />
                    <asp:Button ID="emsDownload" Visible="false" Text="Download" CssClass="btn btn-outline btn-info" runat="server" OnClick="emsDownload_Click" />
                </div>

            </div>

            <br />
        </div>
        <%-- <asp:TextBox ID="Surname" runat="server" Visible="true" ReadOnly="true" Text='<%#Bind("Surname") %>'> ></asp:TextBox>--%>
        <div class="col-lg-4">
            <label>Faculty</label>
            <asp:DropDownList ID="allFaculty" CssClass="form-control" runat="server" DataSourceID="LinqDataSource1" DataTextField="FacultyCode" DataValueField="FacultyCode">
            </asp:DropDownList>
            <asp:LinqDataSource ID="LinqDataSource1" runat="server" ContextTypeName="School365.Model.StudentModel" EntityTypeName="" OrderBy="FacultyCode" Select="new (FacultyID, FacultyCode)" TableName="Facultys">
            </asp:LinqDataSource>
        </div>
        <div class="col-lg-2">
        </div>
    </div>
    <div class="row">
        <div class="col-lg-2">
        </div>
        <div class="col-lg-8">
            <table id="table" class="table table-bordered table-hover table-striped">
                <thead>
                    <tr>
                        <th>Matric Number</th>

                        <th>CA</th>
                        <th>EXAM</th>
                        <th>Total</th>
                    </tr>
                </thead>
                <tbody>
                    <%foreach (var item in stds)
                        {

                    %>
                    <tr>
                        <td><%Response.Write(item.MatricNo); %></td>
                        <td><%Response.Write(item.Ca); %></td>
                        <td><%Response.Write(item.Exam); %></td>
                        <td><%Response.Write(item.Total); %></td>

                    </tr>
                    <%  }

                    %>
                </tbody>

            </table>
            <asp:GridView ID="allStudents" runat="server" CssClass="table table-bordered table-hover table-striped" align="center" EmptyDataText="No Student Available">
            </asp:GridView>

        </div>
        <div class="col-lg-2">
        </div>
    </div>

</asp:Content>
