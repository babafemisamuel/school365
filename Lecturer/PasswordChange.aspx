﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Lecturer/Lecturers.Master" AutoEventWireup="true" CodeBehind="PasswordChange.aspx.cs" Inherits="School365.Lecturer.PasswordChange" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <hr />
    <asp:Label ID="Output" runat="server" Text=" " style="font-weight: 700; color: #FF0000; font-size: x-large" ></asp:Label>
    <div class="row">
        <br />
        <hr />
        <div class="col-lg-4"></div>
        <div class="col-lg-4">
           <asp:TextBox ID="PrevPassword" runat="server" CssClass="form-control" TextMode="Password" placeholder="Old Password"></asp:TextBox>
            <br />
            <asp:TextBox ID="NewPassword" runat="server" CssClass="form-control" TextMode="Password" placeholder="New Password"></asp:TextBox>
             <br />
            <asp:TextBox ID="CompNewPassword" runat="server" CssClass="form-control"  TextMode="Password" placeholder="Confirm Password"></asp:TextBox>
            <asp:CompareValidator ID="ComparePassword" runat="server" ControlToCompare="NewPassword" ControlToValidate="CompNewPassword" Text="The passwords are not the same" style="font-weight: 700; color: #FF0000; font-style: italic;" ></asp:CompareValidator>
             <br />
            <asp:Button ID="InsertPassword" runat="server" CssClass="btn btn-outline btn-info" Text="Update Password"  OnClick="InsertPassword_Click" />
        </div>
    </div>
</asp:Content>
