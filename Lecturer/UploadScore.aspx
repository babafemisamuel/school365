﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Lecturer/Lecturers.Master" AutoEventWireup="true" CodeBehind="UploadScore.aspx.cs" Inherits="School365.Lecturer.UploadScore" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
   
    <div class="row">
        <div class="col-lg-4 ">
        </div>
        <%--<a href="../Upload/WebForm1.aspx"></a>--%>
        <div class="col-lg-4 ">
            

            <label>Subjects</label>
            <asp:DropDownList ID="getAllSubjects" CssClass="form-control" runat="server">
            </asp:DropDownList>
            <asp:Button ID="getSubjects" Text="Upload Score" CssClass="btn btn-outline btn-info" runat="server" OnClick="getStudents_Click" />
        </div>
        <br />
        <div class="col-lg-4">
            <asp:FileUpload ID="UploadFile" runat="server" Visible="false" />
            <asp:Button ID="Button1" Text="Upload Students" CssClass="btn btn-outline btn-success" runat="server" Visible="false" OnClick="Button1_Click"  />
        </div>
        </div>
    <div class="row">
        <div class="col-lg-2"></div>
        <div class="col-lg-8">
            <asp:Button ID="insertScore" Text="Submit Score" CssClass="btn btn-outline btn-danger" runat="server" OnClick="insertScore_Click" />
            <br />
           <center>  <asp:Label ID="message" runat="server" CssClass="label label-success" Text="Scores Added" Visible="false"></asp:Label>
               <br /><br />
                <asp:Button ID="pdfExport" Text="Print Pdf" CssClass="btn btn-outline btn-danger" runat="server" Visible="false" OnClick="pdfExport_Click" />
           </center>
            <br />
               <asp:GridView ID="allStudents" runat="server" CssClass="table table-bordered table-hover table-striped">
            </asp:GridView>
            <%--<asp:GridView ID="allStudents" CssClass="table table-bordered table-hover table-striped" runat="server" AutoGenerateColumns="false" EmptyDataText="No Student Available">
                <Columns>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:TextBox ID="StudentID" CssClass="form-control" runat="server" Visible="false" Text='<%#Bind("StudentID") %>'> ></asp:TextBox>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="MatricNo">
                        <ItemTemplate>
                            <asp:TextBox ID="MatricNo" CssClass="form-control" runat="server" ReadOnly="true" Text='<%#Bind("MatricNo") %>'> </asp:TextBox>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="CA">
                        <ItemTemplate>
                            <asp:TextBox ID="CA" runat="server" CssClass="form-control" Text='<%#Bind("CA") %>'> </asp:TextBox>
                        </ItemTemplate>
                    </asp:TemplateField>

                     <asp:TemplateField HeaderText="EXAM">
                        <ItemTemplate>
                            <asp:TextBox ID="EXAM" runat="server" CssClass="form-control" Text='<%#Bind("EXAM") %>'> </asp:TextBox>
                        </ItemTemplate>
                    </asp:TemplateField>

                </Columns>
            </asp:GridView>--%>
            <br />
            <asp:Button ID="Submit2" Text="Submit Score" CssClass="btn btn-outline btn-success" runat="server" OnClick="insertScore_Click" />
        </div>
      
    </div>
</asp:Content>
