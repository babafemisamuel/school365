﻿using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using School365.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace School365.Lecturer
{
    public partial class UploadScore : System.Web.UI.Page
    {
        StudentModel db = new StudentModel();
        int getCurrentSessionID = 0;
        int getLecturerID = 0;
        int getSession = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            getLecturerID = int.Parse(User.Identity.Name);
            int getPasswordChanged = (from d in db.Lecturers where d.LecturerID == getLecturerID && d.PasswordChanged == true select d).Count();
            if (getPasswordChanged == 0)
            {
                Response.Redirect("PasswordChange.aspx");
                //CheckPassword.Text = "Please Change Your password";
            }

            getSession = (from d in db.Sessions where d.CurrentSession == true select d.SessionID).FirstOrDefault();
            if (!IsPostBack)
            {

                getCurrentSessionID = (from d in db.Sessions where d.CurrentSession == true select d.SessionID).FirstOrDefault();

                var getAllSubjectsDetail = (from d in db.LecturerProfiles where d.LecturerID == getLecturerID join f in db.Subjects on d.SubjectID equals f.SubjectID select new { f.SubjectID, f.SubjectCode }).ToList();
                getAllSubjects.DataSource = getAllSubjectsDetail;
                getAllSubjects.DataValueField = "SubjectID";
                getAllSubjects.DataTextField = "SubjectCode";
                getAllSubjects.DataBind();


            }
        }

        protected void getStudents_Click(object sender, EventArgs e)
        {
            getAllSubjects.Enabled = false;
            getSubjects.Enabled = false;
            UploadFile.Visible = true;
            Button1.Visible = true;
        }

        protected void insertScore_Click(object sender, EventArgs e)
        {
            AddScore();
            //foreach (GridViewRow row in allStudents.Rows)
            //{
            //    TextBox CA = (TextBox)row.FindControl("CA");
            //    TextBox EXAM = (TextBox)row.FindControl("EXAM");
            //    TextBox MatricNo = (TextBox)row.FindControl("MatricNo");

            //    var UpdateResult = (from d in db.Results where d.MatricNo == MatricNo.Text && d.SubjectID.ToString() == getAllSubjects.SelectedItem.Value select d).First();
            //    UpdateResult.CA = double.Parse(CA.Text);
            //    UpdateResult.EXAM = double.Parse(EXAM.Text);
            //    UpdateResult.LecturerInserted = "TRUE";
            //    db.SaveChanges();
            //}
            message.Visible = true;
            pdfExport.Visible = true;
            //Response.Write("Scores Added");
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            if (UploadFile.HasFile != false && UploadFile.PostedFile.ContentLength > 0)
            {
                string FileName = UploadFile.FileName;

                string ImgPath = " ../Upload/" + FileName;
                FileInfo file = new FileInfo(Server.MapPath(ImgPath));
                string FileExt = Path.GetExtension(ImgPath);
                if (FileExt == ".csv")
                {
                    
                    //AddScore();
                    if (file.Exists)
                    {
                        file.Delete();
                        UploadFile.PostedFile.SaveAs(Server.MapPath(ImgPath));
                    }
                    else
                    {
                        UploadFile.PostedFile.SaveAs(Server.MapPath(ImgPath));
                    }
                    GenerateCourse(ImgPath);
                    //string parameter = "ImgPath=" + ImgPath+"&SubjectID="+ getAllSubjects.SelectedItem.Value;
                    //Response.Redirect("ScorePreview.aspx?" + parameter);
                }
                Response.Write("Please check your file extension. make sure it is a csv file");
            }
        }

        public void AddScore()
        {
            int getCurrentSession = (from d in db.Sessions where d.CurrentSession == true select d.SessionID).FirstOrDefault();
            foreach (GridViewRow rows in allStudents.Rows)
            {
                string matricNo = rows.Cells[0].Text == "&nbsp;" ? "PDE/AB/2015/0000" : rows.Cells[0].Text;
                double ca = rows.Cells[1].Text == "&nbsp;" ? 0 : double.Parse(rows.Cells[1].Text);
                double exam = rows.Cells[2].Text == "&nbsp;" ? 0 : double.Parse(rows.Cells[2].Text);
                if (getAllSubjects.SelectedItem.Value != null)
                {
                    var InsertResult = (from d in db.Results where d.MatricNo == matricNo && d.SubjectID.ToString() == getAllSubjects.SelectedItem.Value && d.SessionID == getCurrentSession select d).FirstOrDefault();
                    if (InsertResult != null)
                    {
                        InsertResult.CA = ca;
                        InsertResult.EXAM = exam;
                        InsertResult.LecturerInserted = "TRUE";
                        db.SaveChanges();
                    }
                }
            }
        }
        public void GenerateCourse( string imgPath)
        {
            DataTable tblcsv = new DataTable();
            //creating columns  
            tblcsv.Columns.Add("MatricNo");
            tblcsv.Columns.Add("CA");
            tblcsv.Columns.Add("EXAM");
            tblcsv.Columns.Add("TOTAL");
            tblcsv.Columns.Add("REMARK");

            //Reading All text  
            string[] ReadCSV = System.IO.File.ReadAllLines(Server.MapPath(imgPath));
            List<StudentResult> results = new List<StudentResult>();

            for (int i = 1; i < ReadCSV.Length; i++)
            {
                double @default = 0;
                //int newPage = 0;
                //newPage = int.TryParse(pages.Text, out _pages) == true ? _pages : 10;

                StudentResult result = new StudentResult();
                string[] members = ReadCSV[i].ToString().Split(',');
                string matricno = Regex.Replace(members[0], @"[\""]", "", RegexOptions.None);
                string _ca = Regex.Replace(members[1], @"[\""]", "", RegexOptions.None);
                string _exam = Regex.Replace(members[2], @"[\""]", "", RegexOptions.None);
                double ca = double.TryParse(_ca,out @default)==true?@default:0;
                double exam = double.TryParse(_exam, out @default) == true ? @default : 0;
                double ta = ca + exam;
                result.Ca = ca;

                result.Exam = exam;
                result.Total = ta;
                result.MatricNo = matricno;
                results.Add(result);
                //Bindgrid(tblcsv);
            }
            allStudents.DataSource = results;
            allStudents.DataBind();
        }
        protected void pdfExport_Click(object sender, EventArgs e)
        {
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=Student.pdf");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            allStudents.AllowPaging = false;
            HtmlForm frm = new HtmlForm();
            allStudents.Parent.Controls.Add(frm);
            frm.Attributes["runat"] = "server";
            frm.Controls.Add(allStudents);
            frm.RenderControl(hw);
            allStudents.DataBind();
            StringReader sr = new StringReader(sw.ToString());
            iTextSharp.text.Document pdfDoc = new iTextSharp.text.Document(PageSize.A4, 10f, 10f, 10f, 0f);
            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            pdfDoc.Open();
            htmlparser.Parse(sr);
            pdfDoc.Close();
            Response.Write(pdfDoc);
            Response.End();
        }
    }
}