﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace School365.Lecturer
{
    public class StudentResult
    {
        public string MatricNo { get; set; }
        public double Ca { get; set; }
        public double Exam { get; set; }
        public double Total { get; set; }
        public StudentResult()
        {
            Total = Ca + Exam;
        }

    }
}