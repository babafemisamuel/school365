﻿//using DataStreams.Csv;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using School365.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace School365.Lecturer
{
    public partial class ScorePreview : System.Web.UI.Page
    {
        StudentModel db = new StudentModel();
        int getLecturerID = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            getLecturerID = int.Parse(User.Identity.Name);
            int getPasswordChanged = (from d in db.Lecturers where d.LecturerID == getLecturerID && d.PasswordChanged == true select d).Count();
            if (getPasswordChanged == 0)
            {
                Response.Redirect("PasswordChange.aspx");
                //CheckPassword.Text = "Please Change Your password";
            }

            string FilePath = Request.QueryString["ImgPath"].ToString();
            string SubjectID = Request.QueryString["SubjectID"].ToString();

            //Creating object of datatable  
            DataTable tblcsv = new DataTable();
            //creating columns  
            tblcsv.Columns.Add("MATRIC");
            tblcsv.Columns.Add("CA");
            tblcsv.Columns.Add("EXAM");
            tblcsv.Columns.Add("TOTAL");
            tblcsv.Columns.Add("REMARK");

            //Reading All text  
            string[] ReadCSV = System.IO.File.ReadAllLines(Server.MapPath(FilePath));
            List<StudentResult> results = new List<StudentResult>();

            for (int i = 1; i < ReadCSV.Length; i++)
            {
               StudentResult result = new StudentResult();
                string[] members = ReadCSV[i].ToString().Split(',');
                string matricno =Regex.Replace(members[0], @"[\""]", "", RegexOptions.None);
                string _ca = Regex.Replace(members[1], @"[\""]", "", RegexOptions.None);
                string _exam = Regex.Replace(members[2], @"[\""]", "", RegexOptions.None);
                double ca = double.Parse(_ca);
                double exam = double.Parse(_exam);
                double ta = ca + exam;
                result.Ca = ca;

                result.Exam = exam;
                result.Total = ta;
                result.MatricNo = matricno;
                results.Add(result);
                //Bindgrid(tblcsv);
            }
            allStudents.DataSource = results;
            allStudents.DataBind();
           // results;
            //spliting row after new line  
            //foreach (string csvRow in ReadCSV.Split('\n'))
            //{
            //    if (!string.IsNullOrEmpty(csvRow))
            //    {
            //        //Adding each row into datatable  
            //        tblcsv.Rows.Add();
            //        int count = 0;
            //        foreach (string FileRec in csvRow.Split(','))
            //        {
            //            tblcsv.Rows[tblcsv.Rows.Count - 1][count] = FileRec;
            //            count++;
            //        }
            //    }
            //    //Calling Bind Grid Functions  
            //    Bindgrid(tblcsv);
            //}
            if (!IsPostBack)
            {
                int getCurrentSessionID = 0;
                getCurrentSessionID = (from d in db.Sessions where d.CurrentSession == true select d.SessionID).FirstOrDefault();
                int subjectid = int.Parse(SubjectID);
                var getAllSubjectsDetail = db.Subjects.Where(a => a.SubjectID == subjectid).ToList();
               // var getAllSubjectsDetail = (from d in db.LecturerProfiles where d.LecturerID == getLecturerID join f in db.Subjects on d.SubjectID equals f.SubjectID select new { f.SubjectID, f.SubjectCode }).ToList();
                getAllSubjects.DataSource = getAllSubjectsDetail;
                getAllSubjects.DataValueField = "SubjectID";
                getAllSubjects.DataTextField = "SubjectCode";
                getAllSubjects.DataBind();
                getAllSubjects.Enabled = false;
            }
            

        }
        private void Bindgrid(DataTable csvdt)
        {
            allStudents.DataSource = csvdt;
            allStudents.DataBind();
        }

        protected void insertScore_Click(object sender, EventArgs e)
        {
            int getCurrentSession = (from d in db.Sessions where d.CurrentSession == true select d.SessionID).FirstOrDefault();
            foreach (GridViewRow rows in allStudents.Rows)
            {                
                string matricNo = rows.Cells[0].Text == "&nbsp;" ? "PDE/AB/2015/0000" : rows.Cells[0].Text;
               double ca = rows.Cells[1].Text == "&nbsp;" ? 0 : double.Parse(rows.Cells[1].Text);
                double exam = rows.Cells[2].Text == "&nbsp;" ? 0 : double.Parse(rows.Cells[2].Text);               
                if (getAllSubjects.SelectedItem.Value != null)
                {
                    var InsertResult = (from d in db.Results where d.MatricNo == matricNo && d.SubjectID.ToString() == getAllSubjects.SelectedItem.Value && d.SessionID == getCurrentSession select d).FirstOrDefault();
                    if (InsertResult !=null)
                    {
                        InsertResult.CA = ca;
                        InsertResult.EXAM = exam;
                        InsertResult.LecturerInserted = "TRUE";
                        db.SaveChanges();   
                    }                  
                }
            }
            Response.Write("Data Inserted");
        }

        protected void pdfExport_Click(object sender, EventArgs e)
        {
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=Student.pdf");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            allStudents.AllowPaging = false;
            HtmlForm frm = new HtmlForm();
            allStudents.Parent.Controls.Add(frm);
            frm.Attributes["runat"] = "server";
            frm.Controls.Add(allStudents);
            frm.RenderControl(hw);
            allStudents.DataBind();
            StringReader sr = new StringReader(sw.ToString());
            iTextSharp.text.Document pdfDoc = new iTextSharp.text.Document(PageSize.A4, 10f, 10f, 10f, 0f);
            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
            pdfDoc.Open();
            htmlparser.Parse(sr);
            pdfDoc.Close();
            Response.Write(pdfDoc);
            Response.End();
        }

        protected void excelExport_Click(object sender, EventArgs e)
        {
            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("content-disposition",
             "attachment;filename=Student.csv");
            Response.Charset = "";
            Response.ContentType = "application/text";

            allStudents.AllowPaging = false;
            allStudents.DataBind();

            StringBuilder sb = new StringBuilder();
            for (int k = 0; k < allStudents.Columns.Count; k++)
            {
                //add separator
                sb.Append(allStudents.Columns[k].HeaderText + ',');
            }
            //append new line
            sb.Append("\r\n");
            for (int i = 0; i < allStudents.Rows.Count; i++)
            {
                for (int k = 0; k < allStudents.Columns.Count; k++)
                {
                    //add separator
                    sb.Append(allStudents.Rows[i].Cells[k].Text + ',');
                }
                //append new line
                sb.Append("\r\n");
            }
            Response.Output.Write(sb.ToString());
            Response.Flush();
            Response.End();
        }

        protected void wordExport_Click(object sender, EventArgs e)
        {

        }

        protected void allStudents_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }

        protected void allStudents_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        //protected void allStudents_RowDataBound1(object sender, GridViewRowEventArgs e)
        //{

        //}

        //protected void allStudents_RowDataBound2(object sender, GridViewRowEventArgs e)
        //{
        //    if (e.Row.RowType == DataControlRowType.DataRow)
        //    {
        //        //string _ca=e.Row.Cells[1].Text
        //        int ca = e.Row.Cells[1].Text == "&nbsp;" ? 0 : int.Parse(e.Row.Cells[1].Text);
        //        int exam = e.Row.Cells[2].Text == "&nbsp;" ? 0 : int.Parse(e.Row.Cells[2].Text);
        //        e.Row.Cells[3].Text = (ca + exam).ToString();
        //    }
        //}

    }
}