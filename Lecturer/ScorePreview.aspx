﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Lecturer/Lecturers.Master" AutoEventWireup="true" CodeBehind="ScorePreview.aspx.cs" Inherits="School365.Lecturer.ScorePreview" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br />
    <div class="row">
         <div class="col-lg-2">
        </div>
        <div class="col-lg-4">
            <asp:Button ID="insertScore" Text="Submit Score" CssClass="btn btn-outline btn-success" runat="server" OnClick="insertScore_Click" />
              <asp:Button ID="pdfExport" Text="Print Pdf" CssClass="btn btn-outline btn-danger" runat="server" OnClick="pdfExport_Click" />
        </div>

        <div class="col-lg-4">
            <asp:DropDownList ID="getAllSubjects" CssClass="form-control" runat="server">
            </asp:DropDownList>
        </div>
        <div class="col-lg-2">
        </div>
    </div>
    <div class="row">
        <div class="col-lg-2"></div>
        <div class="col-lg-8">
            <asp:GridView ID="allStudents" runat="server" CssClass="table table-bordered table-hover table-striped">
            </asp:GridView>
        </div>
        <div class="col-lg-2">

        </div>
    </div>

    <div class="row">
       
        <div class="row">
            <div class="col-lg-4">
              
            </div>
            <div class="col-lg-4">
                <asp:Button ID="excelExport" Text="Print Excel" CssClass="button" runat="server" OnClick="excelExport_Click" Visible="false" />
            </div>
            <div class="col-lg-4">
                <asp:Button ID="wordExport" Text="Print Word" CssClass="button" runat="server" OnClick="wordExport_Click" Visible="false" />
            </div>
        </div>
    </div>

</asp:Content>
