﻿using School365.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace School365.Lecturer
{
    public partial class DownloadEMS : System.Web.UI.Page
    {
        public StudentModel db = new StudentModel();
        public List<int> getStudentsID = new List<int>();
        public List<string> AllMatric = new List<string>();
        public string subjectCode = "";

        int getCurrentSessionID = 0;
        int getLecturerID = 0;
        int getSession = 0;
        //public string SubjectName = "";
        public string subCode = "";
        public string newSubName = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            getLecturerID = int.Parse(User.Identity.Name);
            getSession = (from d in db.Sessions where d.CurrentSession == true select d.SessionID).FirstOrDefault();
            if (!IsPostBack)
            {
                int getPasswordChanged = (from d in db.Lecturers where d.LecturerID == getLecturerID && d.PasswordChanged == true select d).Count();
                if (getPasswordChanged == 0)
                {
                    Response.Redirect("PasswordChange.aspx");
                    //CheckPassword.Text = "Please Change Your password";
                }                
            getCurrentSessionID = (from d in db.Sessions where d.CurrentSession == true select d.SessionID).FirstOrDefault();
            var getAllSubjectsDetail = (from d in db.LecturerProfiles where d.LecturerID == getLecturerID join f in db.Subjects on d.SubjectID equals f.SubjectID select new { f.SubjectID, f.SubjectCode }).ToList();
            getAllSubjects.DataSource = getAllSubjectsDetail;
            getAllSubjects.DataValueField = "SubjectID";
            getAllSubjects.DataTextField = "SubjectCode";
            getAllSubjects.DataBind();            
            }
            

        }

        protected void getStudents_Click(object sender, EventArgs e)
        {

            subjectCode = getAllSubjects.SelectedItem.Value;
            //  var getStudents = (from d in db.Results
            //                     where d.SubjectID.ToString() == getAllSubjects.SelectedItem.Value
            //                     && d.EXAM==0 && d.CA==0
            //                     join f in db.Subjects on d.SubjectID equals f.SubjectID
            //                     select new { d.MatricNo,d.CA,d.EXAM,d.CTCP}).Distinct().OrderBy(a=>a.MatricNo).ToList();
            //  //kept ctcp bcos i needed a 0 value to compliment my uploading of total calculation
            //  //on the long run it is not neccessary
            ////  allResult.AddRange(getStudents);
            //  allStudents.DataSource = getStudents;
            //  allStudents.DataBind();

            int getSessionID = db.Sessions.Where(a => a.CurrentSession == true).FirstOrDefault().SessionID;
            AllMatric = (from d in db.Results
                         where d.SubjectID.ToString() == getAllSubjects.SelectedItem.Value && d.SessionID == getSessionID
                               && d.EXAM == 0 && d.CA == 0
                         join f in db.Subjects on d.SubjectID equals f.SubjectID
                         select d.MatricNo).Distinct().OrderBy(a => a).ToList();

        }

        protected void emsDownload_Click(object sender, EventArgs e)
        {
            //Response.Clear();
            //Response.Buffer = true;
            //string filedata = "attachment;filename="+getAllSubjects.SelectedItem.Text.Replace(" ",string.Empty)+".csv";
            //Response.AddHeader("content-disposition", filedata);
            //Response.Charset = "";
            //Response.ContentType = "application/text";
            //StringBuilder sBuilder = new System.Text.StringBuilder();
            //for (int index = 0; index < allStudents.Columns.Count; index++)
            //{
            //    sBuilder.Append(allStudents.Columns[index].HeaderText + ',');
            //}
            //sBuilder.Append("\r\n");
            //for (int i = 0; i < allStudents.Rows.Count; i++)
            //{
            //    for (int k = 0; k < allStudents.HeaderRow.Cells.Count; k++)
            //    {
            //        sBuilder.Append(allStudents.Rows[i].Cells[k].Text.Replace(",", "") + ",");
            //    }
            //    sBuilder.Append("\r\n");
            //}
            //Response.Output.Write(sBuilder.ToString());
            //Response.Flush();
            //Response.End();
        }
    }
}