﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace School365.Model
{
    public class MinMax
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int MinMaxID { get; set; }
        public int Minimum { get; set; }
        public int Maximum { get; set; }
        public int SubjectCombinID { get; set; }
        public int Semester { get; set; }
        public int Level { get; set; }
        public SubjectCombination SubjectCombinations { get; set; }
    }
}