﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace School365.Model
{
    public class Pin
    {
        public int PinID { get; set; }
        public string PinKey { get; set; }

        public int? StudentID { get; set; }
        public int Counter { get; set; }
        public int AmountOfLogin { get; set; }
       

    }
}