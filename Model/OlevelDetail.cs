﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace School365.Model
{
    public class OlevelDetail
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int OlevelDetailsID { get; set; }
        public string ExamYear { get; set; }
        public string ExamType { get; set; }
        public string ExamCenter { get; set; }
        public string ExamDate { get; set; }
        public string CenterNumber { get; set; }
        public string RegNo { get; set; }
        public string SubjectName { get; set; }
        public string Grade { get; set; }
        public int StudentID { get; set; }
        public virtual Student Students { get; set; } 
    }
}