﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace School365.Model
{
    public class OutstandingModel
    {
        public int SubjectCombinID { get; set; }
        public string SubjectCombinName { get; set; }
        public int DepartmantID { get; set; }
        public int SubjectID { get; set; }
        public string SubjectName { get; set; }
        public string SubjectCode { get; set; }
       
        public double SubjectValue { get; set; }
        public string SubjectUnit { get; set; }
        public string SubjectLevel { get; set; }
        public string Semester { get; set; }
        public int SessionID { get; set; }
        public bool Active { get; set; }
        
    }
}