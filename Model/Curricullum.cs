﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace School365.Model
{
    public class Curricullum
    {
        public int CurricullumID { get; set; }
        public string CurricullumName { get; set; }
        public bool IsCurrent { get; set; }
        public ICollection<AllCombined> AllCombineds { get; set; }
    }
}