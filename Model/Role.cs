﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace School365.Model
{
    public class Role
    {
        public int RoleID { get; set; }
        public string RoleName { get; set; }
        public virtual ICollection<RoleAdmin> RoleAdmins { get; set; }

    }
}