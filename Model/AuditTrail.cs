﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace School365.Model
{
    public class AuditTrail
    {
        [Key]
        public int AuditTrailID { get; set; }
        public Guid AuditTrailKey { get; set; }
        public string UserName { get; set; }
        public string Activity { get; set; }
        public string MethodName { get; set; }
        public string ClassName { get; set; }
        public string Info { get; set; }
        public DateTime DateTime { get; set; }
        public AuditTrail()
        {
            AuditTrailKey = Guid.NewGuid();
        }
    }
}