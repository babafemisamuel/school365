﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace School365.Model
{
    public class RoleAdmin
    {
        public int RoleAdminID { get; set; }
        public int RoleID { get; set; }
        public int AdminID { get; set; }

        public virtual Role Roles { get; set; }
        public virtual Admin Admin { get; set; }
    }
}