﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace School365.Model
{
    public class Outstanding
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int OutstandingID { get; set; }
        public int AllCombinedID { get; set; }
        public int StudentID { get; set; }
        public bool Registered { get; set; }
        public int SessionID { get; set; }
        public virtual Model.Session Sessions { get; set; }
        public virtual Student Students { get; set; }
        public virtual AllCombined AllCombineds { get; set; }

    }
}