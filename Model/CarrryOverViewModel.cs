﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace School365.Model
{
    public class CarrryOverViewModel
    {
        public double Ca { get; set; }
        public double Exam { get; set; }
        public int ResultID { get; set; }
        public string SubjectCode { get; set; }
        public double SubjectValue { get; set; }
        public string SubjectUnit { get; set; }
        public string SubjectName { get; set; }
        public int SubjectID { get; set; }
        public string SubjectLevel { get; set; }
        public string Semester { get; set; }
        public bool Active { get; set; }
        public int SubjectCombinID { get; set; }
        public string SubjectCombinName { get; set; }
        public int DepartmantID { get; set; }

        //  select new { j.SubjectCombinID, j.SubjectCombinName, d.DepartmantID, h.SubjectCode, h.SubjectID, h.SubjectName, h.SubjectValue, h.SubjectUnit, h.SubjectLevel, h.Semester, h.Active };
    }
}