﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace School365.Model
{
    public class JambDetail
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int JambDetailID { get; set; }
        public string RegNo { get; set; }
        public string Center { get; set; }
        public string Year { get; set; }
        public string Subject { get; set; }
        public string Score { get; set; }
        public int StudentID { get; set; }
        public virtual Student Students { get; set; } 
    }
}