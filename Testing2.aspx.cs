﻿using School365.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EntityFramework.Extensions;
//using DataStreams.Csv;


namespace School365
{
    public partial class Testing2 : System.Web.UI.Page
    {
        StudentModel db = new StudentModel();
        public List<Model.Result> StdColl = new List<Model.Result>();
        public List<Model.Department> DptColl = new List<Model.Department>();
        public List<int> StudentID = new List<int>();
        public List<int> DeptID = new List<int>();
        int getValue = 0;

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void getB_Click(object sender, EventArgs e)
        {
            var getSubject = (from d in db.Results where d.StudentID == 5580 join f in db.Subjects on d.SubjectID equals f.SubjectID select new { d.SubjectID, f.SubjectValue }).Distinct().ToList();
            foreach (var item in getSubject)
            {
                db.Results.Where(x => x.SubjectID == item.SubjectID && x.StudentID == 5580).Update(x => new Result() { TNU = item.SubjectValue });
            }
        }
    }
}